<?php 

/* test */

/* set the page vars */
$body_class = 'about';

/* include the header + nav partials */
include_once( 'partials/head.php' );
include_once( 'partials/header.php' ); ?>
	
<div id="content">

	<div id="main" class="fixed_wrap white_box clearfix">
		<h1 class="headline">
			Company Profile
		</h1>
		
		<div class="blue_bar"></div>
	
		<div class="marquee_image text-center">
			<img src="images/photo_building.png" alt="Shurtape office building" />
		</div>

		<div class="padded_body">
			<p>
				At Shurtape, we design, develop and craft our tape to one simple standard.  Yours.  
				We know you demand attention to every detail.  We know you strive to do the next job better than 
				the last.  We know you won't settle for anything less than exact.  It's what has guided and inspired us every day since 1955.
			</p>
			<p>
				Created in North Carolina as Shurtape Technologies LLC, we're a privately owned company proudly serving a variety of markets, 
				from painting and packaging to HVAC and transportation.   And we continually remain engaged with our markets and the customers 
				and craftsmen who use our tape so we can deliver what they need.  To the absolute highest standards.
			</p>
			<p>
				As we continue to grow, the basis of our success remains the same: our relationships with our customers and craftsmen.  
				We're grateful that so many of you have been intensely loyal.  It's why we demand consistent product performance, on-time 
				delivery and relentless customer service.  It's also why we never stop investing in improving our manufacturing, our products 
				and our distribution. 
			</p>
			<p>
				In other words, we work to do our job the way you do yours.
			</p>
		</div>
		<div class="clearfix">
			<div class="pull-left">
				<div class="arrow_box_wrap clearfix">
					<div class="blue_arrow_box">
						Watch our video to learn more about the essence of the Shurtape brand:
						<span class="pointer"></span>
					</div>
				</div>
			</div>
			<div class="video pull-right">
				<!--<img src="images/fpo_video_craftsmanship.jpg" alt="Video FPO" />-->
				<object width="473" height="266">
				<param name="movie" value="http://www.youtube.com/v/kk9A_o3O718"></param>
				<param name="allowFullScreen" value="true"></param>
				<param name="allowscriptaccess" value="always"></param>
				<embed src="http://www.youtube.com/v/kk9A_o3O718?rel=0"
				type="application/x-shockwave-flash"
				allowscriptaccess="always"
				allowfullscreen="true"
				width="473" height="266">
				</embed>
				</object>
			</div>
		</div>
	
	</div>

</div>
	
<?php 

/* include the footer partials */
include_once( 'partials/footer.php' );
include_once( 'partials/foot.php' ); ?>
