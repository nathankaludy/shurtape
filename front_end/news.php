<?php 

/* set the page vars */
$body_class = 'news';

/* include the header + nav partials */
include_once( 'partials/head.php' );
include_once( 'partials/header.php' ); ?>
	
<div id="content">

	<div id="main" class="fixed_wrap white_box clearfix">
		<h1 class="headline">
			News &amp; Events
		</h1>
		
		<div class="blue_bar">NEWS</div>
		

		<ul class="list-unstyled link_list">
		
			<li><a href="#">Make Every Job Code-Compliant with DC 181 from Shurtape&reg;</a>  11/21/2013</li>

			<li><a href="#">Shurtape&reg; Honors Commitment to Product Quality and Manufacturing Responsibility as PSTC Responsible Tape Manufacturer</a> 11/19/2013</li>

			<li><a href="#">HP 235 Packaging Tape from Shurtape&reg; Delivers Improved Bond for Recycled Corrugate</a> 11/18/2013</li>

			<li><a href="#">Shurtape's Wayne Helton Named President of Pressure Sensitive Tape Council</a> 11/18/2013</li>

			<li><a href="#">High-Performance Masking Tape: An Essential Tool - and "Unsung Hero" - in Pipe Coating</a> 11/08/2013</li>

			<li><a href="#">Shurtape&reg; AF Series Cold Temperature Foil Tapes Designed for Diverse Weather Conditions</a> 10/28/2013</li>

			<li><a href="#">New FloorMate&reg; Temporary Flooring Tape from Shurtape&reg; Engineered for Water-Based Finishes</a> 10/22/2013</li>

			<li><a href="#">Shurtape&reg; Becomes Institute of Packaging Professionals Benefactor</a> 10/18/2013</li>

			<li><a href="#">Shurtape&reg; Gaffer's Tape Hits the Stage with Frank Hannon</a> 10/12/2013</li>

			<li><a href="#">Shurtape Introduces HW 300 Housewrap Tape, Featuring First-To-Market Water-Based Acrylic Adhesive</a> 10/08/2013</li>

			<li>
				<a href="#">New ShurSEAL&trade; Tape Heads from Shurtape Engineered for Better Performance, Efficiency</a>
				<span class="link_desc">Retrofit tape heads offer soft touch, high speed &8212; all in one tape head 10/02/2013</span>
			</li>
			<li>
				<a href="#">Shurtape Takes on the Cold with New HP 132 Packaging Tape</a>
				<span class="link_desc">New cold temperature tape offers instant, permanent bond for lightweight cartons 10/02/2013</span>
			</li>
		</ul>

		
		<div class="blue_bar">EVENTS</div>

		<ul class="list-unstyled event_list">
		
			<li>
				<a href="#" class="event_thumb"><img src="images/photo_national_hardware.jpg" alt="" /></a>
				<a href="#" class="event_content">
					<h3 class="event_title">National Hardware Show 2013</h3>
					<span class="event_details">
						<b>Booth 5615</b><br />
						May 7 - 9, 2013<br />
						Las Vegas Convention Center<br />
						Las Vegas, NV
					</span>
				</a>
			</li>
			<li>
				<a href="#" class="event_thumb"><img src="images/photo_national_hardware.jpg" alt="" /></a>
				<a href="#" class="event_content">
					<h3 class="event_title">National Hardware Show 2013</h3>
					<span class="event_details">
						<b>Booth 5615</b><br />
						May 7 - 9, 2013<br />
						Las Vegas Convention Center<br />
						Las Vegas, NV
					</span>
				</a>
			</li>

		</ul>

	
	</div>

</div>
	
<?php 

/* include the footer partials */
include_once( 'partials/footer.php' );
include_once( 'partials/foot.php' ); ?>
