<body <?php echo isset( $body_class ) ? 'class="' . $body_class . '"': '';?>>

<div id="header">
	<div class="fixed_wrap clearfix">
		<a href="index" id="logo" class="col-md-6" title="Shurtape - Home">
			<img src="images/logo_header.png" alt="Shurtape - True to Your Work" />
		</a>
		<div id="top_cta">
			Questions/Orders? Call Us: <b class="blue">888-442-8273</b>
		</div>
		<div id="search_box">
			<form id="search_form" method="post" action="search">
				<div class="input-group">
					<input type="text" name="search_term" class="form-control" placeholder="Search">
					<span class="input-group-btn">
						<button class="btn btn-primary" type="button"><span class="icon_search"></span></button>
					</span>
				</div>
			</form>
		</div>
		<div id="nav">
			<ul class="primary_menu list-inline clearfix">
				<li class="first">
					<a id="link_products" href="#" class="open_sub">Products</a>
					<ul class="submenu">
						<li class="first"><a href="#">Index</a></li>
						<li><a href="#">Type</a></li>
						<li><a href="#">Market</a></li>
						<li class="last"><a href="#">Green Point Products</a></li>
					</ul>
				</li>
				<li>
					<a id="link_resources" href="#" class="open_sub">Resources</a>
					<ul class="submenu">
						<li class="first"><a href="#">Media Library</a></li>
						<li><a href="#">Industry Links</a></li>
						<li><a href="#">News &amp; Events</a></li>
						<li class="last"><a href="#">Product Stewardship</a></li>
					</ul>
				</li>
				<li>
					<a id="link_about" href="#" class="open_sub">About</a>
					<ul class="submenu">
						<li class="first"><a href="#">Where to Buy</a></li>
						<li><a href="#">Locations</a></li>
						<li><a href="#">Human Resources</a></li>
						<li><a href="#">Careers</a></li>
						<li><a href="#">Privacy Policy</a></li>
						<li class="last"><a href="#">Return Policy</a></li>

					</ul>
				</li>
				<li><a id="link_contact" href="#">Contact Us</a></li>
				<li class="last"><a id="link_where" href="#">Where to Buy</a></li>
			</ul>
		</div>
	</div>
</div>