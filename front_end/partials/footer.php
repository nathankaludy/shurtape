<div id="footer">
	<div class="fixed_wrap clearfix">
		<div id="footer_home" class="col pull-left">
			<h4>HOME</h4>
		</div>
		<div id="footer_products" class="col pull-left">
			<h4>PRODUCTS</h4>
			<ul class="list-unstyled">
				<li><a href="#">Index</a></li>
				<li><a href="#">Type</a></li>
				<li><a href="#">Market</a></li>
				<li><a href="#">Green Point Products</a></li>
			</ul>
		</div>
		<div id="footer_resources" class="col pull-left">
			<h4>RESOURCES</h4>
			<ul class="list-unstyled">
				<li><a href="#">Media Library</a></li>
				<li><a href="#">Industry Links</a></li>
				<li><a href="#">News &amp; Events</a></li>
				<li><a href="#">Product Stewardship</a></li>
			</ul>
		</div>
		<div id="footer_about" class="col pull-left">
			<h4>ABOUT</h4>
			<ul class="list-unstyled">
				<li><a href="#">Where to Buy</a></li>
				<li><a href="#">Locations</a></li>
				<li><a href="#">Human Resources</a></li>
				<li><a href="#">Careers</a></li>
				<li><a href="#">Contact</a></li>
				<li><a href="#">Privacy Policy</a></li>
				<li><a href="#">Terms &amp; Conditions</a></li>
				<li><a href="#">Merchandise Return Policy</a></li>
			</ul>
		</div>
		<div id="footer_share" class="col pull-left">
			<a href="index" class="pull-right footer_logo" title="ShurTape - True to Your Work">
				<img src="images/logo_footer.png" class="pull-right" alt="ShurTape - True to Your Work" />
			</a>
			
			<div class="share_links pull-right">
				<h4>FOLLOW US</h4>
				<ul class="list-inline">
					<li><a href="#" id="facebook_link"><img src="images/icon_facebook.png" alt="Facebook" /></a></li>
					<li><a href="#" id="twitter_link"><img src="images/icon_twitter.png" alt="Twitter" /></a></li>
					<li><a href="#" id="youtube_link"><img src="images/icon_youtube.png" alt="YouTube" /></a></li>
					<li><a href="#" id="linkedin_link"><img src="images/icon_linkedin.png" alt="LinkedIn" /></a></li>
				</ul>
			</div>
			
		</div>
	</div>
	
</div>

<!-- JS should be called in the bottom for performance reasons (except jQuery) -->
<script type="text/javascript" src="js/respond.min.js"></script>
<script type="text/javascript" src="js/jquery.placeholder.min.js"></script>
<script type="text/javascript" src="js/main.js"></script>
<script type="text/javascript" src="js/home.js"></script><!-- TODO - call this only on home page -->
<script type="text/javascript" src="js/products.js"></script><!-- TODO - call this only on products pages - category / browse, detail, etc. -->
<script type="text/javascript" src="js/search.js"></script><!-- TODO - call this only on search / filter page -->
<script type="text/javascript" src="js/filter_bar.js"></script><!-- TODO - call this only on products pages - category / browse, detail, etc. -->