<?php 

/* set the page vars */
$body_class = 'media_page'; /* .media is a reserved class for bootstrap */

/* include the header + nav partials */
include_once( 'partials/head.php' );
include_once( 'partials/header.php' ); ?>
	
<div id="content">

	<div id="main" class="fixed_wrap white_box clearfix">
		<h1 class="headline">
			Media Library
		</h1>
		
		<div class="blue_bar">WELCOME</div>
		
		<div id="marquee" class="clearfix">
			<div class="marquee_content pull-left">
				<span class="highlight">Welcome to the Shurtape Media Library</span>, where you can view and download product 
				literature, logos, product catalog, and more.
			
				<ul class="link_list">
					<li class="additional">Additional Support</li>
					<li><a href="#">View Articles and Press Releases</a></li>
					<li><a href="#">View Technical Data Sheets</a></li>
					<li><a href="#">View Events</a></li>
				</ul>
			</div>
			<div class="marquee_image pull-right">
				<img src="images/fpo_media_marquee.png" alt="" />
			</div>
		</div>
		
		<div class="blue_bar for_access">
			For access to artwork, photography and media, email us at mediasupport@shurtape.com.
		</div>
		
		<div class="gray_bar clearfix">
			<div class="indicates pull-left">
				(*) Indicates printed version also available
			</div>
			<!--
			<div class="adobe pull-right text-right">
				<a href="#"><img src="images/icon_get_adobe.jpg" class="pull-right" alt="" /></a>
				You may need to download and install the Adobe Acrobat Reader to view all downloaded electronic documents.
			</div>
			-->
		</div>
		<div id="brand_assets">
			<h3>Brand Assets</h3>
			<ul>
				<li>
					<span class="thumb">
						<img src="images/fpo_catalog.jpg" alt="" />
					</span>
					<span class="content">
						<span class="title">Product Catalog</span>
						<span class="link"><a href="#">Download PDF</a></span>
					</span>
				</li>
				<li>
					<span class="thumb">
						<img src="images/fpo_catalog.jpg" alt="" />
					</span>
					<span class="content">
						<span class="title">Brand Standards Guide</span>
						<span class="link"><a href="#">Download PDF</a></span>
					</span>
				</li>
				<li>
					<span class="thumb">
						<img src="images/brand_assets_logo1.png" alt="" />
					</span>
					<span class="content">
						<span class="title">Shurtape Logo with Tag</span>
						<span class="link">
							<span class="sub_left">Download:</span><a href="#" class="sub_right">EPS file</a>
							<span class="sub_left">&nbsp;</span><a href="#" class="sub_right">JPG file</a>
							<span class="sub_left">&nbsp;</span><a href="#" class="sub_right">PNG file</a>
						</span>
					</span>
				</li>
				<li>
					<span class="thumb">
						<img src="images/brand_assets_logo2.png" alt="" />
					</span>
					<span class="content">
						<span class="title">Shurtape Logo</span>
						<span class="link">
							<span class="sub_left">Download:</span><a href="#" class="sub_right">EPS file</a>
							<span class="sub_left">&nbsp;</span><a href="#" class="sub_right">JPG file</a>
							<span class="sub_left">&nbsp;</span><a href="#" class="sub_right">PNG file</a>
						</span>
					</span>
				</li>
			</ul>
		</div>
		
		<div id="accordian">
			<div class="group">
				<h3>
					<a href="#" class="control">Masking Tape Products</a>
				</h3>
				<div class="box">
					<ul>
						<li>
							<a href="#">
								<span class="thumb">
									<img src="images/fpo_media_item.jpg" alt="" />
								</span>
								<span class="title">CP 500 - Industrial Masking Tape</span>
							</a>
						</li>
						<li>
							<a href="#">
								<span class="thumb">
									<img src="images/fpo_media_item.jpg" alt="" />
								</span>
								<span class="title">CP 500 - Industrial Masking Tape</span>
							</a>
						</li>
						<li>
							<a href="#">
								<span class="thumb">
									<img src="images/fpo_media_item.jpg" alt="" />
								</span>
								<span class="title">CP 500 - Industrial Masking Tape</span>
							</a>
						</li>
						<li>
							<a href="#">
								<span class="thumb">
									<img src="images/fpo_media_item.jpg" alt="" />
								</span>
								<span class="title">CP 500 - Industrial Masking Tape</span>
							</a>
						</li>
					</ul>
				</div>
			</div>
			<div class="group">
				<h3>
					<a href="#" class="control">Masking Tape Products</a>
				</h3>
				<div class="box">
					<ul>
						<li>
							<a href="#">
								<span class="thumb">
									<img src="images/fpo_media_item.jpg" alt="" />
								</span>
								<span class="title">CP 500 - Industrial Masking Tape</span>
							</a>
						</li>
						<li>
							<a href="#">
								<span class="thumb">
									<img src="images/fpo_media_item.jpg" alt="" />
								</span>
								<span class="title">CP 500 - Industrial Masking Tape</span>
							</a>
						</li>
						<li>
							<a href="#">
								<span class="thumb">
									<img src="images/fpo_media_item.jpg" alt="" />
								</span>
								<span class="title">CP 500 - Industrial Masking Tape</span>
							</a>
						</li>
						<li>
							<a href="#">
								<span class="thumb">
									<img src="images/fpo_media_item.jpg" alt="" />
								</span>
								<span class="title">CP 500 - Industrial Masking Tape</span>
							</a>
						</li>
					</ul>
				</div>
			</div>
			<div class="group">
				<h3>
					<a href="#" class="control">Masking Tape Products</a>
				</h3>
				<div class="box">
					<ul>
						<li>
							<a href="#">
								<span class="thumb">
									<img src="images/fpo_media_item.jpg" alt="" />
								</span>
								<span class="title">CP 500 - Industrial Masking Tape</span>
							</a>
						</li>
						<li>
							<a href="#">
								<span class="thumb">
									<img src="images/fpo_media_item.jpg" alt="" />
								</span>
								<span class="title">CP 500 - Industrial Masking Tape</span>
							</a>
						</li>
						<li>
							<a href="#">
								<span class="thumb">
									<img src="images/fpo_media_item.jpg" alt="" />
								</span>
								<span class="title">CP 500 - Industrial Masking Tape</span>
							</a>
						</li>
						<li>
							<a href="#">
								<span class="thumb">
									<img src="images/fpo_media_item.jpg" alt="" />
								</span>
								<span class="title">CP 500 - Industrial Masking Tape</span>
							</a>
						</li>
					</ul>
				</div>
			</div>
			<div class="group">
				<h3>
					<a href="#" class="control">Masking Tape Products</a>
				</h3>
				<div class="box">
					<ul>
						<li>
							<a href="#">
								<span class="thumb">
									<img src="images/fpo_media_item.jpg" alt="" />
								</span>
								<span class="title">CP 500 - Industrial Masking Tape</span>
							</a>
						</li>
						<li>
							<a href="#">
								<span class="thumb">
									<img src="images/fpo_media_item.jpg" alt="" />
								</span>
								<span class="title">CP 500 - Industrial Masking Tape</span>
							</a>
						</li>
						<li>
							<a href="#">
								<span class="thumb">
									<img src="images/fpo_media_item.jpg" alt="" />
								</span>
								<span class="title">CP 500 - Industrial Masking Tape</span>
							</a>
						</li>
						<li>
							<a href="#">
								<span class="thumb">
									<img src="images/fpo_media_item.jpg" alt="" />
								</span>
								<span class="title">CP 500 - Industrial Masking Tape</span>
							</a>
						</li>
					</ul>
				</div>
			</div>
		</div>

	</div>

</div>
	
<?php 

/* include the footer partials */
include_once( 'partials/footer.php' ); ?>
<!-- 
init the plugins here 
- between the footer (containing external JS refs, plugins, and site global footer) and the
- foot (containing body and html closing tags, global site JS like tracking scripts, etc.) 
-->
<script type="text/javascript">
	/* define $ as jQuery just in case */
	( function( $ ){

		/* doc ready */
		$( function( ){
		
			/* init the accordian */
			$( '#accordian' ).accordian( );
			
		});
	})( jQuery );
</script>
<?php include_once( 'partials/foot.php' ); ?>
