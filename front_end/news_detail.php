<?php 

/* set the page vars */
$body_class = 'news_detail';

/* include the header + nav partials */
include_once( 'partials/head.php' );
include_once( 'partials/header.php' ); ?>
	
<div id="content">

	<div id="main" class="fixed_wrap white_box clearfix">
		<h1 class="headline">
			News
		</h1>
		
		<div class="blue_bar">&nbsp;</div>
		
		<div class="news_detail_wrap">
		
			<h2 class="news_detail_title">HP 235 Packaging Tape from Shurtape<sup>&reg;</sup> Delivers Improved Bond for Recycled Corrugate</h2>
			
			<h3 class="news_detail_subtitle">New packaging tape features higher adhesion on highly recycled boxes</h3>
			
			<div class="news_detail_content clearfix">
				
				<div class="news_detail_photo pull-left">
					<img src="images/news_foil_tape.jpg" alt="Foil Tape" />
				</div>
				<div class="news_detail_body pull-right">
					<p>
						HICKORY, N.C. (May 2013) – According to the Corrugated Packaging Alliance, "corrugated has one of the best recovery and 
						recycling records of any packaging material on earth. More than 74 percent of the corrugated manufactured today is recovered 
						for reuse."* While finding fewer boxes in landfills is great for the environment, the higher recycled content of corrugated 
						containers can make it difficult for packaging tape to stick – until now. Shurtape<sup>&reg;</sup>, with its commitment to exacting standards, 
						introduces new HP 235 Packaging Tape, a standard-gauge tape solution that delivers better performance and adhesion on highly 
						recycled corrugate – including 100-percent recycled.
					</p>
					<p>
						HP 235 packaging tape<br />
						As the recycled content in corrugated containers increases, so does the volume of fiber and dust, which affects 
						packaging tape's ability to create a secure seal. HP 235 is a 2.0-mil, pressure-sensitive packaging tape with an 
						enhanced adhesive engineered specifically for sealing highly recycled corrugated containers, particularly 100-percent 
						recycled. The optimized adhesive formula results in higher adhesion and higher shear, and also provides for increased 
						fiber tear when the tape is removed, offering tamper-evident security.
					</p>
					<p>
						"Shurtape is committed to continuously improving our products to provide relentlessly reliable tapes that meet the highest of standards," 
						said Bill DeWitt, market manager of packaging tapes, Shurtape. "Recycled corrugate is not an exception to the rule of product performance, 
						so we've developed HP 235 to provide the secure seal and consistent performance that professionals demand – no matter the recycled content 
						of the container to which it's being applied."
					</p>
					<p>
						For more information about Shurtape HP 235 or other packaging tape products, please visit www.shurtape.com or call 888.442.8273.
					</p>
				</div>
			</div>
		
		</div>
	
	</div>

</div>
	
<?php 

/* include the footer partials */
include_once( 'partials/footer.php' );
include_once( 'partials/foot.php' ); ?>