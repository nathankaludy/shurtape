/**
 * main JS (use this for scripts common to all - or most - pages)
 */
/* define $ as jQuery just in case */
( function( $ ){

	/* doc ready */
	$( function( ){
	
		/* main menu dropdown / flyout */
		$( 'ul.primary_menu' ).on( 'mouseover', 'a.open_sub', function( e ) {
			
			/* set the vars */
			var parent_menu	= $( this ).parents( 'ul.primary_menu' );
			var submenus	= parent_menu.find( 'ul.submenu' );
			var parent_item = $( this ).parents( 'li' );
			var target_el	= parent_item.find( 'ul.submenu' );
			
			/* if not already open, animate the submenu open */
			if ( ! target_el.hasClass( 'open' ) )
			{
				/* add the active class to the parent anchor */
				parent_menu.find( 'a.open_sub' ).removeClass( 'current' );
				parent_item.find( 'a.open_sub' ).addClass( 'current' );
				
				/* close the other submenus and open the target submenu */
				submenus.hide( ).removeClass( 'open' );
				target_el.slideDown( 200 ).addClass( 'open' );
			}
			e.preventDefault( );
		});
	
		
		/* close the dropdown / flyout if user clicks outside the submenu */
		$( 'body' ).click( function( e )
		{
			var main_menu 	= $( 'ul.primary_menu' );
			var submenu		= $( 'ul.submenu' );
			var parent_menu	= submenu.parents( 'ul.primary_menu' );
			var top_links	= parent_menu.find( 'a.open_sub' );
			if ( 
				! submenu.is( e.target ) && 
				submenu.has( e.target ).length === 0 && 
				top_links.has( e.target ).length === 0 
			) {
				main_menu.find( 'a.open_sub' ).removeClass( 'current' );
				submenu.hide( ).removeClass( 'open' );
			}
		});
		
		/* init the placeholder plugin */
		$( 'input, textarea' ).placeholder( );
		
		/* submit the form via jquery (without validation) */
		$( '.submit_this_form' ).click( function( e ) {
			var form = $( this ).parents( 'form' );
			form.submit( );
			e.preventDefault( );
		});
		
		/** 
		 * validate form elements with jQuery by class and custom attributes
		 *
		 * TODO - make it more robust and turn into a plugin
		 * ---
		 * usage: 
		 * 1. add "validate_this_form" class to the button, anchor, or other element that submits the form
		 * 2. add the following custom attributes to the input / textarea elements:
		 *    - "error_required" - error message thrown if the field is required
		 *    - "error_match" - error message thrown if a field doesn't match it's counterpart field
		 *    - "data_match" - name attribute of the counterpart field to match
		 *    - "error_email" - error message thrown if field must be a valid email
		 *    - "error_alphanumeric" - error message thrown if field must be alpha-numeric
		 *    - "error_min" - error message thrown if field has a minimum number of characters
		 *    - "data_min" - the minimum number of chars if using "error_min"
		 *    - "error_max" - error message thrown if field has a maximum number of characters
		 *    - "data_max" - the maximum number of chars if using "error_max"
		 *    - "error_counterpart" - error thrown to a counterpart field (e.g. confirm password)
		 *    - "data_counterpart" - name attribute of the counterpart field to throw the error (e.g. password)
		 * 3. error messages are thrown to "error_container" element within the respective "field_group" element and "all_errors_container" 
		 */
		$( '.validate_this_form' ).click( function( e ) {

			var form = $( this ).parents( 'form' );

			/* clear the errors */
			form.find( '.all_errors_container' ).html( '' );
			form.find( '.error_container' ).html( '' );
			form.find( '.error_field' ).removeClass( 'error_field' );
			var errors = { };

			/** 
			 * check standard validations 
			 * go in reverse order of priority
			 * (e.g.) required should be the first thing checked, but it goes on the bottom
			 * so that it takes over the value of the errors[field.name] and the 
			 * lower priority errors do not get displayed at the same time
			 */
			$.each( form.serializeArray( ), function( i, field ) 
			{ 
				var form_input = form.find( '*[name=' + field.name + ' ]' );
				if ( form_input.attr( 'error_match' ) !== undefined && form_input.attr( 'data_match' ) !== undefined )
				{
					var match_field = form_input.attr( 'data_match' );

					/* if the match field exists */
					if ( form.find( '*[name=' + match_field + ' ]' ).length !== 0 ) 
					{
						if ( field.value !== $( '*[name=' + match_field + ' ]' ).val( ) )
						{
							errors[ field.name ] = form_input.attr( 'error_match' );
						}
					}
				}
				if ( form_input.attr( 'error_email' ) !== undefined )
				{
					if ( ! is_valid_email( field.value ) )
					{
						errors[ field.name ] = form_input.attr( 'error_email' );
					}
				}
				if ( form_input.attr( 'error_alphanumeric' ) !== undefined )
				{
					if ( ! is_alphanumeric( field.value ) )
					{
						errors[ field.name ] = form_input.attr( 'error_alphanumeric' );
					}
				}
				if ( form_input.attr( 'error_min' ) !== undefined && form_input.attr( 'data_min' ) !== undefined )
				{
					if ( field.value.length < form_input.attr( 'data_min' ) )
					{
						errors[ field.name ] = form_input.attr( 'error_min' );
					}
				}
				if ( form_input.attr( 'error_max' ) !== undefined && form_input.attr( 'data_max' ) !== undefined )
				{
					if ( field.value.length > form_input.attr( 'data_max' ) )
					{
						errors[ field.name ] = form_input.attr( 'error_max' );
					}
				}
				if ( form_input.attr( 'error_required' ) !== undefined )
				{
					if ( field.value.length === 0 )
					{
						errors[ field.name ] = form_input.attr( 'error_required' );
					}
				}

			});

			/**
			* if the errors aren't empty
			*/
			if ( ! $.isEmptyObject( errors ) ) 
			{
				/* set the counterpart errors */
				$.each( errors, function( key, value ) 
				{
					if ( form.find( '*[name=' + key + ']' ).attr( 'data_counterpart' ) !== undefined )
					{
						var cp_message = form.find( '*[name=' + key + ' ]' ).attr( 'error_counterpart' ) !== undefined ? form.find( '*[name=' + key + ' ]' ).attr( 'error_counterpart' ) : '&nbsp;';
						var counterpart = form.find( '*[name=' + key + ' ]' ).attr( 'data_counterpart' );
						errors[ counterpart ] = ! errors[ counterpart ] ? cp_message : errors[ counterpart ];
					}
				});

				/* display the errors to error elements + set error classes */
				$.each( errors, function( key, value ) 
				{
					if ( form.find( '*[name=' + key + ']' ).length !== 0 )
					{
						var field_group = form.find( '*[name=' + key + ']').parents( '.field_group' );
						field_group.find( '.error_container' ).html( value ); 
						field_group.addClass( 'error_field' ); /* add error class to the whole field group */
						/* displays all errors into the all_errors_container - if desired */
						form.find( '.all_errors_container' ).append( value + '<br />' ).css({ 'display':'block' }).show( );
					}
				}); 

				/* scroll to the top of the form */
				$( 'html, body' ).animate({
					scrollTop: $( form ).offset( ).top
				}, 500 );

				/* clear the password fields */
				$.each( form.serializeArray( ), function( i, field ) 
				{
					var form_input = form.find( '*[name=' + field.name + ' ]' );
					if ( form_input.attr( 'type' ) === 'password' )
					{
						form_input.val( '' );
					}
				});
			}

			/**
			* otherwise submit the form
			*/
			if ( $.isEmptyObject( errors ) ) 
			{
				form.submit( );
			}
			e.preventDefault( );
		});
		
		/* check for alpha-numeric */
		function is_alphanumeric( string ) 
		{
			var regex = /^[A-Za-z0-9_.-]+$/;
			if ( regex.test( string ) === true ) 
			{
				return true;
			} 
			return false;
		}

		/* check for valid email address */
		function is_valid_email( email ) 
		{
			var email_regex = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
			if ( email_regex.test( email ) === true ) 
			{
				return true;
			} 
			return false;
		}
		
		
	});
	
	/* circular carousel - my custom plugin */
	$.fn.tabs = function( ) 
	{
		/* set vars */
		var tab_group 		= this;
		var tab_controls 	= tab_group.find( '.tab_control' );
		var active_tabs 	= tab_group.find( '.tab_control.active' );
		var tab_targets 	= tab_group.find( '.tab_target' );
		var active_tab 		= active_tabs.first( ).length > 0 ? active_tabs.first( ) : tab_group.find( '.tab_control:eq(0)' );
		var target_id		= active_tab.attr( 'rel' );
		var target_el		= tab_group.find( '.tab_target#' + target_id );
		
		/* set the css */
		tab_group.hide( );
		$( window ).load( function( ) {
			
			/* grab the active tab and hide all other tabs */
			tab_controls.removeClass( 'current' );
			active_tab.addClass( 'current' );
			tab_targets.hide( );
			target_el.show( );
			
			/* show the tabs when everything is loaded */
			tab_group.show( );
			
		});
		
		/* tab click */
		tab_group.on( 'click', '.tab_control', function( e ) {
			
			/* set active class on the clicked tab */
			tab_controls.removeClass( 'current' );
			$( this ).addClass( 'current' );
			
			/* grab the target tab and show it */
			var target_id 	= $( this ).attr( 'rel' );
			var target_el	= tab_group.find( '.tab_target#' + target_id );
			tab_targets.hide( );
			target_el.show( );
			e.preventDefault( );
		});
	}
	
	/* thumb slideshow - my custom plugin */
	$.fn.thumb_slider = function( ) 
	{
		/* set vars */
		var thumb_slider	= this;
		var slides 			= thumb_slider.find( '.slide' );
		var controls 		= thumb_slider.find( '.controls .control' );
		var active_controls = controls.find( '.active' );
		var active_control 	= active_controls.first( ).length > 0 ? active_controls.first( ) : thumb_slider.find( '.control:eq(0)' );
		var target_index	= active_control.index( );
		var target_el		= thumb_slider.find( '.slide:eq(' + target_index + ')' );
		
		/* set the css */
		thumb_slider.hide( );
		$( window ).load( function( ) {
			
			/* grab the active slide and hide all other slides */
			controls.removeClass( 'current' ).css({ 'opacity':0.5 });
			active_control.addClass( 'current' ).css({ 'opacity':1 });;
			slides.hide( );
			target_el.show( );
			
			/* show the slideshow when everything is loaded */
			thumb_slider.show( );
			
		});
		
		/* click the thumb */
		thumb_slider.on( 'click', '.control a', function( e ) {
			
			/* set active class on the clicked thumb */
			controls.removeClass( 'current' ).css({ 'opacity':0.5 });
			parent_control = $( this ).parents( '.control' );
			parent_control.addClass( 'current' ).css({ 'opacity':1 });
			
			/* grab the target thumb and show it */
			var target_index 	= $( this ).parents( '.control' ).index( );
			var target_el		= thumb_slider.find( '.slide:eq(' + target_index + ')' );
			slides.hide( );
			target_el.show( );
			e.preventDefault( );
		});
		
	}
	
	/* toggle / accordian events */
	/* thumb slideshow - my custom plugin */
	$.fn.accordian = function( speed ) 
	{

		/* set vars */
		var accordian 		= this;
		var groups 			= accordian.find( '.group' );
		var boxes 			= accordian.find( '.box' );
		var controls 		= accordian.find( '.control' );		
		var active_controls = controls.find( '.active' );
		var active_control 	= active_controls.first( ).length > 0 ? active_controls.first( ) : accordian.find( '.control:eq(0)' );
		var speed = ( ! speed ) ? 300 : speed;
		
		/** 
		 * slide up all of the toggle targets and open the ones with the parent toggle box set to open - very fast so as to not be visible 
		 * - we do this to eliminate the hitch in the animation by animating up first instead of using "display:none;"
		 * - before sliding up, grab the collapsed accordian height plus one expanded box and set as min-height to get rid of Chrome animation choppiness (is that a word?)
		 */
		var exp_height = accordian.height( );
		accordian.hide( );
		boxes.slideUp( 0 );
		
		/* on page load - show the accordian */
		$( window ).load( function( ) {
		
			/* show the accordian when everything is loaded */
			accordian.show( )
			var coll_height = accordian.height( );
			accordian.css({ 
				'min-height':parseInt( coll_height + exp_height / boxes.length )
			});
			
		});
		
		/* accordian control click action */
		accordian.on( 'click', '.control', function( e ) {
			var parent_el 	= $( this ).closest( '.group' );
			var target 		= parent_el.find( '.box' );
			if ( parent_el.hasClass( 'open' ) )
			{
				parent_el.removeClass( 'open' );
				target.slideUp( speed );
			} 
			else
			{
				groups.removeClass( 'open' );
				boxes.slideUp( speed );
				parent_el.addClass( 'open' );
				target.slideDown( speed );
			}
			e.preventDefault( );
		});
		
	}
	
})( jQuery );