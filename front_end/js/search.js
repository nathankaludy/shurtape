/**
 * JS for the product search pages - normal / tecnhical view
 * ==============
 * NOTE: the JS for the detail and category pages was moved to products.js
 * - better to compartmentalize for the dedicated resource for search
 */
/* define $ as jQuery just in case */
( function( $ ){

	/* doc ready */
	$( function( ){
	
		/* init the filter function */
		filter_products( );
		
		/* init the compare products function */
		compare_products( );
		
		/* filter bar filter controls */
		function filter_products( ) {
		
			/* filter control click action */
			$( '.filter_box' ).on( 'click', '.filter_control', function( e ) {
			
				/* checkbox state */
				if ( $( this ).hasClass( 'current' ) )
				{
					$( this ).removeClass( 'current' );
				}
				else
				{
					$( this ).addClass( 'current' );
				}
				
				/*  this is where a significant amount of AJAX needs to go */
				$.ajax({
					url:'',
					type:'get',
					dataType:'json',
					success: function ( data ) 
					{
						/* do some AJAX */
						
					},
					error: function ( errmsg ) 
					{
					
					}
				});
				e.preventDefault( );
			});
			
			/* filter control click action */
			$( '.filter_tags' ).on( 'click', '.control_remove', function( e ) {
			
				/* remove the tag - TODO - put this in success callback */
				$( this ).remove( );
				
				/*  AJAX below */
				$.ajax({
					url:'',
					type:'post',
					dataType:'json',
					success: function ( data ) 
					{
						/* do something */
						
					},
					error: function ( errmsg ) 
					{
					
					}
				});
				e.preventDefault( );
			});
		}
		
		/* filter bar filter controls */
		function compare_products( ) {
		
			/* filter control click action */
			$( '#product_grid' ).on( 'click', '.control_compare', function( e ) {

				/* checkbox state */
				if ( $( this ).hasClass( 'current' ) )
				{
					$( this ).removeClass( 'current' );
				}
				else
				{
					$( this ).addClass( 'current' );
				}
				e.preventDefault( );
			});
		}
		
	});
	
})( jQuery );