<?php 

/* set the page vars */
$body_class = 'elements';

/* include the header + nav partials */
include_once( 'partials/head.php' );
include_once( 'partials/header.php' ); ?>
	
<div id="content">

	<div id="main" class="fixed_wrap clearfix">
		<h1 class="pad">
			ELEMENTS 
		</h1>
		<p>
			It's best to style common site elements before building out of the pages, for the sake of consistency.
			View source for the markup.  The "pad" class can be omitted as it just adds spacing between the elements below.		
		</p>
		
		<hr />
		
		<h1 class="headline pad">
			Headline - .headline
		</h1>
		
		<div class="blue_bar">
			Blue bar - .blue_bar
		</div>
		
		<div class="pad">
			<h1>Heading 1 HEADING 1 - H1</h1>
		</div>

		<div class="pad">
			<h2>Heading 2 HEADING 2 - H2</h2>
		</div>

		<div class="pad">
			<h3>Heading 3 HEADING 3 - H3</h3>
		</div>

		<div class="pad">
			<h4>Heading 4 HEADING 4 - H4</h4>
		</div>

		<div class="pad">
			<h5>Heading 5 HEADING 5 - H5</h5>
		</div>

		<div class="pad">
			<h6>Heading 6 HEADING 6 - H6</h6>
		</div>

		<hr />

		<p>
			Paragraph 1 - p - Default font size 15px - Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor 
			incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud 
			exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. 
		</p>

		<p>
			Paragraph 2 - p - Default font size 15px - Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor 
			incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud 
			exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.  Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor 
			incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud 
			exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.  Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor 
			incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud 
			exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.  Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor 
			incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud 
			exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. 
		</p>

		<hr />
		
		<div class="white_box">
			<h3>White box - .white_box</h3>
			<p>
				Paragraph 1 - p - Default font size 15px - Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor 
				incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud 
				exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. 
			</p>
		</div>
		
		<hr />

		<p>
			Hyperlinks - a - Lorem ipsum dolor sit amet, <a href="#">hyperlink consectetur adipisicing</a> elit
		</p>

		<p>
			Bold text - b - Lorem ipsum dolor sit amet, <b>bolded text consectetur adipisicing</b> elit
		</p>

		<p>
			Strong text - strong - Lorem ipsum dolor sit amet, <strong>strong text consectetur adipisicing</strong> elit
		</p>

		<p>
			Emphasis text - em - Lorem ipsum dolor sit amet, <em>emphasis text consectetur adipisicing</em> elit
		</p>

		<p>
			Italic text - i - Lorem ipsum dolor sit amet, <i>italic text consectetur adipisicing</i> elit
		</p>

		<p class="blue">
			Blue text - .blue - Lorem ipsum dolor sit amet, CAPITALS <a href="#">hyperlink</a> <b>bold</b> <strong>strong</strong> <em>emphasis</em> <i>italic</i>
		</p>
		
		<p class="orange">
			Orange text - .orange - Lorem ipsum dolor sit amet, CAPITALS <a href="#">hyperlink</a> <b>bold</b> <strong>strong</strong> <em>emphasis</em> <i>italic</i>
		</p>

		<p class="white" style="background:#000;padding:10px;">
			White text - .white - Lorem ipsum dolor sit amet, CAPITALS <a href="#">hyperlink</a> <b>bold</b> <strong>strong</strong> <em>emphasis</em> <i>italic</i>
		</p>

		<hr />

		<p class="small">
			Small text - .small / small - Lorem ipsum dolor sit amet, CAPITALS <a href="#">hyperlink</a> <b>bold</b> <strong>strong</strong> <em>emphasis</em> <i>italic</i>
		</p>

		<p class="regular">
			Regular text - .regular / .reg - Lorem ipsum dolor sit amet, CAPITALS <a href="#">hyperlink</a> <b>bold</b> <strong>strong</strong> <em>emphasis</em> <i>italic</i>
		</p>

		<p class="medium">
			Medium text - .medium / .med - Lorem ipsum dolor sit amet, CAPITALS <a href="#">hyperlink</a> <b>bold</b> <strong>strong</strong> <em>emphasis</em> <i>italic</i>
		</p>

		<p class="large">
			Large text - .large / big - Lorem ipsum dolor sit amet, CAPITALS <a href="#">hyperlink</a> <b>bold</b> <strong>strong</strong> <em>emphasis</em> <i>italic</i>
		</p>

		<p class="xlarge">
			X-Large text - .xlarge - Lorem ipsum dolor sit amet, CAPITALS <a href="#">hyperlink</a> <b>bold</b> <strong>strong</strong> <em>emphasis</em> <i>italic</i>
		</p>

		<p class="xxlarge">
			XX-Large text - .xxlarge - Lorem ipsum dolor sit amet, CAPITALS <a href="#">hyperlink</a> <b>bold</b> <strong>strong</strong> <em>emphasis</em> <i>italic</i>
		</p>

		<p class="xxxlarge">
			XXX-Large text - .xxxlarge - Lorem ipsum dolor sit amet, CAPITALS <a href="#">hyperlink</a> <b>bold</b> <strong>strong</strong> <em>emphasis</em> <i>italic</i>
		</p>

		<hr />

		<h1>BUTTONS</h1>

		<div class="pad">
			<a href="#" class="btn btn-default">Default anchor button - a.btn</a>
		</div>
		<div class="pad">
			<button type="button" class="btn">Default button button - button.btn</button>
		</div>

		<div class="pad">
			<a href="#" class="btn btn-lg">Large anchor button - a.btn.btn-lg</a>
		</div>
		<div class="pad">
			<button type="button" class="btn btn-lg">Large button button - button.btn.btn-lg</button>
		</div>
		
		<div class="pad">
			<a href="#" class="btn btn-sm">Small anchor button - a.btn.btn-sm</a>
		</div>
		<div class="pad">
			<button type="button" class="btn btn-sm">Small button button - button.btn.btn-sm</button>
		</div>
		
		<div class="pad">
			<a href="#" class="btn btn-wide">Wide anchor button - a.btn.btn-wide</a>
		</div>
		<div class="pad">
			<button type="button" class="btn btn-wide">Wide button button - button.btn.btn-wide</button>
		</div>
		
		<div class="pad">
			<a href="#" class="btn btn-orange">Orange anchor button - a.btn.btn-orange</a>
		</div>
		<div class="pad">
			<button type="button" class="btn btn-orange">Orange button button - button.btn.btn-orange</button>
		</div>
		
		<div class="pad">
			<a href="#" class="btn btn-orange btn-wide">Wide orange anchor button - a.btn.btn-orange.btn-wide</a>
		</div>
		<div class="pad">
			<button type="button" class="btn btn-orange btn-wide">Wide orange button button - button.btn.btn-orange.btn-wide</button>
		</div>
		
		<div class="pad">
			<a href="#" class="btn btn-orange btn-squared">Squared anchor button - a.btn.btn-squared</a>
		</div>
		<div class="pad">
			<button type="button" class="btn btn-orange btn-squared">Squared button button - button.btn.btn-squared</button>
		</div>

		<hr />

		<h1>FORMS</h1>

		<form action="#" method="post">
		
			<div class="control clearfix" style="margin-bottom:20px;">
				<label for="demo">Select</label><br />
				<select class="form-control" tabindex="-1">
					<option value="">Please select</option>
					<option value="AL">Alabama</option>
					<option value="AK">Alaska</option>
					<option value="AZ">Arizona</option>
					<option value="AR">Arkansas</option>
					<option value="CA">California</option>
					<option value="CO">Colorado</option>
					<option value="CT">Connecticut</option>
					<option value="DE">Delaware</option>
					<option value="DC">District Of Columbia</option>
					<option value="FL">Florida</option>
					<option value="GA">Georgia</option>
					<option value="HI">Hawaii</option>
					<option value="ID">Idaho</option>
					<option value="IL">Illinois</option>
					<option value="IN">Indiana</option>
					<option value="IA">Iowa</option>
					<option value="KS">Kansas</option>
					<option value="KY">Kentucky</option>
					<option value="LA">Louisiana</option>
					<option value="ME">Maine</option>
					<option value="MD">Maryland</option>
					<option value="MA">Massachusetts</option>
					<option value="MI" selected="selected">Michigan</option>
					<option value="MN">Minnesota</option>
					<option value="MS">Mississippi</option>
					<option value="MO">Missouri</option>
					<option value="MT">Montana</option>
					<option value="NE">Nebraska</option>
					<option value="NV">Nevada</option>
					<option value="NH">New Hampshire</option>
					<option value="NJ">New Jersey</option>
					<option value="NM">New Mexico</option>
					<option value="NY">New York</option>
					<option value="NC">North Carolina</option>
					<option value="ND">North Dakota</option>
					<option value="OH">Ohio</option>
					<option value="OK">Oklahoma</option>
					<option value="OR">Oregon</option>
					<option value="PA">Pennsylvania</option>
					<option value="RI">Rhode Island</option>
					<option value="SC">South Carolina</option>
					<option value="SD">South Dakota</option>
					<option value="TN">Tennessee</option>
					<option value="TX">Texas</option>
					<option value="UT">Utah</option>
					<option value="VT">Vermont</option>
					<option value="VA">Virginia</option>
					<option value="WA">Washington</option>
					<option value="WV">West Virginia</option>
					<option value="WI">Wisconsin</option>
					<option value="WY">Wyoming</option>
				</select>
			</div>

			<div class="form-group clearfix" style="margin-bottom:20px;">
				<label>Text input with inline label</label><br />
				<input type="text" class="form-control" placeholder="Text Input with Inline Label" />
			</div>

			<div class="control" style="margin-bottom:20px;">
				<label>Password input with inline label</label><br />
				<input type="password" class="form-control" placeholder="Password Input with Inline Label" />
			</div>

			<div class="control" style="margin-bottom:20px;">
				<label>Textarea input with inline label</label><br />
				<textarea rows="4" class="form-control" placeholder="Textarea - Lorem ipsum dolor sit amet"></textarea>
			</div>

			<div class="checkbox" style="margin-bottom:20px;">
				<label>
					<input type="checkbox" />
					Checkbox
				</label>
			</div>
			
			<div class="checkbox" style="margin-bottom:20px;">
				<label>
					<input type="checkbox" checked />
					Checkbox (checked)
				</label>
			</div>

			<div class="radio">
				<label>
					<input type="radio" name="demo" value="option1" checked>
					Radio 1
				</label>
			</div>
			<div class="radio">
				<label>
					<input type="radio" name="demo" value="option2">
					Radio 2
				</label>
			</div>
			
		</form>
		
		<hr />
		
		<h1>TABLES</h1>
		
		<div class="pad">
			<table class="table">
				<thead>
					<tr>
						<th>Lorem</th>
						<th>Ipsum</th>
						<th class="text-center">Dolor</th>
						<th class="text-right">Amit</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>123</td>
						<td class="gray_cell">123</td>
						<td class="text-center">123</td>
						<td class="text-right">123</td>
					</tr>
					<tr>
						<td>Lorem Ipsum Dolor</td>
						<td class="gray_cell">Lorem Ipsum Dolor</td>
						<td class="text-center">Lorem Ipsum Dolor</td>
						<td class="text-right">Lorem Ipsum Dolor</td>
					</tr>
					<tr>
						<td>Lorem Ipsum Dolor</td>
						<td class="gray_cell">Lorem Ipsum Dolor</td>
						<td class="text-center">Lorem Ipsum Dolor</td>
						<td class="text-right">Lorem Ipsum Dolor</td>
					</tr>
				</tbody>
				<tfoot>
					<tr>
						<td colspan="3">Lorem Ipsum Dolor</td>
						<td class="text-right">2013</td>
					</tr>
				</tfoot>
			</table>
		</div>
		
		
	</div>
</div>
	
<?php 

/* include the footer partials */
include_once( 'partials/footer.php' );
include_once( 'partials/foot.php' ); ?>
