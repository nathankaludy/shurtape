<?php
/**
 * Implements hook_form_FORM_ID_alter().
 *
 * @param $form
 *   The form.
 * @param $form_state
 *   The form state.
 */
function shurtap_form_system_theme_settings_alter( &$form, &$form_state ) 
{
	$form['shur_settings'] = array(
		'#type' => 'fieldset',
		'#title' => t('shurtape Theme Settings'),
		'#collapsible' => FALSE,
		'#collapsed' => FALSE,
	);
	$form['shur_settings']['show_front_content'] = array(
		'#type' => 'checkbox',
		'#title' => t('Show content and sidebar on front page'),
		'#default_value' => theme_get_setting('show_front_content','shurtape'),
		'#description' => t('Check this option to show content and sidebar on the front page.'),
	);
	$form['shur_settings']['breadcrumbs'] = array(
		'#type' => 'checkbox',
		'#title' => t('Show breadcrumbs in a page'),
		'#default_value' => theme_get_setting('breadcrumbs','shurtape'),
		'#description'   => t("Check this option to show breadcrumbs in page. Uncheck to hide."),
	);
	$form['shur_settings']['slideshow'] = array(
		'#type' => 'fieldset',
		'#title' => t('Front Page Slideshow'),
		'#collapsible' => TRUE,
		'#collapsed' => FALSE,
	);
	$form['shur_settings']['slideshow']['slideshow_display'] = array(
		'#type' => 'checkbox',
		'#title' => t('Show slideshow'),
		'#default_value' => theme_get_setting('slideshow_display','shurtape'),
		'#description'   => t("Check this option to show Slideshow in front page. Uncheck to hide."),
	);
	$form['shur_settings']['slideshow']['slide'] = array(
		'#markup' => t('You can change the description and URL of each slide in the following Slide Setting fieldsets.'),
	);
	$form['shur_settings']['slideshow']['slide1'] = array(
		'#type' => 'fieldset',
		'#title' => t('Slide 1'),
		'#collapsible' => TRUE,
		'#collapsed' => TRUE,
	);
	$form['shur_settings']['slideshow']['slide1']['slide1_desc'] = array(
		'#type' => 'textfield',
		'#title' => t('Slide Description'),
		'#default_value' => theme_get_setting('slide1_desc','shurtape'),
	);
	$form['shur_settings']['slideshow']['slide1']['slide1_url'] = array(
		'#type' => 'textfield',
		'#title' => t('Slide URL'),
		'#default_value' => theme_get_setting('slide1_url','shurtape'),
	);
	$form['shur_settings']['slideshow']['slide2'] = array(
		'#type' => 'fieldset',
		'#title' => t('Slide 2'),
		'#collapsible' => TRUE,
		'#collapsed' => TRUE,
	);
	$form['shur_settings']['slideshow']['slide2']['slide2_desc'] = array(
		'#type' => 'textfield',
		'#title' => t('Slide Description'),
		'#default_value' => theme_get_setting('slide2_desc','shurtape'),
	);
	$form['shur_settings']['slideshow']['slide2']['slide2_url'] = array(
		'#type' => 'textfield',
		'#title' => t('Slide URL'),
		'#default_value' => theme_get_setting('slide2_url','shurtape'),
	);
	$form['shur_settings']['slideshow']['slide3'] = array(
		'#type' => 'fieldset',
		'#title' => t('Slide 3'),
		'#collapsible' => TRUE,
		'#collapsed' => TRUE,
	);
	$form['shur_settings']['slideshow']['slide3']['slide3_desc'] = array(
		'#type' => 'textfield',
		'#title' => t('Slide Description'),
		'#default_value' => theme_get_setting('slide3_desc','shurtape'),
	);
	$form['shur_settings']['slideshow']['slide3']['slide3_url'] = array(
		'#type' => 'textfield',
		'#title' => t('Slide URL'),
		'#default_value' => theme_get_setting('slide3_url','shurtape'),
	 );
	$form['shur_settings']['slideshow']['slide4'] = array(
		'#type' => 'fieldset',
		'#title' => t('Slide 4'),
		'#collapsible' => TRUE,
		'#collapsed' => TRUE,
	);
	$form['shur_settings']['slideshow']['slide4']['slide4_desc'] = array(
		'#type' => 'textfield',
		'#title' => t('Slide Description'),
		'#default_value' => theme_get_setting('slide4_desc','shurtape'),
	);
	$form['shur_settings']['slideshow']['slide4']['slide4_url'] = array(
		'#type' => 'textfield',
		'#title' => t('Slide URL'),
		'#default_value' => theme_get_setting('slide4_url','shurtape'),
	);
	$form['shur_settings']['slideshow']['slideimage'] = array(
		'#markup' => t('To change the Slide Images, Replace the slide-image-1.jpg, slide-image-2.jpg and slide-image-3.jpg in the images folder of the shurtape theme folder.'),
	);
	$form['shur_settings']['footer'] = array(
		'#type' => 'fieldset',
		'#title' => t('Footer'),
		'#collapsible' => TRUE,
		'#collapsed' => FALSE,
	);
	$form['shur_settings']['footer']['footer_copyright'] = array(
		'#type' => 'checkbox',
		'#title' => t('Show copyright text in footer'),
		'#default_value' => theme_get_setting('footer_copyright','shurtape'),
		'#description'   => t("Check this option to show copyright text in footer. Uncheck to hide."),
	);
	$form['shur_settings']['footer']['footer_credits'] = array(
		'#type' => 'checkbox',
		'#title' => t('Show theme credits in footer'),
		'#default_value' => theme_get_setting('footer_credits','shurtape'),
		'#description'   => t("Check this option to show copyright text in footer. Uncheck to hide."),
	);
}
