<?php

/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * - $title : The title of this group of rows.  May be empty.
 * - $options['type'] will either be ul or ol.
 * @ingroup views_templates
 */
?>

<?php if ( !empty( $rows ) ): 
	$i=0; 
	$row_count = count( $rows ); ?>
	<?php if ( $row_count > 1 ): ?>
		<a href="#" class="control prev">
			<img src="<?php print base_path() . drupal_get_path('theme', 'shurtape'); ?>/images/btn_subnav_mobile_arrow_left.png" />
		</a>
	<?php endif; ?>
	<div class="slide_window clearfix">
		<?php foreach ($rows as $id => $row): ?>
			<div class="slide">
				<div class="item">
					<?php echo $row; ?>
				</div>
			</div>
			<?php $i++; ?>
		<?php endforeach; ?>
	</div>
	<?php if ( $row_count > 1 ): ?>
		<a href="#" class="control next">
			<img src="<?php print base_path() . drupal_get_path('theme', 'shurtape'); ?>/images/btn_subnav_mobile_arrow_right.png" />
		</a>
	<?php endif; ?>
<?php endif; ?>