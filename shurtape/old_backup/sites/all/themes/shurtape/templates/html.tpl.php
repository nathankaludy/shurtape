<?php
  $products_menu = menu_tree_output(menu_tree_all_data('menu-products-menu'));
  $resources_menu = menu_tree_output(menu_tree_all_data('menu-resources-menu'));
  $about_menu = menu_tree_output(menu_tree_all_data('menu-about-menu'));
  $contact_menu = menu_tree_output(menu_tree_all_data('menu-contact-menu'));

  $products_menu = drupal_render($products_menu);
  $resources_menu = drupal_render($resources_menu);
  $about_menu = drupal_render($about_menu);
  $contact_menu = drupal_render($contact_menu);
?>
<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html lang="<?php print $language->language; ?>" class="no-js" dir="<?php print $language->dir; ?>"<?php print $rdf_namespaces; ?>>
<!--<![endif]-->
<head profile="<?php print $grddl_profile; ?>">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="format-detection" content="telephone=yes">
  <?php print $head; ?>
  <title><?php print $head_title; ?></title>
  <meta name="viewport" content="width=device-width,initial-scale=1">
  <?php print $styles; ?>
  <?php print $scripts; ?>
  <!--[if IE 8]>
  <style>
    .front .product_grid ul li a.item .item_title,
    .product_grid ul li a.item .item_content .item_title {
      padding-bottom: 0px;
      min-height: 0px;
      height: 46px;
    }
  </style>
  <![endif]-->
  <script type="text/javascript" src="<?php print base_path() . drupal_get_path('theme', 'shurtape'); ?>/js/jquery-1.10.2.min.js"></script>
  <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/jquery-ui.min.js"></script>
  <script type="text/javascript" src="<?php print base_path() . drupal_get_path('theme', 'shurtape'); ?>/js/jquery.dragtable.js"></script>
  <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
  <!--[if lt IE 9]>
  <script type="text/javascript" src="<?php print base_path() . drupal_get_path('theme', 'shurtape'); ?>/js/html5.js"></script>
  <script type="text/javascript" src="<?php print base_path() . drupal_get_path('theme', 'shurtape'); ?>/js/respond.min.js"></script>
  <![endif]-->
  <script type="text/javascript" src="<?php print base_path() . drupal_get_path('theme', 'shurtape'); ?>/js/bootstrap.min.js"></script>
  <script type="text/javascript" src="//use.typekit.net/jny7oeb.js"></script>
  <!-- typekit fonts -->
  <script type="text/javascript">try {
      Typekit.load();
    } catch (e) {
    }</script>
  <!-- Start Visual Website Optimizer Asynchronous Code -->
  <script type='text/javascript'>
    var _vwo_code = (function () {
      var account_id = 48704,
        settings_tolerance = 2000,
        library_tolerance = 2500,
        use_existing_jquery = false,
      // DO NOT EDIT BELOW THIS LINE
        f = false, d = document;
      return {
        use_existing_jquery: function () {
          return use_existing_jquery;
        }, library_tolerance: function () {
          return library_tolerance;
        }, finish: function () {
          if (!f) {
            f = true;
            var a = d.getElementById('_vis_opt_path_hides');
            if (a)a.parentNode.removeChild(a);
          }
        }, finished: function () {
          return f;
        }, load: function (a) {
          var b = d.createElement('script');
          b.src = a;
          b.type = 'text/javascript';
          b.innerText;
          b.onerror = function () {
            _vwo_code.finish();
          };
          d.getElementsByTagName('head')[0].appendChild(b);
        }, init: function () {
          settings_timer = setTimeout('_vwo_code.finish()', settings_tolerance);
          this.load('//dev.visualwebsiteoptimizer.com/j.php?a=' + account_id + '&u=' + encodeURIComponent(d.URL) + '&r=' + Math.random());
          var a = d.createElement('style'), b = 'body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}', h = d.getElementsByTagName('head')[0];
          a.setAttribute('id', '_vis_opt_path_hides');
          a.setAttribute('type', 'text/css');
          if (a.styleSheet)a.styleSheet.cssText = b; else a.appendChild(d.createTextNode(b));
          h.appendChild(a);
          return settings_timer;
        }
      };
    }());
    _vwo_settings_timer = _vwo_code.init();
  </script>
  <!-- End Visual Website Optimizer Asynchronous Code -->
</head>
<body class="<?php print $classes; ?>" <?php print $attributes; ?>>
<?php print $page_top; ?>
<div class="nav_overlay"></div>
<div id="mobile_header" class="clearfix mobile" role="banner">
  <div class="control_bar clearfix">
    <a href="#" class="menu_control">
      <img src="<?php print base_path() . drupal_get_path('theme', 'shurtape'); ?>/images/control_mobile_menu.png" alt=""/>
    </a>
    <a href="<?php print base_path(); ?>contact-us" title="Contact" class="tab_control">
      <img src="<?php print base_path() . drupal_get_path('theme', 'shurtape'); ?>/images/btn_mobile_tab_contact.png" alt=""/>
    </a>
  </div>
</div>
<div id="mobile_nav" class="clearfix mobile">
  <div class="search_box">
    <form action="<?php print base_path(); ?>search/products" method="get">
				<span class="input_wrap">
					<input title="Enter the terms you wish to search for." type="text" name="search_api_views_fulltext" value="" maxlength="128" class="form-text">
				</span>
      <a href="#" class="submit_this_form">
        <!--
					<img src="<?php print base_path() . drupal_get_path('theme', 'shurtape'); ?>/images/control_mobile_nav_search.png" alt="" />
					-->
      </a>
    </form>
  </div>
  <div class="mobile_toggle_box open" id="mobile_main_menu">
    <h2><a href="#" class="mobile_toggle_control">Main Menu</a></h2>

    <div class="mobile_toggle_target">
      <ul class="sub_menu">
        <li>
          <a href="/" title="<?php print t('Home'); ?>">
            <?php print t('Home'); ?>
          </a>
        </li>
        <li><?php print $products_menu; ?></li>
        <li><?php print $resources_menu; ?></li>
        <li><?php print $about_menu; ?></li>
        <li><?php print $contact_menu; ?></li>
        <li>
          <a href="<?php print base_path(); ?>where-to-buy" title="<?php print t('Where to Buy'); ?>">
            <?php print t('Where to Buy'); ?>
          </a>
        </li>
      </ul>
    </div>
  </div>
  <div class="mobile_toggle_box open" id="mobile_products_menu">
    <h2><a href="#" class="mobile_toggle_control">Products</a></h2>

    <div class="mobile_toggle_target">
      <ul class="sub_menu">
        <div class="mobile_browse_box">
          <?php

          /* market or type page ? */
          $url = parse_url($_SERVER['REQUEST_URI']);
          $url_parts = explode('/', $url['path']);
          $count = count($url_parts);
          $page_slug = strtolower($url_parts[$count - 1]);
          ?>
          <h3 class="blue_bar">Narrow Your Search:</h3>
          <li>
            <span class="blue_corner"></span>
            <ul class="menu">
              <li class="first last expanded<?php print $page_slug == 'markets' ? ' open' : ''; ?>">
                <a href="#" title="Browse by Market">Browse by Market</a>
                <ul class="menu">
                  <?php
                  $get_market_tree = taxonomy_get_tree(7);
                  $browse_market_tree = taxonomy_nested_tree_render($get_market_tree, FALSE, FALSE, 'Markets');
                  print drupal_render($browse_market_tree);
                  ?>
                </ul>
              </li>
              <li class="first last expanded<?php print $page_slug == 'type' ? ' open' : ''; ?>">
                <a href="#" title="Browse by Type">Browse by Type</a>
                <ul class="menu">
                  <?php
                  $get_type_tree = taxonomy_get_tree(9);
                  $browse_type_tree = taxonomy_nested_tree_render($get_type_tree, FALSE, FALSE, 'Tape Type');
                  print drupal_render($browse_type_tree);
                  ?>
                </ul>
              </li>
            </ul>
          </li>
        </div>
        <div class="mobile_filter_box">
          <?php

          /* filter section of navigation - only available on search pages */
          $filters = shurtape_products_nav_generate_filters();
          if (
            !isset($_GET['search_api_views_fulltext']) &&
            arg(1) !== "markets" &&
            isset($filters)
          ): ?>
            <h3 class="blue_bar">Filter By Specification:</h3>
            <li>
              <span class="blue_corner"></span>
              <ul class="menu">
                <?php for ($i = 0; $i < count($filters); $i++):
                  $id = str_replace(" ", "_", strtolower($filters[$i]['content']['#title']));
                  $title = $filters[$i]['content']['#title']; ?>
                  <a href="#" title="<?php print $title; ?>"><?php print $title; ?></a>
                  <div class="toggle_target">
                    <?php print drupal_render($filters[$i]); ?>
                  </div>
                <?php endfor; ?>
              </ul>
            </li>
          <?php endif; ?>
        </div>
      </ul>
    </div>
  </div>
  <div id="mobile_nav_footer">
    <a href="<?php print $front_page; ?>" id="logo" title="<?php print t('Home'); ?>">
      <img src="<?php print base_path() . drupal_get_path('theme', 'shurtape'); ?>/images/logo_footer_mobile_left.png" alt="<?php print t('Home'); ?>"/>
    </a>
    <a href="tel:888-442-8273" id="cta_mobile_nav" title="<?php print t('Questions/Orders? Call Us: 888-442-8273'); ?>">
      <img src="<?php print base_path() . drupal_get_path('theme', 'shurtape'); ?>/images/cta_mobile_nav.png" alt="<?php print t('Questions/Orders? Call Us: 888-442-8273'); ?>"/>
    </a>
  </div>
</div>
<div class="shift_box">
  <div class="logo_bar mobile">
    <a href="<?php print $front_page; ?>" id="logo" title="<?php print t('Home'); ?>">
      <img src="<?php print base_path() . drupal_get_path('theme', 'shurtape'); ?>/images/logo_mobile_left.png" alt="<?php print t('Home'); ?>"/>
    </a>
    <a href="tel:888-442-8273" id="logo_cta" title="<?php print t('Questions/Orders? Call Us: 888-442-8273'); ?>">
      <img src="<?php print base_path() . drupal_get_path('theme', 'shurtape'); ?>/images/logo_mobile_right.png" alt="<?php print t('Questions/Orders? Call Us: 888-442-8273'); ?>"/>
    </a>
  </div>
  <div id="header" class="clearfix desktop" role="banner">
    <div class="fixed_wrap clearfix">
      <?php if ($logo): ?>
        <a href="<?php print $front_page; ?>" id="logo" title="<?php print t('Home'); ?>">
          <img src="<?php print base_path() . drupal_get_path('theme', 'shurtape'); ?>/images/logo_large.png" alt="<?php print t('Home'); ?>"/>
        </a>
      <?php endif; ?>
      <div id="top_cta">
        Questions/Orders? Call Us: <b class="blue">888-442-8273</b>
      </div>
      <div id="search_box">
        <form action="<?php print base_path(); ?>search/products" method="get">
          <div>
            <div class="container-inline">
              <h2 class="element-invisible">Search form</h2>

              <div class="form-item form-type-textfield form-item-search-block-form">
                <label class="element-invisible" for="edit-search-block-form--2">Search </label>
                <input title="Enter the terms you wish to search for." type="text" id="edit-search-block-form--2" name="search_api_views_fulltext" value="" size="15" maxlength="128" class="form-text">
              </div>
              <div class="form-actions form-wrapper" id="edit-actions">
                <input type="submit" id="edit-submit" class="form-submit"/>
              </div>
            </div>
          </div>
        </form>
      </div>
      <div id="nav">
        <ul class="primary_menu list-inline clearfix">
          <li class="link_products_item"><?php print $products_menu; ?></li>
          <li class="link_resources_item"><?php print $resources_menu; ?></li>
          <li class="link_about_item"><?php print $about_menu; ?></li>
          <li class="link_contact_item"><?php print $contact_menu; ?></li>
          <li class="link_where_item">
            <ul class="menu">
              <li class="first"><?php print l(t('Where to Buy'), 'where-to-buy'); ?></li>
            </ul>
          </li>
        </ul>
      </div>
    </div>
  </div>
  <?php print $page; ?>
  <div id="footer" class="desktop">
    <a href="#" class="scroll_top">Scroll Top</a>

    <div class="fixed_wrap clearfix">
      <div id="footer_first" class="col pull-left">
        <h4>
          <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>"><?php print t('Home'); ?></a>
        </h4>
      </div>

      <div id="footer_second" class="col pull-left">
        <?php
          $menu_products = menu_tree_output(menu_tree_all_data('menu-products-footer-menu'));
          print drupal_render($menu_products);
        ?>
      </div>

      <div id="footer_third" class="col pull-left">
        <?php
          $menu_resources = menu_tree_output(menu_tree_all_data('menu-resources-footer-menu'));
          print drupal_render($menu_resources);
        ?>
      </div>

      <div id="footer_fourth" class="col pull-left">
        <?php
          $footer_menu = menu_tree_output(menu_tree_all_data('menu-about-footer-menu'));
          print drupal_render($footer_menu);
        ?>
      </div>

      <div id="footer_fifth" class="col pull-left">
        <a class="pull-right footer_logo" title="ShurTape - True to Your Work" href="<?php print $front_page; ?>">ShurTape</a>

        <div class="share_links pull-right">
          <!-- <div class="share_links">				     -->

          <h4>FOLLOW US</h4>
          <!-- <ul class="list-inline">
              <li><a target="_blank" class="facebook" href="https://www.facebook.com/Shurtape" title="Shurtape on Facebook">Facebook</a></li>
              <li><a target="_blank" class="twitter" href="https://twitter.com/ShurtapeTech" title="Shurtape on Twitter">Twitter</a></li>
              <li><a target="_blank" class="youtube" href="http://www.youtube.com/user/Shurtapetech" title="Shurtape on YouTube">YouTube</a></li>
              <li><a target="_blank" class="linkedin" href="http://www.linkedin.com/company/shurtape-technologies-llc" title="Shurtape on Linkedin">Linkedin</a></li>

          </ul> -->
          <?php print views_embed_view("social_icons_home", "block");//social icons?>
        </div>
        <!-- <a class="pull-right pstc_logo" title="PSTC" href="<?php print $front_page; ?>pg/pressure-sensitive-tape-council-responsible-tape-manufacturer">PSTC</a> -->
        <?php print views_embed_view("social_icons_home", "block_1");//footer logos?>
      </div>
    </div>
    <div class="fixed_wrap clearfix" id="copyline">&copy; 2006-<?= date('Y')?> Shurtape
      Technologies, LLC
    </div>
  </div>
  <div class="mobile" id="mobile_footer">
    <div class="logo_bar">
      <a title="Home" id="logo" href="/">
        <img alt="Home" src="/sites/all/themes/shurtape/images/logo_footer_mobile_left.png">
      </a>
      <a title="Questions/Orders? Call Us: 888-442-8273" id="logo_cta" href="tel:888-442-8273">
        <img alt="Questions/Orders? Call Us: 888-442-8273" src="/sites/all/themes/shurtape/images/logo_footer_mobile_right.png">
      </a>
    </div>
    <ul class="footer_links unstyled">
      <li>
        <a title="Products" class="btn" href="/products">Products</a>
      </li>
      <li>
        <a title="Resources" class="btn" href="/resources/media">Resources</a>
      </li>
      <li>
        <a title="About" class="btn" href="/about/profile">About</a>
      </li>
      <li>
        <a title="Contact" class="btn" href="/contact-us">Contact Us</a>
      </li>
      <li>
        <a title="Where to Buy" class="btn btn-orange" href="/where-to-buy">Where
          to Buy</a>
      </li>
    </ul>
    <h4>Follow Us</h4>
    <?php print views_embed_view("social_icons_home", "block");//social icons?>
    <br clear="all"/>

    <div class="back_to_top">
      <a href="#">
        <img alt="Back to Top" src="/sites/all/themes/shurtape/images/btn_mobile_scroll_top.png">
      </a>
    </div>
  </div>
  <?php print $page_bottom; ?>
  <script type="text/javascript" src="<?php print base_path() . drupal_get_path('theme', 'shurtape'); ?>/js/jquery.placeholder.min.js"></script>
  <script type="text/javascript" src="<?php print base_path() . drupal_get_path('theme', 'shurtape'); ?>/js/jquery.fitvids.js"></script>
  <script type="text/javascript" src="<?php print base_path() . drupal_get_path('theme', 'shurtape'); ?>/js/jquery.hammer-full.min.js"></script>
  <script type="text/javascript" src="<?php print base_path() . drupal_get_path('theme', 'shurtape'); ?>/js/main.js"></script>
  <script type="text/javascript" src="<?php print base_path() . drupal_get_path('theme', 'shurtape'); ?>/js/home.js"></script>
  <!-- TODO - call this only on home page -->
  <script type="text/javascript" src="<?php print base_path() . drupal_get_path('theme', 'shurtape'); ?>/js/products.js"></script>
  <!-- TODO - call this only on products pages - category / browse, detail, etc. -->
  <script type="text/javascript" src="<?php print base_path() . drupal_get_path('theme', 'shurtape'); ?>/js/search.js"></script>
  <!-- TODO - call this only on search / filter page -->
  <script type="text/javascript" src="<?php print base_path() . drupal_get_path('theme', 'shurtape'); ?>/js/filter_bar.js"></script>
  <!-- TODO - call this only on products pages - category / browse, detail, etc. -->
  <script type="text/javascript">
    /* define $ as jQuery just in case */
    (function ($) {
      /* doc ready */
      $(function () {
        /* make youtube videos responsive */
        $('#video_modal .modal-body').fitVids();
        $('#media_box').fitVids();
      });
    })(jQuery);
  </script>
</body>
</html>
