<?php
/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * - $title : The title of this group of rows.  May be empty.
 * - $options['type'] will either be ul or ol.
 * @ingroup views_templates
 */
if (variable_get("product_catalog_pdf")) {
    $pdf = file_load(variable_get("product_catalog_pdf"));
    $pdf_url = file_create_url($pdf->uri);
}
if (variable_get("product_catalog_thumbnail")) {
    $catalog_thumb_image = file_load(variable_get("product_catalog_thumbnail"));
    $catalog_thumb_image_url = file_create_url($catalog_thumb_image->uri);
}
?>
<div class="group">
    <h3><a class="control" href="#"><?php echo!empty($title) ? $title : ''; ?></a></h3>

    <div class="box">
        <ul>
            <li>
                <span class="thumb">      
                    <img src="<?php print $catalog_thumb_image_url; ?>" alt="" />

                </span>

                <span class="content">
                    <span class="title">Product Catalog*</span>
                    <span class="link"><a target="_blank" href="<?php echo $pdf_url; ?>">Download PDF</a></span>
                </span>
            </li>
            <?php foreach ($rows as $id => $row): ?>
                <li>
                    <?php print $row; ?>
                </li>
            <?php endforeach; ?>
        </ul>
    </div>
</div>