<?php

/**
 * @file
 * Template to display a view as a table.
 *
 * - $title : The title of this group of rows.  May be empty.
 * - $header: An array of header labels keyed by field id.
 * - $caption: The caption for this table. May be empty.
 * - $header_classes: An array of header classes keyed by field id.
 * - $fields: An array of CSS IDs to use for each field id.
 * - $classes: A class or classes to apply to the table, based on settings.
 * - $row_classes: An array of classes to apply to each row, indexed by row
 *   number. This matches the index in $rows.
 * - $rows: An array of row items. Each row is an array of content.
 *   $rows are keyed by row number, fields within rows are keyed by field ID.
 * - $field_classes: An array of classes to apply to each field, indexed by
 *   field id, then row number. This matches the index in $rows.
 * @ingroup views_templates
 * Adhesion to Steel M/in, Tensile LB/IN, Adhesive, Backing
 */

for ($b = 0; $b < count($rows); $b++):

	/* Setup The Title */
	$data[$b]['title'] 	= $rows[$b]['title'];
	$data[$b]['nid'] 	= $rows[$b]['nid'];
	if (isset($image)) 
	{
		unset($image);
	}
	$image = (isset($rows[$b]['field_product_images'])) ? explode(",", $rows[$b]['field_product_images']) : NULL;
	if ($image[0] != NULL ) 
	{
		$load_entity = entity_load('node', array($image[0]));
		$image = theme(
			'image_style',
			array(
				'style_name' => 'search-technical-results',
				'path' => $load_entity[$image[0]]->field_product_image['und'][0]['uri']
			)
		);
	} 
	else 
	{
		$image = "<img src='https://twshurtape.s3.amazonaws.com/default_images/no-image_0.gif'>";
	}

	$data[$b]['image'] = $image;
	/* Setup Array For 0 */
	$data[$b]['data'][375]['metric'] = '-';
	$data[$b]['data'][375]['standard'] = '-';
	$data[$b]['data'][394]['metric'] = '-';
	$data[$b]['data'][394]['standard'] = '-';
	$data[$b]['data'][382]['metric'] = '-';
	$data[$b]['data'][382]['standard'] = '-';
	$data[$b]['data'][399]['metric'] = '-';
	$data[$b]['data'][399]['standard'] = '-';
	$data[$b]['data'][393]['metric'] = '-';
	$data[$b]['data'][393]['standard'] = '-';
	$data[$b]['data'][392]['metric'] = '-';
	$data[$b]['data'][392]['standard'] = '-';
	$data[$b]['data'][377]['metric'] = '-';
	$data[$b]['data'][377]['standard'] = '-';
	$data[$b]['data'][376]['metric'] = '-';
	$data[$b]['data'][376]['standard'] = '-';
	$data[$b]['data'][400]['metric'] = '-';
	$data[$b]['data'][400]['standard'] = '-';
	$data[$b]['data'][401]['metric'] = '-';
	$data[$b]['data'][401]['standard'] = '-';
	$data[$b]['data'][370]['metric'] = '-';
	$data[$b]['data'][370]['standard'] = '-';
	$data[$b]['data'][385]['metric'] = '-';
	$data[$b]['data'][385]['standard'] = '-';
	$data[$b]['data']['adhesive'] = '-';

	/* adhesive */
	$data[$b]['data']['adhesive'] = $rows[$b]['field_product_adhesive'];
	/* backing */
	$data[$b]['data']['backing'] = $rows[$b]['field_product_backing'];

	if (isset($physical_properties)) 
	{
		unset($physical_properties);
	}

	preg_match_all('!\d+!', $rows[$b]['field_physical_properties'], $matches);
	$physical_properties = explode(",", implode(',', $matches[0]));

	if (isset($load_entity)) 
	{
		unset($load_entity);
	}

	$load_entity = entity_load('field_collection_item', $physical_properties);
	foreach ($load_entity as $key => $value) :
		$current_tid = $value->field_property_type['und'][0]['tid'];
		$metric = (isset($value->field_property_metric['und'])) ? $value->field_property_metric['und'][0]['value'] : 0;
		$standard = (isset($value->field_property_standard['und'])) ? $value->field_property_standard['und'][0]['value'] : 0;
		$data[$b]['data'][$current_tid]['metric'] = $metric;
		$data[$b]['data'][$current_tid]['standard'] = $standard;
	endforeach;
endfor;
?>

<p class="drag-drop-note desktop">*Set the order of the grid columns by dragging and dropping.</p>
<div class="tech-table-wrapper-outer desktop">
	<span class="scroll-more">
		<a class="scroll-table" href="#" data-direction="left"><<</a> Scroll for more <a class="scroll-table" href="#" data-direction="right">>></a>
	</span>
	<div class="tech-table-wrapper">
		<script type="text/javascript">
			$(function(){
				var table_order = "";
				<?php 
				$json_order="";
				if (isset($_SESSION["shurtape"]["dragtable_order"]))
				{
					$json_order = ",restoreState: ".drupal_json_encode($_SESSION["shurtape"]["dragtable_order"]["order"]);
				}
				?>
				$('table.tech-view').dragtable({dragaccept:'.accept',persistState:'/ajax/dragtable_store_column_order'<?php print $json_order;?>});
			});
		</script>
		<table <?php if ($classes): print 'class="' . $classes . ' tech-view" '; endif; ?><?php print $attributes; ?>>
			<?php if (!empty($title) || !empty($caption)) : ?>
				<caption><?php print $caption . $title; ?></caption>
			<?php endif; ?>
			<thead>
				<tr>
					<th class="first-col">PRODUCT<span class="desktop"> NAME</span></th>
					<th class="accept" id="tensile">TENSILE STRENGTH</th>
					<th class="accept" id="adhesion">ADHESION TO STEEL</th>
					<th class="accept" id="adhesive">ADHESIVE</th>
					<th class="accept" id="backing">BACKING</th>
					<th class="accept" id="serv_temp_range">SERV. TEMP RANGE</th>
					<th class="accept" id="app_temp_range">APPLICAT. TEMP RANGE</th>
					<th class="accept" id="elongation">ELONGATION</th>
					<th class="accept" id="thickness">THICKNESS</th>
					<th class="accept" id="thickness_liner">THICKNESS WITH LINER</th>
					<th class="accept" id="thickness_no_liner">THICKNESS WITHOUT LINER</th>
					<th class="accept" id="holding_power"><span class="desktop">HOLDING POWER TO FIBERBOARD</span><span class="mobile">HOLDING POWER<br />TO FIBERBOARD</span></th>
				</tr>
			</thead>
			<tbody>
				<tr class="units">
					<td class="first-col units">&nbsp;</td>
					<td>
						<span class="standard">(lbs./in)</span>
						<span class="metric">(N/10 mm)</span>
					</td>
					<td>
						<span class="standard">(oz./in)</span>
						<span class="metric">(N/10 mm)</span>
					</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>
						<span class="standard">(&deg;F)</span>
						<span class="metric">(&deg;C)</span>
					</td>
					<td>
						<span class="standard">(&deg;F)</span>
						<span class="metric">(&deg;C)</span>
					</td>
					<td>
						<span>(%)</span>
					</td>
					<td>
						<span class="standard">(mils)</span>
						<span class="metric">(mm)</span>
					</td>
					<td>
						<span class="standard">(mils)</span>
						<span class="metric">(mm)</span>
					</td>
					<td>
						<span class="standard">(mils)</span>
						<span class="metric">(mm)</span>
					</td>
					<td>
						<span>(min)</span>
					</td>
				</tr>
				<tr class="spacer">
			    	<td class="first-col spacer">&nbsp;</td>
				    <td>&nbsp;</td>
				    <td>&nbsp;</td>
				    <td>&nbsp;</td>
				    <td>&nbsp;</td>
				    <td>&nbsp;</td>
				    <td>&nbsp;</td>
				    <td>&nbsp;</td>
				    <td>&nbsp;</td>
				    <td>&nbsp;</td>
				    <td>&nbsp;</td>
				    <td>&nbsp;</td>
			    </tr>
				<?php for ($i = 0; $i < count($data); $i++): 
					$cr = $data[$i]; ?>
					<tr>
						<td class="product-name first-col">
							<a href="<?php print base_path(); ?>node/<?php print $cr['nid']; ?>"><?php print $cr['image']; ?></a><?php print $cr['title']; ?>
						</td>
						<?php /* TENSILE STRENGTH */ ?>
						<td>
							<span class="standard">
								<?php print $cr['data'][394]['standard']; ?><br />
								<a class="convert-metric" href="#">Convert<br />to Metric</a>
							</span>
							<span class="metric">
								<?php print $cr['data'][394]['metric']; ?><br />
								<a class="convert-standard" href="#">Convert<br />to Standard</a>
							</span>
						</td>
						<?php /* ADHESION TO STEEL */ ?>
						<td>
							<span class="standard">
								<?php print $cr['data'][375]['standard']; ?><br />
								<a class="convert-metric" href="#">Convert<br />to Metric</a>
							</span>
							<span class="metric">
								<?php print $cr['data'][375]['metric']; ?><br />
								<a class="convert-standard" href="#">Convert<br />to Standard</a>
							</span>
						</td>
						<?php /* ADHESIVE */ ?>
						<td>
							<?php print $cr['data']['adhesive']; ?>
						</td>
						<?php /* BACKING */ ?>
						<td>
							<?php print $cr['data']['backing']; ?>
						</td>
						<?php /* SERVICE TEMAPTURE RANGE */ ?>
						<td>
							<span class="standard">
								<?php print $cr['data'][393]['standard']; ?> - <?php print $cr['data'][392]['standard']; ?><br />
								<a class="convert-metric" href="#">Convert<br />to Metric</a>
							</span>
							<span class="metric">
								<?php print $cr['data'][393]['metric']; ?> - <?php print $cr['data'][392]['metric']; ?><br />
								<a class="convert-standard" href="#">Convert<br />to Standard</a>
							</span>
						</td>
						<?php /* APPLICATION TEMPATURE RANGE */ ?>
						<td>
							<span class="standard">
								<?php print $cr['data'][377]['standard']; ?> - <?php print $cr['data'][376]['standard']; ?><br />
								<a class="convert-metric" href="#">Convert<br />to Metric</a>
							</span>
							<span class="metric">
								<?php print $cr['data'][377]['metric']; ?> - <?php print $cr['data'][376]['metric']; ?><br />
								<a class="convert-standard" href="#">Convert<br />to Standard</a>
							</span>
						</td>
						<?php /* ELONGATION */ ?>
						<td>
							<span class="standard">
								<?php print $cr['data'][382]['standard']; ?><br />
								<a class="convert-metric" href="#">Convert<br />to Metric</a>
							</span>
							<span class="metric">
								<?php print $cr['data'][382]['metric']; ?><br />
								<a class="convert-standard" href="#">Convert<br />to Standard</a>
							</span>
						</td>
						<?php /* THICKNESS */ ?>
						<td>
							<span class="standard">
								<?php print $cr['data'][399]['standard']; ?><br />
								<a class="convert-metric" href="#">Convert<br />to Metric</a>
							</span>
							<span class="metric">
								<?php print $cr['data'][399]['metric']; ?><br />
								<a class="convert-standard" href="#">Convert<br />to Standard</a>
							</span>
						</td>
						<?php /* THICKNESS WITH LINER */ ?>
						<td>
							<span class="standard">
								<?php print $cr['data'][400]['standard']; ?><br />
								<a class="convert-metric" href="#">Convert<br />to Metric</a>
							</span>
							<span class="metric">
								<?php print $cr['data'][400]['metric']; ?><br />
								<a class="convert-standard" href="#">Convert<br />to Standard</a>
							</span>
						</td>
						<?php /* THICKNESS WITHOUT LINER */ ?>
						<td>
							<span class="standard">
								<?php print $cr['data'][401]['standard']; ?><br />
								<a class="convert-metric" href="#">Convert<br />to Metric</a>
							</span>
							<span class="metric">
								<?php print $cr['data'][401]['metric']; ?><br />
								<a class="convert-standard" href="#">Convert<br />to Standard</a>
							</span>
						</td>
						<?php /* HOLDING POWER */ ?>
						<td>
							<span class="standard">
								<?php print $cr['data'][385]['standard']; ?><br />
								<a class="convert-metric" href="#">Convert<br />to Metric</a>
							</span>
							<span class="metric">
								<?php print $cr['data'][385]['metric']; ?><br />
								<a class="convert-standard" href="#">Convert<br />to Standard</a>
							</span>
						</td>
					</tr>
				<?php endfor; ?>
			</tbody>
		</table>
	</div>
	<span class="scroll-more-bottom">
		<a class="scroll-table" href="#" data-direction="left"><<</a> Scroll for more <a class="scroll-table" href="#" data-direction="right">>></a>
	</span>
</div>

<div class="mobile tech_view">
	<input type="hidden" id="tech_nav_active" value="1" />
	<div class="convert_bar">
		<div class="button_bar">
			<div class="standard">
				<a href="#" class="btn btn-orange convert-metric">Convert to Metric</a>
			</div>
			<div class="metric">
				<a href="#" class="btn btn-orange convert-standard">Convert to Standard</a>
			</div>
		</div>
		<span class="tap_for_more">Tap and drag for more.</span>
	</div>
	<div class="nav_bar blue_bar">
		<span class="product_heading">Product</span>
		<div class="navigator">
			<a href="#" class="control prev"></a>
			<div class="headings">
				<span class="hdg hdg_1 active">Tensile Strength</span>
				<span class="hdg hdg_2">Adhesion to Steel</span>
				<span class="hdg hdg_3">Adhesive</span>
				<span class="hdg hdg_4">Backing</span>
				<span class="hdg hdg_5">Serv. Temp Range</span>
				<span class="hdg hdg_6">Applic. Temp Range</span>
				<span class="hdg hdg_7">Elongation</span>
				<span class="hdg hdg_8">Thickness</span>
				<span class="hdg hdg_9">Thickness with Liner</span>
				<span class="hdg hdg_10">Thickness w/o Liner</span>
				<span class="hdg hdg_11"><span class="desktop">HOLDING POWER TO FIBERBOARD</span><span class="mobile">HOLDING POWER<br />TO FIBERBOARD</span></span>
			</div>
			<a href="#" class="control next"></a>
		</div>
	</div>
	<div class="subheadings">
		<span class="sub sub_1 active">
			<span class="standard">(lbs./in)</span>
			<span class="metric">(N/10 mm)</span>
		</span>
		<span class="sub sub_2">
			<span class="standard">(oz./in)</span>
			<span class="metric">(N/10 mm)</span>
		</span>
		<span class="sub sub_3">&nbsp;</span>
		<span class="sub sub_4">&nbsp;</span>
		<span class="sub sub_5">
			<span class="standard">(&deg;F)</span>
			<span class="metric">(&deg;C)</span>
		</span>
		<span class="sub sub_6">
			<span class="standard">(&deg;F)</span>
			<span class="metric">(&deg;C)</span>
		</span>
		<span class="sub sub_7">
			<span>(%)</span>
		</span>
		<span class="sub sub_8">
			<span class="standard">(mils)</span>
			<span class="metric">(mm)</span>
		</span>
		<span class="sub sub_9">
			<span class="standard">(mils)</span>
			<span class="metric">(mm)</span>
		</span>
		<span class="sub sub_10">
			<span class="standard">(mils)</span>
			<span class="metric">(mm)</span>
		</span>
		<span class="sub sub_11">
			<span>(min)</span>
		</span>
	</div>
	<div class="results">
		<?php for ($i = 0; $i < count($data); $i++): 
			$cr = $data[$i]; ?>
			<div class="item">
				<span class="mobile_link_icon mobile">
					<img src="<?php print base_path() . drupal_get_path('theme', 'shurtape'); ?>/images/icon_mobile_link_arrow.png" alt="">
				</span>
				<div class="item_image">
					<a href="<?php print base_path(); ?>node/<?php print $cr['nid']; ?>">
						<?php print $cr['image']; ?>
					</a>
					<?php print $cr['title']; ?>
				</div>
				<div class="item_content">
					<div class="data_slide">
						<?php /* TENSILE STRENGTH */ ?>
						<span class="data_item item_1 active">
							<span class="standard">
								<?php print $cr['data'][394]['standard']; ?>
							</span>
							<span class="metric">
								<?php print $cr['data'][394]['metric']; ?>
							</span>
						</span>
						<?php /* ADHESION TO STEEL */ ?>
						<span class="data_item item_2">
							<span class="standard">
								<?php print $cr['data'][375]['standard']; ?>
							</span>
							<span class="metric">
								<?php print $cr['data'][375]['metric']; ?>
							</span>
						</span>
						<?php /* ADHESIVE */ ?>
						<span class="data_item item_3">
							<?php print $cr['data']['adhesive']; ?>
						</span>
						<?php /* BACKING */ ?>
						<span class="data_item item_4">
							<?php print $cr['data']['backing']; ?>
						</span>
						<?php /* SERVICE TEMAPTURE RANGE */ ?>
						<span class="data_item item_5">
							<span class="standard">
								<?php print $cr['data'][393]['standard']; ?> - <?php print $cr['data'][392]['standard']; ?>
							</span>
							<span class="metric">
								<?php print $cr['data'][393]['metric']; ?> - <?php print $cr['data'][392]['metric']; ?>
							</span>
						</span>
						<?php /* APPLICATION TEMPATURE RANGE */ ?>
						<span class="data_item item_6">
							<span class="standard">
								<?php print $cr['data'][377]['standard']; ?> - <?php print $cr['data'][376]['standard']; ?>
							</span>
							<span class="metric">
								<?php print $cr['data'][377]['metric']; ?> - <?php print $cr['data'][376]['metric']; ?>
							</span>
						</span>
						<?php /* ELONGATION */ ?>
						<span class="data_item item_7">
							<span class="standard">
								<?php print $cr['data'][382]['standard']; ?>
							</span>
							<span class="metric">
								<?php print $cr['data'][382]['metric']; ?>
							</span>
						</span>
						<?php /* THICKNESS */ ?>
						<span class="data_item item_8">
							<span class="standard">
								<?php print $cr['data'][399]['standard']; ?>
							</span>
							<span class="metric">
								<?php print $cr['data'][399]['metric']; ?>
							</span>
						</span>
						<?php /* THICKNESS WITH LINER */ ?>
						<span class="data_item item_9">
							<span class="standard">
								<?php print $cr['data'][400]['standard']; ?>
							</span>
							<span class="metric">
								<?php print $cr['data'][400]['metric']; ?>
							</span>
						</span>
						<?php /* THICKNESS WITHOUT LINER */ ?>
						<span class="data_item item_10">
							<span class="standard">
								<?php print $cr['data'][401]['standard']; ?>
							</span>
							<span class="metric">
								<?php print $cr['data'][401]['metric']; ?>
							</span>
						</span>
						<?php /* HOLDING POWER */ ?>
						<span class="data_item item_11">
							<span class="standard">
								<?php print $cr['data'][385]['standard']; ?>
							</span>
							<span class="metric">
								<?php print $cr['data'][385]['metric']; ?>
							</span>
						</span>
					</div>
				</div>
			</div>
		<?php endfor; ?>
	</div>
	<div class="subheadings">
		<span class="sub sub_1 active">
			<span class="standard">(lbs./in)</span>
			<span class="metric">(N/10 mm)</span>
		</span>
		<span class="sub sub_2">
			<span class="standard">(oz./in)</span>
			<span class="metric">(N/10 mm)</span>
		</span>
		<span class="sub sub_3">&nbsp;</span>
		<span class="sub sub_4">&nbsp;</span>
		<span class="sub sub_5">
			<span class="standard">(&deg;F)</span>
			<span class="metric">(&deg;C)</span>
		</span>
		<span class="sub sub_6">
			<span class="standard">(&deg;F)</span>
			<span class="metric">(&deg;C)</span>
		</span>
		<span class="sub sub_7">
			<span>(%)</span>
		</span>
		<span class="sub sub_8">
			<span class="standard">(mils)</span>
			<span class="metric">(mm)</span>
		</span>
		<span class="sub sub_9">
			<span class="standard">(mils)</span>
			<span class="metric">(mm)</span>
		</span>
		<span class="sub sub_10">
			<span class="standard">(mils)</span>
			<span class="metric">(mm)</span>
		</span>
		<span class="sub sub_11">
			<span>(min)</span>
		</span>
	</div>
	<div class="nav_bar blue_bar">
		<span class="product_heading">Product</span>
		<div class="navigator">
			<a href="#" class="control prev"></a>
			<div class="headings">
				<span class="hdg hdg_1 active">Tensile Strength</span>
				<span class="hdg hdg_2">Adhesion to Steel</span>
				<span class="hdg hdg_3">Adhesive</span>
				<span class="hdg hdg_4">Backing</span>
				<span class="hdg hdg_5">Serv. Temp Range</span>
				<span class="hdg hdg_6">Applic. Temp Range</span>
				<span class="hdg hdg_7">Elongation</span>
				<span class="hdg hdg_8">Thickness</span>
				<span class="hdg hdg_9">Thickness with Liner</span>
				<span class="hdg hdg_10">Thickness w/o Liner</span>
				<span class="hdg hdg_11"><span class="desktop">HOLDING POWER TO FIBERBOARD</span><span class="mobile">HOLDING POWER<br />TO FIBERBOARD</span></span>
			</div>
			<a href="#" class="control next"></a>
		</div>
	</div>
	<div class="convert_bar bottom">
		<span class="tap_for_more">Tap and drag for more.</span>
		<div class="button_bar">
			<div class="standard">
				<a href="#" class="btn btn-orange convert-metric">Convert to Metric</a>
			</div>
			<div class="metric">
				<a href="#" class="btn btn-orange convert-standard">Convert to Standard</a>
			</div>
		</div>
	</div>
</div>
