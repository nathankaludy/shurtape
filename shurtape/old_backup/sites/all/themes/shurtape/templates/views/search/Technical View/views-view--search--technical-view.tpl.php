<?php

/**
 * @file
 * Main view template.
 *
 * Variables available:
 * - $classes_array: An array of classes determined in
 *   template_preprocess_views_view(). Default classes are:
 *     .view
 *     .view-[css_name]
 *     .view-id-[view_name]
 *     .view-display-id-[display_name]
 *     .view-dom-id-[dom_id]
 * - $classes: A string version of $classes_array for use in the class attribute
 * - $css_name: A css-safe version of the view name.
 * - $css_class: The user-specified classes names, if any
 * - $header: The view header
 * - $footer: The view footer
 * - $rows: The results of the view query, if any
 * - $empty: The empty text to display if the view is empty
 * - $pager: The pager next/prev links to display, if any
 * - $exposed: Exposed widget form/info to display
 * - $feed_icon: Feed icon to display, if any
 * - $more: A link to view more, if any
 *
 * @ingroup views_templates
 */
?>
<div id="search" class="<?php print $classes; ?> technical-view">
	<?php print render($title_prefix); ?>
	<?php if ($title): ?>
		<?php print $title; ?>
	<?php endif; ?>
	<?php print render($title_suffix); ?>
	<?php if ($header): ?>
		<div class="view-header">
			<?php print $header; ?>
		</div>
	<?php endif; ?>

	<?php if ($attachment_before): ?>
		<div class="attachment attachment-before">
			<?php print $attachment_before; ?>
		</div>
	<?php endif; ?>
	
	<!--
	<div class="mobile_breadcrumbs mobile">
		<span class="you_are_here">You Are Here:</span>
		<span class="breadcrumbs">
			Search
		</span>
	</div>
	-->
	
	<h1 class="headline">
		<?php print $result_title;
		if($result_title == ''):
			print "Product Results";
		endif;
		?>
	</h1>
	<div class="blue_bar">
		<ul class="filter_tags">
			<?php 
			print $filter_tag_output; 
			if($filter_tag_output == ''):
				print 'No filters applied';
			endif;
			?>
		</ul>
	</div>
	<div id="top_filter_nav">
		<div class="clearfix">
			<ul class="tab_controls pull-left">
				<li>
					<a id="normal-view" href="<?php print $link_results;?>" class="tab_control">Normal View</a>
				</li>
				<li>
					<a id="tech-view-tab" href="<?php print base_path(); ?>search/products/technical" class="tab_control current">Technical View</a>
				</li>
			</ul>
			<?php if ($exposed): ?>
				<div class="view-filters">
					<?php print $exposed; ?>
				</div>
			<?php endif; ?>
		</div>
	</div>
	<div id="product_grid">

		<div class="clearfix compare-paginate">
			<?php if ($pager): ?>
				<?php print $pager; ?>
			<?php endif; ?>
		</div>

		<?php if ($rows): ?>
			<div class="view-content">
				<?php print $rows; ?>
			</div>
		<?php elseif ($empty): ?>
			<div class="view-empty">
				<?php print $empty; ?>
			</div>
		<?php endif; ?>
		
		<div class="mobile">
			<div class="clearfix compare-paginate">
				<?php if ($pager): ?>
					<?php print $pager; ?>
				<?php endif; ?>
			</div>
		</div>

	</div>
	
	<div class="desktop">
		<?php if ($pager): ?>
			<?php print $pager; ?>
		<?php endif; ?>
	</div>

	<?php if ($attachment_after): ?>
		<div class="attachment attachment-after">
			<?php print $attachment_after; ?>
		</div>
	<?php endif; ?>

	<?php if ($more): ?>
		<?php print $more; ?>
	<?php endif; ?>

	<?php if ($footer): ?>
		<div class="view-footer">
			<?php print $footer; ?>
		</div>
	<?php endif; ?>

	<?php if ($feed_icon): ?>
		<div class="feed-icon">
			<?php print $feed_icon; ?>
		</div>
	<?php endif; ?>

</div>