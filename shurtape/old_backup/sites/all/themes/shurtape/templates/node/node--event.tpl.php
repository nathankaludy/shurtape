<?php 
/** 
 * DO NOT move the headline etc. back to the page template 
 * - this is necessary for the reordering of elements in the mobile design 
 */ 
?>
<h1 class="headline">Events</h1>

<?php if ($page['content_top']): ?>
	<div id="content_top">
		<?php print render($page['content_top']); ?>
	</div>
<?php endif; ?>

<?php if (isset($page['help'])): ?>
	<?php print render($page['help']); ?>
<?php endif; ?>

<?php if (isset($action_links)): ?>
	<ul class="action-links">
		<?php print render($action_links); ?>
	</ul>
<?php endif; ?>
		
<div class="blue_bar desktop">&nbsp;</div>

<div class="news_detail_wrap">

	<?php if ($title): ?>
	<h2 class="news_detail_title"><?php echo $title; ?></h1>
	<?php endif; ?>

	<?php if (!empty($content['field_time_date'])): ?>
		<?php $content['field_time_date']['#label_display'] = 'hidden'; ?>
		<h3 class="news_detail_subtitle"><?php print render($content['field_time_date']); ?></h3>
	<?php endif; ?>

	<div class="news_detail_content clearfix">
		<?php if (isset($content['field_event_image'])): ?>
		<div class="news_detail_photo pull-left">
			<?php $content['field_event_image']['#label_display'] = 'hidden'; ?>
			<?php print render($content['field_event_image']); ?>
		</div>
		<?php endif; ?>
		<div <?php if (isset($content['field_event_image'])): ?>class="news_detail_body pull-right"<?php endif; ?> >
		<?php print render($content['field_booth']); ?>
		<?php print render($content['field_location']); ?>
		
		<?php print render($content['body']); ?>
	    </div>
	</div>
</div>