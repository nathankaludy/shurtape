<div class="modal fade" id="video_modal" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
			</div>
			<div class="modal-body">
				<!-- loaded dynamically via JS below -->
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
/* define $ as jQuery just in case */
( function( $ ){

	/* doc ready */
	$( function( ){
	
		/* init the thumb slider */
		$( '#thumb_slider' ).thumb_slider( );

		/* init the tabs */
		$( '#tab_section' ).tabs( );

		/* show the video modal */
		$( '.open_video_modal' ).click( function( e ) 
		{
		
			/* set vars */
			var vid = $( this ).attr( 'rel' );
			$( '#video_modal .modal-body' ).hide( ).html( '<iframe width="620" height="349" src="//www.youtube.com/embed/' + vid + '" frameborder="0" allowfullscreen></iframe>' ).show( );
			$( '#video_modal' ).modal( );
			e.preventDefault( );
		});

		/* shut down video on close trigger */
		$( '#video_modal' ).on( 'hidden.bs.modal', function ( )
		{
			$( '#video_modal .modal-body' ).html( '' );
		});
		
		/* set the download attributes */
		/*
		download_atts( );
		function download_atts( )
		{
			var links = $( '#accordian .box a, #brand_assets a' );
			if ( links.length > 0 )
			{
				$.each( links, function( key, value ) 
				{
					var href 	= $( value ).attr( 'href' );
					var index 	= href.lastIndexOf( '/' ) + 1;
					var file	= href.substr( index );
					$( links[key] ).attr( 'download', file );
					console.log( file );
				});
			}
		}
		*/
		
		/* force download for media PDFs and images */
		/*
		$( '.download_file' ).click( function( e )
		{
			var href 	= $( this ).attr( 'href' );
			var index 	= href.lastIndexOf( '/' ) + 1;
			var file	= href.substr( index );
			temp_window = window.open( href, '', 'left=10000,screenX=10000' );
			temp_window.document.execCommand( 'SaveAs', 'null', file );
			temp_window.close( );
			e.preventDefault( );
		});
		*/
		
	});
})( jQuery );
</script>
