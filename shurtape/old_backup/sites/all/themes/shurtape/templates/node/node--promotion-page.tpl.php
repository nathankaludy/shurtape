<?php
/*
$title: the (sanitized) title of the node.
$content: An array of node items. Use render($content) to print them all, or print a subset such as render($content['field_example']). Use hide($content['field_example']) to temporarily suppress the printing of a given element.
$user_picture: The node author's picture from user-picture.tpl.php.
$date: Formatted creation date. Preprocess functions can reformat it by calling format_date() with the desired parameters on the $created variable.
$name: Themed username of node author output from theme_username().
$node_url: Direct URL of the current node.
$display_submitted: Whether submission information should be displayed.
$submitted: Submission information created from $name and $date during template_preprocess_node().
$classes: String of classes that can be used to style contextually through CSS. It can be manipulated through the variable $classes_array from preprocess functions. The default values can be one or more of the following:

node: The current template type; for example, "theming hook".
node-[type]: The current node type. For example, if the node is a "Blog entry" it would result in "node-blog". Note that the machine name will often be in a short form of the human readable label.
node-teaser: Nodes in teaser form.
node-preview: Nodes in preview mode.

The following are controlled through the node publishing options.
node-promoted: Nodes promoted to the front page.
node-sticky: Nodes ordered above other non-sticky nodes in teaser listings.
node-unpublished: Unpublished nodes visible only to administrators.
$title_prefix (array): An array containing additional output populated by modules, intended to be displayed in front of the main title tag that appears in the template.
$title_suffix (array): An array containing additional output populated by modules, intended to be displayed after the main title tag that appears in the template.

Other variables:

$node: Full node object. Contains data that may not be safe.
$type: Node type; for example, story, page, blog, etc.
$comment_count: Number of comments attached to the node.
$uid: User ID of the node author.
$created: Time the node was published formatted in Unix timestamp.
$classes_array: Array of html class attribute values. It is flattened into a string within the variable $classes.
$zebra: Outputs either "even" or "odd". Useful for zebra striping in teaser listings.
$id: Position of the node. Increments each time it's output.

Node status variables:

$view_mode: View mode; for example, "full", "teaser".
$teaser: Flag for the teaser state (shortcut for $view_mode == 'teaser').
$page: Flag for the full page state.
$promote: Flag for front page promotion state.
$sticky: Flags for sticky post setting.
$status: Flag for published status.
$comment: State of comment settings for the node.
$readmore: Flags true if the teaser content of the node cannot hold the main body content.
$is_front: Flags true when presented in the front page.
$logged_in: Flags true when the current user is a logged-in member.
$is_admin: Flags true when the current user is an administrator.
*/
?>

<?php

hide($content["field_promotion_header_img"]); 
hide($content["field_promotion_product_header"]);
hide($content["field_promotion_form_title"]);
hide($content["body"]);
hide($content["webform"]);
hide($content["field_promotion_logos"]);

$field_show_form = field_get_items('node', $node, 'field_promotion_show_form');
$show_form = $field_show_form[0]["value"];
?>

<div class="blue_bar promotion_product_header desktop">
	<?php print render($content["field_promotion_product_header"]); ?>
</div>

<?php if (isset($content["field_promotion_products"]) && isset($content["field_promotion_products"]["#items"])): ?>
	<ul class="product_list">
		<?php foreach ($content["field_promotion_products"]["#items"] as $product): ?>
			<li class="item">
				<?php
				$field_image 	= field_get_items('node', $product["node"], 'field_product_images');
				$image_id 		= $field_image[0]["nid"];
				$display 		= array('label' => 'hidden');
				if ($image_id != NULL): 
					$load_entity = entity_load('node', array($image_id)); 
					$image = theme('image_style', array('style_name' => 'search-product-results', 'path' => $load_entity[$image_id]->field_product_image['und'][0]['uri']));
				else: 
					$image = '<img src="http://twshurtape.s3.amazonaws.com/default_images/no-image_0.gif">';
				endif; ?>
				<span class="item_thumb">
					<a target="_blank" href="/node/<?php print $product['nid']; ?>">
						<?php print $image; ?>
					</a>
				</span>
				<span class="item_title">
					<a target="_blank" href="/node/<?php print $product["nid"]; ?>">
						<?php print $product["node"]->title; ?>
					</a>
				</span>
				<span class="item_desc">
					<?php print render(field_view_field('node', $product["node"], 'field_product_short_description', $display)); ?>
				</span>
			</li>
		<?php endforeach; ?>
	</ul>
<?php endif; ?>
	
<div class="padded_body">

	<?php print render($content["body"]); ?> 
</div>

<div class="promotion_form">
	<div class="form_header">
		<div class="form_title">
			<?php if($show_form == "1"): ?>
				<?php print render($content["field_promotion_form_title"]); ?> 
			<?php endif; ?>
		</div>
		<div class="logo_box">
			<?php print render($content["field_promotion_logos"]); ?>
		</div>
	</div>
	<div class="form_box">
		<?php if($show_form == "1"): ?> 
			<?php print render($content["webform"]); ?>
		<?php endif; ?>
	</div>
</div>