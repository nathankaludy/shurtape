<!--
<div class="mobile_breadcrumbs mobile">
	<span class="you_are_here">You Are Here:</span>
	<span class="breadcrumbs">
		<a href="<?php print base_path(); ?>">Home</a> / 
		<a href="<?php print base_path(); ?>resources/media">Resources</a> / 
		<?php echo $title; ?>
	</span>
</div>
-->

<h1 class="headline">
	<?php if ($title): ?>
		<?php echo $title; ?>
	<?php endif; ?>
</h1>

<?php if ($page['content_top']): ?>
	<div id="content_top">
		<?php print render($page['content_top']); ?>
	</div>
<?php endif; ?>

<?php if (isset($page['help'])): ?>
	<?php print render($page['help']); ?>
<?php endif; ?>

<?php if (isset($action_links)): ?>
	<ul class="action-links">
		<?php print render($action_links); ?>
	</ul>
<?php endif; ?>

<div class="blue_bar">NEWS</div>

<?php if (isset($region['news_section'])): ?>
	<div class="news_section">
		<?php print render($region['news_section']); ?>
	</div>
<?php endif; ?>

<div class="blue_bar">EVENTS</div>
<?php if (isset($region['events_section'])): ?>
	<div class="events_section">
		<?php print render($region['events_section']); ?>
	</div>
<?php endif; ?>
<script type="text/javascript">
	/* define $ as jQuery just in case */
	( function( $ ){

		/* doc ready */
		$( function( )
		{
			/* initiate the news item list */
			init_news_list( );
			function init_news_list( )
			{
				/* set vars */
				var news_list	= $( '.view-news ul.link_list');
				var news_items 	= news_list.find( 'li' );
				var item_count	= news_items.length;
				var num_shown	= 12;
				var remaining	= parseInt( item_count - num_shown );
				var speed		= 200;
				
				/* insert the More News link if remainder is greater than 0 */
				if ( remaining > 0 )
				{
					$( '<div class="more_link_wrap"><a href="#" class="more_news_link">More News</a></div>' ).appendTo( news_list );
				}
				
				/* loop through the news items - hide all but the first N items */
				$.each( news_items, function( i, el ) 
				{ 
					if( i > parseInt( num_shown - 1 ) )
					{
						$( el ).hide( );
					}
				});
				
				/* more news click event */
				$( news_list ).on( 'click', 'a.more_news_link', function( e ) 
				{
					var hidden_items 	= news_list.find( 'li:hidden' );
					var hidden_count	= hidden_items.length;
					var show_count		= num_shown > hidden_count ? hidden_count : num_shown;
					if ( num_shown > hidden_count )
					{
						$( '.more_news_link' ).hide( );
					}
					for ( var x = 0; x < show_count; ++x )
					{
						$( hidden_items[x] ).slideDown( speed ).fadeIn( speed );
					}
					e.preventDefault( );
				});
			}
			
		});
	})( jQuery );
</script>