<div id="content">

	<?php print render( $page[ 'secondary_content' ] ); ?>

	<div id="main" class="fixed_wrap white_box clearfix">
	
		<?php print $messages; ?>
		
		<?php if ( !empty( $tabs['#primary'] )): ?>
			<div class="tabs-wrapper clearfix">
				<?php print render($tabs); ?>
			</div>
		<?php endif; ?>
		
		<?php if (theme_get_setting('breadcrumbs')): ?>
			<div class="mobile_breadcrumbs mobile">
				<?php if ($breadcrumb): 
					print '<span class="you_are_here">You Are Here:</span> ' . $breadcrumb; 
				endif;?>
			</div>
		<?php endif; ?>
		
		<h1 class="headline">
			<?php if ($title): ?>
				<?php echo $title; ?>
			<?php endif; ?>
		</h1>
		
		<?php if ($page['content_top']): ?>
			<div id="content_top"><?php print render($page['content_top']); ?></div>
		<?php endif; ?>
		
		<?php if (isset($page['help'])): ?>
			<?php print render($page['help']); ?>
		<?php endif; ?>
		<?php if (isset($action_links)): ?>
			<ul class="action-links"><?php print render($action_links); ?></ul>
		<?php endif; ?>
		
		<?php if (theme_get_setting('breadcrumbs')): ?>
			<div class="blue_bar desktop">
				<?php if ($breadcrumb): print $breadcrumb; endif;?>
			</div>
		<?php endif; ?>
		
		<?php print render( $page['content'] ); ?>

	</div>

</div>