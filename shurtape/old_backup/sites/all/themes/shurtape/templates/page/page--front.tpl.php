<?php
/**
 * @file
 * Default theme implementation to display a single Drupal page.
 *
 * Available variables:
 *
 * General utility variables:
 * - $base_path: The base URL path of the Drupal installation. At the very
 *   least, this will always default to /.
 * - $directory: The directory the template is located in, e.g. modules/system
 *   or themes/garland.
 * - $is_front: TRUE if the current page is the front page.
 * - $logged_in: TRUE if the user is registered and signed in.
 * - $is_admin: TRUE if the user has permission to access administration pages.
 *
 * Site identity:
 * - $front_page: The URL of the front page. Use this instead of $base_path,
 *   when linking to the front page. This includes the language domain or
 *   prefix.
 * - $logo: The path to the logo image, as defined in theme configuration.
 * - $site_name: The name of the site, empty when display has been disabled
 *   in theme settings.
 * - $site_slogan: The slogan of the site, empty when display has been disabled
 *   in theme settings.
 *
 * Navigation:
 * - $main_menu (array): An array containing the Main menu links for the
 *   site, if they have been configured.
 * - $secondary_menu (array): An array containing the Secondary menu links for
 *   the site, if they have been configured.
 * - $breadcrumb: The breadcrumb trail for the current page.
 *
 * Page content (in order of occurrence in the default page.tpl.php):
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title: The page title, for use in the actual HTML content.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 * - $messages: HTML for status and error messages. Should be displayed
 *   prominently.
 * - $tabs (array): Tabs linking to any sub-pages beneath the current page
 *   (e.g., the view and edit tabs when displaying a node).
 * - $action_links (array): Actions local to the page, such as 'Add menu' on the
 *   menu administration interface.
 * - $feed_icons: A string of all feed icons for the current page.
 * - $node: The node object, if there is an automatically-loaded node
 *   associated with the page, and the node ID is the second argument
 *   in the page's path (e.g. node/12345 and node/12345/revisions, but not
 *   comment/reply/12345).
 *
 * Regions:
 * - $page['help']: Dynamic help text, mostly for admin pages.
 * - $page['content']: The main content of the current page.
 * - $page['sidebar_first']: Items for the first sidebar.
 * - $page['sidebar_second']: Items for the second sidebar.
 * - $page['header']: Items for the header region.
 * - $page['footer']: Items for the footer region.
 *
 * @see template_preprocess()
 * @see template_preprocess_page()
 * @see template_process()
 */
?>

<div id="content">
  <?php print render($page['content']['metatags']); ?>
  <?php print $messages; ?>

  <!--
  <div class="mobile_breadcrumbs mobile">
    <span class="you_are_here">You Are Here:</span>
    <span class="breadcrumbs">Home</span>
  </div>
  -->

  <div id="marquee" class="slideshow">
    <?php print render($page['home_vignette']); ?>
    <div class="controls" id="slideshow-nav"></div>
  </div>

  <div id="subcontent" class="white_box clearfix">

    <div id="cta_nav" class="clearfix">

      <div class="intro pull-left">
        Get the info you need fast
      </div>
      <div class="primary pull-left">
        <a href="<?php echo base_path(); ?>products" class="btn btn-orange btn-lg">Browse Products Online</a>
      </div>
      <div class="separator pull-left">or</div>
      <div class="secondary pull-left">
                            <?php
                                if(variable_get("product_catalog_pdf")){
                                    $pdf = file_load(variable_get("product_catalog_pdf"));
                                    $pdf_url = file_create_url($pdf->uri);
                                }
                            ?>
        <a target="_blank" href="<?php echo $pdf_url;?>" class="btn btn-sm">Flip Through Our Catalog</a>
        <a href="<?php echo base_path(); ?>products/product-index" class="btn btn-sm">Get Technical Data Sheets</a>
      </div>

    </div>
    <div class="desktop">
      <div id="prev_viewed_new" class="carousel clearfix">
        <?php print views_embed_view('recently_viewed', 'desktop_block'); ?>
      </div>
    </div>

    <div class="mobile">
      <div id="prev_viewed_mobile" class="mobile_carousel swipe_scroll_home clearfix">

        <?php print views_embed_view('recently_viewed', 'mobile_block'); ?>

      </div>
    </div>

    <hr />

    <div class="product_grid container">

      <h2>No matter what line of work you're in, Shurtape gets the job done.</h2>

      <!-- COMMENTING THIS TO GO BACK TO STATIC TOUTS - remove when ready to do it dynamically
      <?php if ($page['home_markets']): ?>
        <div id="home_markets"><?php // print render($page['home_markets']); ?></div>
      <?php endif; ?>
      -->
      <div class="product_grid">
        <ul>
          <li>
            <a href="<?php echo base_path(); ?>markets/packaging-solutions" class="item">
              <span class="mobile_link_icon mobile">
                <img src="<?php print base_path() . drupal_get_path('theme', 'shurtape'); ?>/images/icon_mobile_link_arrow.png" alt="" />
              </span>
              <span class="item_image">
                <img src="<?php echo base_path(); ?>sites/default/files/home-callouts/PackagingMarket-homeimage.jpg" alt="Packaging Solutions" />
              </span>
              <span class="item_content">
                <span class="cta_arrow"></span>
                <span class="item_title"><b>Packaging Solutions</b></span>
              </span>
            </a>
          </li>
          <li>
            <a href="<?php echo base_path(); ?>markets/manufacturing-industry" class="item">
              <span class="mobile_link_icon mobile">
                <img src="<?php print base_path() . drupal_get_path('theme', 'shurtape'); ?>/images/icon_mobile_link_arrow.png" alt="" />
              </span>
              <span class="item_image">
                <img src="<?php echo base_path(); ?>sites/default/files/home-callouts/IndustrialMarket-homeimage.jpg" alt="Industrial Tapes" />
              </span>
              <span class="item_content">
                <span class="cta_arrow"></span>
                <span class="item_title"><b>Industrial Tapes</b></span>
              </span>
            </a>
          </li>
          <li>
            <a href="<?php echo base_path(); ?>markets/building-construction/hvac" class="item">
              <span class="mobile_link_icon mobile">
                <img src="<?php print base_path() . drupal_get_path('theme', 'shurtape'); ?>/images/icon_mobile_link_arrow.png" alt="" />
              </span>
              <span class="item_image">
                <img src="<?php echo base_path(); ?>sites/default/files/home-callouts/HVACMarket-homeimage.jpg" alt="HVAC" />
              </span>
              <span class="item_content">
                <span class="cta_arrow"></span>
                <span class="item_title"><b>HVAC</b></span>
              </span>
            </a>
          </li>
          <li>
            <a href="<?php echo base_path(); ?>markets/arts-entertainment" class="item">
              <span class="mobile_link_icon mobile">
                <img src="<?php print base_path() . drupal_get_path('theme', 'shurtape'); ?>/images/icon_mobile_link_arrow.png" alt="" />
              </span>
              <span class="item_image">
                <img src="<?php echo base_path(); ?>sites/default/files/home-callouts/GaffersMarket-homeimage.jpg" alt="Arts & Entertainment" />
              </span>
              <span class="item_content">
                <span class="cta_arrow"></span>
                <span class="item_title"><b>Arts &amp; Entertainment</b></span>
              </span>
            </a>
          </li>
          <li>
            <a href="<?php echo base_path(); ?>markets/building-construction/paint-masking" class="item">
              <span class="mobile_link_icon mobile">
                <img src="<?php print base_path() . drupal_get_path('theme', 'shurtape'); ?>/images/icon_mobile_link_arrow.png" alt="" />
              </span>
              <span class="item_image">
                <img src="<?php echo base_path(); ?>sites/default/files/home-callouts/PaintMarket-homeimage.jpg" alt="Professional Paint" />
              </span>
              <span class="item_content">
                <span class="cta_arrow"></span>
                <span class="item_title"><b>Professional Paint</b></span>
              </span>
            </a>
          </li>
          <li>
            <a href="<?php echo base_path(); ?>markets/transportation" class="item">
              <span class="mobile_link_icon mobile">
                <img src="<?php print base_path() . drupal_get_path('theme', 'shurtape'); ?>/images/icon_mobile_link_arrow.png" alt="" />
              </span>
              <span class="item_image">
                <img src="<?php echo base_path(); ?>sites/default/files/home-callouts/TransportationMarket-homeimage.jpg" alt="Transportation" />
              </span>
              <span class="item_content">
                <span class="cta_arrow"></span>
                <span class="item_title"><b>Transportation</b></span>
              </span>
            </a>
          </li>
        </ul>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  /* define $ as jQuery just in case */
  ( function( $ ){

    /* doc ready */
    $( function( ){

      /* init the carousel */
      $( '.carousel' ).carousel( );

      /* init the mobile carousel */
      $( '.mobile_carousel' ).mobile_carousel( );
    });
  })( jQuery );
</script>
