<?php

/**
 * Implements hook_html_head_alter().
 * This will overwrite the default meta character type tag with HTML5 version.
 */
function shurtape_html_head_alter(&$head_elements) {
  $head_elements['system_meta_content_type']['#attributes'] = array(
    'charset' => 'utf-8'
  );
}

/**
 * Insert themed breadcrumb page navigation at top of the node content
 */
function shurtape_breadcrumb($variables) {
  if (isset($variables['breadcrumb'])) {
    $breadcrumb = $variables['breadcrumb'];
    if (!empty($breadcrumb)) {
      /* comment below line to hide current page to breadcrumb */
      $breadcrumb[] = drupal_get_title();
      $newbread[] = l('Home', '');
      foreach ($breadcrumb as $link) {
        array_push(
          $newbread,
          str_replace("taxonomy/term/", "products?t=", $link)
        );
      }
      $output = '<nav class="breadcrumbs">'
        . implode(' &rsaquo; ', $newbread) . '</nav>';

      return $output;
    }
  }
}

/**
 * Override or insert variables into the html template.
 */
function shurtape_process_html(&$vars) {
  /* Hook into color.module - what does this do? */
  if (module_exists('color')) {
    _color_html_alter($vars);
  }
}

/**
 * Override or insert variables into the html template.
 */
function shurtape_preprocess_html(&$variables) {

  // Set up layout variable.
  $variables['layout'] = 'none';
  if (!empty($variables['page']['sidebar_first'])) {
    $variables['layout'] = 'first';
  }
  if (!empty($variables['page']['sidebar_second'])) {
    $variables['layout'] = ($variables['layout'] == 'first') ? 'both'
      : 'second';
  }

  $variables['base_path'] = base_path();
  $variables['front_page'] = url();
  $variables['feed_icons'] = drupal_get_feeds();
  $variables['language'] = $GLOBALS['language'];
  $variables['language']->dir = $GLOBALS['language']->direction ? 'rtl' : 'ltr';
  $variables['logo'] = theme_get_setting('logo');
  $variables['main_menu'] = theme_get_setting('toggle_main_menu') ? menu_main_menu() : array();
  $variables['secondary_menu'] = theme_get_setting('toggle_secondary_menu') ? menu_secondary_menu() : array();
  $variables['action_links'] = menu_local_actions();
  $variables['site_name'] = (theme_get_setting('toggle_name') ? filter_xss_admin(variable_get('site_name', 'Drupal')) : '');
  $variables['site_slogan'] = (theme_get_setting('toggle_slogan') ? filter_xss_admin(variable_get('site_slogan', '')) : '');
  $variables['tabs'] = menu_local_tabs();

  //template suggestion for html.tpl on content types
  $node = menu_get_object();
  if ($node && $node->nid) {
    $variables['theme_hook_suggestions'][] = 'html__' . $node->type;
    $variables['node'] = $node;//to have access to node content on html template
  }
}

function _shurtape_filter_boolean_display($field, $val, $i) {
  $field = field_info_instance('node', $field, 'product');
  $remove = shurtape_remove_filter($i);
  if (isset($field['label'])) :
    $title = $val == '1' ? $field['label'] : str_replace("Is ", "Is Not ", $field['label']);
    if ($field["widget"]["type"] != "options_onoff") {//is not boolean
      //see if is a term reference to obtain the string for value or just a number
      if ($field['display']["default"]["type"] == "number_integer") {
        $title = $field['label'] . " - " . $val;//append the selected value
      }
      else {
        $term = taxonomy_term_load($val);
        $title = $field['label'] . " - " . $term->name;//append the selected term name (the filter name)
      }
    }

    return sprintf('<li><a href="%s" class="control_remove" data-value="%s">%s</a></li>', $remove, $i, $title);
  endif;
}

function shurtape_preprocess_views_view(&$variables) {
  if ($variables['name'] == "search") {
    $variables['filter_tag_output'] = "";
    $variables['result_title'] = "";
    $variables['link_results'] = "/search/products/";
    $variables['link_other_results'] = "/search/products/other";

    if (isset($_GET['f'])) {
      $original_url = $_GET['f'];

      for ($i = 0; $i < count($original_url); $i++) :
        if (isset($extracted_values)) {
          unset($extracted_values);
        }

        $extracted_values = explode(":", $original_url[$i]);
        $fieldName = $extracted_values[0];
        if ($fieldName == 'field_product_market_list' || $fieldName == 'field_product_type') {
          $load_term = taxonomy_term_load($extracted_values[1]);
          if (isset($load_term->name)) :
            $variables['result_title'] = sprintf('Product Results For "%s"', $load_term->name);
          endif;
        }
        else {
          if ($fieldName == 'field_product_backing' ||
            $fieldName == 'field_product_adhesive' ||
            $fieldName == 'field_is_waterproof' ||
            $fieldName == 'field_product_liner' ||
            $fieldName == 'field_box_weight' ||
            $fieldName == 'field_product_clean_removal_days' ||
            $fieldName == 'field_handling_conditions' ||
            $fieldName == 'field_holding_power' ||
            $fieldName == 'field_is_cold_temp_tape' ||
            $fieldName == 'field_is_uv_resistant' ||
            $fieldName == 'field_is_carpet_tape' ||
            $fieldName == 'field_is_fda_compliant' ||
            $fieldName == 'field_is_ul_listed' ||
            $fieldName == 'field_is_ul_723_tested' ||
            $fieldName == 'field_is_green_point_certified'
          ) {
            $variables['filter_tag_output'] .= _shurtape_filter_boolean_display($fieldName, $extracted_values[1], $i);
          }
        }
      endfor;

      $f_array = isset($_GET['f']) ? $_GET['f'] : NULL;

      if (!is_null($f_array)) :
        for ($i = 0; $i < count($f_array); $i++) :
          if (!isset($link['output'])) {
            $link['output'] = sprintf(
              "f[%s]=%s", $i,
              addslashes($f_array[$i])
            );
          }
          else {
            $link['output'] .= sprintf(
              "&f[%s]=%s", $i,
              addslashes($f_array[$i])
            );
          }
        endfor;
      endif;

      if (isset($_GET['search_api_views_fulltext'])) {
        if (!isset($link['output'])) {
          $link['output'] = sprintf(
            "search_api_views_fulltext=%s",
            htmlspecialchars($_GET['search_api_views_fulltext'])
          );
        }
        else {
          $link['output'] .= sprintf(
            "&search_api_views_fulltext=%s",
            htmlspecialchars($_GET['search_api_views_fulltext'])
          );
        }
      }

      if (isset($_GET['items_per_page'])) {
        if (!isset($link['output'])) {
          $link['output'] = sprintf(
            "items_per_page=%s",
            htmlspecialchars($_GET['items_per_page'])
          );
        }
        else {
          $link['output'] .= sprintf(
            "&items_per_page=%s",
            htmlspecialchars($_GET['items_per_page'])
          );
        }
      }

      if (isset($_GET['page'])) {
        if (!isset($link['output'])) {
          $link['output'] = sprintf(
            "page=%s",
            htmlspecialchars($_GET['page'])
          );
        }
        else {
          $link['output'] .= sprintf(
            "&page=%s",
            htmlspecialchars($_GET['page'])
          );
        }
      }

      $variables['link_results'] .= "?" . $link['output'];
      $variables['link_other_results'] = "?" . $link['output'];
    }

    if (isset($_GET['search_api_views_fulltext'])) {
      if ($_GET['search_api_views_fulltext'] != "") {
        $variables['result_title'] = sprintf(
          'Product Results For "%s"',
          $_GET['search_api_views_fulltext']
        );
      }
    }
  }

  return $variables;

}

function shurtape_remove_filter($f_index) {
  if (isset($_GET['f'])) {
    $original_url = $_GET['f'];
    unset($original_url[$f_index]);
    $new_array = array_values($original_url);

    for ($i = 0; $i < count($new_array); $i++) :
      if (isset($url_path)) {
        $url_path .= sprintf("&f[%s]=%s", $i, $new_array[$i]);
      }
      else {
        $url_path = sprintf("f[%s]=%s", $i, $new_array[$i]);
      }
    endfor;

    if (isset($_GET['search_api_views_fulltext'])) {
      if (!isset($url_path)) {
        $url_path = sprintf(
          "search_api_views_fulltext=%s",
          htmlspecialchars($_GET['search_api_views_fulltext'])
        );
      }
      else {
        $url_path .= sprintf(
          "&search_api_views_fulltext=%s",
          htmlspecialchars($_GET['search_api_views_fulltext'])
        );
      }
    }

    if (isset($_GET['items_per_page'])) {
      if (!isset($url_path)) {
        $url_path = sprintf(
          "items_per_page=%s",
          htmlspecialchars($_GET['items_per_page'])
        );
      }
      else {
        $url_path .= sprintf(
          "&items_per_page=%s",
          htmlspecialchars($_GET['items_per_page'])
        );
      }
    }

    if (isset($_GET['page'])) {
      if (!isset($url_path)) {
        $url_path = sprintf("page=%s", htmlspecialchars($_GET['page']));
      }
      else {
        $url_path .= sprintf(
          "&page=%s",
          htmlspecialchars($_GET['page'])
        );
      }
    }

    if (isset($url_path)) {
      $url_path = "?" . $url_path;
    }
    else {
      $url_path = "";
    }

    if (arg(2) == "others" || arg(2) == "technical") {
      $url_path = sprintf(
        "/%s/%s/%s/%s", arg(0), arg(1), arg(2),
        $url_path
      );
    }
    else {
      $url_path = sprintf("/%s/%s/%s", arg(0), arg(1), $url_path);
    }

    return $url_path;
  }

}

/**
 * Override or insert variables into the page template.
 */
function shurtape_process_page(&$variables) {
  /* Hook into color.module. - what does this do? */
  if (module_exists('color')) {
    _color_page_alter($variables);
  }

}

/**
 * Override or insert variables into the page template.
 */
function shurtape_preprocess_page(&$vars) {
  if (drupal_match_path(current_path(), 'taxonomy/term/*')) {
    $args = arg();
    if (is_numeric($args[2]) && ($term = taxonomy_term_load($args[2]))) {
      if (in_array(
        $term->vocabulary_machine_name, array(
          'newsletter',
          'blog_authors'
        )
      )) {
        $vars['theme_hook_suggestions'][] = 'page__blog';
      }
    }
  }
  if (isset($vars['main_menu'])) {
    $vars['main_menu'] = theme(
      'links__system_main_menu',
      array(
        'links' => $vars['main_menu'],
        'attributes' => array(
          'class' => array(
            'links',
            'main-menu',
            'clearfix'
          )
        ),
        'heading' => array(
          'text' => t('Main menu'),
          'level' => 'h2',
          'class' => array('element-invisible')
        )
      )
    );
  }
  else {
    $vars['main_menu'] = FALSE;
  }
  if (isset($vars['secondary_menu'])) {
    $vars['secondary_menu'] = theme(
      'links__system_secondary_menu',
      array(
        'links' => $vars['secondary_menu'],
        'attributes' => array(
          'class' => array(
            'links',
            'secondary-menu',
            'clearfix'
          )
        ),
        'heading' => array(
          'text' => t('Secondary menu'),
          'level' => 'h2',
          'class' => array('element-invisible'),
        )
      )
    );
  }
  else {
    $vars['secondary_menu'] = FALSE;
  }
  //Obtain the webform NID in a thank-you page for a promotion
  if (count(arg()) == 3 && arg(0) == "promotion" && arg(2) == "thank-you" && is_numeric(arg(1))) {
    $vars["webform_nid"] = arg(1);
    $vars["webform_node"] = node_load($vars["webform_nid"]);
    $vars['theme_hook_suggestions'][] = 'page__promotionthankyou';//template suggestion
  }

  if (isset($vars['node']->type)) {
    $vars['theme_hook_suggestions'][] = 'page__' . $vars['node']->type;//template suggestion
    if ($vars['node']->type == "promotion_page") {
      drupal_add_css(drupal_get_path('theme', 'shurtape') . '/css/promotion.css');
      drupal_add_js(drupal_get_path('theme', 'shurtape') . '/js/promotion.js');
    }
    elseif ($vars['node']->type == 'blog') {
      $vars['theme_hook_suggestions'][] = 'page__blog';
    }
  }

  if (in_array('page__blog', $vars['theme_hook_suggestions'])) {
    if ($file_id = variable_get('shurtape_blog_banner_file', 0)) {
      $file = file_load($file_id);
      $vars['page']['blog_banner'] = array(
        '#theme' => 'image_style',
        '#style_name' => '1145x461sc',
        '#path' => $file->uri,
      );
    }
    drupal_add_js(drupal_get_path('module', 'shurtape_blog') . '/js/shurtape_blog.js');
  }
}

/**
 * Duplicate of theme_menu_local_tasks( ) but adds clearfix to tabs.
 */
function shurtape_menu_local_tasks(&$variables) {
  $output = '';
  if (!empty($variables['primary'])) {
    $variables['primary']['#prefix'] = '<h2 class="element-invisible">'
      . t('Primary tabs') . '</h2>';
    $variables['primary']['#prefix'] .= '<ul class="tabs primary clearfix">';
    $variables['primary']['#suffix'] = '</ul>';
    $output .= drupal_render($variables['primary']);
  }
  if (!empty($variables['secondary'])) {
    $variables['secondary']['#prefix'] = '<h2 class="element-invisible">'
      . t('Secondary tabs') . '</h2>';
    $variables['secondary']['#prefix'] .= '<ul class="tabs secondary clearfix">';
    $variables['secondary']['#suffix'] = '</ul>';
    $output .= drupal_render($variables['secondary']);
  }

  return $output;
}

/**
 * Adds placeholder to form elements
 */

function shurtape_form_alter(&$form, &$form_state, $form_id) {
  if (strpos($form_id, "webform_client_form_") !== FALSE) {//target any webform
    foreach ($form["submitted"] as $key => $value) {
      if (in_array(
        $value["#type"],
        array("textfield", "webform_email", "textarea")
      )) {
        $form["submitted"][$key]['#attributes']["placeholder"] = t(
          $value["#title"]
        );
      }
    }
  }

  if ($form['#id'] == 'user-login') {
    $form['#attributes']['autocomplete'] = 'off';
  }
}

/**
 * Override or insert variables into the node template.
 */
function shurtape_preprocess_node(&$vars) {
  $node = $vars['node'];
  if ($vars['view_mode'] == 'full' && node_is_page($vars['node'])) {
    $vars['classes_array'][] = 'node-full';
  }

  // only do this for node-type nodes and only if Path module exists
  if (module_exists('path') && isset($vars['node'])) {
    // look up the alias from the url_alias table
    $source = 'node/' . $vars['node']->nid;
    $alias = db_query(
      "SELECT alias FROM {url_alias} WHERE source = '$source'"
    )
      ->fetchField();

    if ($alias != '') {
      // build a suggestion for every possibility
      $parts = explode('/', $alias);
      $suggestion = '';
      foreach ($parts as $part) {
        if ($suggestion == '') {
          // first suggestion gets prefaced with 'page--'
          $suggestion .= "node__$part";
        }
        else {
          // subsequent suggestions get appended
          $suggestion .= "__$part";
        }
        // add the suggestion to the array
        $vars['theme_hook_suggestions'][] = $suggestion;
      }
    }
  }

  // Get a list of all the regions for this theme
  foreach (system_region_list($GLOBALS['theme']) as $region_key => $region_name) {
    //echo $region_name;
    // Get the content for each region and add it to the $region variable
    if ($blocks = block_get_blocks_by_region($region_key)) { //echo "333<br>";
      $vars['region'][$region_key] = $blocks;
    }
    else { //echo "444<br>";
      $vars['region'][$region_key] = array();
    }
  }
}

function shurtape_convert_standard_display($type, $value) {
  if ($value == "") {
    $value = 0;
  }
  switch ($type) {
    case "Adhesion to Backing":
      return str_replace("%val%", $value, "%val% oz/in width");
      break;
    case "Adhesion to Backing - Hand Rolls Only":
      return str_replace("%val%", $value, "%val% oz/in width");
      break;
    case "Adhesion to Backing - Machine Rolls Only":
      return str_replace("%val%", $value, "%val% oz/in width");
      break;
    case "Adhesion to Housewrap":
      return str_replace("%val%", $value, "%val% oz/in width");
      break;
    case "Adhesion to Kraft":
      return str_replace("%val%", $value, "%val% oz/in width");
      break;
    case "Adhesion to Stainless Steel":
      return str_replace("%val%", $value, "%val% oz/in width");
      break;
    case "Application Temperature Max":
      return str_replace("%val%", $value, "%val% F");
      break;
    case "Application Temperature Min":
      return str_replace("%val%", $value, "%val% F");
      break;
    case "Bottom Sheet (weight)":
      return str_replace("%val%", $value, "%val% lbs");
      break;
    case "CD Spacing":
      return str_replace("%val%", $value, "%val% in");
      break;
    case "CD Tensile Strength":
      return str_replace("%val%", $value, "%val% lbs/in");
      break;
    case "Dielectric strength":
      return str_replace("%val%", $value, "%val% volts");
      break;
    case "Elongation":
      return str_replace("%val%", $value, "%val%%");
      break;
    case "Elongation - Cross Direction":
      return str_replace("%val%", $value, "%val%%");
      break;
    case "Elongation - Machine Direction":
      return str_replace("%val%", $value, "%val%%");
      break;
    case "Holding Power to Fiberboard":
      return str_replace("%val%", $value, "%val% minutes");
      break;
    case "Laminate":
      return str_replace("%val%", $value, "%val% Hot Melt");
      break;
    case "MD Pattern":
      return str_replace("%val%", $value, "%val%");
      break;
    case "MD Spacing":
      return str_replace("%val%", $value, "%val% inches");
      break;
    case "MD Tensile Strength":
      return str_replace("%val%", $value, "%val% lbs/in");
      break;
    case "Nominal Tear Strength":
      return str_replace("%val%", $value, "%val% lbs/in width");
      break;
    case "Paper Sheet (Weight)":
      return str_replace("%val%", $value, "%val% lbs");
      break;
    case "Service Temperature Max":
      return str_replace("%val%", $value, "%val% F");
      break;
    case "Service Temperature Min":
      return str_replace("%val%", $value, "%val% F");
      break;
    case "Tensile":
      return str_replace("%val%", $value, "%val% lbs/in width");
      break;
    case "Tensile Strength (longitudinal direction)":
      return str_replace("%val%", $value, "%val% lbs/in width");
      break;
    case "Tensile Strength (transverse direction)":
      return str_replace("%val%", $value, "%val% lbs/in width");
      break;
    case "Tensile Strength - Cross Direction":
      return str_replace("%val%", $value, "%val% lbs/in width");
      break;
    case "Tensile Strength - Machine Direction":
      return str_replace("%val%", $value, "%val% lbs/in width");
      break;
    case "Thickness":
      return str_replace("%val%", $value, "%val% mils");
      break;
    case "Thickness - with liner":
      return str_replace("%val%", $value, "%val% mil");
      break;
    case "Thickness - without liner":
      return str_replace("%val%", $value, "%val% mil");
      break;
    case "Top Sheet (weight)":
      return str_replace("%val%", $value, "%val% lbs");
      break;
    case "Water Vapor Transmission Rate":
      return str_replace("%val%", $value, "%val% gm/100 sq in/24 hr");
      break;
    default:
      return $value != "" ? $value : "-";
  }
}

function shurtape_convert_metric_display($type, $value) {
  if ($value == "") {
    $value = 0;
  }
  switch ($type) {
    case "Adhesion to Backing":
      return str_replace("%val%", $value, "%val% N/10 mm");
      break;
    case "Adhesion to Backing - Hand Rolls Only":
      return str_replace("%val%", $value, "%val% N/10 mm");
      break;
    case "Adhesion to Backing - Machine Rolls Only":
      return str_replace("%val%", $value, "%val% N/10 mm");
      break;
    case "Adhesion to Housewrap":
      return str_replace("%val%", $value, "%val% N/10 mm");
      break;
    case "Adhesion to Kraft":
      return str_replace("%val%", $value, "%val% N/10 mm");
      break;
    case "Adhesion to Stainless Steel":
      return str_replace("%val%", $value, "%val% N/10 mm");
      break;
    case "Application Temperature Max":
      return str_replace("%val%", $value, "%val% C");
      break;
    case "Application Temperature Min":
      return str_replace("%val%", $value, "%val% C");
      break;
    case "Bottom Sheet (weight)":
      return str_replace("%val%", $value, "%val% kg");
      break;
    case "CD Spacing":
      return str_replace("%val%", $value, "%val% cm");
      break;
    case "CD Tensile Strength":
      return str_replace("%val%", $value, "%val% N/10 mm");
      break;
    case "Dielectric strength":
      return str_replace("%val%", $value, "%val% volts");
      break;
    case "Elongation":
      return str_replace("%val%", $value, "%val%%");
      break;
    case "Elongation - Cross Direction":
      return str_replace("%val%", $value, "%val%%");
      break;
    case "Elongation - Machine Direction":
      return str_replace("%val%", $value, "%val%%");
      break;
    case "Holding Power to Fiberboard":
      return str_replace("%val%", $value, "%val% minutes");
      break;
    case "Laminate":
      return str_replace("%val%", $value, "%val% Hot Melt");
      break;
    case "MD Pattern":
      return str_replace("%val%", $value, "%val%");
      break;
    case "MD Spacing":
      return str_replace("%val%", $value, "%val% cm");
      break;
    case "MD Tensile Strength":
      return str_replace("%val%", $value, "%val% N/10 mm");
      break;
    case "Nominal Tear Strength":
      return str_replace("%val%", $value, "%val% N/10 mm");
      break;
    case "Paper Sheet (Weight)":
      return str_replace("%val%", $value, "%val% kg");
      break;
    case "Service Temperature Max":
      return str_replace("%val%", $value, "%val% C");
      break;
    case "Service Temperature Min":
      return str_replace("%val%", $value, "%val% C");
      break;
    case "Tensile":
      return str_replace("%val%", $value, "%val% N/10 mm");
      break;
    case "Tensile Strength (longitudinal direction)":
      return str_replace("%val%", $value, "%val% N/10 mm");
      break;
    case "Tensile Strength (transverse direction)":
      return str_replace("%val%", $value, "%val% N/10 mm");
      break;
    case "Tensile Strength - Cross Direction":
      return str_replace("%val%", $value, "%val% N/10 mm");
      break;
    case "Tensile Strength - Machine Direction":
      return str_replace("%val%", $value, "%val% N/10 mm");
      break;
    case "Thickness":
      return str_replace("%val%", $value, "%val% mm");
      break;
    case "Thickness - with liner":
      return str_replace("%val%", $value, "%val% mm");
      break;
    case "Thickness - without liner":
      return str_replace("%val%", $value, "%val% mm");
      break;
    case "Top Sheet (weight)":
      return str_replace("%val%", $value, "%val% kg");
      break;
    case "Water Vapor Transmission Rate":
      return str_replace("%val%", $value, "%val% gm/msq/24 hr");
      break;
    default:
      return $value != "" ? $value : "-";
  }
}

function shurtape_auto_copyright($year = 'auto') {
  if (intval($year) == 'auto') {
    $year = date('Y');
  }
  if (intval($year) == date('Y')) {
    return intval($year);
  }
  if (intval($year) < date('Y')) {
    return intval($year) . '-' . date('Y');
  }
  if (intval($year) > date('Y')) {
    return date('Y');
  }
}
