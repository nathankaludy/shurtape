<?php

/**
 * @author marcolara
 *
 */
class FacetapiWidgetBooleanCheckboxLinks extends FacetapiWidgetCheckboxLinks 
{
	/**
	 * Overrides FacetapiWidgetLinks::init().
	 *
	 * Adds additional JavaScript settings and CSS.
	 */
	public function init() 
	{
		parent::init();
		$this->jsSettings['makeCheckboxes'] = 1;
		drupal_add_css(drupal_get_path('module', 'shurtape_facetapi_boolean') . '/facetapi.css');
	}

	/**
	 * Overrides FacetapiWidgetLinks::getItemClasses().
	 *
	 * Sets the base class for checkbox facet items.
	 */
	public function getItemClasses() 
	{
		return array('facetapi-checkbox');
	}


	/**
	 * build list items
	 */
	function buildListItems($build) 
	{
		$settings = $this->settings->settings;

		/* Initializes links attributes, adds rel="nofollow" if configured. */
		$attributes = ($settings['nofollow']) ? array('rel' => 'nofollow') : array();
		$attributes += array('class' => $this->getItemClasses());

		/* Builds rows. */
		$items = array();
		foreach ($build as $value => $item) 
		{
			$row = array('class' => array());

			/* Initializes variables passed to theme hook. */
			$variables = array(
				'text' => ($item['#markup'] == 'true' ? 'Yes' : 'No'),
				'path' => $item['#path'], 'count' => $item['#count'],
				'options' => array(
					'attributes' => $attributes,
					'html' => $item['#html'],
					'query' => $item['#query']
				)
			);

			/* Adds the facetapi-zero-results class to items that have no results. */
			if (!$item['#count']) 
			{
				$variables['options']['attributes']['class'][] = 'facetapi-zero-results';
			}

			/* Add an ID to identify this link. */
			$variables['options']['attributes']['id'] = drupal_html_id('facetapi-link');

			/* If the item has no children, it is a leaf. */
			if (empty($item['#item_children'])) 
			{
				$row['class'][] = 'leaf';
			} 
			else 
			{
				/* If the item is active or the "show_expanded" setting is selected, show this item as expanded so we see its children. */
				if ($item['#active'] || !empty($settings['show_expanded'])) 
				{
					$row['class'][] = 'expanded';
					$row['children'] = $this->buildListItems($item['#item_children']);
				} 
				else 
				{
					$row['class'][] = 'collapsed';
				}
			}

			/* Gets theme hook, adds last minute classes. */
			$class = ($item['#active']) ? 'facetapi-active' : 'facetapi-inactive';
			$variables['options']['attributes']['class'][] = $class;

			/* Themes the link, adds row to items. */
			$row['data'] = theme($item['#theme'], $variables);
			$items[] = $row;
		}
		return $items;
	}
}
