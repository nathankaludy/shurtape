<div class="blue_bar desktop">&nbsp;</div>

<div id="marquee" class="clearfix">
	<div class="marquee_image pull-right">
		<?php if(isset($term->image_hero)) : ?>
			<?php print render($term->image_hero); ?>
		<?php else : ?>
			<img src="<?php print $path; ?>/images/hero-default.png">
		<?php endif; ?>
	</div>
	<div class="marquee_content pull-left">
		<span><?php print render($term->description); ?></span>
	</div>
</div>

<?php if(isset($term->field_type_term_feature_video)) : ?>
	<div class="desktop">
		<div id="featured_videos">
			<h3 class="intro pull-left">Featured Videos:</h3>
			<ul class="list-inline">
				<?php if(isset($term->field_type_term_feature_video['und'])): ?>
				<?php foreach($term->field_type_term_feature_video['und'] as $vid): ?>
					<li>
						<a href="#" class="video_thumb open_video_modal" rel="<?php print $vid['value']; ?>">
							<span class="play_overlay"></span>
							<img src="//img.youtube.com/vi/<?php print $vid['value']; ?>/0.jpg" alt="" width="96" height="72" />
						</a>
					</li>
				<?php endforeach; ?>
				<?php endif; ?>
			</ul>
		</div>
	</div>
	<div class="mobile">
		<div id="mobile_featured_videos" class="swipe_scroll">
			<h3 class="intro">Featured Videos:</h3>
			<div class="videos_wrap">
				<?php if(isset($term->field_type_term_feature_video['und'])): ?>
				<?php if ( count( $term->field_type_term_feature_video['und'] ) > 1 ): ?>
					<a href="#" class="arrow_control prev"></a>
				<?php endif; ?>
				<?php endif; ?>
				<ul class="list-inline">
					<?php if(isset($term->field_type_term_feature_video['und'])): ?>
					<?php $i=0; foreach($term->field_type_term_feature_video['und'] as $vid): $i++; ?>
						<li class="vid vid_<?php echo $i; echo $i == 1 ? ' active' : ''; ?>">
							<a href="#" class="video_thumb open_video_modal" rel="<?php print $vid['value']; ?>">
								<span class="play_overlay"></span>
								<img src="//img.youtube.com/vi/<?php print $vid['value']; ?>/0.jpg" alt="" width="96" height="72" />
							</a>
						</li>
					<?php endforeach; ?>
					<?php endif; ?>
				</ul>
				<?php if(isset($term->field_type_term_feature_video['und'])): ?>
				<?php if ( count( $term->field_type_term_feature_video['und'] ) > 1 ): ?>
					<a href="#" class="arrow_control next"></a>
				<?php endif; ?>
				<?php endif; ?>
			</div>
		</div>
	</div>
<?php endif; ?>

<div id="product_grid" class="product_grid browse_type">

	<h3><?php print $term->name; ?> Products:</h3>
	<?php if($products) : ?>
		<?php print $products; ?>
	<?php endif; ?>

</div>

<div class="modal fade" id="video_modal" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
			</div>
			<div class="modal-body">
				<!-- loaded dynamically via JS below -->
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
/* define $ as jQuery just in case */
( function( $ ){

	/* doc ready */
	$( function( ){
		/* init the thumb slider */
		$( '#thumb_slider' ).thumb_slider( );

		/* init the tabs */
		$( '#tab_section' ).tabs( );

		/* show the video modal */
		$( '.open_video_modal' ).click( function( e ) 
		{
			/* set vars */
			var vid = $( this ).attr( 'rel' );
			$( '#video_modal .modal-body' ).hide( ).html( '<iframe width="620" height="349" src="//www.youtube.com/embed/' + vid + '" frameborder="0" allowfullscreen></iframe>' ).show( ).fitVids( );;
			$( '#video_modal' ).modal( );
			e.preventDefault( );
		});

		/* shut down video on close trigger */
		$( '#video_modal' ).on( 'hidden.bs.modal', function ( ) 
		{
			$( '#video_modal .modal-body' ).html( '' );
		});
		
		/* init the mobile video slider */
		$( '#mobile_featured_videos' ).video_slider( );
	});
})( jQuery );
</script>

