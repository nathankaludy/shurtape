<div id="content">
	<div class="fixed_wrap white_box clearfix">
		<div id="sidebar" class="pull-left">
			<?php print render($filter_bar['content']); ?>
		</div>
		<div id="main" class="pull-right">
			<!--
			<div class="mobile_breadcrumbs mobile">
				<span class="you_are_here">You Are Here:</span>
				<span class="breadcrumbs">
					<a href="<?php print base_path(); ?>">Home</a> / 
					<?php echo $title; ?>
				</span>
			</div>
			-->
			<h1 class="headline">
				<?php if($headline) : ?>
					<?php print $headline; ?>
				<?php endif; ?>
			</h1>
			<?php print render($products['content']); ?>
		</div>
	</div>
</div>
