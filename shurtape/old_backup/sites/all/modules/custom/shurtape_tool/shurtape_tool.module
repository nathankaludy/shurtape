<?php

/**
 * @file
 * Provides functionality for distributors.
 */

/**
 * Implements hook_menu().
 */
function shurtape_tool_menu() {
  $items = array();
  $items['where-to-buy'] = array(
    'title'           => 'Where to Buy',
    'page callback'   => 'drupal_get_form',
    'page arguments'  => array('shurtape_tool_where_to_buy_form'),
    'access callback' => TRUE,
  );
  $items['admin/config/search/where-to-buy'] = array(
    'title'            => 'Where to Buy',
    'page callback'    => 'drupal_get_form',
    'page arguments'   => array('shurtape_tool_admin_where_to_buy_form'),
    'access arguments' => array('administer search'),
  );
  $items['where-to-buy/results/ajax'] = array(
    'page callback'     => 'shurtape_tool_where_to_buy_get_results',
    'delivery callback' => 'drupal_json_output',
    'access callback'   => TRUE,
    'type'              => MENU_CALLBACK,
  );
  return $items;
}

/**
 * Implementation of hook_form_FORM_ID_alter().
 */
function shurtape_tool_form_distributor_node_form_alter(&$form, &$form_state, $form_id) {
  $form['field_latlng']['#access'] = FALSE;
  $form['field_formatted_address']['#access'] = FALSE;
}

/**
 * Implements hook_node_presave().
 */
function shurtape_tool_node_presave($node) {
  if ($node->type == 'distributor') {
    if ($field_address = field_get_items('node', $node, 'field_address')) {
      if ($fc_item = field_collection_field_get_entity($field_address[0])) {

        $address = array();
        $fields = array(
         'field_distributor_street',
         'field_distributor_city',
         'field_distributor_state',
         'field_distributor_zip',
        );

        foreach ($fields as $field_name) {
          if ($item = field_get_items('field_collection_item', $fc_item, $field_name)) {
            $address[] = $item[0]['value'];
          }
        }

        if ($result = shurtape_tool_geocode($address)) {

          // Set lat lng.
          $field_latlng = &$node->field_latlng[$node->language][0];
          $field_latlng = $result['results'][0]['geometry']['location'];
          $field_latlng['lat_sin'] = sin(deg2rad($field_latlng['lat']));
          $field_latlng['lat_cos'] = cos(deg2rad($field_latlng['lat']));
          $field_latlng['lng_rad'] = deg2rad($field_latlng['lng']);

          // Set formatted address.
          $field_formatted_address = &$node->field_formatted_address[$node->language][0]['value'];
          $field_formatted_address = $result['results'][0]['formatted_address'];
        }
      }
    }
  }
}

/**
 * Implements hook_feeds_processor_targets_alter().
 */
function shurtape_tool_feeds_processor_targets_alter(&$targets, $entity_type, $bundle_name) {
  if ($entity_type == 'node' && $bundle_name == 'distributor') {
    $targets['industrial'] = array(
      'name'     => t('Distributor: Industrial'),
      'callback' => 'shurtape_tool_feeds_set_target',
    );
    $targets['industrial_partner'] = array(
      'name'     => t('Distributor: Industrial Partner'),
      'callback' => 'shurtape_tool_feeds_set_target',
    );
    $targets['hvac'] = array(
      'name'     => t('Distributor: HVAC'),
      'callback' => 'shurtape_tool_feeds_set_target',
    );
    $targets['website'] = array(
      'name'     => t('Distributor: Website'),
      'callback' => 'shurtape_tool_feeds_set_target',
    );
    $targets['address'] = array(
      'name'     => t('Distributor: Address collection'),
      'callback' => 'shurtape_tool_feeds_set_address',
    );
  }
}

/**
 * User has decided to map to and $value contains the value of the feed item
 * element the user has picked as a source.
 */
function shurtape_tool_feeds_set_target($source, $entity, $target, $value, $mapping) {

  if (is_array($value)) {
    $value = reset($value);
  }

  switch ($target) {
    case 'industrial':
    case 'hvac':
      if (!empty($value)) {
        $entity->field_distributor_type[$entity->language][0]['value'] = $target;
      }
      break;

    case 'industrial_partner':
      $entity->field_industrial_partner[$entity->language][0]['value'] = !empty($value) ? 1 : 0;
      break;

    case 'website':
      if (!empty($value)) {
        $entity->field_website_url[$entity->language][0]['url'] = rtrim($value, '/');
      }
      break;
  }
}

/**
 * Save collected address.
 */
function shurtape_tool_feeds_set_address($source, $entity, $target, $value, $mapping) {

  if ($field_address = field_get_items('node', $entity, 'field_address')) {
    $fc_item = field_collection_item_load($field_address[0]['value']);
  }
  else {
    $fc_item = entity_create('field_collection_item', array('field_name' => 'field_address'));
    $fc_item->setHostEntity('node', $entity);
  }

  foreach ($value as $field_name => $data) {
    $fc_item->{$field_name} = array(
      LANGUAGE_NONE => array(array('value' => $data)),
    );
  }

  $fc_item->save(TRUE);
}

/**
 * Implements hook_feeds_after_parse().
 */
function shurtape_tool_feeds_after_parse(FeedsSource $source, FeedsParserResult $result) {
  if ($source->id == 'distributors_import_csv') {

    // Clean values.
    foreach ($result->items as $i => $item) {
      foreach ($item as $key => $value) {
        if (!empty($value)) {
          $value = trim(preg_replace('/[^(\x20-\x7F)]*/', '', $value));
          $result->items[$i][$key] = $value;
        }
      }
    }

    foreach ($result->items as $i => $item) {

      // Address.
      $fulladdress = array(
        'field_distributor_city'   => $item['city'],
        'field_distributor_state'  => $item['state'],
        'field_distributor_street' => $item['address'],
        'field_distributor_zip'    => $item['zip'],
      );
      $result->items[$i]['fulladdress'] = array_filter($fulladdress);

      // GUID.
      $result->items[$i]['guid'] = $item['name'] . $item['zip'];
    }
  }
}

/**
 * Form constructor for generate where to buy settings form.
 */
function shurtape_tool_admin_where_to_buy_form($form, &$form_state) {
  $form['shurtape_tool_description'] = array(
    '#type' => 'textarea',
    '#title' => t('Where to Buy description'),
    '#default_value' => variable_get('shurtape_tool_description', ''),
  );
  $form['shurtape_tool_api_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Google API key'),
    '#default_value' => variable_get('shurtape_tool_api_key', ''),
  );
  $form['headers'] = array(
    '#type' => 'fieldset',
    '#title' => t('Headers results'),
  );
  $form['tooltips'] = array(
    '#type' => 'fieldset',
    '#title' => t('Tooltip settings'),
  );
  if ($field = field_info_field('field_distributor_type')) {
    foreach ($field['settings']['allowed_values'] as $key => $name) {
      $form['headers']["shurtape_tool_{$key}_headers"] = array(
        '#type' => 'textarea',
        '#title' => t('@name individual distributors', array('@name' => $name)),
        '#default_value' => variable_get("shurtape_tool_{$key}_headers", ''),
        '#description' => t('Enter the node title. Each record is located on a separate line, delimited by a line break.'),
      );
      $form['tooltips']["shurtape_tool_{$key}_tooltip"] = array(
        '#type' => 'textarea',
        '#title' => t('@name tooltip', array('@name' => $name)),
        '#default_value' => variable_get("shurtape_tool_{$key}_tooltip", ''),
      );
    }
  }

  return system_settings_form($form);
}

/**
 * Form constructor for generate where to buy page.
 */
function shurtape_tool_where_to_buy_form($form, &$form_state) {
  $form['title']['#markup'] = '<h1 class="title" id="page-title">' . t('Where to Buy') . '</h1>';
  $form['description'] = array(
    '#prefix' => '<div class="shurtape-help-text">',
    '#markup' => variable_get('shurtape_tool_description', ''),
    '#suffix' => '</div>',
  );
  $form['filters'] = array(
    '#type' => 'fieldset',
    '#process' => array('ctools_dependent_process'),
  );
  // Get types.
  $field = field_info_field('field_distributor_type');
  $form['filters']['type'] = array(
    '#type' => 'radios',
    '#title' => t('Find a Location'),
    '#options' => $field['settings']['allowed_values'],
    '#process' => array('shurtape_tool_form_process_radios'),
  );
  // Generate Industrial filters.
  $form['filters']['purchase'] = array(
    '#type' => 'radios',
    '#title' => t('Purchase'),
    '#title_display' => 'invisible',
    '#options' => array(
      'roll' => t('Purchase by Roll'),
      'case' => t('Purchase by Case'),
    ),
    '#default_value' => 'roll',
    '#states' => array(
      'visible' => array(
        ':input[name="type"]' => array('value' => 'industrial'),
      ),
    ),
  );
  $form['filters']['geodata'] = array(
    '#type' => 'container',
    '#states' => array(
      'visible' => array(
        array(':input[name="type"]' => array('value' => 'hvac')),
        array(':input[name="purchase"]' => array('value' => 'case')),
      ),
    ),
    '#weight' => 10,
  );
  // Generate HVAC filters.
  $form['filters']['geodata']['zip-code'] = array(
    '#type' => 'textfield',
    '#title' => t('Zip Code'),
    '#title_display' => 'invisible',
    '#attributes' => array('placeholder' => t('Enter Zip Code')),
    '#states' => array(
      'enabled' => array(
        array(':input[name="type"]' => array('value' => 'hvac')),
        array(':input[name="purchase"]' => array('value' => 'case')),
      ),
    )
  );
  $form['filters']['geodata']['radius'] = array(
    '#type' => 'select',
    '#title' => t('Radius'),
    '#title_display' => 'invisible',
    '#options' => array(
      25 => t('25 Miles'),
      50 => t('50 Miles'),
      100 => t('100 Miles'),
      200 => t('200 Miles'),
    ),
    '#default_value' => 50,
  );
  $form['filters']['actions'] = array(
    '#type' => 'actions',
    '#weight' => 9,
  );
  $form['filters']['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Search'),
    '#states' => array(
      'visible' => array(
        ':input[name="type"]' => array('checked' => TRUE),
      ),
    ),
  );
  // Generate search results.
  $form['results-wrapper'] = array(
    '#type' => 'fieldset',
    '#title' => t('Search Results'),
    '#process' => array('ctools_dependent_process'),
  );
  $form['results-wrapper']['results'] = array(
    '#type' => 'fieldset',
    '#process' => array('ctools_dependent_process'),
  );
  $form['results-wrapper']['results']['headers'] = array(
    '#prefix' => '<div id="header-results">',
    '#suffix' => '</div>',
  );
  $form['results-wrapper']['results']['companies'] = array(
    '#prefix' => '<div id="view-companies">',
    '#suffix' => '</div>',
  );
  $form['results-wrapper']['googlemap'] = array(
    '#type' => 'item',
    '#markup' => '<div id="map_canvas"></div>',
    '#states' => array(
      'invisible' => array(
        ':input[name="type"]' => array('value' => 'industrial'),
      ),
    ),
  );
  $form['#attached']['css'][] = drupal_get_path('module', 'shurtape_tool') . '/css/shurtape_tool.css';
  $form['#attached']['js'][] = array(
    'type' => 'external',
    'data' => '//maps.googleapis.com/maps/api/js?sensor=false&language=en',
  );
  $form['#attached']['js'][] = drupal_get_path('module', 'shurtape_tool') . '/js/shurtape_tool.js';

  return $form;
}

/**
 * Expands a radios element into individual radio elements.
 */
function shurtape_tool_form_process_radios($element) {
  if (count($element['#options']) > 0) {
    $weight = 0;
    foreach ($element['#options'] as $key => $choice) {
      // Maintain order of options as defined in #options, in case the element
      // defines custom option sub-elements, but does not define all option
      // sub-elements.
      $weight += 0.001;

      $element += array($key => array());
      // Generate the parents as the autogenerator does, so we will have a
      // unique id for each radio button.
      $parents_for_id = array_merge($element['#parents'], array($key));
      $element[$key] += array(
        '#type' => 'radio',
        '#title' => $choice,

        // The key is sanitized in drupal_attributes() during output from the
        // theme function.
        '#return_value' => $key,

        // Use default or FALSE. A value of FALSE means that the radio button is
        // not 'checked'.
        '#default_value' => isset($element['#default_value']) ? $element['#default_value'] : FALSE,
        '#attributes' => $element['#attributes'] + array('title' => variable_get("shurtape_tool_{$key}_tooltip", '')),
        '#parents' => $element['#parents'],
        '#id' => drupal_html_id('edit-' . implode('-', $parents_for_id)),
        '#ajax' => isset($element['#ajax']) ? $element['#ajax'] : NULL,
        '#weight' => $weight,
      );
    }
  }
  return $element;
}

/**
 * Page callback for generate where to buy search results.
 */
function shurtape_tool_where_to_buy_get_results() {

  $output = array(
    'data'    => array('path' => drupal_get_path('module', 'shurtape_tool')),
    'headers' => array(),
    'results' => t('No results found.'),
  );

  if (!empty($_POST['form_id']) && $_POST['form_id'] == 'shurtape_tool_where_to_buy_form') {

    $page = isset($_GET['page']) ? $_GET['page'] : 0;
    $headers = ($_POST['type'] == 'hvac')
      ? variable_get('shurtape_tool_hvac_headers')
      : variable_get('shurtape_tool_industrial_headers');

    if ($headers) {
      $output['headers'] = shurtape_tool_get_header_nodes(explode("\n", $headers));
    }

    if ($results = shurtape_tool_get_nodes($_POST['type'], $_POST['zip-code'], $_POST['radius'], $page)) {
      $output['results'] = $results;
      $output['data']['pager'] = theme('pager');
    }

    $output['data']['map'] = ($_POST['type'] == 'hvac');
  }

  return $output;
}

/**
 * Helper function for get nodes by zip and radius.
 */
function shurtape_tool_get_nodes($type = 'hvac', $zip, $radius, $page = 0) {
  $results = array();
  if ($raw = shurtape_tool_geocode($zip)) {
    $zip_center = $raw['results'][0]['geometry']['location'];
    $zip_center['lat'] *=1.00001;
    $zip_center['lon'] *=1.00001;
    $zip_center['sin'] = sin(deg2rad($zip_center['lat']));
    $zip_center['cos'] = cos(deg2rad($zip_center['lat']));
    $query = db_select('node', 'n')
      ->condition('n.status', 1)
      ->condition('n.type', 'distributor')
      ->extend('PagerDefault')
      ->limit(10);
    $query->join('field_data_field_latlng', 'latlng', 'latlng.entity_id = n.nid');
    $query->join('field_data_field_formatted_address', 'fad', 'fad.entity_id = n.nid');
    $query->join('field_data_field_phone_number', 'phone', 'phone.entity_id = n.nid');
    $query->join('field_data_field_industrial_partner', 'partner', 'partner.entity_id = n.nid');
    $query->join('field_data_field_distributor_type', 'type', 'type.entity_id = n.nid');
    $query->leftJoin('field_data_field_website_url', 'url', 'url.entity_id = n.nid');
    $query->addField('latlng', 'field_latlng_lat', 'lat');
    $query->addField('latlng', 'field_latlng_lng', 'lng');
    $query->addField('fad', 'field_formatted_address_value', 'address');
    $query->addField('phone', 'field_phone_number_value', 'phone');
    $query->addField('url', 'field_website_url_url', 'url');
    $query->addField('partner', 'field_industrial_partner_value', 'partner');
    $query->addExpression('(DEGREES(ACOS((:zip_lat_sin * latlng.field_latlng_lat_sin) + (:zip_lat_cos * latlng.field_latlng_lat_cos * COS(RADIANS(:zip_lng - latlng.field_latlng_lng))))) * 69.09)', 'radius', array(
      ':zip_lng' => $zip_center['lng'],
      ':zip_lat_sin' => $zip_center['sin'],
      ':zip_lat_cos' => $zip_center['cos'],
      ':radius' => $radius,
    ));
    $query->condition('type.field_distributor_type_value', $type);
    $query->having('radius <= :radius', array(':radius' => $radius));
    $query->fields('n', array('nid', 'title'));
    $results = $query
      ->orderBy('partner.field_industrial_partner_value', 'DESC')
      ->orderBy('radius', 'ASC')
      ->execute()
      ->fetchAll();

    $indx = $page * 10;

    foreach ($results as $key => $result) {
      $result->id = ($key + 1) + $indx;
    }
  }

  return $results;
}

/**
 * Helper functions for get header results.
 */
function shurtape_tool_get_header_nodes($titles = array()) {

  if (empty($titles)) {
    return FALSE;
  }

  $results = array();
  $titles = array_map(function ($value) {
    return trim($value);
  }, $titles);

  foreach ($titles as $key => $title) {
    $query = db_select('node', 'n')
      ->condition('n.status', 1)
      ->condition('n.type', 'distributor')
      ->condition('n.title', $title);
    $query->leftJoin('field_data_field_phone_number', 'phone', 'phone.entity_id = n.nid');
    $query->leftJoin('field_data_field_website_url', 'url', 'url.entity_id = n.nid');
    $query->leftJoin('field_data_field_formatted_address', 'address', 'address.entity_id = n.nid');
    $query->addField('phone', 'field_phone_number_value', 'phone');
    $query->addField('url', 'field_website_url_url', 'url');
    $query->addField('address', 'field_formatted_address_value', 'address');
    $query->fields('n', array('nid', 'title'));
    if ($result = $query->execute()->fetchAll()) {
      $results[] = reset($result);
    }
  }

  return $results;
}

/**
 * Helper function for geocoding by address.
 */
function shurtape_tool_geocode($address) {

  if (empty($address)) {
    return FALSE;
  }

  if (!is_array($address)) {
    $address = array($address);
  }

  array_push($address, 'USA');

  $url = url('https://maps.googleapis.com/maps/api/geocode/json', array('query' => array(
    'address' => implode(', ', $address),
    'sensor'  => 'false',
    'key'     => variable_get('shurtape_tool_api_key'),
  )));

  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL, $url);
  curl_setopt($ch, CURLOPT_HEADER, 0);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
  $data = drupal_json_decode(curl_exec($ch));
  curl_close($ch);

  if ($data['status'] != 'OK') {
    drupal_add_http_header('debug', drupal_json_encode($data));
    return FALSE;
  }

  return $data;
}
