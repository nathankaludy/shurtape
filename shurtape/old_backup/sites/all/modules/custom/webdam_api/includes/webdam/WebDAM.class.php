<?php

include 'HTTP/Request2.php';

class WebDAM {
	private $apiKey;
	private $username;
	private $password;
	private $apiUrl = 'http://api.webdamdb.com/api.php'; # No trailing slash

	/**
	 * Constructor
	 */
	public function __construct($apiKey = '', $username = '', $password = null) {
		if ($apiKey != '') {
			$this->setApiKey($apiKey);
		}
		if ($username != '') {
			$this->setUsername($username);
		}
		if ($password) {
			$this->setPassword($password);
		}
	}

	/**
	 * Sets the API Key
	 *
	 * @param  string  the API key
	 */
	public function setApiKey($apiKey) {
		$this->apiKey = $apiKey;
		return TRUE;
	}

	/**
	 * Sets the API Username
	 *
	 * @param  string  the API Username
	 */
	public function setUsername($username) {
		$this->username = $username;
		return TRUE;
	}

	/**
	 * Sets the API Password
	 *
	 * @param  string  the API Password
	 */
	public function setPassword($password) {
		$this->password = $password;
		return TRUE;
	}

	public function authenticate() {
		if ($this->apiKey == '' || $this->username == '' || $this->password == '') {
			throw new Exception('The API Key and/or Credentials have not been set.');
		}
		$request = new HTTP_Request2($this->apiUrl, HTTP_Request2::METHOD_POST);
		$request->addPostParameter(array('method' => "login", 'username' => $this->username, 'password' => $this->password, 'api_key' => $this->apiKey));
		try {
			$response = $request->send();
			if (200 == $response->getStatus()) {
				$responseOnj = new SimpleXMLElement($response->getBody());
				if ($responseOnj->status == "success"){
					$apitoken = (string)$responseOnj->apitoken;
				} else{
					throw new Exception('Error: Failed to login to WebDAM');
				}
			} else {
				throw new Exception('Unexpected HTTP status: ' . $response->getStatus() . ' ' . $response->getReasonPhrase());
			}
		} catch (HTTP_Request2_Exception $e) {
			throw new Exception('Error: ' . $e->getMessage());
		}
		return $apitoken;
	}
	
	public function searchProductAssets($apitoken, $pName, $term, $type = ""){
		$arr = array();
		if ($apitoken == '' || $pName == '' || $term == '') {
			throw new Exception('The API Token, Procuct Name and/or search term has not been set.');
		}
		$request = new HTTP_Request2($this->apiUrl, HTTP_Request2::METHOD_POST);
		$request->addPostParameter(array('method' => "search", 'query' => $term, 'field' => "all", 'api_key' => $this->apiKey, 'apitoken' => $apitoken));
		try {
			$response = $request->send();
			if (200 == $response->getStatus()) {
				$responseOnj = new SimpleXMLElement($response->getBody());
				if (isset($responseOnj->total) && intval($responseOnj->total) > 0){
					foreach($responseOnj->assets->asset as $asset){
						array_push($arr, new ProductAsset($asset->id, $asset->status, $asset->userid,
										$asset->folderid, $asset->filename, $asset->name, $asset->size,
										$asset->fileextension, $asset->width, $asset->height,
										$asset->embededlink, $asset->expirationdate,
										$asset->datecreated, $asset->description));
					}
				} else {
					throw new Exception('Warning: No files were found for ' . $pName . ' on term "' . $term . '"');
				}
			} else {
				throw new Exception('Unexpected HTTP status: ' . $response->getStatus() . ' ' . $response->getReasonPhrase());
			}
		} catch (HTTP_Request2_Exception $e) {
			throw new Exception('Error: ' . $e->getMessage());
		}
		return $arr;
	}
	
	public function remoteFileExists($url = ''){
		$curl = curl_init($url);
	
	    //don't fetch the actual page, you only want to check the connection is ok
	    curl_setopt($curl, CURLOPT_NOBODY, true);
	
	    //do request
	    $result = curl_exec($curl);
	
	    $ret = false;
	
	    //if request did not fail
	    if ($result !== false) {
	        //if request was ok, check response code
	        $statusCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);  
	
	        if ($statusCode == 200) {
	            $ret = true;   
	        }
	    }
	
	    curl_close($curl);
	
	    return $ret;
	}
	
	public function getAssetDownloadUrlById($apitoken, $id){
		return  $this->apiUrl . "?" . http_build_query(array('method' => "downloadasset", 'assetid' => $id, 'api_key' => $this->apiKey, 'apitoken' => $apitoken));
	}
	
	public function downloadAssetByID($apitoken, $id){
		if ($apitoken == '' || $id == '') {
			throw new Exception('The API Token and/or Asset ID term has not been set.');
		}
		if ($this->remoteFileExists($this->getAssetDownloadUrlById($apitoken, $id))){
			return file_get_contents($this->getAssetDownloadUrlById($apitoken, $id));
		} else {
			throw new Exception('Error: Failed to download asset ID: '. $id .' from WebDAM');
		}
	}
}
