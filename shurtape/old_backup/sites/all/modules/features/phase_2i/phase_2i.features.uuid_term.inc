<?php
/**
 * @file
 * phase_2i.features.uuid_term.inc
 */

/**
 * Implements hook_uuid_features_default_terms().
 */
function phase_2i_uuid_features_default_terms() {
  $terms = array();

  $terms[] = array(
    'name' => 'Mexico',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 5,
    'uuid' => '21b179c7-4296-4af6-a485-766d41e8001d',
    'vocabulary_machine_name' => 'location_header',
    'metatags' => array(),
  );
  $terms[] = array(
    'name' => 'Distribution Centers',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 2,
    'uuid' => '6eb1c736-91bc-43f3-a876-45b7191dbdf2',
    'vocabulary_machine_name' => 'location_category',
    'metatags' => array(),
  );
  $terms[] = array(
    'name' => 'Corporate Headquarters',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 1,
    'uuid' => '79053a2b-205d-4121-ab32-67d361923b48',
    'vocabulary_machine_name' => 'location_header',
    'metatags' => array(),
  );
  $terms[] = array(
    'name' => 'Sales Offices',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 1,
    'uuid' => '97ba9a74-4864-4381-8c07-3c081b44eea3',
    'vocabulary_machine_name' => 'location_category',
    'metatags' => array(),
  );
  $terms[] = array(
    'name' => 'United States',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 3,
    'uuid' => 'a716276d-58ad-4757-9d43-b7532f7bcbc0',
    'vocabulary_machine_name' => 'location_header',
    'metatags' => array(),
  );
  $terms[] = array(
    'name' => 'Headquarters',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => 'ce887f1d-17d5-4f82-a833-391b1aacbb3c',
    'vocabulary_machine_name' => 'location_category',
    'metatags' => array(),
  );
  $terms[] = array(
    'name' => 'South America',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 6,
    'uuid' => 'e80b833c-3adb-45af-9423-20955c406db0',
    'vocabulary_machine_name' => 'location_header',
    'metatags' => array(),
  );
  return $terms;
}
