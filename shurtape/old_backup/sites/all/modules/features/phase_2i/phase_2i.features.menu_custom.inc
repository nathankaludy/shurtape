<?php
/**
 * @file
 * phase_2i.features.menu_custom.inc
 */

/**
 * Implements hook_menu_default_menu_custom().
 */
function phase_2i_menu_default_menu_custom() {
  $menus = array();

  // Exported menu: menu-products-footer-menu.
  $menus['menu-products-footer-menu'] = array(
    'menu_name' => 'menu-products-footer-menu',
    'title' => 'Products footer menu',
    'description' => '',
  );
  // Exported menu: menu-resources-footer-menu.
  $menus['menu-resources-footer-menu'] = array(
    'menu_name' => 'menu-resources-footer-menu',
    'title' => 'Resources footer menu',
    'description' => '',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Products footer menu');
  t('Resources footer menu');


  return $menus;
}
