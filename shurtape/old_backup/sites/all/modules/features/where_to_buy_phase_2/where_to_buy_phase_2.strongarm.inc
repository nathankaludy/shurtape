<?php
/**
 * @file
 * where_to_buy_phase_2.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function where_to_buy_phase_2_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'additional_settings__active_tab_distributor';
  $strongarm->value = 'edit-location-settings';
  $export['additional_settings__active_tab_distributor'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_anonymous_distributor';
  $strongarm->value = 0;
  $export['comment_anonymous_distributor'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_default_mode_distributor';
  $strongarm->value = 1;
  $export['comment_default_mode_distributor'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_default_per_page_distributor';
  $strongarm->value = '50';
  $export['comment_default_per_page_distributor'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_distributor';
  $strongarm->value = '1';
  $export['comment_distributor'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_form_location_distributor';
  $strongarm->value = 1;
  $export['comment_form_location_distributor'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_preview_distributor';
  $strongarm->value = '0';
  $export['comment_preview_distributor'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_subject_field_distributor';
  $strongarm->value = 1;
  $export['comment_subject_field_distributor'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'field_bundle_settings_node__distributor';
  $strongarm->value = array(
    'view_modes' => array(
      'teaser' => array(
        'custom_settings' => TRUE,
      ),
      'full' => array(
        'custom_settings' => FALSE,
      ),
      'rss' => array(
        'custom_settings' => FALSE,
      ),
      'search_index' => array(
        'custom_settings' => FALSE,
      ),
      'search_result' => array(
        'custom_settings' => FALSE,
      ),
      'token' => array(
        'custom_settings' => FALSE,
      ),
    ),
    'extra_fields' => array(
      'form' => array(
        'locations' => array(
          'weight' => '11',
        ),
        'metatags' => array(
          'weight' => '12',
        ),
        'title' => array(
          'weight' => '0',
        ),
        'path' => array(
          'weight' => '10',
        ),
        'redirect' => array(
          'weight' => '9',
        ),
      ),
      'display' => array(),
    ),
  );
  $export['field_bundle_settings_node__distributor'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'menu_options_distributor';
  $strongarm->value = array(
    0 => 'main-menu',
  );
  $export['menu_options_distributor'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'menu_parent_distributor';
  $strongarm->value = 'main-menu:0';
  $export['menu_parent_distributor'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_options_distributor';
  $strongarm->value = array(
    0 => 'status',
  );
  $export['node_options_distributor'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_preview_distributor';
  $strongarm->value = '0';
  $export['node_preview_distributor'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_submitted_distributor';
  $strongarm->value = 0;
  $export['node_submitted_distributor'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'shurtape_tool_api_key';
  $strongarm->value = 'AIzaSyCnvDUUkMMtansn9T9HFemI7w-7vnLweZk';
  $export['shurtape_tool_api_key'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'shurtape_tool_description';
  $strongarm->value = 'Thank you for your interest in Shurtape. In order to help us locate a distributor that meets your needs, please tell us more about what type of tape you\'re looking for by completing the steps below. ';
  $export['shurtape_tool_description'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'shurtape_tool_hvac_headers';
  $strongarm->value = 'Ferguson
Johnstone Supply
Lennox
';
  $export['shurtape_tool_hvac_headers'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'shurtape_tool_hvac_tooltip';
  $strongarm->value = 'Select for products used in heating, ventilating, and air-conditioning applications. These include foil, film and cloth products designed for this purpose.  Normally used by contractors/installers.
';
  $export['shurtape_tool_hvac_tooltip'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'shurtape_tool_industrial_headers';
  $strongarm->value = 'Grainger
Home Depot Supply
';
  $export['shurtape_tool_industrial_headers'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'shurtape_tool_industrial_tooltip';
  $strongarm->value = 'Select for products used for packaging, repair, color coding, construction, painting or other general applications. These include foil, film, cloth, paper, and double faced products designed for many applications.  
';
  $export['shurtape_tool_industrial_tooltip'] = $strongarm;

  return $export;
}
