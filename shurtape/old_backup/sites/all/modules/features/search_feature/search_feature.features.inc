<?php
/**
 * @file
 * search_feature.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function search_feature_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "facetapi" && $api == "facetapi_defaults") {
    return array("version" => "1");
  }
  if ($module == "page_manager" && $api == "pages_default") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function search_feature_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_image_default_styles().
 */
function search_feature_image_default_styles() {
  $styles = array();

  // Exported image style: search-product-results.
  $styles['search-product-results'] = array(
    'name' => 'search-product-results',
    'label' => 'Search-Product-Results',
    'effects' => array(
      3 => array(
        'label' => 'Scale',
        'help' => 'Scaling will maintain the aspect-ratio of the original image. If only a single dimension is specified, the other dimension will be calculated.',
        'effect callback' => 'image_scale_effect',
        'dimensions callback' => 'image_scale_dimensions',
        'form callback' => 'image_scale_form',
        'summary theme' => 'image_scale_summary',
        'module' => 'image',
        'name' => 'image_scale',
        'data' => array(
          'width' => 110,
          'height' => '',
          'upscale' => 0,
        ),
        'weight' => 2,
      ),
    ),
  );

  // Exported image style: search-technical-results.
  $styles['search-technical-results'] = array(
    'name' => 'search-technical-results',
    'label' => 'Search-Technical-Results',
    'effects' => array(
      4 => array(
        'label' => 'Scale',
        'help' => 'Scaling will maintain the aspect-ratio of the original image. If only a single dimension is specified, the other dimension will be calculated.',
        'effect callback' => 'image_scale_effect',
        'dimensions callback' => 'image_scale_dimensions',
        'form callback' => 'image_scale_form',
        'summary theme' => 'image_scale_summary',
        'module' => 'image',
        'name' => 'image_scale',
        'data' => array(
          'width' => 110,
          'height' => '',
          'upscale' => 0,
        ),
        'weight' => 2,
      ),
    ),
  );

  return $styles;
}

/**
 * Implements hook_default_search_api_index().
 */
function search_feature_default_search_api_index() {
  $items = array();
  $items['search_server_index'] = entity_import('search_api_index', '{
    "name" : "Search Server Index",
    "machine_name" : "search_server_index",
    "description" : null,
    "server" : "db_server_new",
    "item_type" : "node",
    "options" : {
      "index_directly" : 0,
      "cron_limit" : "50",
      "fields" : {
        "nid" : { "type" : "integer" },
        "type" : { "type" : "string" },
        "title" : { "type" : "text", "boost" : "0.1" },
        "title_field" : { "type" : "string" },
        "field_product_meta_description" : { "type" : "text" },
        "field_product_adhesive" : { "type" : "integer", "entity_type" : "taxonomy_term" },
        "field_product_backing" : { "type" : "integer", "entity_type" : "taxonomy_term" },
        "field_product_liner" : { "type" : "integer", "entity_type" : "taxonomy_term" },
        "field_is_linered" : { "type" : "boolean" },
        "field_is_carpet_tape" : { "type" : "boolean" },
        "field_is_uv_resistant" : { "type" : "boolean" },
        "field_is_waterproof" : { "type" : "boolean" },
        "field_is_green_point_certified" : { "type" : "boolean" },
        "field_box_weight" : { "type" : "integer", "entity_type" : "taxonomy_term" },
        "field_handling_conditions" : { "type" : "integer", "entity_type" : "taxonomy_term" },
        "field_holding_power" : { "type" : "integer" },
        "field_is_cold_temp_tape" : { "type" : "boolean" },
        "field_is_ul_listed" : { "type" : "boolean" },
        "field_is_ul_723_tested" : { "type" : "boolean" },
        "field_is_fda_compliant" : { "type" : "boolean" },
        "field_product_videos" : { "type" : "list\\u003Ctext\\u003E" },
        "field_product_type" : { "type" : "list\\u003Cinteger\\u003E", "entity_type" : "taxonomy_term" },
        "field_product_assets" : { "type" : "list\\u003Cinteger\\u003E", "entity_type" : "node" },
        "field_product_images" : { "type" : "list\\u003Cinteger\\u003E", "entity_type" : "node" },
        "field_sizes_colors" : {
          "type" : "list\\u003Cinteger\\u003E",
          "entity_type" : "field_collection_item"
        },
        "field_physical_properties" : {
          "type" : "list\\u003Cinteger\\u003E",
          "entity_type" : "field_collection_item"
        },
        "field_product_clean_removal_days" : { "type" : "integer", "entity_type" : "taxonomy_term" },
        "field_product_market_list" : { "type" : "list\\u003Cinteger\\u003E", "entity_type" : "taxonomy_term" },
        "field_industry_link_group" : { "type" : "integer", "entity_type" : "taxonomy_term" },
        "search_api_language" : { "type" : "string" },
        "body:value" : { "type" : "text" },
        "field_product_type:tid" : { "type" : "list\\u003Cinteger\\u003E" },
        "field_product_type:name" : { "type" : "list\\u003Ctext\\u003E" },
        "field_product_type:description" : { "type" : "list\\u003Ctext\\u003E" },
        "field_product_type:weight" : { "type" : "list\\u003Cinteger\\u003E" },
        "field_product_type:node_count" : { "type" : "list\\u003Cinteger\\u003E" },
        "field_product_type:url" : { "type" : "list\\u003Curi\\u003E" },
        "field_product_type:vocabulary" : {
          "type" : "list\\u003Cinteger\\u003E",
          "entity_type" : "taxonomy_vocabulary"
        },
        "field_product_type:parent" : {
          "type" : "list\\u003Clist\\u003Cinteger\\u003E\\u003E",
          "entity_type" : "taxonomy_term"
        },
        "field_product_type:parents_all" : {
          "type" : "list\\u003Clist\\u003Cinteger\\u003E\\u003E",
          "entity_type" : "taxonomy_term"
        },
        "field_product_short_description:value" : { "type" : "text" },
        "field_product_market_list:parents_all" : {
          "type" : "list\\u003Clist\\u003Cinteger\\u003E\\u003E",
          "entity_type" : "taxonomy_term"
        }
      },
      "data_alter_callbacks" : {
        "search_api_alter_bundle_filter" : {
          "status" : 0,
          "weight" : "-10",
          "settings" : { "default" : "1", "bundles" : [] }
        },
        "search_api_alter_node_access" : { "status" : 0, "weight" : "0", "settings" : [] },
        "search_api_alter_node_status" : { "status" : 0, "weight" : "0", "settings" : [] },
        "search_api_alter_add_hierarchy" : {
          "status" : 1,
          "weight" : "0",
          "settings" : { "fields" : {
              "field_product_type:parent" : "field_product_type:parent",
              "field_product_type:parents_all" : "field_product_type:parents_all"
            }
          }
        },
        "search_api_alter_add_viewed_entity" : { "status" : 0, "weight" : "0", "settings" : { "mode" : "full" } },
        "search_api_alter_add_url" : { "status" : 0, "weight" : "0", "settings" : [] },
        "search_api_alter_add_aggregation" : { "status" : 0, "weight" : "0", "settings" : [] }
      },
      "processors" : {
        "search_api_case_ignore" : {
          "status" : 1,
          "weight" : "0",
          "settings" : { "fields" : {
              "title" : true,
              "title_field" : true,
              "field_product_meta_description" : true,
              "field_product_type:name" : true,
              "field_product_type:description" : true,
              "field_product_short_description:value" : true
            }
          }
        },
        "search_api_html_filter" : {
          "status" : 0,
          "weight" : "10",
          "settings" : {
            "fields" : { "title" : true },
            "title" : 0,
            "alt" : 1,
            "tags" : "h1 = 5\\r\\nh2 = 3\\r\\nh3 = 2\\r\\nstrong = 2\\r\\nb = 2\\r\\nem = 1.5\\r\\nu = 1.5"
          }
        },
        "search_api_tokenizer" : {
          "status" : 0,
          "weight" : "20",
          "settings" : {
            "fields" : { "title" : true },
            "spaces" : "[^[:alnum:]]",
            "ignorable" : "[\\u0027]"
          }
        },
        "search_api_stopwords" : {
          "status" : 0,
          "weight" : "30",
          "settings" : {
            "fields" : { "title" : true },
            "file" : "",
            "stopwords" : "but\\r\\ndid\\r\\nthe this that those\\r\\netc"
          }
        },
        "search_api_highlighting" : {
          "status" : 0,
          "weight" : "35",
          "settings" : {
            "prefix" : "\\u003Cstrong\\u003E",
            "suffix" : "\\u003C\\/strong\\u003E",
            "excerpt" : 1,
            "excerpt_length" : "256",
            "highlight" : "always"
          }
        }
      }
    },
    "enabled" : "1",
    "read_only" : "0",
    "rdf_mapping" : []
  }');
  return $items;
}

/**
 * Implements hook_default_search_api_server().
 */
function search_feature_default_search_api_server() {
  $items = array();
  $items['db_server_new'] = entity_import('search_api_server', '{
    "name" : "DB Server New",
    "machine_name" : "db_server_new",
    "description" : "",
    "class" : "search_api_db_service",
    "options" : {
      "database" : "default:default",
      "min_chars" : "2",
      "indexes" : { "search_server_index" : {
          "nid" : {
            "table" : "search_api_db_search_server_index",
            "column" : "nid",
            "type" : "integer",
            "boost" : "1.0"
          },
          "type" : {
            "table" : "search_api_db_search_server_index",
            "column" : "type",
            "type" : "string",
            "boost" : "1.0"
          },
          "title" : {
            "table" : "search_api_db_search_server_index_text_2",
            "type" : "text",
            "boost" : "0.1"
          },
          "title_field" : {
            "table" : "search_api_db_search_server_index",
            "column" : "title_field",
            "type" : "string",
            "boost" : "1.0"
          },
          "field_product_meta_description" : {
            "table" : "search_api_db_search_server_index_text_2",
            "type" : "text",
            "boost" : "1.0"
          },
          "field_product_adhesive" : {
            "table" : "search_api_db_search_server_index",
            "column" : "field_product_adhesive",
            "type" : "integer",
            "boost" : "1.0"
          },
          "field_product_backing" : {
            "table" : "search_api_db_search_server_index",
            "column" : "field_product_backing",
            "type" : "integer",
            "boost" : "1.0"
          },
          "field_product_liner" : {
            "table" : "search_api_db_search_server_index",
            "column" : "field_product_liner",
            "type" : "integer",
            "boost" : "1.0"
          },
          "field_box_weight" : {
            "table" : "search_api_db_search_server_index",
            "column" : "field_box_weight",
            "type" : "integer",
            "boost" : "1.0"
          },
          "field_handling_conditions" : {
            "table" : "search_api_db_search_server_index",
            "column" : "field_handling_conditions",
            "type" : "integer",
            "boost" : "1.0"
          },
          "field_holding_power" : {
            "table" : "search_api_db_search_server_index",
            "column" : "field_holding_power",
            "type" : "integer",
            "boost" : "1.0"
          },
          "field_product_videos" : {
            "table" : "search_api_db_search_server_index_text_2",
            "type" : "list\\u003Ctext\\u003E",
            "boost" : "1.0"
          },
          "field_product_type" : {
            "table" : "search_api_db_search_server_index_field_product_type",
            "column" : "value",
            "type" : "list\\u003Cinteger\\u003E",
            "boost" : "1.0"
          },
          "field_product_assets" : {
            "table" : "search_api_db_search_server_index_field_product_assets",
            "column" : "value",
            "type" : "list\\u003Cinteger\\u003E",
            "boost" : "1.0"
          },
          "field_product_images" : {
            "table" : "search_api_db_search_server_index_field_product_images",
            "column" : "value",
            "type" : "list\\u003Cinteger\\u003E",
            "boost" : "1.0"
          },
          "field_sizes_colors" : {
            "table" : "search_api_db_search_server_index_field_sizes_colors",
            "column" : "value",
            "type" : "list\\u003Cinteger\\u003E",
            "boost" : "1.0"
          },
          "field_physical_properties" : {
            "table" : "search_api_db_search_server_index_field_physical_properties",
            "column" : "value",
            "type" : "list\\u003Cinteger\\u003E",
            "boost" : "1.0"
          },
          "field_product_market_list" : {
            "table" : "search_api_db_search_server_index_field_product_market_list",
            "column" : "value",
            "type" : "list\\u003Cinteger\\u003E",
            "boost" : "1.0"
          },
          "field_industry_link_group" : {
            "table" : "search_api_db_search_server_index",
            "column" : "field_industry_link_group",
            "type" : "integer",
            "boost" : "1.0"
          },
          "search_api_language" : {
            "table" : "search_api_db_search_server_index",
            "column" : "search_api_language",
            "type" : "string",
            "boost" : "1.0"
          },
          "body:value" : {
            "table" : "search_api_db_search_server_index_text_2",
            "type" : "text",
            "boost" : "1.0"
          },
          "field_product_type:tid" : {
            "table" : "search_api_db_search_server_index_field_product_type_tid",
            "column" : "value",
            "type" : "list\\u003Cinteger\\u003E",
            "boost" : "1.0"
          },
          "field_product_type:name" : {
            "table" : "search_api_db_search_server_index_text_2",
            "type" : "list\\u003Ctext\\u003E",
            "boost" : "1.0"
          },
          "field_product_type:description" : {
            "table" : "search_api_db_search_server_index_text_2",
            "type" : "list\\u003Ctext\\u003E",
            "boost" : "1.0"
          },
          "field_product_type:weight" : {
            "table" : "search_api_db_search_server_index_field_product_type_weight",
            "column" : "value",
            "type" : "list\\u003Cinteger\\u003E",
            "boost" : "1.0"
          },
          "field_product_type:node_count" : {
            "table" : "search_api_db_search_server_index_field_product_type_node_coun",
            "column" : "value",
            "type" : "list\\u003Cinteger\\u003E",
            "boost" : "1.0"
          },
          "field_product_type:url" : {
            "table" : "search_api_db_search_server_index_field_product_type_url",
            "column" : "value",
            "type" : "list\\u003Curi\\u003E",
            "boost" : "1.0"
          },
          "field_product_type:vocabulary" : {
            "table" : "search_api_db_search_server_index_field_product_type_vocabular",
            "column" : "value",
            "type" : "list\\u003Cinteger\\u003E",
            "boost" : "1.0"
          },
          "field_product_type:parent" : {
            "table" : "search_api_db_search_server_index_field_product_type_parent",
            "column" : "value",
            "type" : "list\\u003Clist\\u003Cinteger\\u003E\\u003E",
            "boost" : "1.0"
          },
          "field_product_type:parents_all" : {
            "table" : "search_api_db_search_server_index_field_product_type_parents_a",
            "column" : "value",
            "type" : "list\\u003Clist\\u003Cinteger\\u003E\\u003E",
            "boost" : "1.0"
          },
          "field_product_short_description:value" : {
            "table" : "search_api_db_search_server_index_text_2",
            "type" : "text",
            "boost" : "1.0"
          },
          "field_is_linered" : {
            "table" : "search_api_db_search_server_index",
            "column" : "field_is_linered",
            "type" : "boolean",
            "boost" : "1.0"
          },
          "field_is_carpet_tape" : {
            "table" : "search_api_db_search_server_index",
            "column" : "field_is_carpet_tape",
            "type" : "boolean",
            "boost" : "1.0"
          },
          "field_is_uv_resistant" : {
            "table" : "search_api_db_search_server_index",
            "column" : "field_is_uv_resistant",
            "type" : "boolean",
            "boost" : "1.0"
          },
          "field_is_waterproof" : {
            "table" : "search_api_db_search_server_index",
            "column" : "field_is_waterproof",
            "type" : "boolean",
            "boost" : "1.0"
          },
          "field_is_green_point_certified" : {
            "table" : "search_api_db_search_server_index",
            "column" : "field_is_green_point_certified",
            "type" : "boolean",
            "boost" : "1.0"
          },
          "field_is_cold_temp_tape" : {
            "table" : "search_api_db_search_server_index",
            "column" : "field_is_cold_temp_tape",
            "type" : "boolean",
            "boost" : "1.0"
          },
          "field_is_ul_listed" : {
            "table" : "search_api_db_search_server_index",
            "column" : "field_is_ul_listed",
            "type" : "boolean",
            "boost" : "1.0"
          },
          "field_is_ul_723_tested" : {
            "table" : "search_api_db_search_server_index",
            "column" : "field_is_ul_723_tested",
            "type" : "boolean",
            "boost" : "1.0"
          },
          "field_is_fda_compliant" : {
            "table" : "search_api_db_search_server_index",
            "column" : "field_is_fda_compliant",
            "type" : "boolean",
            "boost" : "1.0"
          },
          "field_product_clean_removal_days" : {
            "table" : "search_api_db_search_server_index",
            "column" : "field_product_clean_removal_days",
            "type" : "integer",
            "boost" : "1.0"
          },
          "field_product_market_list:parents_all" : {
            "table" : "search_api_db_search_server_index_field_product_market_list_pa",
            "column" : "value",
            "type" : "list\\u003Clist\\u003Cinteger\\u003E\\u003E",
            "boost" : "1.0"
          }
        }
      }
    },
    "enabled" : "1",
    "rdf_mapping" : []
  }');
  return $items;
}
