<?php
/**
 * @file
 * shurtape_where_to_buy.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function shurtape_where_to_buy_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'store_locator';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Store Locator';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Where To Buy';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'input_required';
  $handler->display->display_options['exposed_form']['options']['submit_button'] = 'Search';
  $handler->display->display_options['exposed_form']['options']['text_input_required_format'] = 'filtered_html';
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['pager']['options']['items_per_page'] = '26';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['style_options']['grouping'] = array(
    0 => array(
      'field' => 'field_partner',
      'rendered' => 1,
      'rendered_strip' => 0,
    ),
  );
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['row_options']['inline'] = array(
    'field_city' => 'field_city',
    'field_state' => 'field_state',
    'field_zip' => 'field_zip',
  );
  $handler->display->display_options['row_options']['hide_empty'] = TRUE;
  /* No results behavior: Global: Text area */
  $handler->display->display_options['empty']['area']['id'] = 'area';
  $handler->display->display_options['empty']['area']['table'] = 'views';
  $handler->display->display_options['empty']['area']['field'] = 'area';
  $handler->display->display_options['empty']['area']['empty'] = TRUE;
  $handler->display->display_options['empty']['area']['content'] = '<div class="location_results">
	<div class="clearfix">
			<h2>Results</h2>

			<div class="results">
						<div class= last">
							No Results found, please try again.
						</div>
			</div>
	</div>
</div>';
  $handler->display->display_options['empty']['area']['format'] = 'full_html';
  /* Field: Global: View result counter */
  $handler->display->display_options['fields']['counter']['id'] = 'counter';
  $handler->display->display_options['fields']['counter']['table'] = 'views';
  $handler->display->display_options['fields']['counter']['field'] = 'counter';
  $handler->display->display_options['fields']['counter']['label'] = '';
  $handler->display->display_options['fields']['counter']['exclude'] = TRUE;
  $handler->display->display_options['fields']['counter']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['counter']['counter_start'] = '1';
  /* Field: Content: Distributor Name */
  $handler->display->display_options['fields']['field_distributor_name']['id'] = 'field_distributor_name';
  $handler->display->display_options['fields']['field_distributor_name']['table'] = 'field_data_field_distributor_name';
  $handler->display->display_options['fields']['field_distributor_name']['field'] = 'field_distributor_name';
  $handler->display->display_options['fields']['field_distributor_name']['label'] = '';
  $handler->display->display_options['fields']['field_distributor_name']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_distributor_name']['alter']['text'] = '<strong>[counter] - [field_distributor_name-value]</strong>';
  $handler->display->display_options['fields']['field_distributor_name']['element_label_colon'] = FALSE;
  /* Field: Global: Custom text */
  $handler->display->display_options['fields']['nothing_1']['id'] = 'nothing_1';
  $handler->display->display_options['fields']['nothing_1']['table'] = 'views';
  $handler->display->display_options['fields']['nothing_1']['field'] = 'nothing';
  $handler->display->display_options['fields']['nothing_1']['label'] = '';
  $handler->display->display_options['fields']['nothing_1']['alter']['text'] = '<strong>[counter] - [field_distributor_name]</strong>';
  $handler->display->display_options['fields']['nothing_1']['element_label_colon'] = FALSE;
  /* Field: Content: Distributor Address */
  $handler->display->display_options['fields']['field_distributor_address']['id'] = 'field_distributor_address';
  $handler->display->display_options['fields']['field_distributor_address']['table'] = 'field_data_field_distributor_address';
  $handler->display->display_options['fields']['field_distributor_address']['field'] = 'field_distributor_address';
  $handler->display->display_options['fields']['field_distributor_address']['label'] = '';
  $handler->display->display_options['fields']['field_distributor_address']['element_label_colon'] = FALSE;
  /* Field: Content: City */
  $handler->display->display_options['fields']['field_city']['id'] = 'field_city';
  $handler->display->display_options['fields']['field_city']['table'] = 'field_data_field_city';
  $handler->display->display_options['fields']['field_city']['field'] = 'field_city';
  $handler->display->display_options['fields']['field_city']['label'] = '';
  $handler->display->display_options['fields']['field_city']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_city']['alter']['text'] = '[field_city-value],';
  $handler->display->display_options['fields']['field_city']['element_label_colon'] = FALSE;
  /* Field: Content: State */
  $handler->display->display_options['fields']['field_state']['id'] = 'field_state';
  $handler->display->display_options['fields']['field_state']['table'] = 'field_data_field_state';
  $handler->display->display_options['fields']['field_state']['field'] = 'field_state';
  $handler->display->display_options['fields']['field_state']['label'] = '';
  $handler->display->display_options['fields']['field_state']['element_label_colon'] = FALSE;
  /* Field: Content: Zip */
  $handler->display->display_options['fields']['field_zip']['id'] = 'field_zip';
  $handler->display->display_options['fields']['field_zip']['table'] = 'field_data_field_zip';
  $handler->display->display_options['fields']['field_zip']['field'] = 'field_zip';
  $handler->display->display_options['fields']['field_zip']['label'] = '';
  $handler->display->display_options['fields']['field_zip']['element_label_colon'] = FALSE;
  /* Field: Content: Phone */
  $handler->display->display_options['fields']['field_phone']['id'] = 'field_phone';
  $handler->display->display_options['fields']['field_phone']['table'] = 'field_data_field_phone';
  $handler->display->display_options['fields']['field_phone']['field'] = 'field_phone';
  $handler->display->display_options['fields']['field_phone']['label'] = '';
  $handler->display->display_options['fields']['field_phone']['element_label_colon'] = FALSE;
  /* Field: Content: Website */
  $handler->display->display_options['fields']['field_website']['id'] = 'field_website';
  $handler->display->display_options['fields']['field_website']['table'] = 'field_data_field_website';
  $handler->display->display_options['fields']['field_website']['field'] = 'field_website';
  $handler->display->display_options['fields']['field_website']['label'] = '';
  $handler->display->display_options['fields']['field_website']['alter']['text'] = '<a target="_blank" href="[field_website-value]">[field_website-value]</a>';
  $handler->display->display_options['fields']['field_website']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['field_website']['alter']['path'] = '[field_website-value]';
  $handler->display->display_options['fields']['field_website']['alter']['absolute'] = TRUE;
  $handler->display->display_options['fields']['field_website']['alter']['external'] = TRUE;
  $handler->display->display_options['fields']['field_website']['alter']['link_class'] = 'location-website';
  $handler->display->display_options['fields']['field_website']['alter']['target'] = '_blank';
  $handler->display->display_options['fields']['field_website']['element_label_colon'] = FALSE;
  /* Field: Content: Partner */
  $handler->display->display_options['fields']['field_partner']['id'] = 'field_partner';
  $handler->display->display_options['fields']['field_partner']['table'] = 'field_data_field_partner';
  $handler->display->display_options['fields']['field_partner']['field'] = 'field_partner';
  $handler->display->display_options['fields']['field_partner']['label'] = '';
  $handler->display->display_options['fields']['field_partner']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_partner']['element_label_colon'] = FALSE;
  /* Field: Location: Latitude */
  $handler->display->display_options['fields']['latitude']['id'] = 'latitude';
  $handler->display->display_options['fields']['latitude']['table'] = 'location';
  $handler->display->display_options['fields']['latitude']['field'] = 'latitude';
  $handler->display->display_options['fields']['latitude']['label'] = '';
  $handler->display->display_options['fields']['latitude']['exclude'] = TRUE;
  $handler->display->display_options['fields']['latitude']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['latitude']['style'] = 'dd';
  /* Field: Location: Longitude */
  $handler->display->display_options['fields']['longitude']['id'] = 'longitude';
  $handler->display->display_options['fields']['longitude']['table'] = 'location';
  $handler->display->display_options['fields']['longitude']['field'] = 'longitude';
  $handler->display->display_options['fields']['longitude']['label'] = '';
  $handler->display->display_options['fields']['longitude']['exclude'] = TRUE;
  $handler->display->display_options['fields']['longitude']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['longitude']['style'] = 'dd';
  /* Field: Global: Custom text */
  $handler->display->display_options['fields']['nothing']['id'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['table'] = 'views';
  $handler->display->display_options['fields']['nothing']['field'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['label'] = '';
  $handler->display->display_options['fields']['nothing']['alter']['text'] = '<a href="https://maps.google.com/maps?q=[field_distributor_address]+[field_city],+[field_state]+[field_zip]&sll=[latitude],[longitude]" target="_blank" class="location-directions">Directions</a>';
  $handler->display->display_options['fields']['nothing']['alter']['path'] = 'https://maps.google.com/maps?q=BUTLER+DEARDEN+PAPER+80+SHREWSBURY+ST+RT+140+BOYLSTON,+MASSACHUSETTS+[';
  $handler->display->display_options['fields']['nothing']['alter']['external'] = TRUE;
  $handler->display->display_options['fields']['nothing']['element_label_colon'] = FALSE;
  /* Sort criterion: Content: Partner (field_partner) */
  $handler->display->display_options['sorts']['field_partner_value']['id'] = 'field_partner_value';
  $handler->display->display_options['sorts']['field_partner_value']['table'] = 'field_data_field_partner';
  $handler->display->display_options['sorts']['field_partner_value']['field'] = 'field_partner_value';
  $handler->display->display_options['sorts']['field_partner_value']['order'] = 'DESC';
  /* Sort criterion: Location: Distance / Proximity */
  $handler->display->display_options['sorts']['distance']['id'] = 'distance';
  $handler->display->display_options['sorts']['distance']['table'] = 'location';
  $handler->display->display_options['sorts']['distance']['field'] = 'distance';
  $handler->display->display_options['sorts']['distance']['origin'] = 'tied';
  /* Sort criterion: Content: Distributor Name (field_distributor_name) */
  $handler->display->display_options['sorts']['field_distributor_name_value']['id'] = 'field_distributor_name_value';
  $handler->display->display_options['sorts']['field_distributor_name_value']['table'] = 'field_data_field_distributor_name';
  $handler->display->display_options['sorts']['field_distributor_name_value']['field'] = 'field_distributor_name_value';
  /* Sort criterion: Location: Street */
  $handler->display->display_options['sorts']['street']['id'] = 'street';
  $handler->display->display_options['sorts']['street']['table'] = 'location';
  $handler->display->display_options['sorts']['street']['field'] = 'street';
  $handler->display->display_options['filter_groups']['groups'] = array(
    1 => 'AND',
    2 => 'AND',
  );
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'location' => 'location',
  );
  $handler->display->display_options['filters']['type']['group'] = 1;
  /* Filter criterion: Location: Distance / Proximity */
  $handler->display->display_options['filters']['distance']['id'] = 'distance';
  $handler->display->display_options['filters']['distance']['table'] = 'location';
  $handler->display->display_options['filters']['distance']['field'] = 'distance';
  $handler->display->display_options['filters']['distance']['value'] = array(
    'latitude' => '',
    'longitude' => '',
    'postal_code' => '',
    'country' => 'us',
    'php_code' => '',
    'nid_arg' => '',
    'nid_loc_field' => 'node',
    'uid_arg' => '',
    'search_distance' => '10',
    'search_units' => 'mile',
  );
  $handler->display->display_options['filters']['distance']['group'] = 1;
  $handler->display->display_options['filters']['distance']['exposed'] = TRUE;
  $handler->display->display_options['filters']['distance']['expose']['operator_id'] = 'distance_op';
  $handler->display->display_options['filters']['distance']['expose']['operator'] = 'distance_op';
  $handler->display->display_options['filters']['distance']['expose']['identifier'] = 'distance';
  $handler->display->display_options['filters']['distance']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
    4 => 0,
  );
  $handler->display->display_options['filters']['distance']['expose']['gmap_macro'] = array(
    'default' => '[gmap ]',
  );
  $handler->display->display_options['filters']['distance']['expose']['user_location_choose'] = array(
    'default' => FALSE,
  );
  $handler->display->display_options['filters']['distance']['group_info']['label'] = 'Distance / Proximity';
  $handler->display->display_options['filters']['distance']['group_info']['identifier'] = 'distance';
  $handler->display->display_options['filters']['distance']['group_info']['remember'] = FALSE;
  $handler->display->display_options['filters']['distance']['group_info']['group_items'] = array(
    1 => array(),
    2 => array(),
    3 => array(),
  );
  $handler->display->display_options['filters']['distance']['origin'] = 'postal_default';
  /* Filter criterion: Content: City (field_city) */
  $handler->display->display_options['filters']['field_city_value']['id'] = 'field_city_value';
  $handler->display->display_options['filters']['field_city_value']['table'] = 'field_data_field_city';
  $handler->display->display_options['filters']['field_city_value']['field'] = 'field_city_value';
  $handler->display->display_options['filters']['field_city_value']['group'] = 2;
  $handler->display->display_options['filters']['field_city_value']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_city_value']['expose']['operator_id'] = 'field_city_value_op';
  $handler->display->display_options['filters']['field_city_value']['expose']['label'] = 'City';
  $handler->display->display_options['filters']['field_city_value']['expose']['operator'] = 'field_city_value_op';
  $handler->display->display_options['filters']['field_city_value']['expose']['identifier'] = 'field_city_value';
  $handler->display->display_options['filters']['field_city_value']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
    4 => 0,
  );
  /* Filter criterion: Content: State (field_state) */
  $handler->display->display_options['filters']['field_state_value']['id'] = 'field_state_value';
  $handler->display->display_options['filters']['field_state_value']['table'] = 'field_data_field_state';
  $handler->display->display_options['filters']['field_state_value']['field'] = 'field_state_value';
  $handler->display->display_options['filters']['field_state_value']['group'] = 2;
  $handler->display->display_options['filters']['field_state_value']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_state_value']['expose']['operator_id'] = 'field_state_value_op';
  $handler->display->display_options['filters']['field_state_value']['expose']['label'] = 'State/Province';
  $handler->display->display_options['filters']['field_state_value']['expose']['operator'] = 'field_state_value_op';
  $handler->display->display_options['filters']['field_state_value']['expose']['identifier'] = 'field_state_value';
  $handler->display->display_options['filters']['field_state_value']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
    4 => 0,
  );

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['path'] = 'store-locator';

  /* Display: Attachment */
  $handler = $view->new_display('attachment', 'Attachment', 'attachment_1');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['defaults']['css_class'] = FALSE;
  $handler->display->display_options['css_class'] = 'location_map';
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['pager']['options']['items_per_page'] = '26';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['defaults']['style_plugin'] = FALSE;
  $handler->display->display_options['style_plugin'] = 'gmap';
  $handler->display->display_options['style_options']['markertype'] = 'numbers';
  $handler->display->display_options['style_options']['latfield'] = 'counter';
  $handler->display->display_options['style_options']['lonfield'] = 'counter';
  $handler->display->display_options['style_options']['markerfield'] = 'counter';
  $handler->display->display_options['style_options']['geofield'] = 'counter';
  $handler->display->display_options['style_options']['enablermt'] = 0;
  $handler->display->display_options['style_options']['rmtfield'] = 'counter';
  $handler->display->display_options['style_options']['center_on_proximityarg'] = 1;
  $handler->display->display_options['style_options']['animation'] = '0';
  $handler->display->display_options['style_options']['tooltipfield'] = 'counter';
  $handler->display->display_options['style_options']['bubbletextfield'] = 'counter';
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['row_options']['inline'] = array(
    'field_city' => 'field_city',
    'field_state' => 'field_state',
    'field_zip' => 'field_zip',
  );
  $handler->display->display_options['row_options']['hide_empty'] = TRUE;
  $handler->display->display_options['defaults']['row_options'] = FALSE;
  $handler->display->display_options['defaults']['empty'] = FALSE;
  $handler->display->display_options['displays'] = array(
    'page' => 'page',
    'default' => 0,
  );
  $handler->display->display_options['inherit_exposed_filters'] = TRUE;
  $export['store_locator'] = $view;

  return $export;
}
