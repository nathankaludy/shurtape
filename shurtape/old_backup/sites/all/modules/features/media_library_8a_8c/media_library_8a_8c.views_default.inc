<?php
/**
 * @file
 * media_library_8a_8c.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function media_library_8a_8c_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'media_library';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Media Library';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'time';
  $handler->display->display_options['cache']['results_lifespan'] = '21600';
  $handler->display->display_options['cache']['results_lifespan_custom'] = '0';
  $handler->display->display_options['cache']['output_lifespan'] = '21600';
  $handler->display->display_options['cache']['output_lifespan_custom'] = '0';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['style_plugin'] = 'list';
  $handler->display->display_options['style_options']['grouping'] = array(
    0 => array(
      'field' => 'field_media_type',
      'rendered' => 1,
      'rendered_strip' => 0,
    ),
  );
  $handler->display->display_options['style_options']['default_row_class'] = FALSE;
  $handler->display->display_options['style_options']['wrapper_class'] = 'item-list group';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* No results behavior: Global: Text area */
  $handler->display->display_options['empty']['area']['id'] = 'area';
  $handler->display->display_options['empty']['area']['table'] = 'views';
  $handler->display->display_options['empty']['area']['field'] = 'area';
  /* Relationship: Content: Taxonomy terms on node */
  $handler->display->display_options['relationships']['term_node_tid']['id'] = 'term_node_tid';
  $handler->display->display_options['relationships']['term_node_tid']['table'] = 'node';
  $handler->display->display_options['relationships']['term_node_tid']['field'] = 'term_node_tid';
  $handler->display->display_options['relationships']['term_node_tid']['vocabularies'] = array(
    'media_libraries' => 'media_libraries',
    'adhesive' => 0,
    'astm_test_methods' => 0,
    'backing' => 0,
    'box_weight' => 0,
    'clean_removal_days' => 0,
    'handling_conditions' => 0,
    'industry_links' => 0,
    'liner' => 0,
    'markets' => 0,
    'product_asset_type' => 0,
    'product_colors' => 0,
    'product_property_type' => 0,
    'tags' => 0,
    'auto_created_voc9_695' => 0,
  );
  /* Field: Content: Display Image */
  $handler->display->display_options['fields']['field_display_image']['id'] = 'field_display_image';
  $handler->display->display_options['fields']['field_display_image']['table'] = 'field_data_field_display_image';
  $handler->display->display_options['fields']['field_display_image']['field'] = 'field_display_image';
  $handler->display->display_options['fields']['field_display_image']['label'] = '';
  $handler->display->display_options['fields']['field_display_image']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_display_image']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_display_image']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_display_image']['settings'] = array(
    'image_style' => '',
    'image_link' => '',
  );
  /* Field: Content: File Asset */
  $handler->display->display_options['fields']['field_file_asset']['id'] = 'field_file_asset';
  $handler->display->display_options['fields']['field_file_asset']['table'] = 'field_data_field_file_asset';
  $handler->display->display_options['fields']['field_file_asset']['field'] = 'field_file_asset';
  $handler->display->display_options['fields']['field_file_asset']['label'] = '';
  $handler->display->display_options['fields']['field_file_asset']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_file_asset']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_file_asset']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_file_asset']['type'] = 'file_url_plain';
  /* Field: Content: Path */
  $handler->display->display_options['fields']['path']['id'] = 'path';
  $handler->display->display_options['fields']['path']['table'] = 'node';
  $handler->display->display_options['fields']['path']['field'] = 'path';
  $handler->display->display_options['fields']['path']['label'] = '';
  $handler->display->display_options['fields']['path']['exclude'] = TRUE;
  $handler->display->display_options['fields']['path']['element_label_colon'] = FALSE;
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['title']['alter']['text'] = '<a target="_blank" href="[path]" class="thumb">[field_display_image]</a>
<div class="title"><!-- open the title container - this is for mobile styles -->
<a target="_blank" href="[path]">[title]</a>';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['title']['link_to_node'] = FALSE;
  /* Field: Content: Media Type */
  $handler->display->display_options['fields']['field_media_type']['id'] = 'field_media_type';
  $handler->display->display_options['fields']['field_media_type']['table'] = 'field_data_field_media_type';
  $handler->display->display_options['fields']['field_media_type']['field'] = 'field_media_type';
  $handler->display->display_options['fields']['field_media_type']['label'] = '';
  $handler->display->display_options['fields']['field_media_type']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_media_type']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_media_type']['type'] = 'taxonomy_term_reference_plain';
  /* Field: Field: Youtube Video ID */
  $handler->display->display_options['fields']['field_youtube_video_id']['id'] = 'field_youtube_video_id';
  $handler->display->display_options['fields']['field_youtube_video_id']['table'] = 'field_data_field_youtube_video_id';
  $handler->display->display_options['fields']['field_youtube_video_id']['field'] = 'field_youtube_video_id';
  $handler->display->display_options['fields']['field_youtube_video_id']['label'] = '';
  $handler->display->display_options['fields']['field_youtube_video_id']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_youtube_video_id']['type'] = 'text_plain';
  /* Sort criterion: Taxonomy term: Weight */
  $handler->display->display_options['sorts']['weight']['id'] = 'weight';
  $handler->display->display_options['sorts']['weight']['table'] = 'taxonomy_term_data';
  $handler->display->display_options['sorts']['weight']['field'] = 'weight';
  $handler->display->display_options['sorts']['weight']['relationship'] = 'term_node_tid';
  /* Sort criterion: Content: Media Type (field_media_type) */
  $handler->display->display_options['sorts']['field_media_type_tid']['id'] = 'field_media_type_tid';
  $handler->display->display_options['sorts']['field_media_type_tid']['table'] = 'field_data_field_media_type';
  $handler->display->display_options['sorts']['field_media_type_tid']['field'] = 'field_media_type_tid';
  /* Sort criterion: Content: Asset Weight (field_asset_weight) */
  $handler->display->display_options['sorts']['field_asset_weight_value']['id'] = 'field_asset_weight_value';
  $handler->display->display_options['sorts']['field_asset_weight_value']['table'] = 'field_data_field_asset_weight';
  $handler->display->display_options['sorts']['field_asset_weight_value']['field'] = 'field_asset_weight_value';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'media_asset_infographic' => 'media_asset_infographic',
    'media_library_asset' => 'media_library_asset',
    'media_library_video' => 'media_library_video',
  );
  /* Filter criterion: Taxonomy term: Name */
  $handler->display->display_options['filters']['name']['id'] = 'name';
  $handler->display->display_options['filters']['name']['table'] = 'taxonomy_term_data';
  $handler->display->display_options['filters']['name']['field'] = 'name';
  $handler->display->display_options['filters']['name']['relationship'] = 'term_node_tid';
  $handler->display->display_options['filters']['name']['value'] = 'Brand Assets';

  /* Display: Block */
  $handler = $view->new_display('block', 'Block', 'block');
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'media_asset_infographic' => 'media_asset_infographic',
    'media_library_asset' => 'media_library_asset',
    'media_library_video' => 'media_library_video',
  );
  /* Filter criterion: Taxonomy term: Name */
  $handler->display->display_options['filters']['name']['id'] = 'name';
  $handler->display->display_options['filters']['name']['table'] = 'taxonomy_term_data';
  $handler->display->display_options['filters']['name']['field'] = 'name';
  $handler->display->display_options['filters']['name']['relationship'] = 'term_node_tid';
  $handler->display->display_options['filters']['name']['operator'] = '!=';
  $handler->display->display_options['filters']['name']['value'] = 'Brand Assets';
  $handler->display->display_options['block_caching'] = '8';

  /* Display: Brand Assets block */
  $handler = $view->new_display('block', 'Brand Assets block', 'block_1');
  $handler->display->display_options['defaults']['cache'] = FALSE;
  $handler->display->display_options['cache']['type'] = 'time';
  $handler->display->display_options['cache']['results_lifespan'] = '21600';
  $handler->display->display_options['cache']['results_lifespan_custom'] = '0';
  $handler->display->display_options['cache']['output_lifespan'] = '21600';
  $handler->display->display_options['cache']['output_lifespan_custom'] = '0';
  $handler->display->display_options['defaults']['style_plugin'] = FALSE;
  $handler->display->display_options['style_plugin'] = 'list';
  $handler->display->display_options['style_options']['wrapper_class'] = 'item-list group';
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['row_options']['inline'] = array(
    'title' => 'title',
    'field_file_asset' => 'field_file_asset',
  );
  $handler->display->display_options['row_options']['default_field_elements'] = FALSE;
  $handler->display->display_options['defaults']['row_options'] = FALSE;
  $handler->display->display_options['defaults']['empty'] = FALSE;
  /* No results behavior: Global: Text area */
  $handler->display->display_options['empty']['area']['id'] = 'area';
  $handler->display->display_options['empty']['area']['table'] = 'views';
  $handler->display->display_options['empty']['area']['field'] = 'area';
  $handler->display->display_options['empty']['area']['empty'] = TRUE;
  $handler->display->display_options['empty']['area']['format'] = 'filtered_html';
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Content: Nid */
  $handler->display->display_options['fields']['nid']['id'] = 'nid';
  $handler->display->display_options['fields']['nid']['table'] = 'node';
  $handler->display->display_options['fields']['nid']['field'] = 'nid';
  $handler->display->display_options['fields']['nid']['label'] = '';
  $handler->display->display_options['fields']['nid']['exclude'] = TRUE;
  $handler->display->display_options['fields']['nid']['element_label_colon'] = FALSE;
  /* Field: Content: Type */
  $handler->display->display_options['fields']['type']['id'] = 'type';
  $handler->display->display_options['fields']['type']['table'] = 'node';
  $handler->display->display_options['fields']['type']['field'] = 'type';
  $handler->display->display_options['fields']['type']['label'] = '';
  $handler->display->display_options['fields']['type']['exclude'] = TRUE;
  $handler->display->display_options['fields']['type']['element_label_colon'] = FALSE;
  /* Field: Content: Logo thumbnail */
  $handler->display->display_options['fields']['field_logo_thumbnail']['id'] = 'field_logo_thumbnail';
  $handler->display->display_options['fields']['field_logo_thumbnail']['table'] = 'field_data_field_logo_thumbnail';
  $handler->display->display_options['fields']['field_logo_thumbnail']['field'] = 'field_logo_thumbnail';
  $handler->display->display_options['fields']['field_logo_thumbnail']['label'] = '';
  $handler->display->display_options['fields']['field_logo_thumbnail']['element_type'] = 'span';
  $handler->display->display_options['fields']['field_logo_thumbnail']['element_class'] = 'thumb';
  $handler->display->display_options['fields']['field_logo_thumbnail']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_logo_thumbnail']['hide_empty'] = TRUE;
  $handler->display->display_options['fields']['field_logo_thumbnail']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_logo_thumbnail']['settings'] = array(
    'image_style' => '',
    'image_link' => '',
  );
  /* Field: Content: Thumbnail Brand Asset */
  $handler->display->display_options['fields']['field_thumbnail_brand_asset']['id'] = 'field_thumbnail_brand_asset';
  $handler->display->display_options['fields']['field_thumbnail_brand_asset']['table'] = 'field_data_field_thumbnail_brand_asset';
  $handler->display->display_options['fields']['field_thumbnail_brand_asset']['field'] = 'field_thumbnail_brand_asset';
  $handler->display->display_options['fields']['field_thumbnail_brand_asset']['label'] = '';
  $handler->display->display_options['fields']['field_thumbnail_brand_asset']['element_type'] = 'span';
  $handler->display->display_options['fields']['field_thumbnail_brand_asset']['element_class'] = 'thumb';
  $handler->display->display_options['fields']['field_thumbnail_brand_asset']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_thumbnail_brand_asset']['hide_empty'] = TRUE;
  $handler->display->display_options['fields']['field_thumbnail_brand_asset']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_thumbnail_brand_asset']['settings'] = array(
    'image_style' => '',
    'image_link' => '',
  );
  /* Field: Content: Path */
  $handler->display->display_options['fields']['path']['id'] = 'path';
  $handler->display->display_options['fields']['path']['table'] = 'node';
  $handler->display->display_options['fields']['path']['field'] = 'path';
  $handler->display->display_options['fields']['path']['label'] = '';
  $handler->display->display_options['fields']['path']['exclude'] = TRUE;
  $handler->display->display_options['fields']['path']['element_label_colon'] = FALSE;
  /* Field: Content: File Asset */
  $handler->display->display_options['fields']['field_file_asset']['id'] = 'field_file_asset';
  $handler->display->display_options['fields']['field_file_asset']['table'] = 'field_data_field_file_asset';
  $handler->display->display_options['fields']['field_file_asset']['field'] = 'field_file_asset';
  $handler->display->display_options['fields']['field_file_asset']['label'] = '';
  $handler->display->display_options['fields']['field_file_asset']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_file_asset']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_file_asset']['alter']['text'] = '<a href="[field_file_asset]" target="_blank">Download</a>';
  $handler->display->display_options['fields']['field_file_asset']['element_type'] = 'span';
  $handler->display->display_options['fields']['field_file_asset']['element_class'] = 'link';
  $handler->display->display_options['fields']['field_file_asset']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_file_asset']['hide_empty'] = TRUE;
  $handler->display->display_options['fields']['field_file_asset']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_file_asset']['type'] = 'file_url_plain';
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['title']['alter']['text'] = '<span class="title">[title]</span>

						';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  $handler->display->display_options['fields']['title']['element_type'] = 'span';
  $handler->display->display_options['fields']['title']['element_class'] = 'brand-asset-title content';
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['title']['link_to_node'] = FALSE;
  /* Field: Content: Media Type */
  $handler->display->display_options['fields']['field_media_type']['id'] = 'field_media_type';
  $handler->display->display_options['fields']['field_media_type']['table'] = 'field_data_field_media_type';
  $handler->display->display_options['fields']['field_media_type']['field'] = 'field_media_type';
  $handler->display->display_options['fields']['field_media_type']['label'] = '';
  $handler->display->display_options['fields']['field_media_type']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_media_type']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_media_type']['type'] = 'taxonomy_term_reference_plain';
  /* Field: Field: Youtube Video ID */
  $handler->display->display_options['fields']['field_youtube_video_id']['id'] = 'field_youtube_video_id';
  $handler->display->display_options['fields']['field_youtube_video_id']['table'] = 'field_data_field_youtube_video_id';
  $handler->display->display_options['fields']['field_youtube_video_id']['field'] = 'field_youtube_video_id';
  $handler->display->display_options['fields']['field_youtube_video_id']['label'] = '';
  $handler->display->display_options['fields']['field_youtube_video_id']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_youtube_video_id']['hide_empty'] = TRUE;
  $handler->display->display_options['fields']['field_youtube_video_id']['type'] = 'text_plain';
  $handler->display->display_options['defaults']['sorts'] = FALSE;
  /* Sort criterion: Content: Brand Asset Weight (field_brand_asset_weight) */
  $handler->display->display_options['sorts']['field_brand_asset_weight_value']['id'] = 'field_brand_asset_weight_value';
  $handler->display->display_options['sorts']['field_brand_asset_weight_value']['table'] = 'field_data_field_brand_asset_weight';
  $handler->display->display_options['sorts']['field_brand_asset_weight_value']['field'] = 'field_brand_asset_weight_value';
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['filter_groups']['operator'] = 'OR';
  $handler->display->display_options['filter_groups']['groups'] = array(
    1 => 'AND',
    2 => 'AND',
  );
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'media_asset_infographic' => 'media_asset_infographic',
    'media_library_asset' => 'media_library_asset',
    'media_library_video' => 'media_library_video',
  );
  $handler->display->display_options['filters']['type']['group'] = 1;
  /* Filter criterion: Taxonomy term: Name */
  $handler->display->display_options['filters']['name']['id'] = 'name';
  $handler->display->display_options['filters']['name']['table'] = 'taxonomy_term_data';
  $handler->display->display_options['filters']['name']['field'] = 'name';
  $handler->display->display_options['filters']['name']['relationship'] = 'term_node_tid';
  $handler->display->display_options['filters']['name']['value'] = 'Brand Assets';
  $handler->display->display_options['filters']['name']['group'] = 1;
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 2;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type_1']['id'] = 'type_1';
  $handler->display->display_options['filters']['type_1']['table'] = 'node';
  $handler->display->display_options['filters']['type_1']['field'] = 'type';
  $handler->display->display_options['filters']['type_1']['value'] = array(
    'media_library_logos' => 'media_library_logos',
  );
  $handler->display->display_options['filters']['type_1']['group'] = 2;
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status_1']['id'] = 'status_1';
  $handler->display->display_options['filters']['status_1']['table'] = 'node';
  $handler->display->display_options['filters']['status_1']['field'] = 'status';
  $handler->display->display_options['filters']['status_1']['value'] = '1';
  $handler->display->display_options['filters']['status_1']['group'] = 1;
  $handler->display->display_options['block_caching'] = '8';
  $export['media_library'] = $view;

  return $export;
}
