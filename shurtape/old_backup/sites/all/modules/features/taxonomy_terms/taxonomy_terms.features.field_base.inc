<?php
/**
 * @file
 * taxonomy_terms.features.field_base.inc
 */

/**
 * Implements hook_field_default_field_bases().
 */
function taxonomy_terms_field_default_field_bases() {
  $field_bases = array();

  // Exported field_base: 'field_markets_term_image'
  $field_bases['field_markets_term_image'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_markets_term_image',
    'foreign keys' => array(
      'fid' => array(
        'columns' => array(
          'fid' => 'fid',
        ),
        'table' => 'file_managed',
      ),
    ),
    'indexes' => array(
      'fid' => array(
        0 => 'fid',
      ),
    ),
    'locked' => 0,
    'module' => 'image',
    'settings' => array(
      'default_image' => 0,
      'uri_scheme' => 's3',
    ),
    'translatable' => 0,
    'type' => 'image',
    'views_natural_sort_enable_sort' => 0,
  );

  // Exported field_base: 'field_markets_term_image_hero'
  $field_bases['field_markets_term_image_hero'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_markets_term_image_hero',
    'foreign keys' => array(
      'fid' => array(
        'columns' => array(
          'fid' => 'fid',
        ),
        'table' => 'file_managed',
      ),
    ),
    'indexes' => array(
      'fid' => array(
        0 => 'fid',
      ),
    ),
    'locked' => 0,
    'module' => 'image',
    'settings' => array(
      'default_image' => 0,
      'uri_scheme' => 's3',
    ),
    'translatable' => 0,
    'type' => 'image',
    'views_natural_sort_enable_sort' => 0,
  );

  // Exported field_base: 'field_markets_term_image_index'
  $field_bases['field_markets_term_image_index'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_markets_term_image_index',
    'foreign keys' => array(
      'fid' => array(
        'columns' => array(
          'fid' => 'fid',
        ),
        'table' => 'file_managed',
      ),
    ),
    'indexes' => array(
      'fid' => array(
        0 => 'fid',
      ),
    ),
    'locked' => 0,
    'module' => 'image',
    'settings' => array(
      'default_image' => 0,
      'uri_scheme' => 's3',
    ),
    'translatable' => 0,
    'type' => 'image',
    'views_natural_sort_enable_sort' => 0,
  );

  // Exported field_base: 'field_youtube_video_id'
  $field_bases['field_youtube_video_id'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_youtube_video_id',
    'foreign keys' => array(
      'format' => array(
        'columns' => array(
          'format' => 'format',
        ),
        'table' => 'filter_format',
      ),
    ),
    'indexes' => array(
      'format' => array(
        0 => 'format',
      ),
    ),
    'locked' => 0,
    'module' => 'text',
    'settings' => array(
      'max_length' => 20,
    ),
    'translatable' => 0,
    'type' => 'text',
    'views_natural_sort_enable_sort' => 0,
  );

  return $field_bases;
}
