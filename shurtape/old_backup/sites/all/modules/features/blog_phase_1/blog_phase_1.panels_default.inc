<?php
/**
 * @file
 * blog_phase_1.panels_default.inc
 */

/**
 * Implements hook_default_panels_mini().
 */
function blog_phase_1_default_panels_mini() {
  $export = array();

  $mini = new stdClass();
  $mini->disabled = FALSE; /* Edit this to true to make a default mini disabled initially */
  $mini->api_version = 1;
  $mini->name = 'blog_right_sidebar';
  $mini->category = '';
  $mini->admin_title = 'Blog right sidebar';
  $mini->admin_description = '';
  $mini->requiredcontexts = array();
  $mini->contexts = array();
  $mini->relationships = array();
  $display = new panels_display();
  $display->layout = 'flexible';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'center' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = 'ce1fd953-ae1a-46b1-91ca-1e7ebd1aa7a3';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-c33edabb-1c02-4874-89b7-148632ec8a8d';
    $pane->panel = 'center';
    $pane->type = 'block';
    $pane->subtype = 'menu-menu-blog-categories';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => 'Categories',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'pane-blog-categories',
    );
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'c33edabb-1c02-4874-89b7-148632ec8a8d';
    $display->content['new-c33edabb-1c02-4874-89b7-148632ec8a8d'] = $pane;
    $display->panels['center'][0] = 'new-c33edabb-1c02-4874-89b7-148632ec8a8d';
    $pane = new stdClass();
    $pane->pid = 'new-9c2bd9d5-aab0-49e9-8a8e-b923ab7cea18';
    $pane->panel = 'center';
    $pane->type = 'block';
    $pane->subtype = 'menu-menu-blog-authors';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => 'Blog authors',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'pane-blog-authors',
    );
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = '9c2bd9d5-aab0-49e9-8a8e-b923ab7cea18';
    $display->content['new-9c2bd9d5-aab0-49e9-8a8e-b923ab7cea18'] = $pane;
    $display->panels['center'][1] = 'new-9c2bd9d5-aab0-49e9-8a8e-b923ab7cea18';
    $pane = new stdClass();
    $pane->pid = 'new-7c71c9c0-1e25-4e4c-b9fb-b6a9b02627ae';
    $pane->panel = 'center';
    $pane->type = 'block';
    $pane->subtype = 'shurtape_blog-shurtape_notification_signup';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 2;
    $pane->locks = array();
    $pane->uuid = '7c71c9c0-1e25-4e4c-b9fb-b6a9b02627ae';
    $display->content['new-7c71c9c0-1e25-4e4c-b9fb-b6a9b02627ae'] = $pane;
    $display->panels['center'][2] = 'new-7c71c9c0-1e25-4e4c-b9fb-b6a9b02627ae';
  $display->hide_title = PANELS_TITLE_NONE;
  $display->title_pane = '0';
  $mini->display = $display;
  $export['blog_right_sidebar'] = $mini;

  return $export;
}
