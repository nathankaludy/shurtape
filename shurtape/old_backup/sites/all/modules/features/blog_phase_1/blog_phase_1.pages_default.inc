<?php
/**
 * @file
 * blog_phase_1.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_handlers().
 */
function blog_phase_1_default_page_manager_handlers() {
  $export = array();

  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'node_view_panel_context';
  $handler->task = 'node_view';
  $handler->subtask = '';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Blog',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => 'page-blogs page-blog-view',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'access' => array(
      'plugins' => array(
        0 => array(
          'name' => 'node_type',
          'settings' => array(
            'type' => array(
              'blog' => 'blog',
            ),
          ),
          'context' => 'argument_entity_id:node_1',
          'not' => FALSE,
        ),
      ),
      'logic' => 'and',
    ),
  );
  $display = new panels_display();
  $display->layout = 'flexible';
  $display->layout_settings = array(
    'items' => array(
      'canvas' => array(
        'type' => 'row',
        'contains' => 'column',
        'children' => array(
          0 => 'main',
        ),
        'parent' => NULL,
      ),
      'main' => array(
        'type' => 'column',
        'width' => 100,
        'width_type' => '%',
        'children' => array(
          0 => 'main-row',
        ),
        'parent' => 'canvas',
      ),
      'main-row' => array(
        'type' => 'row',
        'contains' => 'region',
        'children' => array(
          0 => 'center',
          1 => 'right_sidebar',
        ),
        'parent' => 'main',
      ),
      'center' => array(
        'type' => 'region',
        'title' => 'Center',
        'width' => '76.9995553014421',
        'width_type' => '%',
        'parent' => 'main-row',
        'class' => 'blog-region-center',
      ),
      'right_sidebar' => array(
        'type' => 'region',
        'title' => 'Right sidebar',
        'width' => '23.000444698557903',
        'width_type' => '%',
        'parent' => 'main-row',
        'class' => 'blog-right-sidebar',
      ),
    ),
  );
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'center' => NULL,
      'right_sidebar' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = 'Shurtape Blog';
  $display->uuid = '38b101b0-203a-489d-8acb-e4cfd5add53a';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-c0d6c3aa-6b88-42b0-9e99-3f995b2116e0';
    $pane->panel = 'center';
    $pane->type = 'custom';
    $pane->subtype = 'custom';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'admin_title' => 'Title',
      'title' => '',
      'body' => '<h2 class="pane-title">Shurtape Blog</h2>',
      'format' => 'full_html',
      'substitute' => 0,
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'pane-shurtape-blogs',
    );
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'c0d6c3aa-6b88-42b0-9e99-3f995b2116e0';
    $display->content['new-c0d6c3aa-6b88-42b0-9e99-3f995b2116e0'] = $pane;
    $display->panels['center'][0] = 'new-c0d6c3aa-6b88-42b0-9e99-3f995b2116e0';
    $pane = new stdClass();
    $pane->pid = 'new-f37f2fa2-7024-4ece-8f70-8b4d25400ab7';
    $pane->panel = 'center';
    $pane->type = 'entity_field';
    $pane->subtype = 'node:field_blog_category';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'hidden',
      'formatter' => 'taxonomy_term_reference_plain',
      'delta_limit' => 0,
      'delta_offset' => '0',
      'delta_reversed' => FALSE,
      'formatter_settings' => array(),
      'context' => 'argument_entity_id:node_1',
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = 'f37f2fa2-7024-4ece-8f70-8b4d25400ab7';
    $display->content['new-f37f2fa2-7024-4ece-8f70-8b4d25400ab7'] = $pane;
    $display->panels['center'][1] = 'new-f37f2fa2-7024-4ece-8f70-8b4d25400ab7';
    $pane = new stdClass();
    $pane->pid = 'new-72f48f4e-c2a5-48e0-a355-9c04b30b98a0';
    $pane->panel = 'center';
    $pane->type = 'node_title';
    $pane->subtype = 'node_title';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'link' => 0,
      'markup' => 'h3',
      'id' => '',
      'class' => '',
      'context' => 'argument_entity_id:node_1',
      'override_title' => 1,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 2;
    $pane->locks = array();
    $pane->uuid = '72f48f4e-c2a5-48e0-a355-9c04b30b98a0';
    $display->content['new-72f48f4e-c2a5-48e0-a355-9c04b30b98a0'] = $pane;
    $display->panels['center'][2] = 'new-72f48f4e-c2a5-48e0-a355-9c04b30b98a0';
    $pane = new stdClass();
    $pane->pid = 'new-63d3e383-f6f5-40be-96a4-33049d90ede3';
    $pane->panel = 'center';
    $pane->type = 'node_created';
    $pane->subtype = 'node_created';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'format' => 'blog',
      'context' => 'argument_entity_id:node_1',
      'override_title' => 1,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 3;
    $pane->locks = array();
    $pane->uuid = '63d3e383-f6f5-40be-96a4-33049d90ede3';
    $display->content['new-63d3e383-f6f5-40be-96a4-33049d90ede3'] = $pane;
    $display->panels['center'][3] = 'new-63d3e383-f6f5-40be-96a4-33049d90ede3';
    $pane = new stdClass();
    $pane->pid = 'new-87653f96-5f1f-47b1-97fa-644a1f25bb3f';
    $pane->panel = 'center';
    $pane->type = 'entity_field';
    $pane->subtype = 'node:field_blog_main_image';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'hidden',
      'formatter' => 'image',
      'delta_limit' => 0,
      'delta_offset' => '0',
      'delta_reversed' => FALSE,
      'formatter_settings' => array(
        'image_style' => '212s',
        'image_link' => '',
      ),
      'context' => 'argument_entity_id:node_1',
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 4;
    $pane->locks = array();
    $pane->uuid = '87653f96-5f1f-47b1-97fa-644a1f25bb3f';
    $display->content['new-87653f96-5f1f-47b1-97fa-644a1f25bb3f'] = $pane;
    $display->panels['center'][4] = 'new-87653f96-5f1f-47b1-97fa-644a1f25bb3f';
    $pane = new stdClass();
    $pane->pid = 'new-ea36c11c-8a9a-4c66-bdd0-f2f51b89ac21';
    $pane->panel = 'center';
    $pane->type = 'entity_field';
    $pane->subtype = 'node:field_youtube_video_id';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'hidden',
      'formatter' => 'shurtape_blog_youtube_formatter',
      'delta_limit' => 0,
      'delta_offset' => '0',
      'delta_reversed' => FALSE,
      'formatter_settings' => array(),
      'context' => 'argument_entity_id:node_1',
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 5;
    $pane->locks = array();
    $pane->uuid = 'ea36c11c-8a9a-4c66-bdd0-f2f51b89ac21';
    $display->content['new-ea36c11c-8a9a-4c66-bdd0-f2f51b89ac21'] = $pane;
    $display->panels['center'][5] = 'new-ea36c11c-8a9a-4c66-bdd0-f2f51b89ac21';
    $pane = new stdClass();
    $pane->pid = 'new-6f9d9bc7-ace4-4393-b55f-841eb7db199c';
    $pane->panel = 'center';
    $pane->type = 'entity_field';
    $pane->subtype = 'node:body';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'hidden',
      'formatter' => 'text_default',
      'delta_limit' => 0,
      'delta_offset' => '0',
      'delta_reversed' => FALSE,
      'formatter_settings' => array(),
      'context' => 'argument_entity_id:node_1',
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'clearfix',
    );
    $pane->extras = array();
    $pane->position = 6;
    $pane->locks = array();
    $pane->uuid = '6f9d9bc7-ace4-4393-b55f-841eb7db199c';
    $display->content['new-6f9d9bc7-ace4-4393-b55f-841eb7db199c'] = $pane;
    $display->panels['center'][6] = 'new-6f9d9bc7-ace4-4393-b55f-841eb7db199c';
    $pane = new stdClass();
    $pane->pid = 'new-eec1217d-d081-4b92-9fe4-d9ae7ca1fea6';
    $pane->panel = 'center';
    $pane->type = 'entity_field';
    $pane->subtype = 'node:field_blog_additional_image';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'hidden',
      'formatter' => 'image',
      'delta_limit' => 0,
      'delta_offset' => '0',
      'delta_reversed' => FALSE,
      'formatter_settings' => array(
        'image_style' => '212s',
        'image_link' => '',
      ),
      'context' => 'argument_entity_id:node_1',
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 7;
    $pane->locks = array();
    $pane->uuid = 'eec1217d-d081-4b92-9fe4-d9ae7ca1fea6';
    $display->content['new-eec1217d-d081-4b92-9fe4-d9ae7ca1fea6'] = $pane;
    $display->panels['center'][7] = 'new-eec1217d-d081-4b92-9fe4-d9ae7ca1fea6';
    $pane = new stdClass();
    $pane->pid = 'new-65f0403c-586d-4c69-9566-a4be2a5a8787';
    $pane->panel = 'center';
    $pane->type = 'sharethis';
    $pane->subtype = 'sharethis';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'path' => 'current',
      'path-external' => '',
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 8;
    $pane->locks = array();
    $pane->uuid = '65f0403c-586d-4c69-9566-a4be2a5a8787';
    $display->content['new-65f0403c-586d-4c69-9566-a4be2a5a8787'] = $pane;
    $display->panels['center'][8] = 'new-65f0403c-586d-4c69-9566-a4be2a5a8787';
    $pane = new stdClass();
    $pane->pid = 'new-eb8d4d0c-020c-4b73-a92e-3fd5e2a90441';
    $pane->panel = 'center';
    $pane->type = 'node_comment_form';
    $pane->subtype = 'node_comment_form';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'anon_links' => 1,
      'context' => 'argument_entity_id:node_1',
      'override_title' => 1,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 9;
    $pane->locks = array();
    $pane->uuid = 'eb8d4d0c-020c-4b73-a92e-3fd5e2a90441';
    $display->content['new-eb8d4d0c-020c-4b73-a92e-3fd5e2a90441'] = $pane;
    $display->panels['center'][9] = 'new-eb8d4d0c-020c-4b73-a92e-3fd5e2a90441';
    $pane = new stdClass();
    $pane->pid = 'new-7e3f5c8c-e751-4f56-a92d-19407b4b6c4e';
    $pane->panel = 'center';
    $pane->type = 'node_comments';
    $pane->subtype = 'node_comments';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'mode' => '0',
      'comments_per_page' => '30',
      'context' => 'argument_entity_id:node_1',
      'override_title' => 1,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 10;
    $pane->locks = array();
    $pane->uuid = '7e3f5c8c-e751-4f56-a92d-19407b4b6c4e';
    $display->content['new-7e3f5c8c-e751-4f56-a92d-19407b4b6c4e'] = $pane;
    $display->panels['center'][10] = 'new-7e3f5c8c-e751-4f56-a92d-19407b4b6c4e';
    $pane = new stdClass();
    $pane->pid = 'new-d608882d-595b-40bc-be16-99215784f731';
    $pane->panel = 'right_sidebar';
    $pane->type = 'panels_mini';
    $pane->subtype = 'blog_right_sidebar';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'd608882d-595b-40bc-be16-99215784f731';
    $display->content['new-d608882d-595b-40bc-be16-99215784f731'] = $pane;
    $display->panels['right_sidebar'][0] = 'new-d608882d-595b-40bc-be16-99215784f731';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $handler->conf['display'] = $display;
  $export['node_view_panel_context'] = $handler;

  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'term_view_panel_context';
  $handler->task = 'term_view';
  $handler->subtask = '';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Blog filter',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => 'page-blogs page-blogs-by-term',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'access' => array(
      'logic' => 'and',
      'plugins' => array(
        0 => array(
          'name' => 'term_vocabulary',
          'settings' => array(
            'machine_name' => array(
              'blog_authors' => 'blog_authors',
              'newsletter' => 'newsletter',
            ),
          ),
          'context' => 'argument_term_1',
          'not' => FALSE,
        ),
      ),
    ),
    'metatag_panels' => array(
      'enabled' => 0,
      'metatags' => array(
        'und' => array(
          'robots' => array(
            'value' => array(
              'index' => 0,
              'follow' => 0,
              'noindex' => 0,
              'nofollow' => 0,
              'noarchive' => 0,
              'nosnippet' => 0,
              'noodp' => 0,
              'noydir' => 0,
              'noimageindex' => 0,
              'notranslate' => 0,
            ),
          ),
        ),
      ),
    ),
  );
  $display = new panels_display();
  $display->layout = 'flexible';
  $display->layout_settings = array(
    'items' => array(
      'canvas' => array(
        'type' => 'row',
        'contains' => 'column',
        'children' => array(
          0 => 'main',
        ),
        'parent' => NULL,
      ),
      'main' => array(
        'type' => 'column',
        'width' => 100,
        'width_type' => '%',
        'children' => array(
          0 => 'main-row',
        ),
        'parent' => 'canvas',
      ),
      'main-row' => array(
        'type' => 'row',
        'contains' => 'region',
        'children' => array(
          0 => 'center',
          1 => 'right_sidebar',
        ),
        'parent' => 'main',
      ),
      'center' => array(
        'type' => 'region',
        'title' => 'Center',
        'width' => '77.95248078266945',
        'width_type' => '%',
        'parent' => 'main-row',
        'class' => 'blog-region-center',
      ),
      'right_sidebar' => array(
        'type' => 'region',
        'title' => 'Right sidebar',
        'width' => '22.047519217330542',
        'width_type' => '%',
        'parent' => 'main-row',
        'class' => 'blog-right-sidebar',
      ),
    ),
  );
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'center' => NULL,
      'right_sidebar' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = 'Shurtape Blog';
  $display->uuid = '71663be3-8fdb-4fed-b8fe-6cc362acf0ca';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-c1acadf6-ee1d-42e5-9adf-ca279a71c5ea';
    $pane->panel = 'center';
    $pane->type = 'views';
    $pane->subtype = 'shurtape_blogs';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_pager_settings' => 0,
      'use_pager' => 1,
      'nodes_per_page' => '4',
      'pager_id' => '0',
      'offset' => '0',
      'more_link' => 0,
      'feed_icons' => 0,
      'panel_args' => 0,
      'link_to_view' => 0,
      'args' => '',
      'url' => '',
      'display' => 'block_1',
      'override_title' => 1,
      'override_title_text' => 'Shurtape Blog',
      'context' => array(
        0 => 'argument_term_1.tid',
      ),
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'c1acadf6-ee1d-42e5-9adf-ca279a71c5ea';
    $display->content['new-c1acadf6-ee1d-42e5-9adf-ca279a71c5ea'] = $pane;
    $display->panels['center'][0] = 'new-c1acadf6-ee1d-42e5-9adf-ca279a71c5ea';
    $pane = new stdClass();
    $pane->pid = 'new-5633786e-357c-4269-b0ef-d56140d89676';
    $pane->panel = 'right_sidebar';
    $pane->type = 'panels_mini';
    $pane->subtype = 'blog_right_sidebar';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '5633786e-357c-4269-b0ef-d56140d89676';
    $display->content['new-5633786e-357c-4269-b0ef-d56140d89676'] = $pane;
    $display->panels['right_sidebar'][0] = 'new-5633786e-357c-4269-b0ef-d56140d89676';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $handler->conf['display'] = $display;
  $export['term_view_panel_context'] = $handler;

  return $export;
}

/**
 * Implements hook_default_page_manager_pages().
 */
function blog_phase_1_default_page_manager_pages() {
  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'blogs';
  $page->task = 'page';
  $page->admin_title = 'Blogs';
  $page->admin_description = '';
  $page->path = 'blog';
  $page->access = array();
  $page->menu = array();
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_blogs_panel_context';
  $handler->task = 'page';
  $handler->subtask = 'blogs';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Panel',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => 'page-blog',
    'body_classes_to_add' => 'page-blogs',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
  );
  $display = new panels_display();
  $display->layout = 'flexible';
  $display->layout_settings = array(
    'items' => array(
      'canvas' => array(
        'type' => 'row',
        'contains' => 'column',
        'children' => array(
          0 => 'main',
        ),
        'parent' => NULL,
      ),
      'main' => array(
        'type' => 'column',
        'width' => 100,
        'width_type' => '%',
        'children' => array(
          0 => 1,
        ),
        'parent' => 'canvas',
      ),
      1 => array(
        'type' => 'row',
        'contains' => 'region',
        'children' => array(
          0 => 'content',
          1 => 'left_sidebar',
        ),
        'parent' => 'main',
        'class' => '',
      ),
      'content' => array(
        'type' => 'region',
        'title' => 'Content',
        'width' => '77.95250870518007',
        'width_type' => '%',
        'parent' => '1',
        'class' => 'blog-region-center',
      ),
      'left_sidebar' => array(
        'type' => 'region',
        'title' => 'Right sidebar',
        'width' => '22.047491294819928',
        'width_type' => '%',
        'parent' => '1',
        'class' => 'blog-right-sidebar',
      ),
    ),
  );
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'center' => NULL,
      'content' => NULL,
      'left_sidebar' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = 'Shurtape Blog';
  $display->uuid = '8e0f0ea6-d37c-4906-b449-46e5d96fa4e6';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-244e35e8-163a-4c08-a9f1-0ab3874937f5';
    $pane->panel = 'content';
    $pane->type = 'views';
    $pane->subtype = 'shurtape_blogs';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_pager_settings' => 0,
      'use_pager' => 1,
      'nodes_per_page' => '4',
      'pager_id' => '0',
      'offset' => '0',
      'more_link' => 0,
      'feed_icons' => 0,
      'panel_args' => 0,
      'link_to_view' => 0,
      'args' => '',
      'url' => '',
      'display' => 'block_1',
      'override_title' => 1,
      'override_title_text' => 'Shurtape Blog',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '244e35e8-163a-4c08-a9f1-0ab3874937f5';
    $display->content['new-244e35e8-163a-4c08-a9f1-0ab3874937f5'] = $pane;
    $display->panels['content'][0] = 'new-244e35e8-163a-4c08-a9f1-0ab3874937f5';
    $pane = new stdClass();
    $pane->pid = 'new-594148ab-08d6-4161-8180-a76c9ddbfbac';
    $pane->panel = 'left_sidebar';
    $pane->type = 'panels_mini';
    $pane->subtype = 'blog_right_sidebar';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '594148ab-08d6-4161-8180-a76c9ddbfbac';
    $display->content['new-594148ab-08d6-4161-8180-a76c9ddbfbac'] = $pane;
    $display->panels['left_sidebar'][0] = 'new-594148ab-08d6-4161-8180-a76c9ddbfbac';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['blogs'] = $page;

  return $pages;

}
