<?php
/**
 * @file
 * blog_phase_1.features.menu_custom.inc
 */

/**
 * Implements hook_menu_default_menu_custom().
 */
function blog_phase_1_menu_default_menu_custom() {
  $menus = array();

  // Exported menu: menu-blog-authors.
  $menus['menu-blog-authors'] = array(
    'menu_name' => 'menu-blog-authors',
    'title' => 'Blog authors',
    'description' => '',
  );
  // Exported menu: menu-blog-categories.
  $menus['menu-blog-categories'] = array(
    'menu_name' => 'menu-blog-categories',
    'title' => 'Blog categories',
    'description' => '',
  );
  // Exported menu: menu-resources-menu.
  $menus['menu-resources-menu'] = array(
    'menu_name' => 'menu-resources-menu',
    'title' => 'Resources menu',
    'description' => 'Resources menu for the header and footer navigation',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Blog authors');
  t('Blog categories');
  t('Resources menu');
  t('Resources menu for the header and footer navigation');


  return $menus;
}
