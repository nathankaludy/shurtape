<?php
/**
 * @file
 * tape_type_taxonomy_only.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function tape_type_taxonomy_only_taxonomy_default_vocabularies() {
  return array(
    'auto_created_voc9_695' => array(
      'name' => 'Tape Type',
      'machine_name' => 'auto_created_voc9_695',
      'description' => 'Vocabulary created automatically by Taxonomy csv import/export module',
      'hierarchy' => 1,
      'module' => 'taxonomy',
      'weight' => 0,
      'rdf_mapping' => array(
        'rdftype' => array(
          0 => 'skos:ConceptScheme',
        ),
        'name' => array(
          'predicates' => array(
            0 => 'dc:title',
          ),
        ),
        'description' => array(
          'predicates' => array(
            0 => 'rdfs:comment',
          ),
        ),
      ),
    ),
  );
}
