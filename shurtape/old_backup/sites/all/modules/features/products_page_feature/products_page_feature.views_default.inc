<?php
/**
 * @file
 * products_page_feature.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function products_page_feature_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'product_finder';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Product Finder';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Product Finder';
  $handler->display->display_options['use_ajax'] = TRUE;
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '25';
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'title' => 'title',
    'field_product_type' => 'field_product_type',
    'field_product_adhesive' => 'field_product_adhesive',
    'field_product_backing' => 'field_product_backing',
  );
  $handler->display->display_options['style_options']['default'] = 'title';
  $handler->display->display_options['style_options']['info'] = array(
    'title' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_product_type' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_product_adhesive' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_product_backing' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  /* Relationship: Content: Product Image (field_product_image:fid) */
  $handler->display->display_options['relationships']['field_product_image_fid']['id'] = 'field_product_image_fid';
  $handler->display->display_options['relationships']['field_product_image_fid']['table'] = 'field_data_field_product_image';
  $handler->display->display_options['relationships']['field_product_image_fid']['field'] = 'field_product_image_fid';
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  /* Field: Content: Product Type */
  $handler->display->display_options['fields']['field_product_type']['id'] = 'field_product_type';
  $handler->display->display_options['fields']['field_product_type']['table'] = 'field_data_field_product_type';
  $handler->display->display_options['fields']['field_product_type']['field'] = 'field_product_type';
  $handler->display->display_options['fields']['field_product_type']['label'] = 'Type';
  /* Field: Content: Product Adhesive */
  $handler->display->display_options['fields']['field_product_adhesive']['id'] = 'field_product_adhesive';
  $handler->display->display_options['fields']['field_product_adhesive']['table'] = 'field_data_field_product_adhesive';
  $handler->display->display_options['fields']['field_product_adhesive']['field'] = 'field_product_adhesive';
  $handler->display->display_options['fields']['field_product_adhesive']['label'] = 'Adhesive';
  $handler->display->display_options['fields']['field_product_adhesive']['type'] = 'taxonomy_term_reference_plain';
  /* Field: Content: Product Backing */
  $handler->display->display_options['fields']['field_product_backing']['id'] = 'field_product_backing';
  $handler->display->display_options['fields']['field_product_backing']['table'] = 'field_data_field_product_backing';
  $handler->display->display_options['fields']['field_product_backing']['field'] = 'field_product_backing';
  $handler->display->display_options['fields']['field_product_backing']['label'] = 'Backing';
  $handler->display->display_options['fields']['field_product_backing']['type'] = 'shs_default';
  $handler->display->display_options['fields']['field_product_backing']['settings'] = array(
    'linked' => 0,
  );
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'product' => 'product',
  );
  /* Filter criterion: Content: Has taxonomy terms (with depth) */
  $handler->display->display_options['filters']['term_node_tid_depth']['id'] = 'term_node_tid_depth';
  $handler->display->display_options['filters']['term_node_tid_depth']['table'] = 'node';
  $handler->display->display_options['filters']['term_node_tid_depth']['field'] = 'term_node_tid_depth';
  $handler->display->display_options['filters']['term_node_tid_depth']['exposed'] = TRUE;
  $handler->display->display_options['filters']['term_node_tid_depth']['expose']['operator_id'] = 'term_node_tid_depth_op';
  $handler->display->display_options['filters']['term_node_tid_depth']['expose']['label'] = 'Tape Type';
  $handler->display->display_options['filters']['term_node_tid_depth']['expose']['operator'] = 'term_node_tid_depth_op';
  $handler->display->display_options['filters']['term_node_tid_depth']['expose']['identifier'] = 'term_node_tid_depth';
  $handler->display->display_options['filters']['term_node_tid_depth']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
    4 => 0,
  );
  $handler->display->display_options['filters']['term_node_tid_depth']['type'] = 'select';
  $handler->display->display_options['filters']['term_node_tid_depth']['vocabulary'] = 'auto_created_voc9_695';
  $handler->display->display_options['filters']['term_node_tid_depth']['hierarchy'] = 1;
  $handler->display->display_options['filters']['term_node_tid_depth']['depth'] = '6';
  /* Filter criterion: Content: Clean Removal Days (field_product_clean_removal_days) */
  $handler->display->display_options['filters']['field_product_clean_removal_days_tid']['id'] = 'field_product_clean_removal_days_tid';
  $handler->display->display_options['filters']['field_product_clean_removal_days_tid']['table'] = 'field_data_field_product_clean_removal_days';
  $handler->display->display_options['filters']['field_product_clean_removal_days_tid']['field'] = 'field_product_clean_removal_days_tid';
  $handler->display->display_options['filters']['field_product_clean_removal_days_tid']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_product_clean_removal_days_tid']['expose']['operator_id'] = 'field_product_clean_removal_days_tid_op';
  $handler->display->display_options['filters']['field_product_clean_removal_days_tid']['expose']['label'] = 'Clean Removal Days';
  $handler->display->display_options['filters']['field_product_clean_removal_days_tid']['expose']['operator'] = 'field_product_clean_removal_days_tid_op';
  $handler->display->display_options['filters']['field_product_clean_removal_days_tid']['expose']['identifier'] = 'field_product_clean_removal_days_tid';
  $handler->display->display_options['filters']['field_product_clean_removal_days_tid']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
    4 => 0,
  );
  $handler->display->display_options['filters']['field_product_clean_removal_days_tid']['type'] = 'select';
  $handler->display->display_options['filters']['field_product_clean_removal_days_tid']['vocabulary'] = 'clean_removal_days';
  /* Filter criterion: Content: Handling Conditions (field_handling_conditions) */
  $handler->display->display_options['filters']['field_handling_conditions_tid_1']['id'] = 'field_handling_conditions_tid_1';
  $handler->display->display_options['filters']['field_handling_conditions_tid_1']['table'] = 'field_data_field_handling_conditions';
  $handler->display->display_options['filters']['field_handling_conditions_tid_1']['field'] = 'field_handling_conditions_tid';
  $handler->display->display_options['filters']['field_handling_conditions_tid_1']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_handling_conditions_tid_1']['expose']['operator_id'] = 'field_handling_conditions_tid_1_op';
  $handler->display->display_options['filters']['field_handling_conditions_tid_1']['expose']['label'] = 'Handling Conditions';
  $handler->display->display_options['filters']['field_handling_conditions_tid_1']['expose']['operator'] = 'field_handling_conditions_tid_1_op';
  $handler->display->display_options['filters']['field_handling_conditions_tid_1']['expose']['identifier'] = 'field_handling_conditions_tid_1';
  $handler->display->display_options['filters']['field_handling_conditions_tid_1']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
    4 => 0,
  );
  $handler->display->display_options['filters']['field_handling_conditions_tid_1']['type'] = 'select';
  $handler->display->display_options['filters']['field_handling_conditions_tid_1']['vocabulary'] = 'handling_conditions';
  /* Filter criterion: Content: Is Carpet Tape (field_is_carpet_tape) */
  $handler->display->display_options['filters']['field_is_carpet_tape_value']['id'] = 'field_is_carpet_tape_value';
  $handler->display->display_options['filters']['field_is_carpet_tape_value']['table'] = 'field_data_field_is_carpet_tape';
  $handler->display->display_options['filters']['field_is_carpet_tape_value']['field'] = 'field_is_carpet_tape_value';
  $handler->display->display_options['filters']['field_is_carpet_tape_value']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_is_carpet_tape_value']['expose']['operator_id'] = 'field_is_carpet_tape_value_op';
  $handler->display->display_options['filters']['field_is_carpet_tape_value']['expose']['label'] = 'Is Carpet Tape';
  $handler->display->display_options['filters']['field_is_carpet_tape_value']['expose']['operator'] = 'field_is_carpet_tape_value_op';
  $handler->display->display_options['filters']['field_is_carpet_tape_value']['expose']['identifier'] = 'field_is_carpet_tape_value';
  $handler->display->display_options['filters']['field_is_carpet_tape_value']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
    4 => 0,
  );
  $handler->display->display_options['filters']['field_is_carpet_tape_value']['reduce_duplicates'] = TRUE;
  /* Filter criterion: Content: Is Cold Temp Tape (field_is_cold_temp_tape) */
  $handler->display->display_options['filters']['field_is_cold_temp_tape_value']['id'] = 'field_is_cold_temp_tape_value';
  $handler->display->display_options['filters']['field_is_cold_temp_tape_value']['table'] = 'field_data_field_is_cold_temp_tape';
  $handler->display->display_options['filters']['field_is_cold_temp_tape_value']['field'] = 'field_is_cold_temp_tape_value';
  $handler->display->display_options['filters']['field_is_cold_temp_tape_value']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_is_cold_temp_tape_value']['expose']['operator_id'] = 'field_is_cold_temp_tape_value_op';
  $handler->display->display_options['filters']['field_is_cold_temp_tape_value']['expose']['label'] = 'Is Cold Temp Tape';
  $handler->display->display_options['filters']['field_is_cold_temp_tape_value']['expose']['operator'] = 'field_is_cold_temp_tape_value_op';
  $handler->display->display_options['filters']['field_is_cold_temp_tape_value']['expose']['identifier'] = 'field_is_cold_temp_tape_value';
  $handler->display->display_options['filters']['field_is_cold_temp_tape_value']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
    4 => 0,
  );
  /* Filter criterion: Content: Is FDA Compliant (field_is_fda_compliant) */
  $handler->display->display_options['filters']['field_is_fda_compliant_value']['id'] = 'field_is_fda_compliant_value';
  $handler->display->display_options['filters']['field_is_fda_compliant_value']['table'] = 'field_data_field_is_fda_compliant';
  $handler->display->display_options['filters']['field_is_fda_compliant_value']['field'] = 'field_is_fda_compliant_value';
  $handler->display->display_options['filters']['field_is_fda_compliant_value']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_is_fda_compliant_value']['expose']['operator_id'] = 'field_is_fda_compliant_value_op';
  $handler->display->display_options['filters']['field_is_fda_compliant_value']['expose']['label'] = 'Is FDA Compliant';
  $handler->display->display_options['filters']['field_is_fda_compliant_value']['expose']['operator'] = 'field_is_fda_compliant_value_op';
  $handler->display->display_options['filters']['field_is_fda_compliant_value']['expose']['identifier'] = 'field_is_fda_compliant_value';
  $handler->display->display_options['filters']['field_is_fda_compliant_value']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
    4 => 0,
  );
  /* Filter criterion: Content: Is Green Point Certified (field_is_green_point_certified) */
  $handler->display->display_options['filters']['field_is_green_point_certified_value']['id'] = 'field_is_green_point_certified_value';
  $handler->display->display_options['filters']['field_is_green_point_certified_value']['table'] = 'field_data_field_is_green_point_certified';
  $handler->display->display_options['filters']['field_is_green_point_certified_value']['field'] = 'field_is_green_point_certified_value';
  $handler->display->display_options['filters']['field_is_green_point_certified_value']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_is_green_point_certified_value']['expose']['operator_id'] = 'field_is_green_point_certified_value_op';
  $handler->display->display_options['filters']['field_is_green_point_certified_value']['expose']['label'] = 'Is Green Point Certified';
  $handler->display->display_options['filters']['field_is_green_point_certified_value']['expose']['operator'] = 'field_is_green_point_certified_value_op';
  $handler->display->display_options['filters']['field_is_green_point_certified_value']['expose']['identifier'] = 'field_is_green_point_certified_value';
  $handler->display->display_options['filters']['field_is_green_point_certified_value']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
    4 => 0,
  );
  /* Filter criterion: Content: Is Linered (field_is_linered) */
  $handler->display->display_options['filters']['field_is_linered_value']['id'] = 'field_is_linered_value';
  $handler->display->display_options['filters']['field_is_linered_value']['table'] = 'field_data_field_is_linered';
  $handler->display->display_options['filters']['field_is_linered_value']['field'] = 'field_is_linered_value';
  $handler->display->display_options['filters']['field_is_linered_value']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_is_linered_value']['expose']['operator_id'] = 'field_is_linered_value_op';
  $handler->display->display_options['filters']['field_is_linered_value']['expose']['label'] = 'Is Linered';
  $handler->display->display_options['filters']['field_is_linered_value']['expose']['operator'] = 'field_is_linered_value_op';
  $handler->display->display_options['filters']['field_is_linered_value']['expose']['identifier'] = 'field_is_linered_value';
  $handler->display->display_options['filters']['field_is_linered_value']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
    4 => 0,
  );
  /* Filter criterion: Content: Is UL 723 Tested (field_is_ul_723_tested) */
  $handler->display->display_options['filters']['field_is_ul_723_tested_value']['id'] = 'field_is_ul_723_tested_value';
  $handler->display->display_options['filters']['field_is_ul_723_tested_value']['table'] = 'field_data_field_is_ul_723_tested';
  $handler->display->display_options['filters']['field_is_ul_723_tested_value']['field'] = 'field_is_ul_723_tested_value';
  $handler->display->display_options['filters']['field_is_ul_723_tested_value']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_is_ul_723_tested_value']['expose']['operator_id'] = 'field_is_ul_723_tested_value_op';
  $handler->display->display_options['filters']['field_is_ul_723_tested_value']['expose']['label'] = 'Is UL 723 Tested';
  $handler->display->display_options['filters']['field_is_ul_723_tested_value']['expose']['operator'] = 'field_is_ul_723_tested_value_op';
  $handler->display->display_options['filters']['field_is_ul_723_tested_value']['expose']['identifier'] = 'field_is_ul_723_tested_value';
  $handler->display->display_options['filters']['field_is_ul_723_tested_value']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
    4 => 0,
  );
  /* Filter criterion: Content: Is UL Listed (field_is_ul_listed) */
  $handler->display->display_options['filters']['field_is_ul_listed_value']['id'] = 'field_is_ul_listed_value';
  $handler->display->display_options['filters']['field_is_ul_listed_value']['table'] = 'field_data_field_is_ul_listed';
  $handler->display->display_options['filters']['field_is_ul_listed_value']['field'] = 'field_is_ul_listed_value';
  $handler->display->display_options['filters']['field_is_ul_listed_value']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_is_ul_listed_value']['expose']['operator_id'] = 'field_is_ul_listed_value_op';
  $handler->display->display_options['filters']['field_is_ul_listed_value']['expose']['label'] = 'Is UL Listed';
  $handler->display->display_options['filters']['field_is_ul_listed_value']['expose']['operator'] = 'field_is_ul_listed_value_op';
  $handler->display->display_options['filters']['field_is_ul_listed_value']['expose']['identifier'] = 'field_is_ul_listed_value';
  $handler->display->display_options['filters']['field_is_ul_listed_value']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
    4 => 0,
  );
  /* Filter criterion: Content: Is UV Resistant (field_is_uv_resistant) */
  $handler->display->display_options['filters']['field_is_uv_resistant_value']['id'] = 'field_is_uv_resistant_value';
  $handler->display->display_options['filters']['field_is_uv_resistant_value']['table'] = 'field_data_field_is_uv_resistant';
  $handler->display->display_options['filters']['field_is_uv_resistant_value']['field'] = 'field_is_uv_resistant_value';
  $handler->display->display_options['filters']['field_is_uv_resistant_value']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_is_uv_resistant_value']['expose']['operator_id'] = 'field_is_uv_resistant_value_op';
  $handler->display->display_options['filters']['field_is_uv_resistant_value']['expose']['label'] = 'Is UV Resistant';
  $handler->display->display_options['filters']['field_is_uv_resistant_value']['expose']['operator'] = 'field_is_uv_resistant_value_op';
  $handler->display->display_options['filters']['field_is_uv_resistant_value']['expose']['identifier'] = 'field_is_uv_resistant_value';
  $handler->display->display_options['filters']['field_is_uv_resistant_value']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
    4 => 0,
  );
  /* Filter criterion: Content: Is Waterproof (field_is_waterproof) */
  $handler->display->display_options['filters']['field_is_waterproof_value']['id'] = 'field_is_waterproof_value';
  $handler->display->display_options['filters']['field_is_waterproof_value']['table'] = 'field_data_field_is_waterproof';
  $handler->display->display_options['filters']['field_is_waterproof_value']['field'] = 'field_is_waterproof_value';
  $handler->display->display_options['filters']['field_is_waterproof_value']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_is_waterproof_value']['expose']['operator_id'] = 'field_is_waterproof_value_op';
  $handler->display->display_options['filters']['field_is_waterproof_value']['expose']['label'] = 'Is Waterproof';
  $handler->display->display_options['filters']['field_is_waterproof_value']['expose']['operator'] = 'field_is_waterproof_value_op';
  $handler->display->display_options['filters']['field_is_waterproof_value']['expose']['identifier'] = 'field_is_waterproof_value';
  $handler->display->display_options['filters']['field_is_waterproof_value']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
    4 => 0,
  );
  /* Filter criterion: Content: Box Weight (field_box_weight) */
  $handler->display->display_options['filters']['field_box_weight_tid']['id'] = 'field_box_weight_tid';
  $handler->display->display_options['filters']['field_box_weight_tid']['table'] = 'field_data_field_box_weight';
  $handler->display->display_options['filters']['field_box_weight_tid']['field'] = 'field_box_weight_tid';
  $handler->display->display_options['filters']['field_box_weight_tid']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_box_weight_tid']['expose']['operator_id'] = 'field_box_weight_tid_op';
  $handler->display->display_options['filters']['field_box_weight_tid']['expose']['label'] = 'Box Weight';
  $handler->display->display_options['filters']['field_box_weight_tid']['expose']['operator'] = 'field_box_weight_tid_op';
  $handler->display->display_options['filters']['field_box_weight_tid']['expose']['identifier'] = 'field_box_weight_tid';
  $handler->display->display_options['filters']['field_box_weight_tid']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
    4 => 0,
  );
  $handler->display->display_options['filters']['field_box_weight_tid']['type'] = 'select';
  $handler->display->display_options['filters']['field_box_weight_tid']['vocabulary'] = 'box_weight';

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['path'] = 'admin/find/products';
  $export['product_finder'] = $view;

  $view = new view();
  $view->name = 'products_categories';
  $view->description = 'A view containing views displays for Shurtape product categories.';
  $view->tag = 'default';
  $view->base_table = 'taxonomy_term_data';
  $view->human_name = 'Products Categories';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Products Categories by Term ID';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Field: Taxonomy term: Name */
  $handler->display->display_options['fields']['name']['id'] = 'name';
  $handler->display->display_options['fields']['name']['table'] = 'taxonomy_term_data';
  $handler->display->display_options['fields']['name']['field'] = 'name';
  $handler->display->display_options['fields']['name']['label'] = '';
  $handler->display->display_options['fields']['name']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['name']['alter']['ellipsis'] = FALSE;
  $handler->display->display_options['fields']['name']['link_to_taxonomy'] = TRUE;
  /* Sort criterion: Taxonomy term: Weight */
  $handler->display->display_options['sorts']['weight']['id'] = 'weight';
  $handler->display->display_options['sorts']['weight']['table'] = 'taxonomy_term_data';
  $handler->display->display_options['sorts']['weight']['field'] = 'weight';

  /* Display: Products Categories by Term ID */
  $handler = $view->new_display('block', 'Products Categories by Term ID', 'by_term_id');
  $handler->display->display_options['display_description'] = 'Displays a 1 level of child terms given a parent term ID.';
  $handler->display->display_options['defaults']['cache'] = FALSE;
  $handler->display->display_options['cache']['type'] = 'time';
  $handler->display->display_options['cache']['results_lifespan'] = '518400';
  $handler->display->display_options['cache']['results_lifespan_custom'] = '0';
  $handler->display->display_options['cache']['output_lifespan'] = '518400';
  $handler->display->display_options['cache']['output_lifespan_custom'] = '0';
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Taxonomy term: Term ID */
  $handler->display->display_options['fields']['tid']['id'] = 'tid';
  $handler->display->display_options['fields']['tid']['table'] = 'taxonomy_term_data';
  $handler->display->display_options['fields']['tid']['field'] = 'tid';
  $handler->display->display_options['fields']['tid']['label'] = '';
  $handler->display->display_options['fields']['tid']['element_label_colon'] = FALSE;
  /* Field: Taxonomy term: Name */
  $handler->display->display_options['fields']['name']['id'] = 'name';
  $handler->display->display_options['fields']['name']['table'] = 'taxonomy_term_data';
  $handler->display->display_options['fields']['name']['field'] = 'name';
  $handler->display->display_options['fields']['name']['label'] = '';
  $handler->display->display_options['fields']['name']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['name']['alter']['ellipsis'] = FALSE;
  $handler->display->display_options['fields']['name']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['name']['link_to_taxonomy'] = TRUE;
  /* Field: Taxonomy term: Category page - sub-category images */
  $handler->display->display_options['fields']['field_term_image']['id'] = 'field_term_image';
  $handler->display->display_options['fields']['field_term_image']['table'] = 'field_data_field_term_image';
  $handler->display->display_options['fields']['field_term_image']['field'] = 'field_term_image';
  $handler->display->display_options['fields']['field_term_image']['label'] = '';
  $handler->display->display_options['fields']['field_term_image']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_term_image']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_term_image']['settings'] = array(
    'image_style' => '',
    'image_link' => '',
  );
  /* Field: Taxonomy term: Category page - sub-category images */
  $handler->display->display_options['fields']['field_markets_term_image']['id'] = 'field_markets_term_image';
  $handler->display->display_options['fields']['field_markets_term_image']['table'] = 'field_data_field_markets_term_image';
  $handler->display->display_options['fields']['field_markets_term_image']['field'] = 'field_markets_term_image';
  $handler->display->display_options['fields']['field_markets_term_image']['label'] = '';
  $handler->display->display_options['fields']['field_markets_term_image']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_markets_term_image']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_markets_term_image']['settings'] = array(
    'image_style' => '',
    'image_link' => '',
  );
  $handler->display->display_options['defaults']['arguments'] = FALSE;
  /* Contextual filter: Taxonomy term: Parent term */
  $handler->display->display_options['arguments']['parent']['id'] = 'parent';
  $handler->display->display_options['arguments']['parent']['table'] = 'taxonomy_term_hierarchy';
  $handler->display->display_options['arguments']['parent']['field'] = 'parent';
  $handler->display->display_options['arguments']['parent']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['parent']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['parent']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['parent']['summary_options']['items_per_page'] = '25';
  $handler->display->display_options['block_description'] = 'Products Categories by Term ID';
  $export['products_categories'] = $view;

  return $export;
}
