<?php
/**
 * @file
 * products_navigation_feature.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function products_navigation_feature_views_default_views() 
{
	$export = array();

	$view = new view();
	$view->name = 'product_count';
	$view->description = 'Returns the total number of nodes under a given taxonomy.';
	$view->tag = 'default';
	$view->base_table = 'node';
	$view->human_name = 'Product Count';
	$view->core = 7;
	$view->api_version = '3.0';
	$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

	/* Display: Master */
	$handler = $view->new_display('default', 'Master', 'default');
	$handler->display->display_options['title'] = 'Product Count by Term';
	$handler->display->display_options['use_more_always'] = FALSE;
	$handler->display->display_options['access']['type'] = 'perm';
	$handler->display->display_options['cache']['type'] = 'none';
	$handler->display->display_options['query']['type'] = 'views_query';
	$handler->display->display_options['exposed_form']['type'] = 'basic';
	$handler->display->display_options['pager']['type'] = 'none';
	$handler->display->display_options['pager']['options']['offset'] = '0';
	$handler->display->display_options['style_plugin'] = 'default';
	$handler->display->display_options['row_plugin'] = 'fields';
	
	/* Field: Content: Title */
	$handler->display->display_options['fields']['title']['id'] = 'title';
	$handler->display->display_options['fields']['title']['table'] = 'node';
	$handler->display->display_options['fields']['title']['field'] = 'title';
	$handler->display->display_options['fields']['title']['label'] = '';
	$handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
	$handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
	
	/* Sort criterion: Content: Post date */
	$handler->display->display_options['sorts']['created']['id'] = 'created';
	$handler->display->display_options['sorts']['created']['table'] = 'node';
	$handler->display->display_options['sorts']['created']['field'] = 'created';
	$handler->display->display_options['sorts']['created']['order'] = 'DESC';
	
	/* Contextual filter: Content: Has taxonomy term ID (with depth) */
	$handler->display->display_options['arguments']['term_node_tid_depth']['id'] = 'term_node_tid_depth';
	$handler->display->display_options['arguments']['term_node_tid_depth']['table'] = 'node';
	$handler->display->display_options['arguments']['term_node_tid_depth']['field'] = 'term_node_tid_depth';
	$handler->display->display_options['arguments']['term_node_tid_depth']['default_argument_type'] = 'fixed';
	$handler->display->display_options['arguments']['term_node_tid_depth']['summary']['number_of_records'] = '0';
	$handler->display->display_options['arguments']['term_node_tid_depth']['summary']['format'] = 'default_summary';
	$handler->display->display_options['arguments']['term_node_tid_depth']['summary_options']['items_per_page'] = '25';
	$handler->display->display_options['arguments']['term_node_tid_depth']['depth'] = '10';
	
	/* Filter criterion: Content: Published */
	$handler->display->display_options['filters']['status']['id'] = 'status';
	$handler->display->display_options['filters']['status']['table'] = 'node';
	$handler->display->display_options['filters']['status']['field'] = 'status';
	$handler->display->display_options['filters']['status']['value'] = 1;
	$handler->display->display_options['filters']['status']['group'] = 1;
	$handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;

	/* Display: Product Count by Term */
	$handler = $view->new_display('block', 'Product Count by Term', 'by_term_id');
	$handler->display->display_options['display_description'] = 'Displays the total number of nodes under a term.';
	$handler->display->display_options['defaults']['fields'] = FALSE;
	
	/* Field: Content: Nid */
	$handler->display->display_options['fields']['nid']['id'] = 'nid';
	$handler->display->display_options['fields']['nid']['table'] = 'node';
	$handler->display->display_options['fields']['nid']['field'] = 'nid';
	$handler->display->display_options['fields']['nid']['label'] = '';
	$handler->display->display_options['fields']['nid']['element_label_colon'] = FALSE;
	$handler->display->display_options['defaults']['filter_groups'] = FALSE;
	$handler->display->display_options['defaults']['filters'] = FALSE;
	
	/* Filter criterion: Content: Published */
	$handler->display->display_options['filters']['status']['id'] = 'status';
	$handler->display->display_options['filters']['status']['table'] = 'node';
	$handler->display->display_options['filters']['status']['field'] = 'status';
	$handler->display->display_options['filters']['status']['value'] = 1;
	$handler->display->display_options['filters']['status']['group'] = 1;
	$handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
	
	/* Filter criterion: Content: Type */
	$handler->display->display_options['filters']['type']['id'] = 'type';
	$handler->display->display_options['filters']['type']['table'] = 'node';
	$handler->display->display_options['filters']['type']['field'] = 'type';
	$handler->display->display_options['filters']['type']['value'] = array(
		'product' => 'product',
	);
	$handler->display->display_options['block_description'] = 'Product Count by Term';
	$export['product_count'] = $view;
	return $export;
}