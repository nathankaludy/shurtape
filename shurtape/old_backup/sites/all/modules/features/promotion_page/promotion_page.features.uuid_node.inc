<?php
/**
 * @file
 * promotion_page.features.uuid_node.inc
 */

/**
 * Implements hook_uuid_features_default_content().
 */
function promotion_page_uuid_features_default_content() {
  $nodes = array();

  $nodes[] = array(
  'uid' => 1,
  'title' => 'Promotion form template',
  'log' => '',
  'status' => 1,
  'comment' => 1,
  'promote' => 0,
  'sticky' => 0,
  'vuuid' => '7d0b7279-b3a3-49da-8ff9-b25811bc8e89',
  'type' => 'webform',
  'language' => 'und',
  'created' => 1393435574,
  'tnid' => 0,
  'translate' => 0,
  'uuid' => '4a220085-e863-4a90-b98c-7bdbd0c238ec',
  'revision_uid' => 1,
  'body' => array(),
  'metatags' => array(),
  'rdf_mapping' => array(
    'rdftype' => array(
      0 => 'sioc:Item',
      1 => 'foaf:Document',
    ),
    'title' => array(
      'predicates' => array(
        0 => 'dc:title',
      ),
    ),
    'created' => array(
      'predicates' => array(
        0 => 'dc:date',
        1 => 'dc:created',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'changed' => array(
      'predicates' => array(
        0 => 'dc:modified',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'body' => array(
      'predicates' => array(
        0 => 'content:encoded',
      ),
    ),
    'uid' => array(
      'predicates' => array(
        0 => 'sioc:has_creator',
      ),
      'type' => 'rel',
    ),
    'name' => array(
      'predicates' => array(
        0 => 'foaf:name',
      ),
    ),
    'comment_count' => array(
      'predicates' => array(
        0 => 'sioc:num_replies',
      ),
      'datatype' => 'xsd:integer',
    ),
    'last_activity' => array(
      'predicates' => array(
        0 => 'sioc:last_activity_date',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
  ),
  'webform' => array(
    'nid' => 13353,
    'confirmation' => '',
    'confirmation_format' => 'full_html',
    'redirect_url' => 'promotion/[node:nid]/thank-you',
    'status' => 1,
    'block' => 0,
    'allow_draft' => 0,
    'auto_save' => 0,
    'submit_notice' => 0,
    'submit_text' => '',
    'submit_limit' => -1,
    'submit_interval' => -1,
    'total_submit_limit' => -1,
    'total_submit_interval' => -1,
    'progressbar_bar' => 0,
    'progressbar_page_number' => 0,
    'progressbar_percent' => 0,
    'progressbar_pagebreak_labels' => 0,
    'progressbar_include_confirmation' => 0,
    'progressbar_label_first' => NULL,
    'progressbar_label_confirmation' => NULL,
    'preview' => 0,
    'preview_next_button_label' => NULL,
    'preview_prev_button_label' => NULL,
    'preview_title' => NULL,
    'preview_message' => '',
    'preview_message_format' => NULL,
    'preview_excluded_components' => array(),
    'next_serial' => 1,
    'record_exists' => TRUE,
    'roles' => array(
      0 => 1,
      1 => 2,
      2 => 3,
      3 => 4,
    ),
    'emails' => array(),
    'components' => array(
      1 => array(
        'nid' => 13353,
        'cid' => 1,
        'pid' => 0,
        'form_key' => 'contact_name',
        'name' => 'Name',
        'type' => 'textfield',
        'value' => '',
        'extra' => array(
          'locked' => 1,
          'title_display' => 'none',
          'private' => 0,
          'width' => 32,
          'disabled' => 0,
          'unique' => 0,
          'maxlength' => '',
          'field_prefix' => '',
          'field_suffix' => '',
          'description' => '',
          'attributes' => array(),
          'placeholder' => '',
          'analysis' => FALSE,
        ),
        'required' => 1,
        'weight' => 19,
        'page_num' => 1,
      ),
      2 => array(
        'nid' => 13353,
        'cid' => 2,
        'pid' => 0,
        'form_key' => 'email',
        'name' => 'Email',
        'type' => 'email',
        'value' => '',
        'extra' => array(
          'locked' => 1,
          'title_display' => 'none',
          'private' => 0,
          'width' => 31,
          'disabled' => 0,
          'unique' => 0,
          'description' => '',
          'attributes' => array(),
          'placeholder' => '',
          'analysis' => FALSE,
        ),
        'required' => 1,
        'weight' => 20,
        'page_num' => 1,
      ),
      21 => array(
        'nid' => 13353,
        'cid' => 21,
        'pid' => 0,
        'form_key' => 'name_error_msg',
        'name' => 'Name error msg',
        'type' => 'markup',
        'value' => '<p class="promotion_error_message"><span style="color: #ff0000;">Please enter your name.</span></p>',
        'extra' => array(
          'locked' => 1,
          'format' => 'full_html',
          'private' => 0,
        ),
        'required' => 0,
        'weight' => 21,
        'page_num' => 1,
      ),
      17 => array(
        'nid' => 13353,
        'cid' => 17,
        'pid' => 0,
        'form_key' => 'email_error_message',
        'name' => 'Email Error message',
        'type' => 'markup',
        'value' => '<p class="promotion_error_message"><span style="color: #ff0000;">Please enter a valid email address.</span></p>',
        'extra' => array(
          'locked' => 1,
          'format' => 'full_html',
          'private' => 0,
        ),
        'required' => 0,
        'weight' => 22,
        'page_num' => 1,
      ),
      13 => array(
        'nid' => 13353,
        'cid' => 13,
        'pid' => 0,
        'form_key' => 'shipping_address',
        'name' => 'Shipping Address',
        'type' => 'textfield',
        'value' => '',
        'extra' => array(
          'locked' => 1,
          'title_display' => 'none',
          'private' => 0,
          'width' => 32,
          'disabled' => 0,
          'unique' => 0,
          'maxlength' => '',
          'field_prefix' => '',
          'field_suffix' => '',
          'description' => '',
          'attributes' => array(),
          'placeholder' => '',
          'analysis' => FALSE,
        ),
        'required' => 1,
        'weight' => 23,
        'page_num' => 1,
      ),
      3 => array(
        'nid' => 13353,
        'cid' => 3,
        'pid' => 0,
        'form_key' => 'address2',
        'name' => 'Address 2',
        'type' => 'textfield',
        'value' => '',
        'extra' => array(
          'locked' => 1,
          'title_display' => 'none',
          'private' => 0,
          'width' => 32,
          'disabled' => 0,
          'unique' => 0,
          'maxlength' => '',
          'field_prefix' => '',
          'field_suffix' => '',
          'description' => '',
          'attributes' => array(),
          'placeholder' => '',
          'analysis' => FALSE,
        ),
        'required' => 0,
        'weight' => 24,
        'page_num' => 1,
      ),
      22 => array(
        'nid' => 13353,
        'cid' => 22,
        'pid' => 0,
        'form_key' => 'shipping_error_msg',
        'name' => 'Shipping error msg',
        'type' => 'markup',
        'value' => '<p><span style="color: #ff0000;">Please enter a shipping address.</span></p>',
        'extra' => array(
          'locked' => 1,
          'format' => 'full_html',
          'private' => 0,
        ),
        'required' => 0,
        'weight' => 25,
        'page_num' => 1,
      ),
      4 => array(
        'nid' => 13353,
        'cid' => 4,
        'pid' => 0,
        'form_key' => 'city',
        'name' => 'City',
        'type' => 'textfield',
        'value' => '',
        'extra' => array(
          'locked' => 1,
          'title_display' => 'none',
          'private' => 0,
          'width' => 7,
          'disabled' => 0,
          'unique' => 0,
          'maxlength' => '',
          'field_prefix' => '',
          'field_suffix' => '',
          'description' => '',
          'attributes' => array(),
          'placeholder' => '',
          'analysis' => FALSE,
        ),
        'required' => 1,
        'weight' => 26,
        'page_num' => 1,
      ),
      11 => array(
        'nid' => 13353,
        'cid' => 11,
        'pid' => 0,
        'form_key' => 'states',
        'name' => 'State',
        'type' => 'select',
        'value' => 0,
        'extra' => array(
          'locked' => 1,
          'title_display' => 'none',
          'private' => 0,
          'aslist' => 1,
          'optrand' => 0,
          'items' => '0|State:
AL|Alabama
AK|Alaska
AS|American Samoa
AZ|Arizona
AR|Arkansas
CA|California
CO|Colorado
CT|Connecticut
DE|Delaware
DC|District of Columbia
FL|Florida
GA|Georgia
GU|Guam
HI|Hawaii
ID|Idaho
IL|Illinois
IN|Indiana
IA|Iowa
KS|Kansas
KY|Kentucky
LA|Louisiana
ME|Maine
MH|Marshall Islands
MD|Maryland
MA|Massachusetts
MI|Michigan
MN|Minnesota
MS|Mississippi
MO|Missouri
MT|Montana
NE|Nebraska
NV|Nevada
NH|New Hampshire
NJ|New Jersey
NM|New Mexico
NY|New York
NC|North Carolina
ND|North Dakota
MP|Northern Marianas Islands
OH|Ohio
OK|Oklahoma
OR|Oregon
PW|Palau
PA|Pennsylvania
PR|Puerto Rico
RI|Rhode Island
SC|South Carolina
SD|South Dakota
TN|Tennessee
TX|Texas
UT|Utah
VT|Vermont
VI|Virgin Islands
VA|Virginia
WA|Washington
WV|West Virginia
WI|Wisconsin
WY|Wyoming
',
          'multiple' => 0,
          'custom_keys' => 1,
          'other_option' => NULL,
          'other_text' => 'Other...',
          'description' => '',
          'options_source' => '',
          'analysis' => TRUE,
        ),
        'required' => 1,
        'weight' => 27,
        'page_num' => 1,
      ),
      6 => array(
        'nid' => 13353,
        'cid' => 6,
        'pid' => 0,
        'form_key' => 'zip',
        'name' => 'Zip',
        'type' => 'textfield',
        'value' => '',
        'extra' => array(
          'locked' => 1,
          'title_display' => 'none',
          'private' => 0,
          'width' => 6,
          'disabled' => 0,
          'unique' => 0,
          'maxlength' => '',
          'field_prefix' => '',
          'field_suffix' => '',
          'description' => '',
          'attributes' => array(),
          'placeholder' => '',
          'analysis' => FALSE,
        ),
        'required' => 1,
        'weight' => 28,
        'page_num' => 1,
      ),
      23 => array(
        'nid' => 13353,
        'cid' => 23,
        'pid' => 0,
        'form_key' => 'city_error_msg',
        'name' => 'City error msg',
        'type' => 'markup',
        'value' => '<p><span style="color: #ff0000;">Please enter your city and select a state.</span></p>',
        'extra' => array(
          'locked' => 1,
          'format' => 'full_html',
          'private' => 0,
        ),
        'required' => 0,
        'weight' => 29,
        'page_num' => 1,
      ),
      20 => array(
        'nid' => 13353,
        'cid' => 20,
        'pid' => 0,
        'form_key' => 'zip_error_message',
        'name' => 'ZIP Error message',
        'type' => 'markup',
        'value' => '<p class="promotion_error_message"><span style="color: #ff0000;">Please enter a ZIP code.</span></p>',
        'extra' => array(
          'locked' => 1,
          'format' => 'full_html',
          'private' => 0,
        ),
        'required' => 0,
        'weight' => 30,
        'page_num' => 1,
      ),
      14 => array(
        'nid' => 13353,
        'cid' => 14,
        'pid' => 0,
        'form_key' => 'custom_options_1',
        'name' => 'Custom Options 1',
        'type' => 'select',
        'value' => 1,
        'extra' => array(
          'title_display' => 'none',
          'private' => 0,
          'aslist' => 1,
          'optrand' => 0,
          'items' => '1|Select a Tape Sample
',
          'multiple' => 0,
          'custom_keys' => 0,
          'other_option' => NULL,
          'other_text' => 'Other...',
          'description' => '',
          'options_source' => '',
          'analysis' => TRUE,
        ),
        'required' => 1,
        'weight' => 31,
        'page_num' => 1,
      ),
      15 => array(
        'nid' => 13353,
        'cid' => 15,
        'pid' => 0,
        'form_key' => 'custom_options_2',
        'name' => 'Custom Options 2',
        'type' => 'select',
        'value' => 1,
        'extra' => array(
          'title_display' => 'none',
          'private' => 0,
          'aslist' => 1,
          'optrand' => 0,
          'items' => '1|Select a Job Function
',
          'multiple' => 0,
          'custom_keys' => 0,
          'other_option' => NULL,
          'other_text' => 'Other...',
          'description' => '',
          'options_source' => '',
          'analysis' => TRUE,
        ),
        'required' => 1,
        'weight' => 32,
        'page_num' => 1,
      ),
    ),
    'conditionals' => array(),
  ),
  'cid' => 0,
  'last_comment_name' => NULL,
  'last_comment_uid' => 1,
  'comment_count' => 0,
  'locations' => array(),
  'location' => array(),
  'name' => 'admin',
  'picture' => 0,
  'data' => 'b:0;',
  'date' => '2014-02-26 12:26:14 -0500',
);
  return $nodes;
}
