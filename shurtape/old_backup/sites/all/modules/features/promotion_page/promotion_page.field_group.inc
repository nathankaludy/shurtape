<?php
/**
 * @file
 * promotion_page.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function promotion_page_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_logos|node|promotion_page|form';
  $field_group->group_name = 'group_logos';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'promotion_page';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Logos',
    'weight' => '10',
    'children' => array(
      0 => 'field_promotion_logos',
      1 => 'field_promotion_logos_links',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-logos field-group-fieldset',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_logos|node|promotion_page|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_promo_thank_you|node|promotion_page|form';
  $field_group->group_name = 'group_promo_thank_you';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'promotion_page';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Thank-you',
    'weight' => '14',
    'children' => array(
      0 => 'field_promo_content_title',
      1 => 'field_promotion_content_body',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-promo-thank-you field-group-fieldset',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_promo_thank_you|node|promotion_page|form'] = $field_group;

  return $export;
}
