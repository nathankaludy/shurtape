<?php
/**
 * @file
 * promotion_page.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function promotion_page_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create promotion_page content'.
  $permissions['create promotion_page content'] = array(
    'name' => 'create promotion_page content',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any promotion_page content'.
  $permissions['delete any promotion_page content'] = array(
    'name' => 'delete any promotion_page content',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own promotion_page content'.
  $permissions['delete own promotion_page content'] = array(
    'name' => 'delete own promotion_page content',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any promotion_page content'.
  $permissions['edit any promotion_page content'] = array(
    'name' => 'edit any promotion_page content',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
    ),
    'module' => 'node',
  );

  return $permissions;
}
