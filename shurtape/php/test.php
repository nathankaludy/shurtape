<thead>
				<tr>
					<th class="first-col">PRODUCT<span class="desktop"> NAME</span></th>
					<th class="accept" id="tensile">TENSILE STRENGTH</th>
					<th class="accept" id="adhesion">ADHESION TO STEEL</th>
					<th class="accept" id="adhesive">ADHESIVE</th>
					<th class="accept" id="backing">BACKING</th>
					<th class="accept" id="serv_temp_range">SERV. TEMP RANGE</th>
					<th class="accept" id="app_temp_range">APPLICAT. TEMP RANGE</th>
					<th class="accept" id="elongation">ELONGATION</th>
					<th class="accept" id="thickness">THICKNESS</th>
					<th class="accept" id="thickness_liner">THICKNESS WITH LINER</th>
					<th class="accept" id="thickness_no_liner">THICKNESS WITHOUT LINER</th>
					<th class="accept" id="holding_power"><span class="desktop">HOLDING POWER TO FIBERBOARD</span><span class="mobile">HOLDING POWER<br />TO FIBERBOARD</span></th>
				</tr>
			</thead>
