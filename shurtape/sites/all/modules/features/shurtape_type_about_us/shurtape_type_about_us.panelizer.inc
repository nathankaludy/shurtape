<?php
/**
 * @file
 * shurtape_type_about_us.panelizer.inc
 */

/**
 * Implements hook_panelizer_defaults().
 */
function shurtape_type_about_us_panelizer_defaults() {
  $export = array();

  $panelizer = new stdClass();
  $panelizer->disabled = FALSE; /* Edit this to true to make a default panelizer disabled initially */
  $panelizer->api_version = 1;
  $panelizer->name = 'node:about_us:default';
  $panelizer->title = 'Default';
  $panelizer->panelizer_type = 'node';
  $panelizer->panelizer_key = 'about_us';
  $panelizer->no_blocks = FALSE;
  $panelizer->css_id = '';
  $panelizer->css = '';
  $panelizer->pipeline = 'standard';
  $panelizer->contexts = array();
  $panelizer->relationships = array();
  $panelizer->access = array();
  $panelizer->view_mode = 'page_manager';
  $panelizer->css_class = '';
  $panelizer->title_element = 'H2';
  $panelizer->link_to_entity = TRUE;
  $panelizer->extra = array();
  $display = new panels_display();
  $display->layout = 'page_basic';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'title' => NULL,
      'content_top' => NULL,
      'help' => NULL,
      'breadcrumb_section' => NULL,
      'content' => NULL,
      'bottom' => NULL,
      'default' => NULL,
    ),
    'title' => array(
      'style' => 'naked',
    ),
    'content_top' => array(
      'style' => 'naked',
    ),
    'help' => array(
      'style' => 'naked',
    ),
    'breadcrumb_section' => array(
      'style' => 'naked',
    ),
    'content' => array(
      'style' => 'naked',
    ),
    'bottom' => array(
      'style' => 'naked',
    ),
  );
  $display->cache = array();
  $display->title = '%node:title';
  $display->uuid = '5a51c623-0607-4eb9-9efb-2f61ce42deea';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-22ca7c1a-29e0-4b2f-99cb-86a266eeadf8';
    $pane->panel = 'content';
    $pane->type = 'entity_field';
    $pane->subtype = 'node:body';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'hidden',
      'formatter' => 'text_default',
      'delta_limit' => 0,
      'delta_offset' => '0',
      'delta_reversed' => FALSE,
      'formatter_settings' => array(),
      'context' => 'panelizer',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
      'style' => 'naked',
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '22ca7c1a-29e0-4b2f-99cb-86a266eeadf8';
    $display->content['new-22ca7c1a-29e0-4b2f-99cb-86a266eeadf8'] = $pane;
    $display->panels['content'][0] = 'new-22ca7c1a-29e0-4b2f-99cb-86a266eeadf8';
    $pane = new stdClass();
    $pane->pid = 'new-800444b0-391f-4827-8ab2-e17f8bb6f06f';
    $pane->panel = 'content';
    $pane->type = 'entity_field';
    $pane->subtype = 'node:field_company_pro';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'hidden',
      'formatter' => 'image',
      'delta_limit' => 0,
      'delta_offset' => '0',
      'delta_reversed' => FALSE,
      'formatter_settings' => array(
        'image_link' => '',
        'image_style' => '',
      ),
      'context' => 'panelizer',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
      'style' => 'naked',
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = '800444b0-391f-4827-8ab2-e17f8bb6f06f';
    $display->content['new-800444b0-391f-4827-8ab2-e17f8bb6f06f'] = $pane;
    $display->panels['content'][1] = 'new-800444b0-391f-4827-8ab2-e17f8bb6f06f';
    $pane = new stdClass();
    $pane->pid = 'new-9b8aed90-b0e3-49ef-bbbc-ed86f7419df0';
    $pane->panel = 'title';
    $pane->type = 'node_title';
    $pane->subtype = 'node_title';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'link' => 0,
      'markup' => 'h1',
      'id' => '',
      'class' => 'headline',
      'context' => 'panelizer',
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
      'style' => 'naked',
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '9b8aed90-b0e3-49ef-bbbc-ed86f7419df0';
    $display->content['new-9b8aed90-b0e3-49ef-bbbc-ed86f7419df0'] = $pane;
    $display->panels['title'][0] = 'new-9b8aed90-b0e3-49ef-bbbc-ed86f7419df0';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = 'new-800444b0-391f-4827-8ab2-e17f8bb6f06f';
  $panelizer->display = $display;
  $export['node:about_us:default'] = $panelizer;

  return $export;
}
