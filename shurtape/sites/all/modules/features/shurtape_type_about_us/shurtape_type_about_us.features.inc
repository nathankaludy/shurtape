<?php
/**
 * @file
 * shurtape_type_about_us.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function shurtape_type_about_us_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "panelizer" && $api == "panelizer") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function shurtape_type_about_us_node_info() {
  $items = array(
    'about_us' => array(
      'name' => t('About Us'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => t('Company Profile , Static page '),
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
