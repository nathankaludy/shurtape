<?php
/**
 * @file
 * where_to_buy_phase_2.feeds_tamper_default.inc
 */

/**
 * Implements hook_feeds_tamper_default().
 */
function where_to_buy_phase_2_feeds_tamper_default() {
  $export = array();

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'distributors_import_csv-guid-rewrite';
  $feeds_tamper->importer = 'distributors_import_csv';
  $feeds_tamper->source = 'GUID';
  $feeds_tamper->plugin_id = 'rewrite';
  $feeds_tamper->settings = array(
    'text' => '[name][fulladdress]',
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Rewrite';
  $export['distributors_import_csv-guid-rewrite'] = $feeds_tamper;

  return $export;
}
