<?php
/**
 * @file
 * where_to_buy_phase_2.feeds_importer_default.inc
 */

/**
 * Implements hook_feeds_importer_default().
 */
function where_to_buy_phase_2_feeds_importer_default() {
  $export = array();

  $feeds_importer = new stdClass();
  $feeds_importer->disabled = FALSE; /* Edit this to true to make a default feeds_importer disabled initially */
  $feeds_importer->api_version = 1;
  $feeds_importer->id = 'distributors_import';
  $feeds_importer->config = array(
    'name' => 'Distributors Import',
    'description' => '',
    'fetcher' => array(
      'plugin_key' => 'FeedsFileFetcher',
      'config' => array(
        'allowed_extensions' => 'xls xlsx',
        'direct' => 0,
        'directory' => 'public://feeds/distributors',
        'allowed_schemes' => array(
          's3' => 's3',
          'public' => 'public',
        ),
      ),
    ),
    'parser' => array(
      'plugin_key' => 'FeedsExcelParser',
      'config' => array(
        'no_headers' => 0,
        'all_worksheets' => 0,
      ),
    ),
    'processor' => array(
      'plugin_key' => 'FeedsNodeProcessor',
      'config' => array(
        'expire' => '-1',
        'author' => '6',
        'authorize' => 0,
        'mappings' => array(
          0 => array(
            'source' => 'INDUSTRIAL',
            'target' => 'industrial',
            'unique' => FALSE,
          ),
          1 => array(
            'source' => 'INDUSTRIAL PARTNER',
            'target' => 'industrial_partner',
            'unique' => FALSE,
          ),
          2 => array(
            'source' => 'HVAC',
            'target' => 'hvac',
            'unique' => FALSE,
          ),
          3 => array(
            'source' => 'NAME',
            'target' => 'company_name',
            'unique' => FALSE,
          ),
          4 => array(
            'source' => 'ADDRESS',
            'target' => 'street',
            'unique' => FALSE,
          ),
          5 => array(
            'source' => 'CITY',
            'target' => 'city',
            'unique' => FALSE,
          ),
          6 => array(
            'source' => 'STATE',
            'target' => 'state',
            'unique' => FALSE,
          ),
          7 => array(
            'source' => 'ZIP',
            'target' => 'zip',
            'unique' => FALSE,
          ),
          8 => array(
            'source' => 'PHONE',
            'target' => 'phone',
            'unique' => FALSE,
          ),
          9 => array(
            'source' => 'WEBSITE',
            'target' => 'website',
            'unique' => FALSE,
          ),
        ),
        'update_existing' => '0',
        'input_format' => 'plain_text',
        'skip_hash_check' => 0,
        'bundle' => 'distributor',
      ),
    ),
    'content_type' => '',
    'update' => 0,
    'import_period' => '-1',
    'expire_period' => 3600,
    'import_on_create' => 1,
    'process_in_background' => 0,
  );
  $export['distributors_import'] = $feeds_importer;

  $feeds_importer = new stdClass();
  $feeds_importer->disabled = FALSE; /* Edit this to true to make a default feeds_importer disabled initially */
  $feeds_importer->api_version = 1;
  $feeds_importer->id = 'distributors_import_csv';
  $feeds_importer->config = array(
    'name' => 'Distributors import CSV',
    'description' => '',
    'fetcher' => array(
      'plugin_key' => 'FeedsFileFetcher',
      'config' => array(
        'allowed_extensions' => 'csv',
        'direct' => 0,
        'directory' => 'public://feeds/distributors',
        'allowed_schemes' => array(
          's3' => 's3',
          'public' => 'public',
        ),
      ),
    ),
    'parser' => array(
      'plugin_key' => 'FeedsCSVParser',
      'config' => array(
        'delimiter' => ',',
        'no_headers' => 0,
      ),
    ),
    'processor' => array(
      'plugin_key' => 'FeedsNodeProcessor',
      'config' => array(
        'expire' => '-1',
        'author' => '6',
        'authorize' => 0,
        'mappings' => array(
          0 => array(
            'source' => 'GUID',
            'target' => 'guid',
            'unique' => 1,
          ),
          1 => array(
            'source' => 'NAME',
            'target' => 'title',
            'unique' => FALSE,
          ),
          2 => array(
            'source' => 'PHONE',
            'target' => 'field_phone_number',
            'unique' => FALSE,
          ),
          3 => array(
            'source' => 'FULLADDRESS',
            'target' => 'address',
            'unique' => FALSE,
          ),
          4 => array(
            'source' => 'INDUSTRIAL',
            'target' => 'industrial',
            'unique' => FALSE,
          ),
          5 => array(
            'source' => 'HVAC',
            'target' => 'hvac',
            'unique' => FALSE,
          ),
          6 => array(
            'source' => 'INDUSTRIAL PARTNER',
            'target' => 'industrial_partner',
            'unique' => FALSE,
          ),
          7 => array(
            'source' => 'WEBSITE',
            'target' => 'website',
            'unique' => FALSE,
          ),
        ),
        'update_existing' => '2',
        'input_format' => 'plain_text',
        'skip_hash_check' => 1,
        'bundle' => 'distributor',
        'update_non_existent' => 'skip',
        'insert_new' => '1',
      ),
    ),
    'content_type' => '',
    'update' => 0,
    'import_period' => '-1',
    'expire_period' => 3600,
    'import_on_create' => 1,
    'process_in_background' => 0,
  );
  $export['distributors_import_csv'] = $feeds_importer;

  return $export;
}
