<?php
/**
 * @file
 * blog_phase_1.default_field_validation_rules.inc
 */

/**
 * Implements hook_default_field_validation_rule().
 */
function blog_phase_1_default_field_validation_rule() {
  $export = array();

  $rule = new stdClass();
  $rule->disabled = FALSE; /* Edit this to true to make a default rule disabled initially */
  $rule->api_version = 2;
  $rule->rulename = 'YouTube Video IDs';
  $rule->name = 'youtube_video_ids';
  $rule->field_name = 'field_product_videos';
  $rule->col = 'value';
  $rule->entity_type = 'node';
  $rule->bundle = 'product';
  $rule->validator = 'field_validation_length_validator';
  $rule->settings = array(
    'min' => '5',
    'max' => '15',
    'bypass' => 0,
    'roles' => array(
      2 => 0,
      3 => 0,
      4 => 0,
    ),
    'errors' => 0,
  );
  $rule->error_message = 'The YouTube Video ID you are trying to use seems to be invalid';
  $export['youtube_video_ids'] = $rule;

  return $export;
}
