<?php
/**
 * @file
 * shurtape_site_layout.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function shurtape_site_layout_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'product_assets';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Product Assets';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Product Detail Assets';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'time';
  $handler->display->display_options['cache']['results_lifespan'] = '3600';
  $handler->display->display_options['cache']['results_lifespan_custom'] = '0';
  $handler->display->display_options['cache']['output_lifespan'] = '3600';
  $handler->display->display_options['cache']['output_lifespan_custom'] = '0';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Relationship: Content: Product Assets (field_product_assets) - reverse */
  $handler->display->display_options['relationships']['reverse_field_product_assets_node']['id'] = 'reverse_field_product_assets_node';
  $handler->display->display_options['relationships']['reverse_field_product_assets_node']['table'] = 'node';
  $handler->display->display_options['relationships']['reverse_field_product_assets_node']['field'] = 'reverse_field_product_assets_node';
  $handler->display->display_options['relationships']['reverse_field_product_assets_node']['required'] = TRUE;
  /* Field: Content: File Asset */
  $handler->display->display_options['fields']['field_file_asset']['id'] = 'field_file_asset';
  $handler->display->display_options['fields']['field_file_asset']['table'] = 'field_data_field_file_asset';
  $handler->display->display_options['fields']['field_file_asset']['field'] = 'field_file_asset';
  $handler->display->display_options['fields']['field_file_asset']['label'] = '';
  $handler->display->display_options['fields']['field_file_asset']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_file_asset']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_file_asset']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_file_asset']['type'] = 'file_url_plain';
  /* Field: Content: File Asset Type */
  $handler->display->display_options['fields']['field_file_asset_type']['id'] = 'field_file_asset_type';
  $handler->display->display_options['fields']['field_file_asset_type']['table'] = 'field_data_field_file_asset_type';
  $handler->display->display_options['fields']['field_file_asset_type']['field'] = 'field_file_asset_type';
  $handler->display->display_options['fields']['field_file_asset_type']['label'] = '';
  $handler->display->display_options['fields']['field_file_asset_type']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_file_asset_type']['alter']['text'] = '<a href="[field_file_asset]" class="grid_item pull-left">
	<img alt="[field_file_asset_type]" src="http://twshurtape.s3.amazonaws.com/imce/icon_pdf.png" width="31" height="30">
	<span class="title">[field_file_asset_type]</span>
</a>';
  $handler->display->display_options['fields']['field_file_asset_type']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_file_asset_type']['type'] = 'taxonomy_term_reference_plain';
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  /* Contextual filter: Content: Nid */
  $handler->display->display_options['arguments']['nid']['id'] = 'nid';
  $handler->display->display_options['arguments']['nid']['table'] = 'node';
  $handler->display->display_options['arguments']['nid']['field'] = 'nid';
  $handler->display->display_options['arguments']['nid']['relationship'] = 'reverse_field_product_assets_node';
  $handler->display->display_options['arguments']['nid']['default_action'] = 'default';
  $handler->display->display_options['arguments']['nid']['default_argument_type'] = 'node';
  $handler->display->display_options['arguments']['nid']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['nid']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['nid']['summary_options']['items_per_page'] = '25';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'product_asset' => 'product_asset',
  );
  /* Filter criterion: Content: Language */
  $handler->display->display_options['filters']['language']['id'] = 'language';
  $handler->display->display_options['filters']['language']['table'] = 'node';
  $handler->display->display_options['filters']['language']['field'] = 'language';
  $handler->display->display_options['filters']['language']['value'] = array(
    '***CURRENT_LANGUAGE***' => '***CURRENT_LANGUAGE***',
    'und' => 'und',
  );

  /* Display: Block */
  $handler = $view->new_display('block', 'Block', 'block');
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Content: File Asset */
  $handler->display->display_options['fields']['field_file_asset']['id'] = 'field_file_asset';
  $handler->display->display_options['fields']['field_file_asset']['table'] = 'field_data_field_file_asset';
  $handler->display->display_options['fields']['field_file_asset']['field'] = 'field_file_asset';
  $handler->display->display_options['fields']['field_file_asset']['label'] = '';
  $handler->display->display_options['fields']['field_file_asset']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_file_asset']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_file_asset']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_file_asset']['type'] = 'file_url_plain';
  /* Field: Content: File Asset Type */
  $handler->display->display_options['fields']['field_file_asset_type']['id'] = 'field_file_asset_type';
  $handler->display->display_options['fields']['field_file_asset_type']['table'] = 'field_data_field_file_asset_type';
  $handler->display->display_options['fields']['field_file_asset_type']['field'] = 'field_file_asset_type';
  $handler->display->display_options['fields']['field_file_asset_type']['label'] = '';
  $handler->display->display_options['fields']['field_file_asset_type']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_file_asset_type']['alter']['text'] = '<a target="_blank" href="[field_file_asset]" class="grid_item pull-left">
	<img alt="[field_file_asset_type]" src="/sites/default/files/imce/icon_pdf.png" width="31" height="30">
	<span class="title">[field_file_asset_type]</span>
</a>';
  $handler->display->display_options['fields']['field_file_asset_type']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_file_asset_type']['type'] = 'i18n_taxonomy_term_reference_plain';
  $handler->display->display_options['block_caching'] = '-1';

  /* Display: TDS Assets */
  $handler = $view->new_display('block', 'TDS Assets', 'block_1');
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Content: File Asset */
  $handler->display->display_options['fields']['field_file_asset']['id'] = 'field_file_asset';
  $handler->display->display_options['fields']['field_file_asset']['table'] = 'field_data_field_file_asset';
  $handler->display->display_options['fields']['field_file_asset']['field'] = 'field_file_asset';
  $handler->display->display_options['fields']['field_file_asset']['label'] = '';
  $handler->display->display_options['fields']['field_file_asset']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_file_asset']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_file_asset']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_file_asset']['type'] = 'file_url_plain';
  /* Field: Content: File Asset Type */
  $handler->display->display_options['fields']['field_file_asset_type']['id'] = 'field_file_asset_type';
  $handler->display->display_options['fields']['field_file_asset_type']['table'] = 'field_data_field_file_asset_type';
  $handler->display->display_options['fields']['field_file_asset_type']['field'] = 'field_file_asset_type';
  $handler->display->display_options['fields']['field_file_asset_type']['label'] = '';
  $handler->display->display_options['fields']['field_file_asset_type']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_file_asset_type']['alter']['text'] = '<a target="_blank" href="[field_file_asset]" class="grid_item pull-left"><img alt="[field_file_asset_type]" src="/sites/default/files/imce/icon_pdf.png" width="31" height="30"></a>';
  $handler->display->display_options['fields']['field_file_asset_type']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_file_asset_type']['type'] = 'taxonomy_term_reference_plain';
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'product_asset' => 'product_asset',
  );
  /* Filter criterion: Content: File Asset Type (field_file_asset_type) */
  $handler->display->display_options['filters']['field_file_asset_type_tid']['id'] = 'field_file_asset_type_tid';
  $handler->display->display_options['filters']['field_file_asset_type_tid']['table'] = 'field_data_field_file_asset_type';
  $handler->display->display_options['filters']['field_file_asset_type_tid']['field'] = 'field_file_asset_type_tid';
  $handler->display->display_options['filters']['field_file_asset_type_tid']['value'] = array(
    253 => '253',
  );
  $handler->display->display_options['filters']['field_file_asset_type_tid']['type'] = 'select';
  $handler->display->display_options['filters']['field_file_asset_type_tid']['vocabulary'] = 'product_asset_type';
  $export['product_assets'] = $view;

  return $export;
}
