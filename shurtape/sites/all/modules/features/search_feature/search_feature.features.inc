<?php
/**
 * @file
 * search_feature.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function search_feature_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "facetapi" && $api == "facetapi_defaults") {
    return array("version" => "1");
  }
  if ($module == "page_manager" && $api == "pages_default") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function search_feature_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_image_default_styles().
 */
function search_feature_image_default_styles() {
  $styles = array();

  // Exported image style: search-product-results.
  $styles['search-product-results'] = array(
    'label' => 'Search-Product-Results',
    'effects' => array(
      3 => array(
        'name' => 'image_scale',
        'data' => array(
          'width' => 110,
          'height' => '',
          'upscale' => 0,
        ),
        'weight' => 2,
      ),
    ),
  );

  // Exported image style: search-technical-results.
  $styles['search-technical-results'] = array(
    'label' => 'Search-Technical-Results',
    'effects' => array(
      4 => array(
        'name' => 'image_scale',
        'data' => array(
          'width' => 110,
          'height' => '',
          'upscale' => 0,
        ),
        'weight' => 2,
      ),
    ),
  );

  return $styles;
}

/**
 * Implements hook_default_search_api_index().
 */
function search_feature_default_search_api_index() {
  $items = array();
  $items['search_server_index'] = entity_import('search_api_index', '{
    "name" : "Search Server Index",
    "machine_name" : "search_server_index",
    "description" : null,
    "server" : "solr",
    "item_type" : "node",
    "options" : {
      "index_directly" : 0,
      "cron_limit" : "50",
      "fields" : {
        "body:value" : { "type" : "text" },
        "field_box_weight" : { "type" : "integer", "entity_type" : "taxonomy_term" },
        "field_handling_conditions" : { "type" : "integer", "entity_type" : "taxonomy_term" },
        "field_holding_power" : { "type" : "integer" },
        "field_industry_link_group" : { "type" : "integer", "entity_type" : "taxonomy_term" },
        "field_is_carpet_tape" : { "type" : "boolean" },
        "field_is_cold_temp_tape" : { "type" : "boolean" },
        "field_is_fda_compliant" : { "type" : "boolean" },
        "field_is_green_point_certified" : { "type" : "boolean" },
        "field_is_linered" : { "type" : "boolean" },
        "field_is_ul_723_tested" : { "type" : "boolean" },
        "field_is_ul_listed" : { "type" : "boolean" },
        "field_is_uv_resistant" : { "type" : "boolean" },
        "field_is_waterproof" : { "type" : "boolean" },
        "field_physical_properties" : {
          "type" : "list\\u003Cinteger\\u003E",
          "entity_type" : "field_collection_item"
        },
        "field_product_adhesive" : { "type" : "integer", "entity_type" : "taxonomy_term" },
        "field_product_assets" : { "type" : "list\\u003Cinteger\\u003E", "entity_type" : "node" },
        "field_product_backing" : { "type" : "integer", "entity_type" : "taxonomy_term" },
        "field_product_clean_removal_days" : { "type" : "integer", "entity_type" : "taxonomy_term" },
        "field_product_images" : { "type" : "list\\u003Cinteger\\u003E", "entity_type" : "node" },
        "field_product_liner" : { "type" : "integer", "entity_type" : "taxonomy_term" },
        "field_product_market_list" : { "type" : "list\\u003Cinteger\\u003E", "entity_type" : "taxonomy_term" },
        "field_product_market_list:parents_all" : {
          "type" : "list\\u003Clist\\u003Cinteger\\u003E\\u003E",
          "entity_type" : "taxonomy_term"
        },
        "field_product_meta_description" : { "type" : "text" },
        "field_product_short_description:value" : { "type" : "text" },
        "field_product_type" : { "type" : "list\\u003Cinteger\\u003E", "entity_type" : "taxonomy_term" },
        "field_product_type:description" : { "type" : "list\\u003Ctext\\u003E" },
        "field_product_type:name" : { "type" : "list\\u003Ctext\\u003E" },
        "field_product_type:node_count" : { "type" : "list\\u003Cinteger\\u003E" },
        "field_product_type:parent" : {
          "type" : "list\\u003Clist\\u003Cinteger\\u003E\\u003E",
          "entity_type" : "taxonomy_term"
        },
        "field_product_type:parents_all" : {
          "type" : "list\\u003Clist\\u003Cinteger\\u003E\\u003E",
          "entity_type" : "taxonomy_term"
        },
        "field_product_type:tid" : { "type" : "list\\u003Cinteger\\u003E" },
        "field_product_type:url" : { "type" : "list\\u003Curi\\u003E" },
        "field_product_type:vocabulary" : {
          "type" : "list\\u003Cinteger\\u003E",
          "entity_type" : "taxonomy_vocabulary"
        },
        "field_product_type:weight" : { "type" : "list\\u003Cinteger\\u003E" },
        "field_product_videos" : { "type" : "list\\u003Ctext\\u003E" },
        "field_sizes_colors" : {
          "type" : "list\\u003Cinteger\\u003E",
          "entity_type" : "field_collection_item"
        },
        "nid" : { "type" : "integer" },
        "search_api_language" : { "type" : "string" },
        "title" : { "type" : "text", "boost" : "0.1" },
        "title_field" : { "type" : "string" },
        "type" : { "type" : "string" }
      },
      "data_alter_callbacks" : {
        "search_api_alter_bundle_filter" : {
          "status" : 0,
          "weight" : "-10",
          "settings" : { "default" : "1", "bundles" : [] }
        },
        "search_api_alter_node_access" : { "status" : 0, "weight" : "0", "settings" : [] },
        "search_api_alter_node_status" : { "status" : 0, "weight" : "0", "settings" : [] },
        "search_api_alter_add_hierarchy" : {
          "status" : 1,
          "weight" : "0",
          "settings" : { "fields" : {
              "field_product_type:parent" : "field_product_type:parent",
              "field_product_type:parents_all" : "field_product_type:parents_all"
            }
          }
        },
        "search_api_alter_add_viewed_entity" : { "status" : 0, "weight" : "0", "settings" : { "mode" : "full" } },
        "search_api_alter_add_url" : { "status" : 0, "weight" : "0", "settings" : [] },
        "search_api_alter_add_aggregation" : { "status" : 0, "weight" : "0", "settings" : [] }
      },
      "processors" : {
        "search_api_case_ignore" : {
          "status" : 1,
          "weight" : "0",
          "settings" : { "fields" : {
              "title" : true,
              "title_field" : true,
              "field_product_meta_description" : true,
              "field_product_type:name" : true,
              "field_product_type:description" : true,
              "field_product_short_description:value" : true
            }
          }
        },
        "search_api_html_filter" : {
          "status" : 0,
          "weight" : "10",
          "settings" : {
            "fields" : { "title" : true },
            "title" : 0,
            "alt" : 1,
            "tags" : "h1 = 5\\r\\nh2 = 3\\r\\nh3 = 2\\r\\nstrong = 2\\r\\nb = 2\\r\\nem = 1.5\\r\\nu = 1.5"
          }
        },
        "search_api_tokenizer" : {
          "status" : 0,
          "weight" : "20",
          "settings" : {
            "fields" : { "title" : true },
            "spaces" : "[^[:alnum:]]",
            "ignorable" : "[\\u0027]"
          }
        },
        "search_api_stopwords" : {
          "status" : 0,
          "weight" : "30",
          "settings" : {
            "fields" : { "title" : true },
            "file" : "",
            "stopwords" : "but\\r\\ndid\\r\\nthe this that those\\r\\netc"
          }
        },
        "search_api_highlighting" : {
          "status" : 0,
          "weight" : "35",
          "settings" : {
            "prefix" : "\\u003Cstrong\\u003E",
            "suffix" : "\\u003C\\/strong\\u003E",
            "excerpt" : 1,
            "excerpt_length" : "256",
            "highlight" : "always"
          }
        }
      }
    },
    "enabled" : "1",
    "read_only" : "0",
    "rdf_mapping" : []
  }');
  return $items;
}

/**
 * Implements hook_default_search_api_server().
 */
function search_feature_default_search_api_server() {
  $items = array();
  $items['db_server_new'] = entity_import('search_api_server', '{
    "name" : "DB Server New",
    "machine_name" : "db_server_new",
    "description" : "",
    "class" : "search_api_db_service",
    "options" : { "database" : "default:default", "min_chars" : "2", "indexes" : [] },
    "enabled" : "1",
    "rdf_mapping" : []
  }');
  return $items;
}
