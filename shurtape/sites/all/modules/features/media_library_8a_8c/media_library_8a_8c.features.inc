<?php
/**
 * @file
 * media_library_8a_8c.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function media_library_8a_8c_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function media_library_8a_8c_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function media_library_8a_8c_node_info() {
  $items = array(
    'media_asset_infographic' => array(
      'name' => t('Media Asset Infographic'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'media_library_asset' => array(
      'name' => t('Media Library Asset'),
      'base' => 'node_content',
      'description' => t('Use this type to add content to the Media Library Page of the site'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'media_library_logos' => array(
      'name' => t('Media Library Logos'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'media_library_video' => array(
      'name' => t('Media Library Video'),
      'base' => 'node_content',
      'description' => t('Use this type to add videos to the Media Library Videos section on that Page'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
