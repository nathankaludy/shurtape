<?php
/**
 * @file
 * media_library_8a_8c.features.metatag.inc
 */

/**
 * Implements hook_metatag_export_default().
 */
function media_library_8a_8c_metatag_export_default() {
  $config = array();

  // Exported Metatag config instance: node:media_asset_infographic.
  $config['node:media_asset_infographic'] = array(
    'instance' => 'node:media_asset_infographic',
    'config' => array(
      'description' => array(
        'value' => '[node:field_social_share_description]',
      ),
      'image_src' => array(
        'value' => '[node:field_infographic_full_image]',
      ),
      'og:description' => array(
        'value' => '[node:field_social_share_description]',
      ),
      'og:type' => array(
        'value' => 'article',
      ),
      'og:image' => array(
        'value' => '[node:field_infographic_full_image]',
      ),
      'og:url' => array(
        'value' => '[current-page:url:absolute]',
      ),
      'twitter:description' => array(
        'value' => '[node:field_social_share_description]',
      ),
      'twitter:image:src' => array(
        'value' => '[node:field_infographic_full_image]',
      ),
    ),
  );

  // Exported Metatag config instance: node:media_library_asset.
  $config['node:media_library_asset'] = array(
    'instance' => 'node:media_library_asset',
    'config' => array(
      'description' => array(
        'value' => '[node:field_social_share_description]',
      ),
      'image_src' => array(
        'value' => '[node:field_asset_thumbnail]',
      ),
      'og:description' => array(
        'value' => '[node:field_social_share_description]',
      ),
      'og:type' => array(
        'value' => 'article',
      ),
      'og:image' => array(
        'value' => '[node:field_asset_thumbnail]',
      ),
      'og:url' => array(
        'value' => '[current-page:url:absolute]',
      ),
    ),
  );

  // Exported Metatag config instance: node:media_library_video.
  $config['node:media_library_video'] = array(
    'instance' => 'node:media_library_video',
    'config' => array(
      'description' => array(
        'value' => '[node:field_social_share_description]',
      ),
      'og:description' => array(
        'value' => '[node:field_social_share_description]',
      ),
      'og:type' => array(
        'value' => 'article',
      ),
      'og:image' => array(
        'value' => '//img.youtube.com/vi/[node:field_youtube_video_id]/0.jpg',
      ),
      'og:url' => array(
        'value' => '[current-page:url:absolute]',
      ),
      'og:video' => array(
        'value' => '//www.youtube.com/v/[node:field_youtube_video_id]',
      ),
    ),
  );

  return $config;
}
