<?php
/**
 * @file
 * phase_2i.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function phase_2i_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'contact_regions';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Contact Regions';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['pager']['options']['items_per_page'] = '5';
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['default_row_class'] = FALSE;
  $handler->display->display_options['style_options']['row_class_special'] = FALSE;
  $handler->display->display_options['style_options']['columns'] = array(
    'field_region_name' => 'field_region_name',
    'field_contact_data_1' => 'field_contact_data_1',
    'field_contact_data_2' => 'field_contact_data_2',
    'field_contact_data_3' => 'field_contact_data_3',
  );
  $handler->display->display_options['style_options']['default'] = '-1';
  $handler->display->display_options['style_options']['info'] = array(
    'field_region_name' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_contact_data_1' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_contact_data_2' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_contact_data_3' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  $handler->display->display_options['style_options']['override'] = FALSE;
  /* Field: Content: Region Name */
  $handler->display->display_options['fields']['field_region_name']['id'] = 'field_region_name';
  $handler->display->display_options['fields']['field_region_name']['table'] = 'field_data_field_region_name';
  $handler->display->display_options['fields']['field_region_name']['field'] = 'field_region_name';
  $handler->display->display_options['fields']['field_region_name']['label'] = '';
  $handler->display->display_options['fields']['field_region_name']['element_class'] = 'region';
  $handler->display->display_options['fields']['field_region_name']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_region_name']['element_default_classes'] = FALSE;
  /* Field: Content: Contact data 1 */
  $handler->display->display_options['fields']['field_contact_data_1']['id'] = 'field_contact_data_1';
  $handler->display->display_options['fields']['field_contact_data_1']['table'] = 'field_data_field_contact_data_1';
  $handler->display->display_options['fields']['field_contact_data_1']['field'] = 'field_contact_data_1';
  $handler->display->display_options['fields']['field_contact_data_1']['label'] = '';
  $handler->display->display_options['fields']['field_contact_data_1']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_contact_data_1']['element_default_classes'] = FALSE;
  /* Field: Content: Contact data 2 */
  $handler->display->display_options['fields']['field_contact_data_2']['id'] = 'field_contact_data_2';
  $handler->display->display_options['fields']['field_contact_data_2']['table'] = 'field_data_field_contact_data_2';
  $handler->display->display_options['fields']['field_contact_data_2']['field'] = 'field_contact_data_2';
  $handler->display->display_options['fields']['field_contact_data_2']['label'] = '';
  $handler->display->display_options['fields']['field_contact_data_2']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_contact_data_2']['element_default_classes'] = FALSE;
  /* Field: Content: Contact data 3 */
  $handler->display->display_options['fields']['field_contact_data_3']['id'] = 'field_contact_data_3';
  $handler->display->display_options['fields']['field_contact_data_3']['table'] = 'field_data_field_contact_data_3';
  $handler->display->display_options['fields']['field_contact_data_3']['field'] = 'field_contact_data_3';
  $handler->display->display_options['fields']['field_contact_data_3']['label'] = '';
  $handler->display->display_options['fields']['field_contact_data_3']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_contact_data_3']['element_default_classes'] = FALSE;
  /* Sort criterion: Draggableviews: Weight */
  $handler->display->display_options['sorts']['weight']['id'] = 'weight';
  $handler->display->display_options['sorts']['weight']['table'] = 'draggableviews_structure';
  $handler->display->display_options['sorts']['weight']['field'] = 'weight';
  $handler->display->display_options['sorts']['weight']['draggableviews_setting_view'] = 'contact_regions:page_1';
  $handler->display->display_options['sorts']['weight']['draggableviews_setting_new_items_bottom_list'] = 1;
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'region_contact' => 'region_contact',
  );

  /* Display: Contact Regions */
  $handler = $view->new_display('block', 'Contact Regions', 'block');
  $handler->display->display_options['defaults']['css_class'] = FALSE;
  $handler->display->display_options['defaults']['header'] = FALSE;
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'region_contact' => 'region_contact',
  );
  /* Filter criterion: Content: Language */
  $handler->display->display_options['filters']['language']['id'] = 'language';
  $handler->display->display_options['filters']['language']['table'] = 'node';
  $handler->display->display_options['filters']['language']['field'] = 'language';
  $handler->display->display_options['filters']['language']['value'] = array(
    '***CURRENT_LANGUAGE***' => '***CURRENT_LANGUAGE***',
    'und' => 'und',
  );

  /* Display: Contact Region Sort */
  $handler = $view->new_display('page', 'Contact Region Sort', 'page_1');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['title'] = 'Contact Regions Sort';
  $handler->display->display_options['defaults']['style_plugin'] = FALSE;
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'nid' => 'nid',
    'field_region_name' => 'field_region_name',
    'field_contact_data_1' => 'field_contact_data_1',
    'field_contact_data_2' => 'field_contact_data_2',
    'field_contact_data_3' => 'field_contact_data_3',
    'draggableviews' => 'draggableviews',
  );
  $handler->display->display_options['style_options']['default'] = '-1';
  $handler->display->display_options['style_options']['info'] = array(
    'nid' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_region_name' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_contact_data_1' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_contact_data_2' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_contact_data_3' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'draggableviews' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['defaults']['row_options'] = FALSE;
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Content: Nid */
  $handler->display->display_options['fields']['nid']['id'] = 'nid';
  $handler->display->display_options['fields']['nid']['table'] = 'node';
  $handler->display->display_options['fields']['nid']['field'] = 'nid';
  $handler->display->display_options['fields']['nid']['label'] = '';
  $handler->display->display_options['fields']['nid']['exclude'] = TRUE;
  $handler->display->display_options['fields']['nid']['element_label_colon'] = FALSE;
  /* Field: Content: Region Name */
  $handler->display->display_options['fields']['field_region_name']['id'] = 'field_region_name';
  $handler->display->display_options['fields']['field_region_name']['table'] = 'field_data_field_region_name';
  $handler->display->display_options['fields']['field_region_name']['field'] = 'field_region_name';
  $handler->display->display_options['fields']['field_region_name']['label'] = '';
  $handler->display->display_options['fields']['field_region_name']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_region_name']['element_default_classes'] = FALSE;
  /* Field: Content: Contact data 1 */
  $handler->display->display_options['fields']['field_contact_data_1']['id'] = 'field_contact_data_1';
  $handler->display->display_options['fields']['field_contact_data_1']['table'] = 'field_data_field_contact_data_1';
  $handler->display->display_options['fields']['field_contact_data_1']['field'] = 'field_contact_data_1';
  $handler->display->display_options['fields']['field_contact_data_1']['label'] = '';
  $handler->display->display_options['fields']['field_contact_data_1']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_contact_data_1']['element_default_classes'] = FALSE;
  /* Field: Content: Contact data 2 */
  $handler->display->display_options['fields']['field_contact_data_2']['id'] = 'field_contact_data_2';
  $handler->display->display_options['fields']['field_contact_data_2']['table'] = 'field_data_field_contact_data_2';
  $handler->display->display_options['fields']['field_contact_data_2']['field'] = 'field_contact_data_2';
  $handler->display->display_options['fields']['field_contact_data_2']['label'] = '';
  $handler->display->display_options['fields']['field_contact_data_2']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_contact_data_2']['element_default_classes'] = FALSE;
  /* Field: Content: Contact data 3 */
  $handler->display->display_options['fields']['field_contact_data_3']['id'] = 'field_contact_data_3';
  $handler->display->display_options['fields']['field_contact_data_3']['table'] = 'field_data_field_contact_data_3';
  $handler->display->display_options['fields']['field_contact_data_3']['field'] = 'field_contact_data_3';
  $handler->display->display_options['fields']['field_contact_data_3']['label'] = '';
  $handler->display->display_options['fields']['field_contact_data_3']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_contact_data_3']['element_default_classes'] = FALSE;
  /* Field: Draggableviews: Content */
  $handler->display->display_options['fields']['draggableviews']['id'] = 'draggableviews';
  $handler->display->display_options['fields']['draggableviews']['table'] = 'node';
  $handler->display->display_options['fields']['draggableviews']['field'] = 'draggableviews';
  $handler->display->display_options['fields']['draggableviews']['label'] = '';
  $handler->display->display_options['fields']['draggableviews']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['draggableviews']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['draggableviews']['hide_alter_empty'] = FALSE;
  $handler->display->display_options['fields']['draggableviews']['draggableviews']['ajax'] = 0;
  $handler->display->display_options['defaults']['sorts'] = FALSE;
  /* Sort criterion: Draggableviews: Weight */
  $handler->display->display_options['sorts']['weight']['id'] = 'weight';
  $handler->display->display_options['sorts']['weight']['table'] = 'draggableviews_structure';
  $handler->display->display_options['sorts']['weight']['field'] = 'weight';
  $handler->display->display_options['sorts']['weight']['draggableviews_setting_view'] = 'self';
  $handler->display->display_options['sorts']['weight']['draggableviews_setting_new_items_bottom_list'] = 1;
  $handler->display->display_options['path'] = 'admin/region/sort';
  $export['contact_regions'] = $view;

  $view = new view();
  $view->name = 'locations_page';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Locations page';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Locations page';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['style_plugin'] = 'grid';
  $handler->display->display_options['style_options']['grouping'] = array(
    0 => array(
      'field' => 'field_location_category',
      'rendered' => 1,
      'rendered_strip' => 1,
    ),
  );
  $handler->display->display_options['style_options']['row_class_special'] = FALSE;
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['row_options']['default_field_elements'] = FALSE;
  /* Relationship: Content: Taxonomy terms on node */
  $handler->display->display_options['relationships']['term_node_tid']['id'] = 'term_node_tid';
  $handler->display->display_options['relationships']['term_node_tid']['table'] = 'node';
  $handler->display->display_options['relationships']['term_node_tid']['field'] = 'term_node_tid';
  $handler->display->display_options['relationships']['term_node_tid']['vocabularies'] = array(
    'location_category' => 'location_category',
    'adhesive' => 0,
    'astm_test_methods' => 0,
    'backing' => 0,
    'box_weight' => 0,
    'clean_removal_days' => 0,
    'handling_conditions' => 0,
    'industry_links' => 0,
    'liner' => 0,
    'location_header' => 0,
    'markets' => 0,
    'media_libraries' => 0,
    'product_asset_type' => 0,
    'product_colors' => 0,
    'product_property_type' => 0,
    'tags' => 0,
    'auto_created_voc9_695' => 0,
  );
  /* Relationship: Content: Taxonomy terms on node */
  $handler->display->display_options['relationships']['term_node_tid_1']['id'] = 'term_node_tid_1';
  $handler->display->display_options['relationships']['term_node_tid_1']['table'] = 'node';
  $handler->display->display_options['relationships']['term_node_tid_1']['field'] = 'term_node_tid';
  $handler->display->display_options['relationships']['term_node_tid_1']['vocabularies'] = array(
    'location_header' => 'location_header',
    'adhesive' => 0,
    'astm_test_methods' => 0,
    'backing' => 0,
    'box_weight' => 0,
    'clean_removal_days' => 0,
    'handling_conditions' => 0,
    'industry_links' => 0,
    'liner' => 0,
    'location_category' => 0,
    'markets' => 0,
    'media_libraries' => 0,
    'product_asset_type' => 0,
    'product_colors' => 0,
    'product_property_type' => 0,
    'tags' => 0,
    'auto_created_voc9_695' => 0,
  );
  /* Field: Content: Location Category */
  $handler->display->display_options['fields']['field_location_category']['id'] = 'field_location_category';
  $handler->display->display_options['fields']['field_location_category']['table'] = 'field_data_field_location_category';
  $handler->display->display_options['fields']['field_location_category']['field'] = 'field_location_category';
  $handler->display->display_options['fields']['field_location_category']['label'] = '';
  $handler->display->display_options['fields']['field_location_category']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_location_category']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_location_category']['element_default_classes'] = FALSE;
  /* Field: Content: Location Header */
  $handler->display->display_options['fields']['field_location_header']['id'] = 'field_location_header';
  $handler->display->display_options['fields']['field_location_header']['table'] = 'field_data_field_location_header';
  $handler->display->display_options['fields']['field_location_header']['field'] = 'field_location_header';
  $handler->display->display_options['fields']['field_location_header']['label'] = '';
  $handler->display->display_options['fields']['field_location_header']['alter']['text'] = '<h3>[field_location_header] </h3>';
  $handler->display->display_options['fields']['field_location_header']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_location_header']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_location_header']['type'] = 'i18n_taxonomy_term_reference_plain';
  /* Field: Content: Body */
  $handler->display->display_options['fields']['body']['id'] = 'body';
  $handler->display->display_options['fields']['body']['table'] = 'field_data_body';
  $handler->display->display_options['fields']['body']['field'] = 'body';
  $handler->display->display_options['fields']['body']['label'] = '';
  $handler->display->display_options['fields']['body']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['body']['element_default_classes'] = FALSE;
  /* Sort criterion: Taxonomy term: Weight */
  $handler->display->display_options['sorts']['weight']['id'] = 'weight';
  $handler->display->display_options['sorts']['weight']['table'] = 'taxonomy_term_data';
  $handler->display->display_options['sorts']['weight']['field'] = 'weight';
  $handler->display->display_options['sorts']['weight']['relationship'] = 'term_node_tid';
  /* Sort criterion: Taxonomy term: Weight */
  $handler->display->display_options['sorts']['weight_1']['id'] = 'weight_1';
  $handler->display->display_options['sorts']['weight_1']['table'] = 'taxonomy_term_data';
  $handler->display->display_options['sorts']['weight_1']['field'] = 'weight';
  $handler->display->display_options['sorts']['weight_1']['relationship'] = 'term_node_tid_1';
  /* Sort criterion: Content: Location Weight (field_location_weight) */
  $handler->display->display_options['sorts']['field_location_weight_value']['id'] = 'field_location_weight_value';
  $handler->display->display_options['sorts']['field_location_weight_value']['table'] = 'field_data_field_location_weight';
  $handler->display->display_options['sorts']['field_location_weight_value']['field'] = 'field_location_weight_value';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'location_item' => 'location_item',
  );
  /* Filter criterion: Content: Language */
  $handler->display->display_options['filters']['language']['id'] = 'language';
  $handler->display->display_options['filters']['language']['table'] = 'node';
  $handler->display->display_options['filters']['language']['field'] = 'language';
  $handler->display->display_options['filters']['language']['value'] = array(
    '***CURRENT_LANGUAGE***' => '***CURRENT_LANGUAGE***',
    'und' => 'und',
  );

  /* Display: Block */
  $handler = $view->new_display('block', 'Block', 'block');
  $export['locations_page'] = $view;

  $view = new view();
  $view->name = 'social_icons_home';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Social icons home';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['style_plugin'] = 'list';
  $handler->display->display_options['style_options']['default_row_class'] = FALSE;
  $handler->display->display_options['style_options']['row_class_special'] = FALSE;
  $handler->display->display_options['style_options']['class'] = 'list-inline';
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['row_options']['default_field_elements'] = FALSE;
  /* Field: Content: Social icons */
  $handler->display->display_options['fields']['field_social_icons']['id'] = 'field_social_icons';
  $handler->display->display_options['fields']['field_social_icons']['table'] = 'field_data_field_social_icons';
  $handler->display->display_options['fields']['field_social_icons']['field'] = 'field_social_icons';
  $handler->display->display_options['fields']['field_social_icons']['label'] = '';
  $handler->display->display_options['fields']['field_social_icons']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_social_icons']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_social_icons']['settings'] = array(
    'edit' => '',
    'delete' => '',
    'add' => '',
    'description' => 0,
    'view_mode' => 'full',
  );
  $handler->display->display_options['fields']['field_social_icons']['group_rows'] = FALSE;
  $handler->display->display_options['fields']['field_social_icons']['delta_offset'] = '0';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'home_social_icons' => 'home_social_icons',
  );
  /* Filter criterion: Content: Language */
  $handler->display->display_options['filters']['language']['id'] = 'language';
  $handler->display->display_options['filters']['language']['table'] = 'node';
  $handler->display->display_options['filters']['language']['field'] = 'language';
  $handler->display->display_options['filters']['language']['value'] = array(
    '***CURRENT_LANGUAGE***' => '***CURRENT_LANGUAGE***',
    'und' => 'und',
  );

  /* Display: Social icons home */
  $handler = $view->new_display('block', 'Social icons home', 'block');

  /* Display: Footer logos */
  $handler = $view->new_display('block', 'Footer logos', 'block_1');
  $handler->display->display_options['defaults']['style_plugin'] = FALSE;
  $handler->display->display_options['style_plugin'] = 'list';
  $handler->display->display_options['style_options']['default_row_class'] = FALSE;
  $handler->display->display_options['style_options']['row_class_special'] = FALSE;
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['row_options']['default_field_elements'] = FALSE;
  $handler->display->display_options['defaults']['row_options'] = FALSE;
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Content: Footer logos */
  $handler->display->display_options['fields']['field_footer_logos']['id'] = 'field_footer_logos';
  $handler->display->display_options['fields']['field_footer_logos']['table'] = 'field_data_field_footer_logos';
  $handler->display->display_options['fields']['field_footer_logos']['field'] = 'field_footer_logos';
  $handler->display->display_options['fields']['field_footer_logos']['label'] = '';
  $handler->display->display_options['fields']['field_footer_logos']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_footer_logos']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_footer_logos']['settings'] = array(
    'edit' => '',
    'delete' => '',
    'add' => '',
    'description' => 0,
    'view_mode' => 'full',
  );
  $handler->display->display_options['fields']['field_footer_logos']['group_rows'] = FALSE;
  $handler->display->display_options['fields']['field_footer_logos']['delta_limit'] = '2';
  $handler->display->display_options['fields']['field_footer_logos']['delta_offset'] = '0';
  $handler->display->display_options['fields']['field_footer_logos']['separator'] = '';
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'footer_logos' => 'footer_logos',
  );
  $export['social_icons_home'] = $view;

  return $export;
}
