<?php
/**
 * @file
 * shurtape_type_distributor.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function shurtape_type_distributor_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "panelizer" && $api == "panelizer") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}
