<?php
/**
 * @file
 * shurtape_type_distributor.panelizer.inc
 */

/**
 * Implements hook_panelizer_defaults().
 */
function shurtape_type_distributor_panelizer_defaults() {
  $export = array();

  $panelizer = new stdClass();
  $panelizer->disabled = FALSE; /* Edit this to true to make a default panelizer disabled initially */
  $panelizer->api_version = 1;
  $panelizer->name = 'node:distributor:default';
  $panelizer->title = 'Default';
  $panelizer->panelizer_type = 'node';
  $panelizer->panelizer_key = 'distributor';
  $panelizer->no_blocks = FALSE;
  $panelizer->css_id = '';
  $panelizer->css = '';
  $panelizer->pipeline = 'standard';
  $panelizer->contexts = array();
  $panelizer->relationships = array();
  $panelizer->access = array();
  $panelizer->view_mode = 'page_manager';
  $panelizer->css_class = '';
  $panelizer->title_element = 'H2';
  $panelizer->link_to_entity = TRUE;
  $panelizer->extra = array();
  $display = new panels_display();
  $display->layout = 'page_basic';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'title' => NULL,
      'default' => NULL,
      'content_top' => NULL,
      'help' => NULL,
      'breadcrumb_section' => NULL,
      'content' => NULL,
      'bottom' => NULL,
    ),
    'title' => array(
      'style' => 'naked',
    ),
  );
  $display->cache = array();
  $display->title = '%node:title';
  $display->uuid = 'f80e1ca5-118a-466a-8852-366842c547cb';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-0da90b35-16d4-4e2a-9217-4c70ea73280e';
    $pane->panel = 'content';
    $pane->type = 'entity_field';
    $pane->subtype = 'node:field_formatted_address';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'hidden',
      'formatter' => 'text_default',
      'delta_limit' => 0,
      'delta_offset' => '0',
      'delta_reversed' => FALSE,
      'formatter_settings' => array(),
      'context' => 'panelizer',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
      'style' => 'naked',
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '0da90b35-16d4-4e2a-9217-4c70ea73280e';
    $display->content['new-0da90b35-16d4-4e2a-9217-4c70ea73280e'] = $pane;
    $display->panels['content'][0] = 'new-0da90b35-16d4-4e2a-9217-4c70ea73280e';
    $pane = new stdClass();
    $pane->pid = 'new-38ebb01f-42bd-4e1b-8130-3164c721b829';
    $pane->panel = 'content';
    $pane->type = 'entity_field';
    $pane->subtype = 'node:field_phone_number';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'hidden',
      'formatter' => 'text_default',
      'delta_limit' => 0,
      'delta_offset' => '0',
      'delta_reversed' => FALSE,
      'formatter_settings' => array(),
      'context' => 'panelizer',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
      'style' => 'naked',
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = '38ebb01f-42bd-4e1b-8130-3164c721b829';
    $display->content['new-38ebb01f-42bd-4e1b-8130-3164c721b829'] = $pane;
    $display->panels['content'][1] = 'new-38ebb01f-42bd-4e1b-8130-3164c721b829';
    $pane = new stdClass();
    $pane->pid = 'new-401a0ebb-61e8-4493-965a-36eae79898c3';
    $pane->panel = 'content';
    $pane->type = 'entity_field';
    $pane->subtype = 'node:field_website_url';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'hidden',
      'formatter' => 'link_default',
      'delta_limit' => 0,
      'delta_offset' => '0',
      'delta_reversed' => FALSE,
      'formatter_settings' => array(),
      'context' => 'panelizer',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
      'style' => 'naked',
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 2;
    $pane->locks = array();
    $pane->uuid = '401a0ebb-61e8-4493-965a-36eae79898c3';
    $display->content['new-401a0ebb-61e8-4493-965a-36eae79898c3'] = $pane;
    $display->panels['content'][2] = 'new-401a0ebb-61e8-4493-965a-36eae79898c3';
    $pane = new stdClass();
    $pane->pid = 'new-9c4430fb-bfb2-492a-9b25-42f8b25cec09';
    $pane->panel = 'content';
    $pane->type = 'entity_field';
    $pane->subtype = 'node:field_latlng';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'above',
      'formatter' => 'hidden',
      'delta_limit' => 0,
      'delta_offset' => '0',
      'delta_reversed' => FALSE,
      'formatter_settings' => array(),
      'context' => 'panelizer',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
      'style' => 'naked',
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 3;
    $pane->locks = array();
    $pane->uuid = '9c4430fb-bfb2-492a-9b25-42f8b25cec09';
    $display->content['new-9c4430fb-bfb2-492a-9b25-42f8b25cec09'] = $pane;
    $display->panels['content'][3] = 'new-9c4430fb-bfb2-492a-9b25-42f8b25cec09';
    $pane = new stdClass();
    $pane->pid = 'new-2eb3d724-a6fb-4807-bd39-6dbe457f3cc9';
    $pane->panel = 'content';
    $pane->type = 'entity_field';
    $pane->subtype = 'node:field_address';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'above',
      'formatter' => 'hidden',
      'delta_limit' => 0,
      'delta_offset' => '0',
      'delta_reversed' => FALSE,
      'formatter_settings' => array(),
      'context' => 'panelizer',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
      'style' => 'naked',
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 4;
    $pane->locks = array();
    $pane->uuid = '2eb3d724-a6fb-4807-bd39-6dbe457f3cc9';
    $display->content['new-2eb3d724-a6fb-4807-bd39-6dbe457f3cc9'] = $pane;
    $display->panels['content'][4] = 'new-2eb3d724-a6fb-4807-bd39-6dbe457f3cc9';
    $pane = new stdClass();
    $pane->pid = 'new-e46c9648-3681-486c-9c22-97470dfb1dea';
    $pane->panel = 'content';
    $pane->type = 'entity_field';
    $pane->subtype = 'node:field_distributor_type';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'above',
      'formatter' => 'hidden',
      'delta_limit' => 0,
      'delta_offset' => '0',
      'delta_reversed' => FALSE,
      'formatter_settings' => array(),
      'context' => 'panelizer',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
      'style' => 'naked',
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 5;
    $pane->locks = array();
    $pane->uuid = 'e46c9648-3681-486c-9c22-97470dfb1dea';
    $display->content['new-e46c9648-3681-486c-9c22-97470dfb1dea'] = $pane;
    $display->panels['content'][5] = 'new-e46c9648-3681-486c-9c22-97470dfb1dea';
    $pane = new stdClass();
    $pane->pid = 'new-ea6e11c9-54b4-4404-b215-fc2703f2348f';
    $pane->panel = 'content';
    $pane->type = 'entity_field';
    $pane->subtype = 'node:field_industrial_partner';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'above',
      'formatter' => 'hidden',
      'delta_limit' => 0,
      'delta_offset' => '0',
      'delta_reversed' => FALSE,
      'formatter_settings' => array(),
      'context' => 'panelizer',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
      'style' => 'naked',
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 6;
    $pane->locks = array();
    $pane->uuid = 'ea6e11c9-54b4-4404-b215-fc2703f2348f';
    $display->content['new-ea6e11c9-54b4-4404-b215-fc2703f2348f'] = $pane;
    $display->panels['content'][6] = 'new-ea6e11c9-54b4-4404-b215-fc2703f2348f';
    $pane = new stdClass();
    $pane->pid = 'new-2db5c1eb-0d98-4a95-82f2-442323140052';
    $pane->panel = 'title';
    $pane->type = 'node_title';
    $pane->subtype = 'node_title';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'link' => 0,
      'markup' => 'h1',
      'id' => '',
      'class' => 'headline',
      'context' => 'panelizer',
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
      'style' => 'naked',
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '2db5c1eb-0d98-4a95-82f2-442323140052';
    $display->content['new-2db5c1eb-0d98-4a95-82f2-442323140052'] = $pane;
    $display->panels['title'][0] = 'new-2db5c1eb-0d98-4a95-82f2-442323140052';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = 'new-0da90b35-16d4-4e2a-9217-4c70ea73280e';
  $panelizer->display = $display;
  $export['node:distributor:default'] = $panelizer;

  return $export;
}
