<?php
/**
 * @file
 * taxonomy_terms.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function taxonomy_terms_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'taxonomy_term-markets-field_markets_term_image'.
  $field_instances['taxonomy_term-markets-field_markets_term_image'] = array(
    'bundle' => 'markets',
    'deleted' => 0,
    'description' => 'Must be aligned center / bottom with the product shadow but extract the background shadow image (Transparency enabled)',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'image',
        'settings' => array(
          'image_link' => '',
          'image_style' => '',
        ),
        'type' => 'image',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'taxonomy_term',
    'field_name' => 'field_markets_term_image',
    'label' => 'Category page - sub-category images',
    'required' => 0,
    'settings' => array(
      'alt_field' => 0,
      'default_image' => 0,
      'entity_translation_sync' => FALSE,
      'file_directory' => 'markets/category',
      'file_extensions' => 'png gif jpg jpeg',
      'max_filesize' => '',
      'max_resolution' => '160x120',
      'min_resolution' => '',
      'title_field' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'image',
      'settings' => array(
        'preview_image_style' => 'category-page',
        'progress_indicator' => 'throbber',
      ),
      'type' => 'image_image',
      'weight' => 4,
    ),
  );

  // Exported field_instance:
  // 'taxonomy_term-markets-field_markets_term_image_hero'.
  $field_instances['taxonomy_term-markets-field_markets_term_image_hero'] = array(
    'bundle' => 'markets',
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'image',
        'settings' => array(
          'image_link' => '',
          'image_style' => '',
        ),
        'type' => 'image',
        'weight' => 3,
      ),
    ),
    'entity_type' => 'taxonomy_term',
    'field_name' => 'field_markets_term_image_hero',
    'label' => 'Category page marquee',
    'required' => 0,
    'settings' => array(
      'alt_field' => 0,
      'default_image' => 0,
      'entity_translation_sync' => FALSE,
      'file_directory' => 'markets/marquee',
      'file_extensions' => 'png gif jpg jpeg',
      'max_filesize' => '',
      'max_resolution' => '469x225',
      'min_resolution' => '',
      'title_field' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'image',
      'settings' => array(
        'preview_image_style' => 'thumbnail',
        'progress_indicator' => 'throbber',
      ),
      'type' => 'image_image',
      'weight' => 3,
    ),
  );

  // Exported field_instance:
  // 'taxonomy_term-markets-field_markets_term_image_index'.
  $field_instances['taxonomy_term-markets-field_markets_term_image_index'] = array(
    'bundle' => 'markets',
    'deleted' => 0,
    'description' => 'This is the image thumbnail displayed in the products index page. (aligned center / bottom)',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'image',
        'settings' => array(
          'image_link' => '',
          'image_style' => '',
        ),
        'type' => 'image',
        'weight' => 2,
      ),
    ),
    'entity_type' => 'taxonomy_term',
    'field_name' => 'field_markets_term_image_index',
    'label' => 'Home page market callouts',
    'required' => 0,
    'settings' => array(
      'alt_field' => 0,
      'default_image' => 0,
      'entity_translation_sync' => FALSE,
      'file_directory' => 'markets/callouts',
      'file_extensions' => 'png gif jpg jpeg',
      'max_filesize' => '',
      'max_resolution' => '260x195',
      'min_resolution' => '',
      'title_field' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'image',
      'settings' => array(
        'preview_image_style' => 'thumbnail',
        'progress_indicator' => 'throbber',
      ),
      'type' => 'image_image',
      'weight' => 2,
    ),
  );

  // Exported field_instance:
  // 'taxonomy_term-media_libraries-field_youtube_video_id'.
  $field_instances['taxonomy_term-media_libraries-field_youtube_video_id'] = array(
    'bundle' => 'media_libraries',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'taxonomy_term',
    'field_name' => 'field_youtube_video_id',
    'label' => 'Youtube Video ID',
    'required' => FALSE,
    'settings' => array(
      'entity_translation_sync' => FALSE,
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 42,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Category page - sub-category images');
  t('Category page marquee');
  t('Home page market callouts');
  t('Must be aligned center / bottom with the product shadow but extract the background shadow image (Transparency enabled)');
  t('This is the image thumbnail displayed in the products index page. (aligned center / bottom)');
  t('Youtube Video ID');

  return $field_instances;
}
