<?php
/**
 * @file
 * shurtape_views.features.translations_es.inc
 */

/**
 * Implements hook_translations_es_defaults().
 */
function shurtape_views_translations_es_defaults() {
  $translations = array();
  $translatables = array();
  $translations['es:views']['4ef6fb4a8b1ceb9b9e495fbead99361e'] = array(
    'source' => 'Standard',
    'context' => 'physical_properties:default:field:field_data_field_property_standard:field_property_standard:label',
    'location' => 'views:physical_properties:default:field:field_data_field_property_standard:field_property_standard:label',
    'translation' => 'Standard sp',
    'plid' => 0,
    'plural' => 0,
  );
  $translatables[] = t('Standard', array(), array('context' => 'physical_properties:default:field:field_data_field_property_standard:field_property_standard:label'));
  $translations['es:views']['6051dee16a81f008ace11561896cbad8'] = array(
    'source' => 'Dimensions sp',
    'context' => 'product_colors:default:field:field_data_field_product_dimensions:field_product_dimensions:label',
    'location' => 'views:product_colors:default:field:field_data_field_product_dimensions:field_product_dimensions:label',
    'translation' => 'sp',
    'plid' => 0,
    'plural' => 0,
  );
  $translatables[] = t('Dimensions sp', array(), array('context' => 'product_colors:default:field:field_data_field_product_dimensions:field_product_dimensions:label'));
  return $translations;
}
