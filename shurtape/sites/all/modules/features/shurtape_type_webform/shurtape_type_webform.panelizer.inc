<?php
/**
 * @file
 * shurtape_type_webform.panelizer.inc
 */

/**
 * Implements hook_panelizer_defaults().
 */
function shurtape_type_webform_panelizer_defaults() {
  $export = array();

  $panelizer = new stdClass();
  $panelizer->disabled = FALSE; /* Edit this to true to make a default panelizer disabled initially */
  $panelizer->api_version = 1;
  $panelizer->name = 'node:webform:default';
  $panelizer->title = 'Default';
  $panelizer->panelizer_type = 'node';
  $panelizer->panelizer_key = 'webform';
  $panelizer->no_blocks = FALSE;
  $panelizer->css_id = '';
  $panelizer->css = '';
  $panelizer->pipeline = 'standard';
  $panelizer->contexts = array();
  $panelizer->relationships = array();
  $panelizer->access = array();
  $panelizer->view_mode = 'page_manager';
  $panelizer->css_class = '';
  $panelizer->title_element = 'H2';
  $panelizer->link_to_entity = TRUE;
  $panelizer->extra = array();
  $display = new panels_display();
  $display->layout = 'webform';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'content' => NULL,
      'content_top' => NULL,
      'header' => NULL,
      'footer' => NULL,
      'title' => NULL,
      'help' => NULL,
      'action_links' => NULL,
      'breadcrumb_section' => NULL,
      'image' => NULL,
      'annotation' => NULL,
      'node_main' => NULL,
      'bottom' => NULL,
    ),
    'title' => array(
      'style' => 'naked',
    ),
    'breadcrumb_section' => array(
      'style' => 'naked',
    ),
    'image' => array(
      'style' => 'naked',
    ),
    'annotation' => array(
      'style' => 'naked',
    ),
    'node_main' => array(
      'style' => 'naked',
    ),
    'bottom' => array(
      'style' => 'naked',
    ),
  );
  $display->cache = array();
  $display->title = '%node:title';
  $display->uuid = 'b4113b63-379c-4425-8ce0-5d7e036c3349';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-ea6bf9fe-b986-451c-8362-b2c7d8fba36d';
    $pane->panel = 'annotation';
    $pane->type = 'custom';
    $pane->subtype = 'custom';
    $pane->shown = TRUE;
    $pane->access = array(
      'plugins' => array(),
    );
    $pane->configuration = array(
      'admin_title' => 'Annotation text',
      'title' => '',
      'body' => '<p>At Shurtape, we\'re eager to answer any questions you have about our products or our company. We also love to hear your comments about your Shurtape experience. All feedback is welcome&nbsp;—&nbsp;we look forward to hearing from you!</p>',
      'format' => 'full_html',
      'substitute' => 1,
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
      'style' => 'naked',
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'ea6bf9fe-b986-451c-8362-b2c7d8fba36d';
    $display->content['new-ea6bf9fe-b986-451c-8362-b2c7d8fba36d'] = $pane;
    $display->panels['annotation'][0] = 'new-ea6bf9fe-b986-451c-8362-b2c7d8fba36d';
    $pane = new stdClass();
    $pane->pid = 'new-f1a5c9c3-5c0d-43df-a5f1-4d82c5c324d9';
    $pane->panel = 'bottom';
    $pane->type = 'views';
    $pane->subtype = 'contact_regions';
    $pane->shown = TRUE;
    $pane->access = array(
      'plugins' => array(
        0 => array(
          'name' => 'path_visibility',
          'settings' => array(
            'visibility_setting' => '1',
            'paths' => 'contact-us',
          ),
          'context' => 'empty',
          'not' => FALSE,
        ),
      ),
    );
    $pane->configuration = array(
      'override_pager_settings' => 0,
      'use_pager' => 0,
      'nodes_per_page' => '5',
      'pager_id' => '0',
      'offset' => '0',
      'more_link' => 0,
      'feed_icons' => 0,
      'panel_args' => 0,
      'link_to_view' => 0,
      'args' => '',
      'url' => '',
      'display' => 'block',
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
      'style' => 'naked',
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'f1a5c9c3-5c0d-43df-a5f1-4d82c5c324d9';
    $display->content['new-f1a5c9c3-5c0d-43df-a5f1-4d82c5c324d9'] = $pane;
    $display->panels['bottom'][0] = 'new-f1a5c9c3-5c0d-43df-a5f1-4d82c5c324d9';
    $pane = new stdClass();
    $pane->pid = 'new-66d4ae8f-5764-44f8-be21-2b930f77728d';
    $pane->panel = 'breadcrumb_section';
    $pane->type = 'page_breadcrumb';
    $pane->subtype = 'page_breadcrumb';
    $pane->shown = FALSE;
    $pane->access = array();
    $pane->configuration = array();
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
      'style' => 'naked',
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '66d4ae8f-5764-44f8-be21-2b930f77728d';
    $display->content['new-66d4ae8f-5764-44f8-be21-2b930f77728d'] = $pane;
    $display->panels['breadcrumb_section'][0] = 'new-66d4ae8f-5764-44f8-be21-2b930f77728d';
    $pane = new stdClass();
    $pane->pid = 'new-9a675613-04bc-404b-9293-3fce0b6f0560';
    $pane->panel = 'content';
    $pane->type = 'entity_field_extra';
    $pane->subtype = 'node:webform';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'view_mode' => 'full',
      'context' => 'panelizer',
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '9a675613-04bc-404b-9293-3fce0b6f0560';
    $display->content['new-9a675613-04bc-404b-9293-3fce0b6f0560'] = $pane;
    $display->panels['content'][0] = 'new-9a675613-04bc-404b-9293-3fce0b6f0560';
    $pane = new stdClass();
    $pane->pid = 'new-8909685d-3265-4fce-8d2d-176860a11032';
    $pane->panel = 'image';
    $pane->type = 'custom';
    $pane->subtype = 'custom';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'admin_title' => 'Image source',
      'title' => '',
      'body' => '<img class="desktop" src="/sites/all/themes/shurtape/images/photo_operator.png" alt="" />',
      'format' => 'php_code',
      'substitute' => 1,
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
      'style' => 'naked',
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '8909685d-3265-4fce-8d2d-176860a11032';
    $display->content['new-8909685d-3265-4fce-8d2d-176860a11032'] = $pane;
    $display->panels['image'][0] = 'new-8909685d-3265-4fce-8d2d-176860a11032';
    $pane = new stdClass();
    $pane->pid = 'new-1a8040bc-3f3d-496c-adb9-43e10c9a4fb9';
    $pane->panel = 'title';
    $pane->type = 'node_title';
    $pane->subtype = 'node_title';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'link' => 0,
      'markup' => 'none',
      'id' => '',
      'class' => '',
      'context' => 'panelizer',
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
      'style' => 'naked',
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '1a8040bc-3f3d-496c-adb9-43e10c9a4fb9';
    $display->content['new-1a8040bc-3f3d-496c-adb9-43e10c9a4fb9'] = $pane;
    $display->panels['title'][0] = 'new-1a8040bc-3f3d-496c-adb9-43e10c9a4fb9';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $panelizer->display = $display;
  $export['node:webform:default'] = $panelizer;

  return $export;
}
