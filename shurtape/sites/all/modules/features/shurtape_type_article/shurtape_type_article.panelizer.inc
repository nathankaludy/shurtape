<?php
/**
 * @file
 * shurtape_type_article.panelizer.inc
 */

/**
 * Implements hook_panelizer_defaults().
 */
function shurtape_type_article_panelizer_defaults() {
  $export = array();

  $panelizer = new stdClass();
  $panelizer->disabled = FALSE; /* Edit this to true to make a default panelizer disabled initially */
  $panelizer->api_version = 1;
  $panelizer->name = 'node:article:default';
  $panelizer->title = 'Default';
  $panelizer->panelizer_type = 'node';
  $panelizer->panelizer_key = 'article';
  $panelizer->no_blocks = FALSE;
  $panelizer->css_id = '';
  $panelizer->css = '';
  $panelizer->pipeline = 'standard';
  $panelizer->contexts = array();
  $panelizer->relationships = array();
  $panelizer->access = array();
  $panelizer->view_mode = 'page_manager';
  $panelizer->css_class = '';
  $panelizer->title_element = 'H2';
  $panelizer->link_to_entity = TRUE;
  $panelizer->extra = array();
  $display = new panels_display();
  $display->layout = 'page_news';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'title' => NULL,
      'bottom' => NULL,
      'default' => NULL,
      'content_top' => NULL,
      'help' => NULL,
      'action_links' => NULL,
      'breadcrumb_section' => NULL,
      'content' => NULL,
      'field_sub_heading' => NULL,
      'field_image' => NULL,
    ),
    'title' => array(
      'style' => 'naked',
    ),
    'bottom' => array(
      'style' => 'naked',
    ),
    'field_image' => array(
      'style' => 'naked',
    ),
    'content' => array(
      'style' => 'naked',
    ),
    'field_sub_heading' => array(
      'style' => 'naked',
    ),
  );
  $display->cache = array();
  $display->title = '%node:title';
  $display->uuid = 'd932af8b-e8bb-45dd-ab03-6dfc90856210';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-16f2cf0e-04a1-45cf-b46f-0d56d7bf4231';
    $pane->panel = 'content';
    $pane->type = 'entity_field';
    $pane->subtype = 'node:body';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'hidden',
      'formatter' => 'text_default',
      'delta_limit' => 0,
      'delta_offset' => '0',
      'delta_reversed' => FALSE,
      'formatter_settings' => array(),
      'context' => 'panelizer',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
      'style' => 'naked',
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '16f2cf0e-04a1-45cf-b46f-0d56d7bf4231';
    $display->content['new-16f2cf0e-04a1-45cf-b46f-0d56d7bf4231'] = $pane;
    $display->panels['content'][0] = 'new-16f2cf0e-04a1-45cf-b46f-0d56d7bf4231';
    $pane = new stdClass();
    $pane->pid = 'new-6512236f-2e3a-45a8-928a-ba63f398e3a3';
    $pane->panel = 'field_image';
    $pane->type = 'entity_field';
    $pane->subtype = 'node:field_image';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'hidden',
      'formatter' => 'image',
      'delta_limit' => 0,
      'delta_offset' => '0',
      'delta_reversed' => FALSE,
      'formatter_settings' => array(
        'image_link' => '',
        'image_style' => 'large',
      ),
      'context' => 'panelizer',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
      'style' => 'naked',
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '6512236f-2e3a-45a8-928a-ba63f398e3a3';
    $display->content['new-6512236f-2e3a-45a8-928a-ba63f398e3a3'] = $pane;
    $display->panels['field_image'][0] = 'new-6512236f-2e3a-45a8-928a-ba63f398e3a3';
    $pane = new stdClass();
    $pane->pid = 'new-6e665334-6588-44e8-819e-206cabc0c3a8';
    $pane->panel = 'field_sub_heading';
    $pane->type = 'entity_field';
    $pane->subtype = 'node:field_sub_heading';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'hidden',
      'formatter' => 'text_default',
      'delta_limit' => 0,
      'delta_offset' => '0',
      'delta_reversed' => FALSE,
      'formatter_settings' => array(),
      'context' => 'panelizer',
      'override_title' => 0,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
      'style' => 'naked',
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '6e665334-6588-44e8-819e-206cabc0c3a8';
    $display->content['new-6e665334-6588-44e8-819e-206cabc0c3a8'] = $pane;
    $display->panels['field_sub_heading'][0] = 'new-6e665334-6588-44e8-819e-206cabc0c3a8';
    $pane = new stdClass();
    $pane->pid = 'new-59dd8843-2b35-4c9b-b384-53a86021c27f';
    $pane->panel = 'title';
    $pane->type = 'node_title';
    $pane->subtype = 'node_title';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'link' => 0,
      'markup' => 'none',
      'id' => '',
      'class' => '',
      'context' => 'panelizer',
      'override_title' => 0,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
      'style' => 'naked',
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '59dd8843-2b35-4c9b-b384-53a86021c27f';
    $display->content['new-59dd8843-2b35-4c9b-b384-53a86021c27f'] = $pane;
    $display->panels['title'][0] = 'new-59dd8843-2b35-4c9b-b384-53a86021c27f';
  $display->hide_title = PANELS_TITLE_NONE;
  $display->title_pane = 'new-6e665334-6588-44e8-819e-206cabc0c3a8';
  $panelizer->display = $display;
  $export['node:article:default'] = $panelizer;

  return $export;
}
