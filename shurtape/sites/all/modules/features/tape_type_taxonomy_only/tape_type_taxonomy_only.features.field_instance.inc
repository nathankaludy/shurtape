<?php
/**
 * @file
 * tape_type_taxonomy_only.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function tape_type_taxonomy_only_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance:
  // 'taxonomy_term-auto_created_voc9_695-field_term_image'.
  $field_instances['taxonomy_term-auto_created_voc9_695-field_term_image'] = array(
    'bundle' => 'auto_created_voc9_695',
    'deleted' => 0,
    'description' => 'This is the image thumbnail displayed on the filtered product page (products?t=[tid]). 160 x 120 please edit the image to aligned center / bottom with the product shadow but extract the background shadow image. ',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'image',
        'settings' => array(
          'image_link' => '',
          'image_style' => '',
        ),
        'type' => 'image',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'taxonomy_term',
    'field_name' => 'field_term_image',
    'label' => 'Category page - sub-category images',
    'required' => 0,
    'settings' => array(
      'alt_field' => 0,
      'default_image' => 0,
      'entity_translation_sync' => FALSE,
      'file_directory' => 'tape-type/category',
      'file_extensions' => 'png gif jpg jpeg',
      'max_filesize' => '',
      'max_resolution' => '160x120',
      'min_resolution' => '',
      'title_field' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'image',
      'settings' => array(
        'preview_image_style' => 'thumbnail',
        'progress_indicator' => 'throbber',
      ),
      'type' => 'image_image',
      'weight' => 4,
    ),
  );

  // Exported field_instance:
  // 'taxonomy_term-auto_created_voc9_695-field_type_term_feature_video'.
  $field_instances['taxonomy_term-auto_created_voc9_695-field_type_term_feature_video'] = array(
    'bundle' => 'auto_created_voc9_695',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 5,
      ),
    ),
    'entity_type' => 'taxonomy_term',
    'field_name' => 'field_type_term_feature_video',
    'label' => 'Feature Video',
    'required' => 0,
    'settings' => array(
      'entity_translation_sync' => FALSE,
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 5,
    ),
  );

  // Exported field_instance:
  // 'taxonomy_term-auto_created_voc9_695-field_type_term_image_hero'.
  $field_instances['taxonomy_term-auto_created_voc9_695-field_type_term_image_hero'] = array(
    'bundle' => 'auto_created_voc9_695',
    'deleted' => 0,
    'description' => 'This is the hero image displayed in the product filtered page (/products?t=[tid]).',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'image',
        'settings' => array(
          'image_link' => '',
          'image_style' => '',
        ),
        'type' => 'image',
        'weight' => 4,
      ),
    ),
    'entity_type' => 'taxonomy_term',
    'field_name' => 'field_type_term_image_hero',
    'label' => 'Category page marquee',
    'required' => 0,
    'settings' => array(
      'alt_field' => 0,
      'default_image' => 0,
      'entity_translation_sync' => FALSE,
      'file_directory' => 'tape-type/marquee',
      'file_extensions' => 'png gif jpg jpeg',
      'max_filesize' => '',
      'max_resolution' => '469x225',
      'min_resolution' => '',
      'title_field' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'image',
      'settings' => array(
        'preview_image_style' => 'thumbnail',
        'progress_indicator' => 'throbber',
      ),
      'type' => 'image_image',
      'weight' => 3,
    ),
  );

  // Exported field_instance:
  // 'taxonomy_term-auto_created_voc9_695-field_type_term_image_index_page'.
  $field_instances['taxonomy_term-auto_created_voc9_695-field_type_term_image_index_page'] = array(
    'bundle' => 'auto_created_voc9_695',
    'deleted' => 0,
    'description' => 'This is the image thumbnail displayed in the products index page.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'image',
        'settings' => array(
          'image_link' => '',
          'image_style' => '',
        ),
        'type' => 'image',
        'weight' => 3,
      ),
    ),
    'entity_type' => 'taxonomy_term',
    'field_name' => 'field_type_term_image_index_page',
    'label' => 'Category page types callouts',
    'required' => 0,
    'settings' => array(
      'alt_field' => 0,
      'default_image' => 0,
      'entity_translation_sync' => FALSE,
      'file_directory' => 'tape-type/callouts',
      'file_extensions' => 'png gif jpg jpeg',
      'max_filesize' => '',
      'max_resolution' => '260x195',
      'min_resolution' => '',
      'title_field' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'image',
      'settings' => array(
        'preview_image_style' => 'thumbnail',
        'progress_indicator' => 'throbber',
      ),
      'type' => 'image_image',
      'weight' => 2,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Category page - sub-category images');
  t('Category page marquee');
  t('Category page types callouts');
  t('Feature Video');
  t('This is the hero image displayed in the product filtered page (/products?t=[tid]).');
  t('This is the image thumbnail displayed in the products index page.');
  t('This is the image thumbnail displayed on the filtered product page (products?t=[tid]). 160 x 120 please edit the image to aligned center / bottom with the product shadow but extract the background shadow image. ');

  return $field_instances;
}
