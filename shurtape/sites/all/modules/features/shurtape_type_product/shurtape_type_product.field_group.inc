<?php
/**
 * @file
 * shurtape_type_product.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function shurtape_type_product_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_info_groups|node|product|form';
  $field_group->group_name = 'group_info_groups';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'product';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_product_info';
  $field_group->data = array(
    'label' => 'Info Groups',
    'weight' => '30',
    'children' => array(
      0 => 'group_product_details',
      1 => 'group_priduct_data',
      2 => 'group_product_flasg',
      3 => 'group_product_testimonial',
    ),
    'format_type' => 'htabs',
    'format_settings' => array(
      'formatter' => '',
      'instance_settings' => array(
        'classes' => 'group-info-groups field-group-htabs',
      ),
    ),
  );
  $export['group_info_groups|node|product|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_priduct_data|node|product|form';
  $field_group->group_name = 'group_priduct_data';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'product';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_info_groups';
  $field_group->data = array(
    'label' => 'Data',
    'weight' => '32',
    'children' => array(
      0 => 'field_product_adhesive',
      1 => 'field_product_backing',
      2 => 'field_product_liner',
      3 => 'field_box_weight',
      4 => 'field_handling_conditions',
      5 => 'field_holding_power',
      6 => 'field_product_type',
      7 => 'field_related_products',
      8 => 'field_product_clean_removal_days',
      9 => 'field_product_market_list',
    ),
    'format_type' => 'htab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
        'id' => '',
      ),
    ),
  );
  $export['group_priduct_data|node|product|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_product_details|node|product|form';
  $field_group->group_name = 'group_product_details';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'product';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_info_groups';
  $field_group->data = array(
    'label' => 'Details',
    'weight' => '31',
    'children' => array(
      0 => 'body',
      1 => 'field_product_bullet_description',
      2 => 'field_product_meta_description',
      3 => 'field_product_applications',
      4 => 'field_product_short_description',
      5 => 'title_field',
    ),
    'format_type' => 'htab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
        'id' => '',
      ),
    ),
  );
  $export['group_product_details|node|product|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_product_flasg|node|product|form';
  $field_group->group_name = 'group_product_flasg';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'product';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_info_groups';
  $field_group->data = array(
    'label' => 'Flags',
    'weight' => '33',
    'children' => array(
      0 => 'field_is_linered',
      1 => 'field_is_carpet_tape',
      2 => 'field_is_uv_resistant',
      3 => 'field_is_waterproof',
      4 => 'field_is_green_point_certified',
      5 => 'field_is_cold_temp_tape',
      6 => 'field_is_ul_listed',
      7 => 'field_is_ul_723_tested',
      8 => 'field_is_fda_compliant',
    ),
    'format_type' => 'htab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-product-flasg field-group-htab',
        'required_fields' => 1,
        'id' => '',
      ),
    ),
  );
  $export['group_product_flasg|node|product|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_product_media|node|product|form';
  $field_group->group_name = 'group_product_media';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'product';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Media',
    'weight' => '4',
    'children' => array(
      0 => 'field_product_videos',
      1 => 'field_product_assets',
      2 => 'field_product_images',
      3 => 'group_webdam_api',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-product-media field-group-tab',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_product_media|node|product|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_product_properties|node|product|form';
  $field_group->group_name = 'group_product_properties';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'product';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Physical Properties',
    'weight' => '23',
    'children' => array(
      0 => 'field_physical_properties',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-product-properties field-group-tab',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_product_properties|node|product|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_product_sizes_colors|node|product|form';
  $field_group->group_name = 'group_product_sizes_colors';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'product';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Sizes and Colors',
    'weight' => '24',
    'children' => array(
      0 => 'field_sizes_colors',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-product-sizes-colors field-group-tab',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_product_sizes_colors|node|product|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_product_testimonial|node|product|form';
  $field_group->group_name = 'group_product_testimonial';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'product';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_info_groups';
  $field_group->data = array(
    'label' => 'Testimonials',
    'weight' => '34',
    'children' => array(
      0 => 'field_product_testimonial',
    ),
    'format_type' => 'htab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
        'id' => '',
      ),
    ),
  );
  $export['group_product_testimonial|node|product|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_webdam_api|node|product|form';
  $field_group->group_name = 'group_webdam_api';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'product';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_product_media';
  $field_group->data = array(
    'label' => 'WebDAM API',
    'weight' => '10',
    'children' => array(
      0 => 'field_api_msds_tag_term',
      1 => 'field_api_tds_tag_term',
      2 => 'field_api_rcl_tag_term',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_webdam_api|node|product|form'] = $field_group;

  return $export;
}
