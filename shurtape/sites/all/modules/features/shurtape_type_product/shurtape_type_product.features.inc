<?php
/**
 * @file
 * shurtape_type_product.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function shurtape_type_product_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  if ($module == "panelizer" && $api == "panelizer") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function shurtape_type_product_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function shurtape_type_product_node_info() {
  $items = array(
    'product' => array(
      'name' => t('Product'),
      'base' => 'node_content',
      'description' => t('Use this to add new products to the site and give Technical Specs that can be used for Search Filters'),
      'has_title' => '1',
      'title_label' => t('Product Name'),
      'help' => '',
    ),
    'product_asset' => array(
      'name' => t('Product Asset'),
      'base' => 'node_content',
      'description' => t('Use this to add product assets such as images, pdf, word or any sort of document that needs to be associated to a product'),
      'has_title' => '1',
      'title_label' => t('File Name'),
      'help' => '',
    ),
    'product_image' => array(
      'name' => t('Product Image'),
      'base' => 'node_content',
      'description' => t('Use this to add product images'),
      'has_title' => '1',
      'title_label' => t('Image Name'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
