<?php
/**
 * @file
 * shurtape_type_product.panelizer.inc
 */

/**
 * Implements hook_panelizer_defaults().
 */
function shurtape_type_product_panelizer_defaults() {
  $export = array();

  $panelizer = new stdClass();
  $panelizer->disabled = FALSE; /* Edit this to true to make a default panelizer disabled initially */
  $panelizer->api_version = 1;
  $panelizer->name = 'node:product:default:default';
  $panelizer->title = 'Default';
  $panelizer->panelizer_type = 'node';
  $panelizer->panelizer_key = 'product';
  $panelizer->no_blocks = FALSE;
  $panelizer->css_id = '';
  $panelizer->css = '';
  $panelizer->pipeline = 'standard';
  $panelizer->contexts = array();
  $panelizer->relationships = array();
  $panelizer->access = array();
  $panelizer->view_mode = 'default';
  $panelizer->css_class = '';
  $panelizer->title_element = 'H2';
  $panelizer->link_to_entity = TRUE;
  $panelizer->extra = array();
  $display = new panels_display();
  $display->layout = 'page_product';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'title' => NULL,
      'content_top' => NULL,
      'help' => NULL,
      'action_links' => NULL,
      'breadcrumb_section' => NULL,
      'content' => NULL,
      'bottom' => NULL,
      'product_images_gallery' => NULL,
      'subtitle' => NULL,
      'body' => NULL,
      'product_detail_links' => NULL,
      'product_popout_region' => NULL,
      'green_point' => NULL,
      'related_products' => NULL,
      'product_properties_tab_content' => NULL,
      'product_markets' => NULL,
      'product_applications' => NULL,
      'product_sizes_colors_tab_content' => NULL,
      'product_downloads_tab_content' => NULL,
      'product_videos' => NULL,
      'product_testimonial' => NULL,
    ),
    'content' => array(
      'style' => 'naked',
    ),
    'product_images_gallery' => array(
      'style' => 'naked',
    ),
    'title' => array(
      'style' => 'naked',
    ),
    'subtitle' => array(
      'style' => 'naked',
    ),
    'body' => array(
      'style' => 'default',
    ),
    'product_detail_links' => array(
      'style' => 'naked',
    ),
    'product_popout_region' => array(
      'style' => 'naked',
    ),
    'green_point' => array(
      'style' => 'naked',
    ),
    'related_products' => array(
      'style' => 'naked',
    ),
    'product_properties_tab_content' => array(
      'style' => 'naked',
    ),
    'product_markets' => array(
      'style' => 'naked',
    ),
    'product_sizes_colors_tab_content' => array(
      'style' => 'naked',
    ),
    'product_downloads_tab_content' => array(
      'style' => 'naked',
    ),
    'product_videos' => array(
      'style' => 'naked',
    ),
    'product_testimonial' => array(
      'style' => 'naked',
    ),
  );
  $display->cache = array();
  $display->title = '%node:title';
  $display->uuid = '360dee0c-f489-4128-8539-2c1b74ca0b4b';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-d9e12231-b89c-4951-b13b-dac9dac69529';
    $pane->panel = 'body';
    $pane->type = 'entity_field';
    $pane->subtype = 'node:body';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'hidden',
      'formatter' => 'text_default',
      'delta_limit' => 0,
      'delta_offset' => '0',
      'delta_reversed' => FALSE,
      'formatter_settings' => array(),
      'context' => 'panelizer',
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
      'style' => 'naked',
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'd9e12231-b89c-4951-b13b-dac9dac69529';
    $display->content['new-d9e12231-b89c-4951-b13b-dac9dac69529'] = $pane;
    $display->panels['body'][0] = 'new-d9e12231-b89c-4951-b13b-dac9dac69529';
    $pane = new stdClass();
    $pane->pid = 'new-3f57c64a-5791-4c9a-b1ad-645d7dca5953';
    $pane->panel = 'body';
    $pane->type = 'entity_field';
    $pane->subtype = 'node:field_product_bullet_description';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'hidden',
      'formatter' => 'text_default',
      'delta_limit' => 0,
      'delta_offset' => '0',
      'delta_reversed' => FALSE,
      'formatter_settings' => array(),
      'context' => 'panelizer',
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
      'style' => 'naked',
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = '3f57c64a-5791-4c9a-b1ad-645d7dca5953';
    $display->content['new-3f57c64a-5791-4c9a-b1ad-645d7dca5953'] = $pane;
    $display->panels['body'][1] = 'new-3f57c64a-5791-4c9a-b1ad-645d7dca5953';
    $pane = new stdClass();
    $pane->pid = 'new-e0a8a863-f9bb-4552-be4e-373b4dd1741a';
    $pane->panel = 'green_point';
    $pane->type = 'custom';
    $pane->subtype = 'custom';
    $pane->shown = TRUE;
    $pane->access = array(
      'plugins' => array(
        0 => array(
          'name' => 'entity_field_value:node:product:field_is_green_point_certified',
          'settings' => array(
            'field_is_green_point_certified' => array(
              'und' => array(
                0 => array(
                  'value' => 1,
                ),
              ),
            ),
            'field_is_green_point_certified_value' => array(
              0 => 1,
            ),
          ),
          'context' => 'panelizer',
          'not' => FALSE,
        ),
      ),
    );
    $pane->configuration = array(
      'admin_title' => 'detail_badge with conditions',
      'title' => '',
      'body' => '<div class="detail_badge">
        <img src="/sites/default/files/CO-EX Technology icon.jpg" height="56" />
        <img src="/sites/default/files/imce/Shurtape-GREEN-Icon.jpg" width="138" height="64" />
</div>',
      'format' => 'php_code',
      'substitute' => 1,
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
      'style' => 'naked',
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'e0a8a863-f9bb-4552-be4e-373b4dd1741a';
    $display->content['new-e0a8a863-f9bb-4552-be4e-373b4dd1741a'] = $pane;
    $display->panels['green_point'][0] = 'new-e0a8a863-f9bb-4552-be4e-373b4dd1741a';
    $pane = new stdClass();
    $pane->pid = 'new-14a517a6-5d24-4bd1-9d7d-20beb9512a83';
    $pane->panel = 'product_applications';
    $pane->type = 'entity_field';
    $pane->subtype = 'node:field_product_applications';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'hidden',
      'formatter' => 'title_linked',
      'delta_limit' => 0,
      'delta_offset' => '0',
      'delta_reversed' => FALSE,
      'formatter_settings' => array(
        'title_style' => '_none',
        'title_link' => '',
        'title_class' => '',
      ),
      'context' => 'panelizer',
      'override_title' => 0,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '14a517a6-5d24-4bd1-9d7d-20beb9512a83';
    $display->content['new-14a517a6-5d24-4bd1-9d7d-20beb9512a83'] = $pane;
    $display->panels['product_applications'][0] = 'new-14a517a6-5d24-4bd1-9d7d-20beb9512a83';
    $pane = new stdClass();
    $pane->pid = 'new-7791540c-fcb1-4a38-93b4-72de2c0873c9';
    $pane->panel = 'product_detail_links';
    $pane->type = 'block';
    $pane->subtype = 'menu-menu-product-detail-menu';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
      'style' => 'naked',
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '7791540c-fcb1-4a38-93b4-72de2c0873c9';
    $display->content['new-7791540c-fcb1-4a38-93b4-72de2c0873c9'] = $pane;
    $display->panels['product_detail_links'][0] = 'new-7791540c-fcb1-4a38-93b4-72de2c0873c9';
    $pane = new stdClass();
    $pane->pid = 'new-7ff1f42b-a3b7-4caf-9da9-baa6f59b2e23';
    $pane->panel = 'product_downloads_tab_content';
    $pane->type = 'views';
    $pane->subtype = 'product_assets';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_pager_settings' => 0,
      'use_pager' => 0,
      'nodes_per_page' => '0',
      'pager_id' => '0',
      'offset' => '0',
      'more_link' => 0,
      'feed_icons' => 0,
      'panel_args' => 0,
      'link_to_view' => 0,
      'args' => '',
      'url' => '',
      'display' => 'block',
      'context' => array(
        0 => '',
      ),
      'override_title' => 0,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
      'style' => 'naked',
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '7ff1f42b-a3b7-4caf-9da9-baa6f59b2e23';
    $display->content['new-7ff1f42b-a3b7-4caf-9da9-baa6f59b2e23'] = $pane;
    $display->panels['product_downloads_tab_content'][0] = 'new-7ff1f42b-a3b7-4caf-9da9-baa6f59b2e23';
    $pane = new stdClass();
    $pane->pid = 'new-dd4af540-f987-472d-8bb4-3c2672358dba';
    $pane->panel = 'product_images_gallery';
    $pane->type = 'views';
    $pane->subtype = 'product_images';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_pager_settings' => 0,
      'use_pager' => 0,
      'nodes_per_page' => '0',
      'pager_id' => '0',
      'offset' => '0',
      'more_link' => 0,
      'feed_icons' => 0,
      'panel_args' => 0,
      'link_to_view' => 0,
      'args' => '',
      'url' => '',
      'display' => 'block_1',
      'context' => array(
        0 => '',
      ),
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
      'style' => 'naked',
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'dd4af540-f987-472d-8bb4-3c2672358dba';
    $display->content['new-dd4af540-f987-472d-8bb4-3c2672358dba'] = $pane;
    $display->panels['product_images_gallery'][0] = 'new-dd4af540-f987-472d-8bb4-3c2672358dba';
    $pane = new stdClass();
    $pane->pid = 'new-92901c2b-a00e-4c4e-9a6f-f5db008fa006';
    $pane->panel = 'product_images_gallery';
    $pane->type = 'views';
    $pane->subtype = 'product_images';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_pager_settings' => 0,
      'use_pager' => 0,
      'nodes_per_page' => '0',
      'pager_id' => '0',
      'offset' => '0',
      'more_link' => 0,
      'feed_icons' => 0,
      'panel_args' => 0,
      'link_to_view' => 0,
      'args' => '',
      'url' => '',
      'display' => 'block_2',
      'context' => array(
        0 => '',
      ),
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
      'style' => 'naked',
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = '92901c2b-a00e-4c4e-9a6f-f5db008fa006';
    $display->content['new-92901c2b-a00e-4c4e-9a6f-f5db008fa006'] = $pane;
    $display->panels['product_images_gallery'][1] = 'new-92901c2b-a00e-4c4e-9a6f-f5db008fa006';
    $pane = new stdClass();
    $pane->pid = 'new-acfa4264-154a-4c2a-aad2-4e1875881c98';
    $pane->panel = 'product_markets';
    $pane->type = 'entity_field';
    $pane->subtype = 'node:field_product_market_list';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'hidden',
      'formatter' => 'taxonomy_term_reference_link',
      'delta_limit' => '0',
      'delta_offset' => '0',
      'delta_reversed' => 0,
      'formatter_settings' => array(
        'linked' => 0,
      ),
      'context' => 'panelizer',
      'override_title' => 0,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'acfa4264-154a-4c2a-aad2-4e1875881c98';
    $display->content['new-acfa4264-154a-4c2a-aad2-4e1875881c98'] = $pane;
    $display->panels['product_markets'][0] = 'new-acfa4264-154a-4c2a-aad2-4e1875881c98';
    $pane = new stdClass();
    $pane->pid = 'new-c257ef83-36ea-49df-818b-30e41bda338a';
    $pane->panel = 'product_popout_region';
    $pane->type = 'block';
    $pane->subtype = 'shurtape_products_detail-product__popup';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
      'style' => 'naked',
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'c257ef83-36ea-49df-818b-30e41bda338a';
    $display->content['new-c257ef83-36ea-49df-818b-30e41bda338a'] = $pane;
    $display->panels['product_popout_region'][0] = 'new-c257ef83-36ea-49df-818b-30e41bda338a';
    $pane = new stdClass();
    $pane->pid = 'new-c7cd7ec1-1482-42a0-8357-1dfe612a772d';
    $pane->panel = 'product_popout_region';
    $pane->type = 'block';
    $pane->subtype = 'shurtape_products_detail-product_r_o_c_popup';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
      'style' => 'naked',
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = 'c7cd7ec1-1482-42a0-8357-1dfe612a772d';
    $display->content['new-c7cd7ec1-1482-42a0-8357-1dfe612a772d'] = $pane;
    $display->panels['product_popout_region'][1] = 'new-c7cd7ec1-1482-42a0-8357-1dfe612a772d';
    $pane = new stdClass();
    $pane->pid = 'new-5a22f480-2885-4e19-9552-d236d0ff2f13';
    $pane->panel = 'product_popout_region';
    $pane->type = 'block';
    $pane->subtype = 'shurtape_products_detail-product_questions_popup';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
      'style' => 'naked',
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 2;
    $pane->locks = array();
    $pane->uuid = '5a22f480-2885-4e19-9552-d236d0ff2f13';
    $display->content['new-5a22f480-2885-4e19-9552-d236d0ff2f13'] = $pane;
    $display->panels['product_popout_region'][2] = 'new-5a22f480-2885-4e19-9552-d236d0ff2f13';
    $pane = new stdClass();
    $pane->pid = 'new-080946b2-aaff-4f14-9fdd-68cbf6e779ca';
    $pane->panel = 'product_properties_tab_content';
    $pane->type = 'views';
    $pane->subtype = 'physical_properties';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_pager_settings' => 0,
      'use_pager' => 0,
      'nodes_per_page' => '0',
      'pager_id' => '0',
      'offset' => '0',
      'more_link' => 0,
      'feed_icons' => 0,
      'panel_args' => 0,
      'link_to_view' => 0,
      'args' => '',
      'url' => '',
      'display' => 'block',
      'context' => array(
        0 => '',
      ),
      'override_title' => 0,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
      'style' => 'naked',
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '080946b2-aaff-4f14-9fdd-68cbf6e779ca';
    $display->content['new-080946b2-aaff-4f14-9fdd-68cbf6e779ca'] = $pane;
    $display->panels['product_properties_tab_content'][0] = 'new-080946b2-aaff-4f14-9fdd-68cbf6e779ca';
    $pane = new stdClass();
    $pane->pid = 'new-3b503276-dc77-49e7-81f5-d4d4224555a5';
    $pane->panel = 'product_sizes_colors_tab_content';
    $pane->type = 'views';
    $pane->subtype = 'product_colors';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_pager_settings' => 0,
      'use_pager' => 0,
      'nodes_per_page' => '0',
      'pager_id' => '0',
      'offset' => '0',
      'more_link' => 0,
      'feed_icons' => 0,
      'panel_args' => 0,
      'link_to_view' => 0,
      'args' => '',
      'url' => '',
      'display' => 'block_1',
      'context' => array(
        0 => '',
      ),
      'override_title' => 0,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
      'style' => 'naked',
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '3b503276-dc77-49e7-81f5-d4d4224555a5';
    $display->content['new-3b503276-dc77-49e7-81f5-d4d4224555a5'] = $pane;
    $display->panels['product_sizes_colors_tab_content'][0] = 'new-3b503276-dc77-49e7-81f5-d4d4224555a5';
    $pane = new stdClass();
    $pane->pid = 'new-88c39f21-9fee-4291-9602-2d7dacd1b525';
    $pane->panel = 'product_sizes_colors_tab_content';
    $pane->type = 'block';
    $pane->subtype = 'shurtape_products_detail-product_r_o_c_link';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
      'style' => 'naked',
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = '88c39f21-9fee-4291-9602-2d7dacd1b525';
    $display->content['new-88c39f21-9fee-4291-9602-2d7dacd1b525'] = $pane;
    $display->panels['product_sizes_colors_tab_content'][1] = 'new-88c39f21-9fee-4291-9602-2d7dacd1b525';
    $pane = new stdClass();
    $pane->pid = 'new-bf905955-a291-4564-915e-cea76f3f4cc5';
    $pane->panel = 'product_testimonial';
    $pane->type = 'entity_field';
    $pane->subtype = 'node:field_product_testimonial';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'hidden',
      'formatter' => 'text_default',
      'delta_limit' => '0',
      'delta_offset' => '0',
      'delta_reversed' => 0,
      'formatter_settings' => array(),
      'context' => 'panelizer',
      'override_title' => 0,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
      'style' => 'naked',
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'bf905955-a291-4564-915e-cea76f3f4cc5';
    $display->content['new-bf905955-a291-4564-915e-cea76f3f4cc5'] = $pane;
    $display->panels['product_testimonial'][0] = 'new-bf905955-a291-4564-915e-cea76f3f4cc5';
    $pane = new stdClass();
    $pane->pid = 'new-aa336cac-a0c7-49e0-902b-5db7bb17082f';
    $pane->panel = 'product_videos';
    $pane->type = 'entity_field';
    $pane->subtype = 'node:field_product_videos';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'hidden',
      'formatter' => 'text_default',
      'delta_limit' => '0',
      'delta_offset' => '0',
      'delta_reversed' => 0,
      'formatter_settings' => array(),
      'context' => 'panelizer',
      'override_title' => 0,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
      'style' => 'naked',
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'aa336cac-a0c7-49e0-902b-5db7bb17082f';
    $display->content['new-aa336cac-a0c7-49e0-902b-5db7bb17082f'] = $pane;
    $display->panels['product_videos'][0] = 'new-aa336cac-a0c7-49e0-902b-5db7bb17082f';
    $pane = new stdClass();
    $pane->pid = 'new-1e0da9dc-52a2-4c89-8887-e37a852d832f';
    $pane->panel = 'related_products';
    $pane->type = 'entity_field';
    $pane->subtype = 'node:field_related_products';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'hidden',
      'formatter' => 'node_reference_default',
      'delta_limit' => '0',
      'delta_offset' => '0',
      'delta_reversed' => 0,
      'formatter_settings' => array(),
      'context' => 'panelizer',
      'override_title' => 0,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
      'style' => 'naked',
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '1e0da9dc-52a2-4c89-8887-e37a852d832f';
    $display->content['new-1e0da9dc-52a2-4c89-8887-e37a852d832f'] = $pane;
    $display->panels['related_products'][0] = 'new-1e0da9dc-52a2-4c89-8887-e37a852d832f';
    $pane = new stdClass();
    $pane->pid = 'new-c6c9a812-8e7d-4ad4-8752-8005327c0ce1';
    $pane->panel = 'title';
    $pane->type = 'node_title';
    $pane->subtype = 'node_title';
    $pane->shown = FALSE;
    $pane->access = array();
    $pane->configuration = array(
      'link' => 0,
      'markup' => 'none',
      'id' => '',
      'class' => '',
      'context' => 'panelizer',
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
      'style' => 'naked',
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'c6c9a812-8e7d-4ad4-8752-8005327c0ce1';
    $display->content['new-c6c9a812-8e7d-4ad4-8752-8005327c0ce1'] = $pane;
    $display->panels['title'][0] = 'new-c6c9a812-8e7d-4ad4-8752-8005327c0ce1';
  $display->hide_title = PANELS_TITLE_NONE;
  $display->title_pane = '0';
  $panelizer->display = $display;
  $export['node:product:default:default'] = $panelizer;

  return $export;
}
