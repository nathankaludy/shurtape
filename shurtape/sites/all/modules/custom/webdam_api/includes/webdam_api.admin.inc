<?php
/**
 * Form builder. Configure WebDAM.
 *
 * @ingroup forms
 * @see system_settings_form()
 */

require_once 'webdam/WebDAM.class.php';
require_once 'webdam/ProductAsset.class.php';
require_once 'webdam_api.helpers.inc';

function webdam_api_admin_settings() {
	# define a fieldset
	$form['general'] = array('#type' => 'fieldset',
			'#title' => t('General settings'),);
	$form['general']['webdam_api_key'] = array('#type' => 'textfield',
			'#title' => t('API KEY'), '#size' => '20',
			'#default_value' => variable_get('webdam_api_key', ''),
			'#description' => t('The alpha code that represents the API Key.'),);
	$form['general']['webdam_api_username'] = array('#type' => 'textfield',
			'#title' => t('API Username'), '#size' => '20',
			'#default_value' => variable_get('webdam_api_username', ''),
			'#description' => t('The WebDAM api username'),);
	$form['general']['webdam_api_password'] = array('#type' => 'textfield',
			'#title' => t('API Password'), '#size' => '20',
			'#default_value' => variable_get('webdam_api_password', ''),
			'#description' => t('The WebDAM api password'),);
	$form['general']['webdam_api_allowed_file_extensions'] = array(
			'#type' => 'textfield',
			'#title' => t('Allowed Product Asset Extensions'), '#size' => '20',
			'#default_value' => variable_get('webdam_api_allowed_file_extensions', '.pdf,.doc'),
			'#description' => t('Comma delimited set of allowed file extensions for product assets.'),);
	$form['import_products'] = array('#type' => 'fieldset',
			'#title' => t('Sync all product files from WebDAM'),
			'#description' => t('This will pull all the product files from the WebDAM Products API based on the Key ID defined by the WebDAM API module.'),);
	
	$form['import_products']['import'] = array('#type' => 'submit',
			'#value' => t('Sync product data with the WebDAM API'),
			'#submit' => array('webdam_api_import_products_submit'),);

	return system_settings_form($form);
}

/**
 * Submit callback; import all product data.
 */
function webdam_api_import_products_submit($form, &$form_state) {
	global $apitoken;
	$apiKey = variable_get('webdam_api_key', '');
	$username = variable_get('webdam_api_username', '');
	$password = variable_get('webdam_api_password', '');
	
	if ($apiKey == '') {
		drupal_set_message(
				'An API key needs to be set to sync with the API. Make sure a URL for this site level is configured properly (no trailing slash).',
				'error');
		return;
	}
	if ($username == '') {
		drupal_set_message(
				'A WebDAM Username needs to be set to sync with the API.',
				'error');
		return;
	}
	if ($password == '') {
		drupal_set_message(
				'A WebDAM Password needs to be set to sync with the API.',
				'error');
		return;
	}

	// authenticate to the API and keep the API Token for this request
	try {
		$webdam = new WebDAM($apiKey, $username, $password);
		$apitoken = $webdam->authenticate();
	} catch (Exception $e) {
		drupal_set_message('Authentication: ' . $e->getMessage() . '. Please try again.','error');
	}
	
	$_SESSION['http_request_count'] = 0; // reset counter for debug information.
	$function = 'process_batch';
	$batch = $function();
	batch_set($batch);
}

function webdam_api_sync_node($node){
	global $apitoken;
	$apiKey = variable_get('webdam_api_key', '');
	$username = variable_get('webdam_api_username', '');
	$password = variable_get('webdam_api_password', '');
	
	if ($apiKey == '') {
		drupal_set_message(
				'An API key needs to be set to sync with the API. Make sure a URL for this site level is configured properly (no trailing slash).',
				'error');
		return;
	}
	if ($username == '') {
		drupal_set_message(
				'A WebDAM Username needs to be set to sync with the API.',
				'error');
		return;
	}
	if ($password == '') {
		drupal_set_message(
				'A WebDAM Password needs to be set to sync with the API.',
				'error');
		return;
	}

	// authenticate to the API and keep the API Token for this request
	try {
		$webdam = new WebDAM($apiKey, $username, $password);
		$apitoken = $webdam->authenticate();
	} catch (Exception $e) {
		drupal_set_message('Authentication: ' . $e->getMessage() . '. Please try again.','error');
	}

	$function = 'process_node_batch';
	$batch = $function($node);
	batch_set($batch);
	// If you set a blank parameter, the batch_process() will cause an infinite loop
	batch_process('node/' . $node->nid);
}

function process_node_batch($node){
	$apiKey = variable_get('webdam_api_key', '');
	$username = variable_get('webdam_api_username', '');
	$password = variable_get('webdam_api_password', '');
	$webdam = new WebDAM($apiKey, $username, $password);

	$operations = array();
	
	// download the list of all product related search queries from the API once authenticated.
	$operations[] = array('webdam_api_syncbynid', array($node->nid, $webdam, 1));
	$batch = array(
			'operations' => $operations,
			'finished' => 'webdam_api_finished',
	);
	return $batch;
}

/**
 * Submit callback; import all product data.
 */
function process_batch() {
	$apiKey = variable_get('webdam_api_key', '');
	$username = variable_get('webdam_api_username', '');
	$password = variable_get('webdam_api_password', '');
	$webdam = new WebDAM($apiKey, $username, $password);
	
	// Save node count for the termination message.
	$nids = _webdam_api_get_node_terms();

	$operations = array();
	// download the list of all product related search queries from the API once authenticated.
	foreach ($nids as $nid) {
		$operations[] = array('webdam_api_syncbynid', array($nid, $webdam, count($nids)));
	}
	$batch = array(
			'operations' => $operations,
			'finished' => 'webdam_api_finished',
	);
	return $batch;
}

function webdam_api_syncbynid($nid, $webdam, $cnt, &$context){
	if (empty($context['sandbox'])) {
		$context['sandbox'] = array();
		$context['sandbox']['progress'] = 0;
		$context['sandbox']['current_node'] = 0;
		$context['sandbox']['max'] = $cnt;
	}
	if ($context['sandbox']['current_node'] == $nid){
		return;
	}
	$node = node_load($nid, NULL, TRUE);
	
	// clear the nodes so we can get the latest and greatest
	if (isset($node->field_product_assets[$node->language]) && count($node->field_product_assets[$node->language]) > 0){
		foreach($node->field_product_assets[$node->language] as $index => $asset) {
			unset($node->field_product_assets[$node->language][$index]);
		}
		node_save($node);
	}

	if (!isset($node->nid)) {
		drupal_set_message(t('The product ' . $prodName . ' does not exist in the CMS.'));
	} else {
		$context['sandbox']['current_node'] = $node->nid;
		$context['message'] = check_plain($node->title);
		if (isset($node->field_api_msds_tag_term[$node->language][0]["value"])){
			try {
				$term = taxonomy_get_term_by_name('Material Safety Data Sheet (MSDS)');
				_webdam_api_load_assets_by_term($webdam->authenticate(), $webdam, $node, $node->field_api_msds_tag_term[$node->language][0]["value"], key($term));
			} catch (Exception $e) {
				drupal_set_message($e->getMessage() . '. Please try again.','error');
			}
		}
		if (isset($node->field_api_tds_tag_term[$node->language][0]["value"])) {
			try {
				$term = taxonomy_get_term_by_name('Technical Data Sheet (TDS)');
				_webdam_api_load_assets_by_term($webdam->authenticate(), $webdam, $node, $node->field_api_tds_tag_term[$node->language][0]["value"], key($term));
			} catch (Exception $e) {
				drupal_set_message($e->getMessage() . '. Please try again.','error');
			}
		}
		if (isset($node->field_api_rcl_tag_term[$node->language][0]["value"])) {
			try {
				$term = taxonomy_get_term_by_name('Regulatory Compliance Letter (RCL)');
				_webdam_api_load_assets_by_term($webdam->authenticate(), $webdam, $node, $node->field_api_rcl_tag_term[$node->language][0]["value"], key($term));
			} catch (Exception $e) {
				drupal_set_message($e->getMessage() . '. Please try again.','error');
			}
		}
	}
	// Update our progress information.
	$context['sandbox']['progress']++;
	
	// Inform the batch engine that we are not finished,
	// and provide an estimation of the completion level we reached.
	if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
		$context['finished'] = ($context['sandbox']['progress'] >= $context['sandbox']['max']);
	}
}



function webdam_api_finished($success, $results, $operations){
	if ($success) {
		// Here we could do something meaningful with the results.
		// We just display the number of nodes we processed...
		drupal_set_message(t('The product data has been sync\'d with the WebDAM API.'));
	} else {
		// An error occurred.
		// $operations contains the operations that remained unprocessed.
		drupal_set_message(t('An error occurred while processing'));
	}
}

function _webdam_api_load_assets_by_term($apitoken, $webdam, $node, $tag, $tid){
	foreach ($webdam->searchProductAssets($apitoken, $node->title, $tag) as $assets) {
		if (webdam_api_validate_file_extensions($assets->getFileextension())) {
			// if assets are found in the API, then download
			try {
				$file = webdam_api_download_asset($webdam->getAssetDownloadUrlById($apitoken, $assets->getId()), $assets->getFilename());
				
				$nid = _webdam_api_get_nid($assets->getId());
				if ($nid != FALSE) {
					// load the node
					$edit = node_load($nid, NULL, TRUE);
					foreach ($edit->field_file_asset_type as $lang_key=>$terms) {
						foreach ($terms as $term_key=>$term) {
							$tid = $term['tid'];
							// Remove NULL tids
							if (!$tid) {
								echo "Found NULL tid in node nid " . $edit->nid ." under field_file_asset_type<br/>";
								unset($edit->field_file_asset_type[$lang_key][$term_key]);
								$save = TRUE;
							}
							// Remove empty fields
							if (sizeof($edit->field_file_asset_type[$lang_key]) === 0 ) {
								unset ($edit->field_file_asset_type[$lang_key]);
								$save = TRUE;
							}
						}
					}
				} else {
					// add new node
					$edit = new stdClass;
					$edit->type = 'product_asset';
					node_object_prepare($edit);
					$edit->promote = 0;
					$edit->comment = 0;
					$edit->status = 1;
					$edit->format = '1';
					$edit->language = LANGUAGE_NONE;
				}
	
				$title = _webdam_api_entities_to_ascii($assets->getName());
				$edit->title = $title;
				$edit->field_file_size[$edit->language][0]['value'] = $assets->getSize();
				$edit->field_api_asset_id[$edit->language][0]['value'] = $assets->getId();
	
				$body_text = _webdam_api_entities_to_ascii($assets->getDescription());
				$edit->body[$edit->language][0]['value'] = $body_text;
				$edit->body[$edit->language][0]['format'] = 'filtered_html';
	
				$edit->field_file_asset[$edit->language][0] = (array) $file;
				$edit->field_file_asset[$edit->language][0]['display'] = 1;
				$edit->field_file_asset[$edit->language][0]['description'] = $body_text;
				
				$edit->field_file_asset_type[$edit->language][]['tid'] = $tid;
				
				node_submit($edit);
				$fake_form = array();
				$fake_form_state = array();
				node_validate($edit, $fake_form, $fake_form_state);
	
				$errors = form_get_errors();
				if (!$errors) {
					node_save($edit);
					// Connect to Product $node is product
					if (!_webdam_api_check_field_exist($edit->nid, $node)){
						$node->field_product_assets[$node->language][]['nid'] = $edit->nid;
						node_save($node);
					}
				} else {
					drupal_set_message("<pre style='text-align: left'>" . print_r($errors, true) . "</pre>");
				}
			} catch (Exception $e) {
				drupal_set_message('Asset: Unable to download asset for product ' . $node->title . ' asset ID ' . $assets->getId() . '. ' . $e->getMessage(), 'error');
			}
		}
	}
}

function _webdam_api_check_field_exist($nid, $node){
	$found = false;
	if (!isset($node->field_product_assets[$node->language])){
		return false;
	}
	foreach ($node->field_product_assets[$node->language] as $asset) {
		if ($asset["nid"] == $nid){
			$found = true;
		}
	}
	return $found;
}

function _webdam_api_get_nid($id) {
	$query = new EntityFieldQuery();
	$query->entityCondition('entity_type', 'node')
			->entityCondition('bundle', 'product_asset')
			->propertyCondition('status', 1)
			->fieldCondition('field_api_asset_id', 'value', $id, '=');
	$result = $query->execute();
	if (isset($result['node'])) {
		return implode(",", array_keys($result['node']));
	}
	return false;
}

function webdam_api_validate_file_extensions($ext) {
	return in_array(strtolower($ext),explode(',', variable_get('webdam_api_allowed_file_extensions', '')));
}

function webdam_api_download_asset($uri, $name) {
	global $user;
	$field_storage_scheme = "s3";
	$destination = $field_storage_scheme . "://" . $name;

	//copy file into drupal files directory
	$contents = @file_get_contents($uri);
	if ($contents) {
		if (file_exists($destination) && $fid = webdam_get_file_by_filename($name)) {
			$file = file_load($fid);
			if ($uri = file_unmanaged_save_data($contents, $destination, FILE_EXISTS_REPLACE)) {
				$file->uri = $uri;
			}
			return $file;
		}
		return file_save_data($contents, $destination, FILE_EXISTS_REPLACE);
	}
	return false;
}

function webdam_get_file_by_filename($name) {
	$file = db_query('SELECT fid FROM file_managed WHERE filename = :filename', array(':filename' => $name))->fetchObject();
	if ($file) {
		return $file->fid;
	}
	return false;
}

function _webdam_api_get_node_terms(){
	$query = new EntityFieldQuery();
	$query->entityCondition('entity_type', 'node')
	->entityCondition('bundle', 'product')
	->propertyCondition('status', 1);
	$result = $query->execute();
	if (isset($result['node'])) {
		return array_keys($result['node']);
	}
	return false;
}
