<?php
  global $language;
  $lang = ($language->language != "en") ? '/'.$language->language : '';
?>

<div id="filter_bar">

  <h2 class="product_results"><a href="<?php print $lang.$link_results;?>"><?php print t('Product Results') ?></a> <span class="count">(<?php print render($total_products); ?>)</span></h2>

  <h3><?php print t('Narrow Your Search:') ?></h3>

  <div class="toggle_box <?php print isset($browse_markets_css_class) ? $browse_markets_css_class : ''; ?>" id="browse_markets">
    <?php if(isset($redirect_to_product_landing)):?>
        <h4><a href="<?php print $lang; ?>/products/markets" class=""><span class="toggle_arrow"></span><?php print t('Browse by Market') ?></a></h4>
    <?php else:?>
        <h4><a href="#" class="toggle_control"><span class="toggle_arrow"></span><?php print t('Browse by Market') ?></a></h4>
    <?php endif;?>
    <?php print render($browse_markets); ?>
  </div>

  <div class="toggle_box <?php print isset($browse_type_css_class) ? $browse_type_css_class : ''; ?>" id="browse_types">
    <?php if(isset($redirect_to_product_landing)):?>
        <h4><a href="<?php print $lang; ?>/products/type" class=""><span class="toggle_arrow"></span><?php print t('Browse by Type') ?></a></h4>
    <?php else:?>
        <h4><a href="#" class="toggle_control"><span class="toggle_arrow"></span><?php print t('Browse by Type') ?></a></h4>
    <?php endif;?>    
    <?php print render($browse_type); ?>

  </div>

  <?php

  if(!isset($_GET['search_api_views_fulltext']) && arg(1) != "markets"  && $language->language == "en"){
    if(isset($filters)){
		
		//echo "<pre>"; print_r($filters); exit;
    print  "<h3>".t('Filter By Specification:')."</h3>";

    for($i=0;$i<count($filters);$i++):
        $id = str_replace(" ","_",strtolower($filters[$i]['content']['#title']));
        $title = $filters[$i]['content']['#title'];
        print sprintf('<div class="filter_bar toggle_box " id="%s">',$id);
        print sprintf('<h4><a href="#" class="toggle_control"><span class="toggle_arrow"></span>%s</a></h4>',$title);
        print '<div class="toggle_target">';
        print render($filters[$i]);
        print  '</div></div>';
    endfor;

      }
	 }
  ?>


  <?php if(isset($search_page)) : ?>
    <h2 class="product_results"><a href="<?php print $link_other_results;?>"><?php print t('Other Results') ?></a> <span class="count">(<?php print render($total_product_assets); ?>)</span></h2>
  <?php endif; ?>

</div>
