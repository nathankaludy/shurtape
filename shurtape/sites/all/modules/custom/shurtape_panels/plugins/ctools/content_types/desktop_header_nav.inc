<?php
$plugin = [
  'title' => t('Desktop header nav'),
  'single' => TRUE,
  'render callback' => 'shurtape_panels_desktop_header_nav_render',
  'description' => t('desktop_header_nav'),
  'category' => t('Shurtape panes'),
];

function shurtape_panels_desktop_header_nav_render() {
  $block = new stdClass();

  $products_menu = menu_tree_output(menu_tree_all_data('menu-products-menu'));
  $resources_menu = menu_tree_output(menu_tree_all_data('menu-resources-menu'));
  $about_menu = menu_tree_output(menu_tree_all_data('menu-about-menu'));
  $contact_menu = menu_tree_output(menu_tree_all_data('menu-contact-menu'));

  $products_menu = drupal_render($products_menu);
  $resources_menu = drupal_render($resources_menu);
  $about_menu = drupal_render($about_menu);
  $contact_menu = drupal_render($contact_menu);

  $base_path = base_path();
  $wtb = t('Where to Buy');

  $block->content['#markup'] = <<<HTML
<div id="nav">
        <ul class="primary_menu list-inline clearfix">
          <li class="link_products_item">{$products_menu}</li>
          <li class="link_resources_item">{$resources_menu}</li>
          <li class="link_about_item">{$about_menu}</li>
          <li class="link_contact_item">{$contact_menu}</li>
          <li class="link_where_item">
            <ul class="menu">
              <li class="first"><a href="{$base_path}where-to-buy" title="{$wtb}">{$wtb}</a></li>
            </ul>
          </li>
        </ul>
      </div>
HTML;

  return $block;

}