<?php

namespace Drupal\d_submodules;

trait Callback {
  static function getCallbackName($method) {
    return Submodules::generateCallbackName(get_called_class() . '::' . $method);
  }

  static function getEncodedName($suffix = '', $class = NULL) {
    if (NULL === $class) {
      $class = get_called_class();
    }
    return base_convert(md5($class . $suffix), 16, 36);
  }
}