<ul class="tab_controls pull-left">
    <?php if (!empty($variables['product_properties_tab_content'])): ?>
        <li><a href="#" class="tab_control" rel="physical_tab"><?php print t('Physical Properties'); ?></a></li>
    <?php endif; ?>
    <?php if (!empty($variables['product_markets']) || !empty($variables['product_applications'])): ?>
            <li><a href="#" class="tab_control active" rel="market_tab"><?php print t('Markets/Applications'); ?></a></li>
    <?php endif; ?>
    <?php if (!empty($variables['product_sizes_colors_tab_content'])): ?>
        <li><a href="#" class="tab_control" rel="size_color_tab"><?php print t('Sizes & Colors'); ?></a></li>
    <?php endif; ?>
    <?php if (!empty($variables['product_downloads_tab_content'])): ?>
        <li><a href="#" name="downloads" id="downloads-tab-link" class="tab_control" rel="downloads_tab"><?php print t('Downloads'); ?></a></li>
    <?php endif; ?>
    <?php if (!empty($variables['product_videos'])): ?>
        <li><a href="#" class="tab_control" rel="videos_tab" id="product_videos-tab-link"><?php print t('Videos'); ?></a></li>
    <?php endif; ?>
    <?php if (!empty($variables['product_testimonial'])): ?>
        <li><a href="#" class="tab_control" rel="testimonials_tab"><?php print t('Testimonials'); ?></a></li>
    <?php endif; ?>
</ul>
