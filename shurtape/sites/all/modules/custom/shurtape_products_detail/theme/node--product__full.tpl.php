<?php
$product_properties_tab_content = views_embed_view('physical_properties', 'block');
$product_sizes_colors_tab_content = views_embed_view('product_colors', 'block_1');
$product_downloads_tab_content = views_embed_view('product_assets', 'block');
?>

<div id="detail_content" class="row">
	<div class="detail_media col-md-6">
		<div id="thumb_slider" class="thumb_slider swipe_scroll">
			<a href="#" class="arrow_control mobile prev"></a>
			<?php $view = views_embed_view('product_images', 'block_1') . views_embed_view('product_images', 'block_2')?>
			<?php if ($view): ?>
				<?= $view ?>
			<?php else: ?>
				<div class="slides">
					<div class="slide"><img src="//twshurtape.s3.amazonaws.com/default_images/no-image-product-detal.png" alt=""></div>
				</div>
			<?php endif; ?>
			<a href="#" class="arrow_control mobile next"></a>
		</div>
	</div>

	<div class="detail_content col-md-6">
		<?php if (isset($content['field_is_green_point_certified']['#items'][0]["value"]) && $content['field_is_green_point_certified']['#items'][0]["value"] == "1"): ?>
			<div class="detail_badge">
				<img src="//twshurtape.s3.amazonaws.com/imce/shurtape_green_point.jpg" width="138" height="64" />
			</div>
		<?php endif; ?>
		<div class="detail_title">
			<?php if ($title): ?>
				<?php echo $title; ?>
			<?php endif; ?>
		</div>
		<div class="detail_subtitle">
			<?php $content['field_product_bullet_description']['#label_display'] = 'hidden'; ?>
			<?php print render($content['field_product_bullet_description']); ?>
		</div>
		<div class="detail_desc">
			<?php $content['body']['#label_display'] = 'hidden'; ?>
			<?php print render($content['body']); ?>
		</div>
		<div class="cta_box_wrap">
			<div class="cta_box">
				<div class="desktop">
					<a class="btn btn-orange find_data_sheet" href="#downloads">Find Data Sheets &amp; MSDS</a>
				</div>
				<div class="mobile">
					<a class="btn btn-orange find_data_sheet_mobile" href="#">Find Data Sheets &amp; MSDS</a>
				</div>
				<div class="desktop">
					<a class="btn btn-orange" href="/where-to-buy">Find a Distributor</a>
				</div>
				<div class="mobile">
					<a class="btn btn-orange" href="/where-to-buy">Find a Distributor</a>
				</div>
				<?php $block = module_invoke('menu', 'block_view', 'menu-product-detail-menu'); ?>
				<?= render($block['content']) ?>
			</div>
			<div id="request_sample_popout" class="popout">
				<a href="#" class="close_popout">CLOSE</a>
				<h3>Request a Sample</h3>
				<div class="popout_body">
					To request a sample please call us at<br />
					<div class="desktop">
						<span class="phone_number">1-888-442-8273 (TAPE)</span>
					</div>
					<div class="mobile">
						<a href="tel:888-442-8273" class="phone_number">1-888-442-8273 (TAPE)</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="desktop" id="downloads">
	<div id="tab_section" class="tab_group clearfix">
		<ul class="tab_controls pull-left">
			<?php if (!empty($product_properties_tab_content)): ?>
				<li><a href="#" class="tab_control" rel="physical_tab">Physical Properties</a></li>
			<?php endif; ?>
			<?php if (isset($content['field_product_market_list']) || isset($content['field_product_applications'])): ?>
				<li><a href="#" class="tab_control active" rel="market_tab">Markets/Applications</a></li>
			<?php endif; ?>
			<?php if (!empty($product_sizes_colors_tab_content)): ?>
				<li><a href="#" class="tab_control" rel="size_color_tab">Sizes &amp; Colors</a></li>
			<?php endif; ?>
			<?php if (!empty($product_downloads_tab_content)): ?>
				<li><a href="#" name="downloads" id="downloads-tab-link" class="tab_control" rel="downloads_tab">Downloads</a></li>
			<?php endif; ?>
			<?php if (isset($content['field_product_videos'])): ?>
				<li><a href="#" class="tab_control" rel="videos_tab">Videos</a></li>
			<?php endif; ?>
			<?php if (isset($content['field_product_testimonial'])): ?>
				<li><a href="#" class="tab_control" rel="testimonials_tab">Testimonials</a></li>
			<?php endif; ?>
		</ul>
		<div class="tab_wrapper">
			<?php if (isset($content['field_related_products'])): ?>
				<div class="related_products">
					<h3>Related products</h3>
					<?php $content['field_related_products']['#label_display'] = 'hidden'; ?>
					<?php print render($content['field_related_products']); ?>
				</div>
			<?php endif; ?>

			<div class="tab_targets">
				<?php if (!empty($product_properties_tab_content)): ?>
					<div id="physical_tab" class="tab_target product_data_table">
						<h3>Physical Properties</h3>
						<div class="blue_bar">
							<div class="col title_col">&nbsp;</div>
							<div class="col standard_col">Standard</div>
							<div class="col metric_col">Metric</div>
							<div class="col astm_col">ASTM Test Method</div>
						</div>
						<?php print render($product_properties_tab_content); ?>
					</div>
				<?php endif; ?>
				<?php if (isset($content['field_product_market_list']) || isset($content['field_product_applications'])): ?>
					<div id="market_tab" class="tab_target inline_grid">
						<?php if (isset($content['field_product_market_list'])): ?>
							<div class="grid_group clearfix">
								<div class="grid_label text-right">
									<h4>MARKETS</h4>
								</div>
								<div class="grid_content">
									<?php $content['field_product_market_list']['#label_display'] = 'hidden'; ?>
									<?php print render($content['field_product_market_list']); ?>
								</div>
							</div>
						<?php endif; ?>
						<?php if (isset($content['field_product_applications'])): ?>
							<div class="grid_item clearfix">
								<div class="grid_label text-right">
									<h4>TYPICAL APPLICATIONS</h4>
								</div>
								<div class="grid_content ">
									<?php $content['field_product_applications']['#label_display'] = 'hidden'; ?>
									<?php print render($content['field_product_applications']); ?>
								</div>
							</div>
						<?php endif; ?>
					</div>
				<?php endif; ?>
				<?php if (!empty($product_sizes_colors_tab_content)): ?>
					<div id="size_color_tab" class="tab_target product_data_table">
						<h3>Sizes and Colors Available</h3>
						<div class="blue_bar">
							<div class="col dimensions_col">Dimensions</div>
							<div class="col rolls_col">Rolls per case</div>
							<div class="col weight_col">Weight lbs/case</div>
							<div class="col cases_col">Cases per pallet</div>
							<div class="col colors_col">Available colors</div>
						</div>
						<?php print render($product_sizes_colors_tab_content); ?>
					</div>
				<?php endif; ?>
				<?php if (!empty($product_downloads_tab_content)): ?>
					<div id="downloads_tab" class="tab_target inline_grid">
						<div class="grid_group clearfix">
							<div class="grid_label text-right">
								<h4>Downloads</h4>
							</div>
							<?php print render($product_downloads_tab_content); ?>
						</div>
					</div>
				<?php endif; ?>
				<?php if (isset($content['field_product_videos'])): ?>
					<div id="videos_tab" class="tab_target">
						<?php $content['field_product_videos']['#label_display'] = 'hidden'; ?>
						<?php print render($content['field_product_videos']); ?>
					</div>
				<?php endif; ?>
				<?php if (isset($content['field_product_testimonial'])): ?>
					<div id="testimonials_tab" class="tab_target">
						<?php $content['field_product_testimonial']['#label_display'] = 'hidden'; ?>
						<?php print render($content['field_product_testimonial']); ?>
					</div>
				<?php endif; ?>
			</div>
		</div>
	</div>
</div>
<div id="mobile_toggle" class="mobile">
	<?php if (!empty($product_properties_tab_content)): ?>
		<div class="mobile_toggle_box" id="physical_box">
			<h4>
				<a href="#" class="mobile_toggle_control">
					<span class="icon">
						<img src="<?php print base_path() . drupal_get_path('theme', 'shurtape'); ?>/images/btn_mobile_toggle.png" alt="" />
					</span>
					<span class="title">Physical Properties</span>
				</a>
			</h4>
			<div class="mobile_toggle_target">
				<?php print render($product_properties_tab_content); ?>
				<?php if (isset($content['field_related_products'])): ?>
					<div class="related_products">
						<b>Related products</b>
						<?php $content['field_related_products']['#label_display'] = 'hidden'; ?>
						<?php print render($content['field_related_products']); ?>
					</div>
				<?php endif; ?>
			</div>
		</div>
	<?php endif; ?>
	<?php if (isset($content['field_product_market_list']) || isset($content['field_product_applications'])): ?>
		<div class="mobile_toggle_box" id="market_box">
			<h4>
				<a href="#" class="mobile_toggle_control">
					<span class="icon">
						<img src="<?php print base_path() . drupal_get_path('theme', 'shurtape'); ?>/images/btn_mobile_toggle.png" alt="" />
					</span>
					<span class="title">Markets/Applications</span>
				</a>
			</h4>
			<div class="mobile_toggle_target">
				<?php if (isset($content['field_product_market_list'])): ?>
					<div class="grid_group clearfix">
						<div class="grid_label text-right">
							<h4>MARKETS</h4>
						</div>
						<div class="grid_content">
							<?php $content['field_product_market_list']['#label_display'] = 'hidden'; ?>
							<?php print render($content['field_product_market_list']); ?>
						</div>
					</div>
				<?php endif; ?>
				<?php if (isset($content['field_product_applications'])): ?>
					<div class="grid_item clearfix">
						<div class="grid_label text-right">
							<h4>TYPICAL APPLICATIONS</h4>
						</div>
						<div class="grid_content ">
							<?php $content['field_product_applications']['#label_display'] = 'hidden'; ?>
							<?php print render($content['field_product_applications']); ?>
						</div>
					</div>
				<?php endif; ?>
				<?php if (isset($content['field_related_products'])): ?>
					<div class="related_products">
						<b>Related products</b>
						<?php $content['field_related_products']['#label_display'] = 'hidden'; ?>
						<?php print render($content['field_related_products']); ?>
					</div>
				<?php endif; ?>
			</div>
		</div>
	<?php endif; ?>
	<?php if (!empty($product_sizes_colors_tab_content)): ?>
		<div class="mobile_toggle_box" id="size_box">
			<h4>
				<a href="#" class="mobile_toggle_control">
					<span class="icon">
						<img src="<?php print base_path() . drupal_get_path('theme', 'shurtape'); ?>/images/btn_mobile_toggle.png" alt="" />
					</span>
					<span class="title">Sizes &amp; Colors</span>
				</a>
			</h4>
			<div class="mobile_toggle_target">
				<div class="box_heading">
					Sizes and Colors Available
				</div>
				<div class="box_content">
					<?php print render($product_sizes_colors_tab_content); ?>
				</div>
				<div class="box_footer">
					<?php if (isset($content['field_related_products'])): ?>
						<div class="related_products">
							<b>Related products</b>
							<?php $content['field_related_products']['#label_display'] = 'hidden'; ?>
							<?php print render($content['field_related_products']); ?>
						</div>
					<?php endif; ?>
				</div>
			</div>
		</div>
	<?php endif; ?>
	<?php if (!empty($product_downloads_tab_content)): ?>
		<div class="mobile_toggle_box" id="downloads_box">
			<h4>
				<a href="#" class="mobile_toggle_control">
					<span class="icon">
						<img src="<?php print base_path() . drupal_get_path('theme', 'shurtape'); ?>/images/btn_mobile_toggle.png" alt="" />
					</span>
					<span class="title">Downloads</span>
				</a>
			</h4>
			<div class="mobile_toggle_target">
				<div class="grid_group clearfix">
					<?php print render($product_downloads_tab_content); ?>
				</div>
			</div>
		</div>
	<?php endif; ?>
	<?php if (isset($content['field_product_videos'])): ?>
		<div class="mobile_toggle_box" id="videos_box">
			<h4>
				<a href="#" class="mobile_toggle_control">
					<span class="icon">
						<img src="<?php print base_path() . drupal_get_path('theme', 'shurtape'); ?>/images/btn_mobile_toggle.png" alt="" />
					</span>
					<span class="title">Videos</span>
				</a>
			</h4>
			<div class="mobile_toggle_target">
				<?php $content['field_product_videos']['#label_display'] = 'hidden'; ?>
				<?php print render($content['field_product_videos']); ?>
				<?php if (isset($content['field_related_products'])): ?>
					<div class="related_products">
						<b>Related products</b>
						<?php $content['field_related_products']['#label_display'] = 'hidden'; ?>
						<?php print render($content['field_related_products']); ?>
					</div>
				<?php endif; ?>
			</div>
		</div>
	<?php endif; ?>
	<?php if (isset($content['field_product_testimonial'])): ?>
		<div class="mobile_toggle_box" id="testimonials_box">
			<h4>
				<a href="#" class="mobile_toggle_control">
					<span class="icon">
						<img src="<?php print base_path() . drupal_get_path('theme', 'shurtape'); ?>/images/btn_mobile_toggle.png" alt="" />
					</span>
					<span class="title">Testimonials</span>
				</a>
			</h4>
			<div class="mobile_toggle_target">
				<?php $content['field_product_testimonial']['#label_display'] = 'hidden'; ?>
				<?php print render($content['field_product_testimonial']); ?>
			</div>
		</div>
	<?php endif; ?>
</div>
<div class="modal fade" id="video_modal" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
			</div>
			<div class="modal-body">
				<!-- loaded dynamically via JS below -->
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	/* define $ as jQuery just in case */
	( function( $ ){

		/* doc ready */
		$( function( ){
			/* init the thumb slider */
			$( '#thumb_slider' ).thumb_slider( );

			/* init the tabs */
			$( '#tab_section' ).tabs( );

			/* show the video modal */
			$( '.open_video_modal' ).click( function( e )
			{
				/* set vars */
				var vid = $( this ).attr( 'rel' );
				$( '#video_modal .modal-body' ).hide( ).html( '<iframe width="620" height="349" src="//www.youtube.com/embed/' + vid + '" frameborder="0" allowfullscreen></iframe>' ).show( ).fitVids( );
				$( '#video_modal' ).modal( );
				e.preventDefault( );
			});

			/* shut down video on close trigger */
			$( '#video_modal' ).on( 'hidden.bs.modal', function ( )
			{
				$( '#video_modal .modal-body' ).html( '' );
			});

			/* open product sample popout */
			$( '.open_request_popout' ).click( function( e )
			{
				/* set vars */
				var popout = $( '#request_sample_popout' );
				popout.show( );
				e.preventDefault( );
			});

			/* close popout */
			$( '.close_popout' ).click( function( e )
			{
				/* set vars */
				var popout = $( this ).parents( '.popout' );
				popout.hide( );
				e.preventDefault( );
			});

			/* find data sheet link */
			$( '.cta_box' ).on( 'click', '.find_data_sheet', function( e )
			{
				$( 'a#downloads-tab-link' ).click( );
			});

			/* find data sheet link */
			$( '.cta_box' ).on( 'click', '.find_data_sheet_mobile', function( e )
			{
				/* show toggle target */
				var parent_el 	= $( '.mobile_toggle_box#downloads_box' );
				var mobile_toggle_target = parent_el.find( '.mobile_toggle_target' );
				parent_el.addClass( 'open' );
				mobile_toggle_target.show( );

				/* switch the toggle_image */
				var toggle_icon = parent_el.find( '.icon img' ).attr( 'src' );
				var new_icon	= toggle_icon.replace( 'btn_mobile_toggle.png', 'btn_mobile_toggle_open.png' );
				parent_el.find( '.icon img' ).attr( 'src', new_icon );
				$('html, body').animate({
					scrollTop:$( '#downloads_box' ).offset().top
				}, 300);
				e.preventDefault( );
			});

			/* init the mobile video slider */
			$( '.mobile_toggle_box#videos_box .mobile_toggle_target .video_thumbs' ).video_slider( );

			/**
			 * resize size boxes to even height
			 */
			resize_size_boxes( );
			function resize_size_boxes( )
			{
				var size_boxes = $( '#mobile_toggle .mobile_toggle_box#size_box.open .mobile_toggle_target .box_content' ).find( '.views-row' );
				if ( size_boxes.length > 0 )
				{
					var max_box_height = 0;
					$.each( size_boxes, function( i )
					{
						var box_height = $( size_boxes[i] ).outerHeight();
						max_box_height = parseInt( box_height ) > parseInt( max_box_height ) ? box_height : max_box_height;
					});
					size_boxes.css({ 'min-height' : max_box_height + 'px' });
				}
			}

			/* fire size box equalizer on window resize */
			$( window ).resize(function()
			{
				resize_size_boxes( );
			});

			/* fire size box equalizer on click */
			$( '.mobile_toggle_box#size_box .mobile_toggle_control' ).click(function(e)
			{
				resize_size_boxes( );
			});

		});
	})( jQuery );
</script>
