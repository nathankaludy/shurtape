/**
 * @file
 * Main JS file for shurtape_blog module.
 */
(function($) {
  Drupal.behaviors.shurtapeBlog = {
    attach: function (context, settings) {
      $('.page-blog-view #subcontent .pane-node-body', context).once('shurtapeBlog', function () {
        $(this).find('.field-item').prepend($(this).parent().find('.pane-node-field-blog-additional-image img').addClass('additional-image'));
        $(this).find('.field-item').prepend('<div id="spacer"></div>');
        $(this).parent().find('.pane-node-field-blog-additional-image').remove();
      });
      $('.pane-shurtape-blog-shurtape-notification-signup', context).once('shurtapeModal', function () {
        $(this).find('a[href*="blog/signup/ajax"]').bind('click', function () {
          $('#modalContent').addClass('blog-modal-signup');
          $('#modalContent .modal-header a.close').html('<button class="close">×</button>');
        });
      });
      $(window).bind('load resize', function () {
        var spacer = document.getElementById('spacer');
        if (spacer) {
          spacer.style.height = 0;
          var container = spacer.parentNode;
          var img = spacer.nextElementSibling || spacer.nextSibling;
          var lastContentNode = container.children[container.children.length - 1];
          var h = Math.max(0, container.clientHeight - img.clientHeight);
          spacer.style.height = h + "px";
          while (h > 0 && img.getBoundingClientRect().bottom > lastContentNode.getBoundingClientRect().bottom) {
            spacer.style.height = --h + "px";
          }
        }
      });
      $('body.not-logged-in.page-blog-view .pane-node-comment-form', context).once('shurtapeComments', function () {
        var $pane = $(this).find('> .pane-content');
        var text = $pane.text().trim();
        var $link = $pane.find('> a');
        $link.text(text);
        $pane.html($link);
      });
    }
  };
})(jQuery);
