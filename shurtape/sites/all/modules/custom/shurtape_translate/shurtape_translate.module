<?php

/**
 * Implements hook_menu().
 */
function shurtape_translate_menu() {
  $items['node/%node/translate/default'] = array(
    'title' => t('Set node default language'),
    'page callback' => 'shurtape_translate_set_default_language',
    'page arguments' => array(1),
    'access callback' => 'shurtape_tranlate_access',
    'access arguments' => array(1),
    'type' => MENU_LOCAL_TASK,
  );
  return $items;
}

/**
 * Access callback: Checks that the user has permission to 'translate content'.
 *
 * Only allows the translation for nodes of types that
 * have translation enabled.
 *
 * @param $node
 *   A node object.
 *
 * @return
 *   TRUE if the translation tab should be displayed, FALSE otherwise.
 *
 * @see translation_menu()
 */
function shurtape_tranlate_access($node) {
  if (locale_multilingual_node_type($node->type) && node_access('view', $node)) {
    return user_access('translate content');
  }
  return false;
}

/**
 * Set node default language which will be used as translation source.
 * @param $node
 * @return mixed
 */
function shurtape_translate_set_default_language($node) {
  $default_language = language_default();
  $source_language = $default_language->language;

  // Set source language
  if (entity_language('node', $node) == LANGUAGE_NONE) {
    $node->language = $source_language;

    //  If alias exists
    if (isset($node->_path['alias'])) {
      $path = path_load(array('source' => 'node/' . $node->nid, 'language' => LANGUAGE_NONE));
      if (isset($path['pid'])) {
        // Disable pathauto url generation
        $path['pathauto'] = false;
        // Update old alias language
        $path['language'] = $source_language;

        $node->path = $path;
      }
    }
    node_save($node);
  }

  module_load_include('inc', 'entity_translation', 'entity_translation.admin');
  return entity_translation_overview('node', $node);
}

/**
 * Implements hook_form_BASE_FORM_ID_alter().
 * Set default language path alias to empty translation path field.
 */
function shurtape_translate_form_node_form_alter(&$form, &$form_state, $form_id) {
  $default_language = language_default();
  if (isset($form['#node']->nid) && empty($form['path']['alias']['#default_value'])) {
    $node = $form['#node'];
    $path = drupal_get_path_alias('node/' . $node->nid, $default_language->language);
    if ($path) {
      $form['path']['alias']['#default_value'] = $path;
      $form['path']['pathauto']['#default_value'] = false;
    }
  }
}