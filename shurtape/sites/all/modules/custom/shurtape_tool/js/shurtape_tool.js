(function ($) {

  /**
   * Main behavior for shurtape_tool module.
   */
  Drupal.behaviors.shurtapeTool = {
    map: false,
    markers: [],
    renderItems: function (data, items, headers) {
      var i, index, item;
      if (this.markers.length) {
        for (i in this.markers) {
          this.markers[i].setMap(null);
        }
        this.markers = [];
      }
      var $container = $('#view-companies'), output = '<ul>', company_info = '';
      var $headersContainer = $('#header-results'), outputHeaders = '<ul>';
      if (Array.isArray(items)) {
        var bounds = new google.maps.LatLngBounds();
        for (i in items) {
          item = items[i];
          // Build results.
          company_info = '<div class="company-wrapper">';
          if (item['title']) {
            company_info += '<div class="company-title">' + item['title'] + '</div>';
          }
          if (item['address']) {
            company_info += '<div class="company-address">' + item['address'] + '</div>';
          }
          if (item['phone']) {
            company_info += '<div class="company-phone">' + item['phone'] + '</div>';
          }
          if (item['url']) {
            company_info += '<div class="company-url"><a href="' + item['url'] + '" target="_blank" onclick="_gaq.push(\'_trackEvent\', \'Link\', \'Click\', \'' + item['title'] + '\']);">' + item['url'] + '</a></div>';
          }
          company_info += '</div>';
          output += '<li class="company-item" data-index="' + item['id'] + '.">' + company_info + '</li>';
          // Add a markers.
          if (data['map']) {
            var Latlng = new google.maps.LatLng(item['lat'], item['lng']);
            var _id = parseInt(item['id']);
            if (_id > 100) {
              _id = _id % 100;
            }
            this.markers[i] = new google.maps.Marker({
              position: Latlng,
              icon: data['path'] + '/i/icons/number_' + _id + '.png',
              map: this.map,
              title: item['title'],
              infoWindow: new google.maps.InfoWindow({
                content: company_info
              })
            });
            bounds.extend(Latlng);
            google.maps.event.addListener(this.markers[i], 'click', function () {
              var map = Drupal.behaviors.shurtapeTool.map;
              for (var index in Drupal.behaviors.shurtapeTool.markers) {
                var marker = Drupal.behaviors.shurtapeTool.markers[index];
                marker.infoWindow.close(map, marker);
              }
              this.infoWindow.open(map, this);
              _gaq.push(['_trackEvent', 'Link', 'Map', this.title]);
            });
          }
        }
        output += '</ul>';
      }
      if (this.markers.length || data['map'] == false) {
        if (output == '<ul>') {
          $('#header-results').hide();
          $container.html($('<div class="item-list" />').html(data['ecommerce_distributors']));
        } else {
          $('#header-results').show();
          $container.html($('<div class="item-list" />').html(output));
        }
        if (data['pager']) {
          $container.append('<div class="pagination">' + data['pager'] + '</div>');
          $container.find('ul.pager > li > a').bind('click', function (e) {
            e.preventDefault();
            $container.html('');
            $container.addClass('data-loading');
            $.post($(this).attr('href'), $('#shurtape-tool-where-to-buy-form').serialize(), function (data) {
              var items = data['results'] ? data['results'] : false;
              Drupal.behaviors.shurtapeTool.renderItems(data['data'], items, false);
            }, 'json');
          });
        }
        if (data['map']) {
          this.map.fitBounds(bounds);
        }
      }
      else {
        if (data['map']) {
          this.map.panTo(new google.maps.LatLng(38.680632, -96.5001));
          this.map.setZoom(4);
        }
        $container.html('<div class="empty-text">' + items + '</div>');
      }
      $('#edit-results').removeClass("data-loading");
      $container.removeClass('data-loading');
      // Prepare header results.
      if (Array.isArray(headers)) {
        $headersContainer.html('');
        for (i in headers) {
          item = headers[i];
          // Build results.
          company_info = '<div class="company-wrapper">';
          if (item['title']) {
            company_info += '<div class="company-title">' + item['title'] + '</div>';
          }
          // if (item['address']) {
          //   company_info += '<div class="company-address">' + item['address'] + '</div>';
          // }
          if (item['phone']) {
            company_info += '<div class="company-phone">' + item['phone'] + '</div>';
          }
          if (item['url']) {
            company_info += '<div class="company-url"><a href="' + item['url'] + '" target="_blank" onclick="_gaq.push(\'_trackEvent\', \'Link\', \'Click\', \'' + item['title'] + '\']);">' + item['url'] + '</a></div>';
          }
          company_info += '</div>';
          outputHeaders += '<li class="company-item">' + company_info + '</li>';
        }
        outputHeaders += '</ul>';
        $headersContainer.html('<div class="item-list">' + outputHeaders + '</div>');
      }
    },
    attach: function (context, settings) {
      $('#nav', context).once('shurtapeModal',function () {
        $('a[href*="/where-to-buy/nojs"]').bind('click', function () {
          $('#modalContent').addClass('page-where-to-buy');
          $('#modalContent .modal-header a.close', context).html('<button class="close">×</button>');
        });
      });
      $('#map_canvas', context).once('shurtapeGmap', function () {
        var mapOptions = {
          zoom: 4,
          center: new google.maps.LatLng(38.680632, -96.5001),
          mapTypeId: google.maps.MapTypeId.ROADMAP,
          panControl: true,
          mapTypeControl: false,
          panControlOptions: {
            position: google.maps.ControlPosition.RIGHT_CENTER
          },
          zoomControl: true,
          zoomControlOptions: {
            style: google.maps.ZoomControlStyle.LARGE,
            position: google.maps.ControlPosition.RIGHT_CENTER
          },
          scaleControl: false,
          streetViewControl: false,
          streetViewControlOptions: {
            position: google.maps.ControlPosition.RIGHT_CENTER
          }
        };
        Drupal.behaviors.shurtapeTool.map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);
      });
      $('#shurtape-tool-where-to-buy-form', context).once('shurtapeSearch', function () {
        var $this = $(this),
            $results = $this.find('#view-companies'),
            $headerResults = $this.find('#header-results'),
            $resultWrapper = $this.find('#edit-results');
        // Set tooltips.
        $this.find('#edit-type > .form-item-type').each(function () {
          var $input = $(this).find('input[name="type"]');
          $(this).find('label').attr('title', $input.attr('title'));
          $input.removeAttr('title');
        });
        $this.find('#edit-results-wrapper').addClass('map-hide');
        $this.find('input[name="type"]').bind('change', function () {
          $this.find('#edit-results-wrapper').addClass('map-hide');
          $this.find('input.error').removeClass('error');
        });
        $this.find('input.form-submit').bind('click', function (e) {
          e.preventDefault();
          var $zipWrapper = $this.find('#edit-geodata'),
              $zip = $zipWrapper.find('input');
          if ($zipWrapper.css('display') != 'none' && $.trim($zip.val()) == '') {
            $zip.addClass('error');
          }
          else {
            $zip.removeClass('error');
          }
          if ($zip.hasClass('error') == false) {
            var zipCode = $.trim($zip.val());
            if (zipCode) {
              _gaq.push(['_trackEvent', 'Zip', 'Search', zipCode]);
            }
            $this.find('#edit-results-wrapper').removeClass('map-hide');
            $results.html('');
            $headerResults.html('');
            $resultWrapper.addClass("data-loading");
            $.post(settings.basePath + 'where-to-buy/results/ajax', $this.serialize(), function (data) {
              var items = data['results'] ? data['results'] : false;
              var headers = data['headers'] ? data['headers'] : false;
              Drupal.behaviors.shurtapeTool.renderItems(data['data'], items, headers);
            }, 'json');
          }
        });
        $(window).bind('load resize', function () {
          if ($(window).width() <= 640) {
            $this.find('#edit-geodata').after($this.find('.form-actions'));
          }
          else {
            $this.find('#edit-geodata').before($this.find('.form-actions'));
          }
        });
      });
    }
  };
})(jQuery);
