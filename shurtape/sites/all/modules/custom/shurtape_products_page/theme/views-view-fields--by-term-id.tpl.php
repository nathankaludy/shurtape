<a href="<?php print $link;?>" class="item">
	<span class="mobile_link_icon mobile">
		<img src="<?php print base_path() . drupal_get_path('theme', 'shurtape'); ?>/images/icon_mobile_link_arrow.png" alt="" />
	</span>
	<span class="item_thumb">

		<?php if($render= render($row->field_field_term_image[0]['rendered'])): ?>

			<?php print $render ?>

		<?php elseif($render = render($row->field_field_markets_term_image[0]['rendered'])): ?>

			<?php print $render ?>

		<?php else : ?>

			<img src="<?php print base_path() . drupal_get_path('theme', 'shurtape'); ?>/images/fpo_sample_type.png" />

		<?php endif; ?>

	</span>
	<span class="item_content">

		<span class="cta_arrow"></span>
		<?php

		/* strip the "Tapes" from the item title - but only if it's the last word */
		$split = explode( ' ', $row->taxonomy_term_data_name_i18n );
		$last = $split[ count( $split ) - 1 ];
		$item_title = ( strtolower( $last ) == 'tapes' ) ? '<b>' . str_replace( 'Tapes', '', $row->taxonomy_term_data_name_i18n ) . '</b> Tapes' : '<b>' . $row->taxonomy_term_data_name_i18n . '</b>'; ?>

		<span class="item_title"><?php echo $item_title; ?></b></span>
	</span>
</a>
