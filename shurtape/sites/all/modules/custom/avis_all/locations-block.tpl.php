<div class="locations" style="margin-top:20px;">
<?php
$result = views_get_view_result("locations_page", "block");

$countCateg = 0;
$countHeader = 0;
$countTotal = 0;
$lastCateg = "";
$lastHeader = "";
$stringItems = "";
$stringHeaders = "";
$stringTotal = "";
$mobileHeader = "";
$newCategory = 0;
$newCatName = "";

$countItems = count($result);
for ($x = 0; $x <= $countItems; $x++) {
  if(isset($result[$x])){
    $location = $result[$x];

    $currentCateg = $location -> field_field_location_category["0"]["raw"]["taxonomy_term"] -> name;
    $currentHeader = $location -> field_field_location_header["0"]["raw"]["taxonomy_term"] -> name;
    $currentBody = $location -> field_body["0"]["rendered"]["#markup"];
  }


  if ($currentHeader != $lastHeader || $currentCateg != $lastCateg) {

    if($lastCateg != "Headquarters"){
      if ($countHeader > 0) {
        $mobileHeader = '<div class="blue_bar mobile"><h3>' . t($currentHeader) . '</h3></div>';
        $mdCol = 3 * $countHeader;
        $stringHeaders .= '<div class="col-md-' . $mdCol . '"><h3> ' . t($lastHeader) . '</h3></div>';
      }
      if($lastCateg == ""){
        $mobileHeader = '<div class="blue_bar mobile"><h3>' . t($currentHeader) . '</h3></div>';
      }
    } else {
      $mobileHeader = '<div class="blue_bar mobile"><h3>' . t($currentHeader) . '</h3></div>';
      $stringHeaders .= '<div class="col-md-6"><h3> ' . t($lastHeader) . '</h3></div>';
    }

    $lastHeader = $currentHeader;
    $countHeader = 0;
  }

  if($lastCateg != $newCatName){
    $newCategory = 0;
  }


  if (($countTotal % 4 == 0 && $countTotal > 0) || ($currentCateg != $lastCateg && $countTotal > 0)) {//cada 4 o cambio de categ
    if ($lastCateg != "Headquarters" && $newCategory == 0) {
      $categoryH2 = '<h2>' . t($lastCateg) . '</h2>';
      $newCategory = 1;
      $newCatName = $lastCateg;
    } else {
      $categoryH2 = "";
    }
    $stringHeaders = $categoryH2 . '<div class="blue_bar row">' . $stringHeaders . '</div>';
    $stringItems = '<div class="offices row">' . $stringItems . '</div>';
    $stringTotal .= $stringHeaders . $stringItems;
    $stringItems = "";
    $stringHeaders = "";
    $countTotal = 0;
  }
  if ($currentCateg != $lastCateg) {
    $lastCateg = $currentCateg;
    $countCateg = 0;
  }


  $stringItems .= $mobileHeader;
  if($lastCateg != "Headquarters"){
    $stringItems .= '<div class="col-md-3 item"><div class="mobile_pad">' . $currentBody . '</div></div>';
  } else {
    $stringItems .= '<div class="col-md-6 item"><div class="mobile_pad">' . $currentBody . '</div></div>';
  }

  $countCateg++;
  $countHeader++;
  $countTotal++;
  $mobileHeader = "";
}//foreach grande

print $stringTotal;
?>
</div>
