<?php
/**
 * @file
 * products_import.feeds_importer_default.inc
 */

/**
 * Implements hook_feeds_importer_default().
 */
function products_import_feeds_importer_default() {
  $export = array();

  $feeds_importer = new stdClass();
  $feeds_importer->disabled = FALSE; /* Edit this to true to make a default feeds_importer disabled initially */
  $feeds_importer->api_version = 1;
  $feeds_importer->id = 'products';
  $feeds_importer->config = array(
    'name' => 'Products',
    'description' => 'All products',
    'fetcher' => array(
      'plugin_key' => 'FeedsFileFetcher',
      'config' => array(
        'allowed_extensions' => 'txt csv tsv xml opml',
        'direct' => FALSE,
        'directory' => 'public://feeds',
        'allowed_schemes' => array(
          0 => 'public',
        ),
      ),
    ),
    'parser' => array(
      'plugin_key' => 'FeedsCSVParser',
      'config' => array(
        'delimiter' => ',',
        'no_headers' => 0,
      ),
    ),
    'processor' => array(
      'plugin_key' => 'FeedsNodeProcessor',
      'config' => array(
        'expire' => '-1',
        'author' => 0,
        'authorize' => 1,
        'mappings' => array(
          0 => array(
            'source' => 'Item Number',
            'target' => 'title',
            'unique' => 1,
          ),
          1 => array(
            'source' => 'Item Number',
            'target' => 'title_field:et:en',
            'unique' => FALSE,
          ),
          2 => array(
            'source' => 'STT Short Description',
            'target' => 'field_product_short_description:et:en',
            'unique' => FALSE,
          ),
          3 => array(
            'source' => 'STT Product Description',
            'target' => 'body:et:en',
            'unique' => FALSE,
          ),
          4 => array(
            'source' => 'product_features',
            'target' => 'field_product_bullet_description:et:en',
            'unique' => FALSE,
          ),
          5 => array(
            'source' => 'applicable_standards',
            'target' => 'field_applicable_standards:et:en',
            'unique' => FALSE,
          ),
          6 => array(
            'source' => 'typical_applications',
            'target' => 'field_typical_applications:et:en',
            'unique' => FALSE,
          ),
          7 => array(
            'source' => 'markets',
            'target' => 'field_product_market_list',
            'unique' => FALSE,
          ),
        ),
        'update_existing' => '2',
        'update_non_existent' => 'skip',
        'input_format' => 'plain_text',
        'skip_hash_check' => 0,
        'bundle' => 'product',
      ),
    ),
    'content_type' => '',
    'update' => 0,
    'import_period' => '-1',
    'expire_period' => 3600,
    'import_on_create' => 1,
    'process_in_background' => 0,
  );
  $export['products'] = $feeds_importer;

  $feeds_importer = new stdClass();
  $feeds_importer->disabled = FALSE; /* Edit this to true to make a default feeds_importer disabled initially */
  $feeds_importer->api_version = 1;
  $feeds_importer->id = 'products_spain';
  $feeds_importer->config = array(
    'name' => 'Products Spain',
    'description' => '',
    'fetcher' => array(
      'plugin_key' => 'FeedsFileFetcher',
      'config' => array(
        'allowed_extensions' => 'txt csv tsv xml opml',
        'direct' => FALSE,
        'directory' => 'public://feeds',
        'allowed_schemes' => array(
          0 => 'public',
        ),
      ),
    ),
    'parser' => array(
      'plugin_key' => 'FeedsCSVParser',
      'config' => array(
        'delimiter' => ',',
        'no_headers' => 0,
      ),
    ),
    'processor' => array(
      'plugin_key' => 'FeedsNodeProcessor',
      'config' => array(
        'expire' => '-1',
        'author' => 0,
        'authorize' => 1,
        'mappings' => array(
          0 => array(
            'source' => 'Item Number',
            'target' => 'title',
            'unique' => 1,
          ),
          1 => array(
            'source' => 'Item Number',
            'target' => 'title_field:et:es',
            'unique' => FALSE,
          ),
          2 => array(
            'source' => 'STT Short Description (Spanish)',
            'target' => 'field_product_short_description:et:es',
            'unique' => FALSE,
          ),
          3 => array(
            'source' => 'STT Product Description (Spanish)',
            'target' => 'body:et:es',
            'unique' => FALSE,
          ),
          4 => array(
            'source' => 'product_features',
            'target' => 'field_product_bullet_description:et:es',
            'unique' => FALSE,
          ),
          5 => array(
            'source' => 'applicable_standards',
            'target' => 'field_applicable_standards:et:es',
            'unique' => FALSE,
          ),
          6 => array(
            'source' => 'typical_applications',
            'target' => 'field_typical_applications:et:es',
            'unique' => FALSE,
          ),
        ),
        'update_existing' => '2',
        'update_non_existent' => 'skip',
        'input_format' => 'plain_text',
        'skip_hash_check' => 0,
        'bundle' => 'product',
      ),
    ),
    'content_type' => '',
    'update' => 0,
    'import_period' => '-1',
    'expire_period' => 3600,
    'import_on_create' => 1,
    'process_in_background' => 0,
  );
  $export['products_spain'] = $feeds_importer;

  return $export;
}
