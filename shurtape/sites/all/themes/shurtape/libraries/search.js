/**
 * JS for the product search pages - normal / tecnhical view
 * ==============
 * NOTE: the JS for the detail and category pages was moved to products.js
 * - better to compartmentalize for the dedicated resource for search
 */
/* define $ as jQuery just in case */
( function( $ ){

  /* doc ready */
  $( function( ){

    var check_compare = window.location.href.split('checked=');
    if(typeof(check_compare[1]) != 'undefined') {
      var checks = check_compare[1].split('+');
      for(x=0; x < checks.length; x++) {
        $('a.control_compare[data-value="' + checks[x] + '"]').addClass('current');
      }
    }

    $('a.scroll-table').on('click', function(e) {
      e.preventDefault();

      var scroller = $('.tech-table-wrapper');

      var curr_left = scroller.scrollLeft();

      if($(this).data('direction') == 'left') {
        scroller.scrollLeft(curr_left - 100);
      }
      else {
        scroller.scrollLeft(curr_left + 100);
      }
    });

    $('a.convert-standard').on('click', function(e) {
      $('span.metric').hide();
      $('span.standard').show();
	  $('tr.units td.first-col.units').css({'height' : '27px'});
      e.preventDefault();
    });

    $('a.convert-metric').on('click', function(e) {
      $('span.standard').hide();
      $('span.metric').show();
	  $('tr.units td.first-col.units').css({'height' : '41px'});
      e.preventDefault();
    });

    $('.compare_selected').on('click', function(e) 
		{
			e.preventDefault();
            var current_url = window.location.href.split('?');

            if(current_url[0].slice(-1,current_url[0].length) == '/') {
                current_url[0] = current_url[0].slice(0,-1);
            }

            var dest_url = current_url[0] + '/technical/';
            var search = window.location.search;
            var products = '';;
            search = remove_url_param(search,"page");
            search = remove_url_param(search,"items_per_page");
            $.ajax({
                url:'/ajax/shurtape_products_nav_compare_products/get',
                type:'get',
                dataType:'json',
                success: function ( data )
                {                                            
                    if(data.product_count > 1) {        
                        products = data.product_string;
                        if(search.match(/\?/) != null) {
                            if (search.match(/checked/) == null) {
                                dest_url += products;                                
                            }
                            else {
                                var replace = search.split('checked=');
                                search = search.replace(replace[1], products);
                                dest_url += products + search;                                
                            }
                        }
                        else {
                            dest_url += products + '?checked=' + products;                            
                        }
                        window.location.href = dest_url;
                    }else{
                        alert('Please select at least two products to compare.');                        
                    }    
                }                    
            });
		});

    /* items per page dropdown */
	$('select[name="per_page"]').change( function( e )
	{
		var curr_url    = window.location.href;
		var results_param   = 'items_per_page';
		var results_num   = $( this ).val( );
		var redirect    = update_param( curr_url, results_param, results_num );
		window.location.href = redirect;
		e.preventDefault( );
	});


    var query = window.location.search;

		if(query.match(/product_adhesive/) != null) 
		{
			$('#product_adhesive').addClass('open');
		}
		if(query.match(/product_backing/) != null) 
		{
			$('#product_backing').addClass('open');
		}
		if(query.match(/product_liner/) != null) 
		{
			$('#product_liner').addClass('open');
		}


    /* init the filter function */
    filter_products( );

    /* init the compare products function */
    compare_products( );

    /* filter bar filter controls */
    function filter_products( ) 
		{
			/* filter control click action */
			$( '.filter_box' ).on( 'click', '.filter_control', function( e ) 
			{
				/* checkbox state */
				if ( $( this ).hasClass( 'current' ) )
				{
					$( this ).removeClass( 'current' );
				}
				else
				{
					$( this ).addClass( 'current' );
				}

				/*  this is where a significant amount of AJAX needs to go */
				$.ajax({
					url:'',
					type:'get',
					dataType:'json',
					success: function ( data )
					{
						/* do some AJAX */
					},
					error: function ( errmsg )
					{

					}
				});
				e.preventDefault( );
			});
		}

    /* filter bar filter controls */
        function compare_products( ) {

            /* filter control click action */
            $( '#product_grid' ).on( 'click', '.control_compare', function( e ) {
                e.preventDefault();
                var action = null;
                /* checkbox state */
                if ( $( this ).hasClass( 'current' ) )
                {
                    $( this ).removeClass( 'current' );
                    action = "remove";
                }
                else
                {
                    $( this ).addClass( 'current' );
                    action = "add";
                }
                
                $.ajax({
                    url:'/ajax/shurtape_products_nav_compare_products/'+action+"/"+$(this).attr("data-value"),
                    type:'get',
                    dataType:'json',
                    success: function ( data )
                    {                
                    }                    
                });
            });
            
        }
     
     /* tech view navigation controls */
		$( '.tech_view .navigator' ).on( 'click', '.control', function( e ) 
		{
			/* set vars */
			var curr_index	= parseInt( $( 'input#tech_nav_active' ).val( ) );
			var parent_el	= $( this ).parents( '.navigator' );
			var headings	= parent_el.find( '.hdg' );
			var item_count	= headings.length;

			/* checkbox state */
			if ( $( this ).hasClass( 'prev' ) )
			{
				var target_index = curr_index == 1 ? item_count : parseInt( curr_index - 1 );
			}
			else
			{
				var target_index = curr_index == item_count ? 1 : parseInt( curr_index + 1 );
			}
			$( 'input#tech_nav_active' ).val( target_index )
			$( '.hdg' ).hide( ).removeClass( 'active' );
			$( '.hdg_' + target_index ).show( ).addClass( 'active' );
			$( '.sub' ).hide( ).removeClass( 'active' );
			$( '.sub_' + target_index ).show( ).addClass( 'active' );
			$( '.data_item' ).hide( ).removeClass( 'active' );
			$( '.data_item.item_' + target_index ).show( ).addClass( 'active' );
			e.preventDefault( );
		});
		
		/* swipe scroller for the tech view slider */
		$('.mobile.tech_view').hammer({ }).on( 'swipeleft dragleft', function( e ) 
		{ 
			var parent_el = $( this );
			var trigger_link = parent_el.find( '.navigator .control.next' );
			trigger_link.trigger( 'click' );
			e.stopPropagation( );
		});
		
		/* swipe scroller for the tech view slider */
		$('.mobile.tech_view').hammer({ }).on( 'swiperight dragright', function( e ) 
		{ 
			var parent_el = $( this );
			var trigger_link = parent_el.find( '.navigator .control.prev' );
			trigger_link.trigger( 'click' );
			e.stopPropagation( );
		});

  });
  
  $('#tech-view-tab').on('click', function(e) {

      var curr_params = window.location.search;
      var curr_url = window.location.href.split('?');
      if(curr_url[0].slice(-1,curr_url[0].length) == '/') {
        curr_url[0] = curr_url[0].slice(0,-1);
      }
      var dest_url = curr_url[0] + '/technical' + curr_params;

      //console.log(dest_url);
      window.location.href = dest_url;

      e.preventDefault();
    });
	
	$('#normal-view').on('click', function(e) {
		e.preventDefault();
		var curr_params = window.location.search;
		var curr_url = window.location.href.split('?');
		curr_params = remove_url_param(curr_params,"checked");
		if(curr_url[0].slice(-1,curr_url[0].length) == '/') {
			curr_url[0] = curr_url[0].slice(0,-1);
		}
		var dest_url = '/search/products' + curr_params;      
		window.location.href = dest_url;
	});

	/* return GET URL variables as an associative array */
	function get_url_vars( )
	{
		var vars = [ ], hash;
		var hashes = window.location.href.slice(window.location.href.indexOf( '?' ) + 1 ).split( '&' );
		for( var i = 0;i < hashes.length;i++ )
		{
			hash = hashes[ i ].split( '=' );
			vars.push( hash[ 0 ] );
			vars[ hash[ 0 ] ] = hash[ 1 ];
		}
		return vars;
	}

	/* remove url parameter from query string */
	function remove_url_param( query, param ) 
	{
		var output = query;
		if ( query.length >= 2 )
		{
			var prefix  = param + '=';
			var pars  = query.split(/[&;]/g);
			for ( var i = pars.length;i-->0; )
			{
				if ( pars[ i ].lastIndexOf( prefix, 0 ) !== -1 )
				{
					pars.splice( i, 1 );
				}
			}
			if ( pars.length > 0 )
			{
				var output = pars.join( '&' );
			}
		}
		return output
	}

	/* update url parameter in query string */
	function update_param( url, key, value )
	{
		var re = new RegExp( "([?|&])" + key + "=.*?(&|$)", "i" );
		var separator   = url.indexOf( '?' ) !== -1 ? "&" : "?";
		if ( url.match( re ) )
		{
			var output = url.replace( re, '$1' + key + "=" + value + '$2' );
		}
		else
		{
			var output = url + separator + key + "=" + value;
		}
		return output;
	}

	/* check if param exists */
	function check_param( param )
	{
		var url = window.location.href;
		if( url.indexOf( '?' + param + '=' ) != -1 )
		{
			return true;
		}
		else if(url.indexOf('&' + param + '=') != -1)
		{
			return true;
		}
		return false
	}
})( jQuery );
