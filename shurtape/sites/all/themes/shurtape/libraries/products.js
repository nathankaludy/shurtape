/**
 * common JS for the products pages - category, products index, and product detail (but NOT search)
 */
/* define $ as jQuery just in case */
( function( $ )
{
	/* doc ready */
	$( function( )
	{
		/**	
		 * go through each mobile toggle control to check for open boxes 
		 */
		var open_toggle_controls = $( '#mobile_toggle .mobile_toggle_box.open' ).find( '.mobile_toggle_control' );
		if ( open_toggle_controls.length > 0 )
		{
			$.each( open_toggle_controls, function( i ) 
			{
				var toggle_parent = $( this ).closest( '.mobile_toggle_box' );
				var this_toggle_target = toggle_parent.find( '.mobile_toggle_target' );
				
				/* open it if already open */
				if ( toggle_parent.hasClass( 'open' ) ) 
				{
					/* show toggle target */
					this_toggle_target.show( ); 
					
					/* switch the toggle_image */
					var toggle_icon = $( this ).find( '.icon img' ).attr( 'src' );
					var new_icon	= toggle_icon.replace( 'btn_mobile_toggle.png', 'btn_mobile_toggle_open.png' );
					$( this ).find( '.icon img' ).attr( 'src', new_icon );
				} 
				else 
				{
					/* hide toggle target */
					this_toggle_target.hide( );
				}
			});
		}
		
		/**	
		 * grab number of thumbs and display controls if more than one
		 */
		mobile_thumbnail_slider( );
		function mobile_thumbnail_slider( )
		{
			/* set vars */
			var thumb_slider 	= $( '#thumb_slider' );
			var thumb_slides	= thumb_slider.find( '.slides .slide' );
			var thumb_controls	= thumb_slider.find( '.controls .control' );
			var slide_count		= thumb_slides.length;
			if ( slide_count < 2 )
			{
				$( '#thumb_slider .arrow_control' ).hide( );
			}
			
			/* mobile click events */
			$( '#thumb_slider' ).on( 'click', '.arrow_control', function( e ) 
			{
				var curr_index = $( '#thumb_slider .controls .control.current' ).index( );
				if ( $( this ).hasClass( 'prev' ) ) 
				{
					var target_index = curr_index == 0 ? parseInt( slide_count - 1 ) : parseInt( curr_index - 1 );
				}
				else
				{
					var target_index = curr_index == parseInt( slide_count - 1 ) ? 0 : parseInt( curr_index + 1 );
				}
				var target_el		= thumb_slider.find( '.slide:eq(' + target_index + ')' );
				var target_control	= thumb_slider.find( '.control:eq(' + target_index + ')' );
				thumb_controls.removeClass( 'current' ).css({ 'opacity':'0.5' });
				target_control.addClass( 'current' ).css({ 'opacity':1 });
				thumb_slides.hide( );
				target_el.show( );
				e.preventDefault( );
			});
			
		}
		
		/**	
		 * download link for product index page
		 */
		$( '.view-product-index .download_link' ).click(function(e)
		{
			var parent_el = $( this ).parents( '.views-row' );
			var pdf_link = parent_el.find( '.grid_item' ).attr( 'href' );
			window.open( pdf_link );
			e.preventDefault( );
		});
		
		
	});
})( jQuery );