/**
 * JS for the filter bar - used on the sidebar for the search + filter page
 */
/* define $ as jQuery just in case */
(function($) 
{
	/* doc ready */
	$(function() 
	{
		/*
		 * getting rid of animations / click toggles for left sidebar in favor
		 * of simple CSS
		 */

		/* init the toggle box function */
		toggle_box();

		/* toggle / accordian events */
		function toggle_box() 
		{
			/* toggle control click action */
			$('.toggle_box').on( 
				'click', 
				'.toggle_control',
				function(e) {
					var toggle_boxes = $('.toggle_box#browse_markets, .toggle_box#browse_types');
					var toggle_targets = $('.toggle_box#browse_markets .toggle_target, .toggle_box#browse_types .toggle_target');
					var parent_el = $(this).closest('.toggle_box');
					var target = parent_el.find('.toggle_target');

					/* open it if it wasn't already open */
					if (!parent_el.hasClass('open')) {
						if (!parent_el.hasClass('filter_bar')) {
							/* hide all toggle boxes */
							toggle_targets.hide();
							toggle_boxes.removeClass('open');
						}
						parent_el.addClass('open');
						target.show();
					} 
					else 
					{
						/* hide all toggle boxes */
						// toggle_targets.hide();
						// toggle_boxes.removeClass('open');
						parent_el.removeClass('open');
						target.hide();
					}
					e.preventDefault();
				}
			);
		}
		

		$('body.product a.facetapi-checkbox-processed').each(function() 
		{
			this.href = "/search/products?" + this.href.split('?')[1];
		});

		$('body.product a.facetapi-inactive').each(function() 
		{
			this.href = "/search/products?" + this.href.split('?')[1];
		});

		$('input.facetapi-checkbox').click(function() 
		{
			window.location = this.nextSibling.href;
		});

	});

})(jQuery);
