( function( $ ){

    /* doc ready */
    $( function( ){
        $(".webform_promotion_container input.required.error").each(function(index,value){
            var fieldName = $(value).attr("name");
            var fieldError = "";
            switch(fieldName)
            {
                case "submitted[contact_name]":
                     
                    fieldError = ".prom_name_error_msg";
                    break;
                case "submitted[email]":
                    fieldError = ".prom_email_error_msg";
                    break;
               
                case "submitted[city]":
                    fieldError = ".prom_city_error_msg";
                    break;
                case "submitted[shipping_address]":
                    fieldError = ".prom_shipping_error_msg";
                    break;
                case "submitted[zip]":
                    fieldError = ".prom_zip_error_msg";
                    break;
                 
                default:
                    
                    break;
            }
            $(fieldError).parents(".row").first().removeClass("hidden");
            $(fieldError).removeClass("invisible");
          
        });
    });
	
})( jQuery );