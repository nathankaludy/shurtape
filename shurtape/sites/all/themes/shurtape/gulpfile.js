var gulp          = require('gulp');
var sass          = require('gulp-sass');
var autoprefixer  = require('gulp-autoprefixer');
var sourcemaps    = require('gulp-sourcemaps');
var include       = require('gulp-include');
//var livereload = require('gulp-livereload');

gulp.task('sass', function () {
  gulp
    .src('scss/styles.scss')
    .pipe(include())
      .on('error', console.log)
    .pipe(sourcemaps.init())
    .pipe(sass())
    .pipe(autoprefixer({
      browsers: ['last 2 versions'],
      cascade: false
    }))
    .pipe(sourcemaps.write('./maps'))
    .pipe(gulp.dest('./css/'))
});

gulp.task('autoprefixer', function () {
    gulp.src('css/styles.css')
        .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
        .pipe(gulp.dest('./css/'))
//        .pipe(livereload());
});



gulp.task('default', function () {
//        livereload.listen();
        gulp.start('sass');
        gulp.watch('scss/**', function() {
            setTimeout(function () {
                    gulp.start('sass');
            }, 200);
        });
//        gulp.watch('css/**', function() {
//            setTimeout(function () {
//                    gulp.start('autoprefixer');
//            }, 200);
//        });
});