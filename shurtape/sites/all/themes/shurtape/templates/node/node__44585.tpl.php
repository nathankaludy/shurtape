<?php
// Industry links node
?>
<h1 class="headline">
  <?php if ($title): ?>
    <?php echo t($title); ?>
  <?php endif; ?>
</h1>

<?php if ($page['content_top']): ?>
  <div id="content_top">
    <?php print render($page['content_top']); ?>
  </div>
<?php endif; ?>

<?php if (isset($page['help'])): ?>
  <?php print render($page['help']); ?>
<?php endif; ?>

<?php if (isset($action_links)): ?>
  <ul class="action-links">
    <?php print render($action_links); ?>
  </ul>
<?php endif; ?>

<div class="page-resources-industry-links">
  <?php print render( $content ); ?>
</div>