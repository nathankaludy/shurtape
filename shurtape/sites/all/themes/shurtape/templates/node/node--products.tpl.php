<!--
<div class="mobile_breadcrumbs mobile">
	<span class="you_are_here">You Are Here:</span>
	<span class="breadcrumbs">
		<a href="<?php print base_path(); ?>">Home</a> / 
		<a href="<?php print base_path(); ?>products">Products</a> / 
		<?php echo $title; ?>
	</span>
</div>
-->

<h1 class="headline">
	<?php if ($title): ?>
		<?php echo t($title); ?>
	<?php endif; ?>
</h1>

<?php if ($page['content_top']): ?>
	<div id="content_top">
		<?php print render($page['content_top']); ?>
	</div>
<?php endif; ?>

<?php if (isset($page['help'])): ?>
	<?php print render($page['help']); ?>
<?php endif; ?>

<?php if (isset($action_links)): ?>
	<ul class="action-links">
		<?php print render($action_links); ?>
	</ul>
<?php endif; ?>

<div class="blue_bar desktop"></div>
<div class="padded_body">
	<?php print render( $content ); ?>
</div>
