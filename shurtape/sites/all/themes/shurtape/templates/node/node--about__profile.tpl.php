<!--
<div class="mobile_breadcrumbs mobile">
	<span class="you_are_here">You Are Here:</span>
	<span class="breadcrumbs">
		<a href="<?php print base_path(); ?>">Home</a> / 
		<a href="<?php print base_path(); ?>about/profile">About</a> / 
		<?php echo $title; ?>
	</span>
</div>
-->

<h1 class="headline">
	<?php if ($title): ?>
		<?php echo t($title); ?>
	<?php endif; ?>
</h1>

<?php if ($page['content_top']): ?>
	<div id="content_top">
		<?php print render($page['content_top']); ?>
	</div>
<?php endif; ?>

<?php if (isset($page['help'])): ?>
	<?php print render($page['help']); ?>
<?php endif; ?>

<?php if (isset($action_links)): ?>
	<ul class="action-links">
		<?php print render($action_links); ?>
	</ul>
<?php endif; ?>

<div class="blue_bar desktop">
</div>
<!--
<div class="marquee_image text-center">
	<div class="desktop">
		<img src="<?php // print base_path() . drupal_get_path('theme', 'shurtape'); ?>/images/photo_building.png" alt="Shurtape office building" />
	</div>
	<div class="mobile">
		<img src="<?php // print base_path() . drupal_get_path('theme', 'shurtape'); ?>/images/photo_building_mobile.jpg" alt="Shurtape office building" />
	</div>
</div> -->
		
<div class="padded_body">

	<?php print render( $content ); ?>

</div>
<!--
<div class="clearfix">
	<div class="video desktop pull-right">
		<iframe width="473" height="266" src="//www.youtube.com/embed/kk9A_o3O718" frameborder="0" allowfullscreen></iframe>-->
		<!--<img src="images/fpo_video_craftsmanship.jpg" alt="Video FPO" />-->
		<!--
		<object width="473" height="266">
		<param name="movie" value="https://youtube.googleapis.com/v/kk9A_o3O718"></param>
		<param name="allowFullScreen" value="true"></param>
		<param name="allowscriptaccess" value="always"></param>
		<embed src="https://youtube.googleapis.com/v/kk9A_o3O718?rel=0"
		type="application/x-shockwave-flash"
		allowscriptaccess="always"
		allowfullscreen="true"
		width="473" height="266">
		</embed>
		</object>
		-->
<!--	</div> 
	<div class="video mobile">
		<iframe width="560" height="315" src="//www.youtube.com/embed/kk9A_o3O718?showinfo=0" frameborder="0" allowfullscreen></iframe>
	</div> 

	<div class="pointer_block pull-left">
		<div class="arrow_box_wrap clearfix">
			<div class="blue_arrow_box">
				Watch our video to learn more about the essence of the Shurtape brand:
				<span class="pointer"></span>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
jQuery(function($)
{
	/* find all YouTube videos */
	var videos 		= $( '.mobile iframe[src^="//www.youtube.com"]' );
	var fluid_el 	= $( 'body' );

	/* Figure out and save aspect ratio for each video */
	videos.each( function( ) 
	{
		$( this ).data( 'aspectRatio', this.height / this.width ).removeAttr( 'height' ).removeAttr( 'width' );
	});

	/* resize videos when window is resized according to their own aspect ratio */
	$( window ).resize( function( ) 
	{
		var new_width = fluid_el.width( );
		videos.each( function( ) 
		{
			var el = $( this );
			el.width( new_width ).height( new_width * el.data( 'aspectRatio' ) );
		});
		
	/* resize all videos on page load */
	}).resize( );
});
</script> -->
