<?php
/*
$title: the (sanitized) title of the node.
$content: An array of node items. Use render($content) to print them all, or print a subset such as render($content['field_example']). Use hide($content['field_example']) to temporarily suppress the printing of a given element.
$user_picture: The node author's picture from user-picture.tpl.php.
$date: Formatted creation date. Preprocess functions can reformat it by calling format_date() with the desired parameters on the $created variable.
$name: Themed username of node author output from theme_username().
$node_url: Direct URL of the current node.
$display_submitted: Whether submission information should be displayed.
$submitted: Submission information created from $name and $date during template_preprocess_node().
$classes: String of classes that can be used to style contextually through CSS. It can be manipulated through the variable $classes_array from preprocess functions. The default values can be one or more of the following:

node: The current template type; for example, "theming hook".
node-[type]: The current node type. For example, if the node is a "Blog entry" it would result in "node-blog". Note that the machine name will often be in a short form of the human readable label.
node-teaser: Nodes in teaser form.
node-preview: Nodes in preview mode.

The following are controlled through the node publishing options.
node-promoted: Nodes promoted to the front page.
node-sticky: Nodes ordered above other non-sticky nodes in teaser listings.
node-unpublished: Unpublished nodes visible only to administrators.
$title_prefix (array): An array containing additional output populated by modules, intended to be displayed in front of the main title tag that appears in the template.
$title_suffix (array): An array containing additional output populated by modules, intended to be displayed after the main title tag that appears in the template.

Other variables:

$node: Full node object. Contains data that may not be safe.
$type: Node type; for example, story, page, blog, etc.
$comment_count: Number of comments attached to the node.
$uid: User ID of the node author.
$created: Time the node was published formatted in Unix timestamp.
$classes_array: Array of html class attribute values. It is flattened into a string within the variable $classes.
$zebra: Outputs either "even" or "odd". Useful for zebra striping in teaser listings.
$id: Position of the node. Increments each time it's output.

Node status variables:

$view_mode: View mode; for example, "full", "teaser".
$teaser: Flag for the teaser state (shortcut for $view_mode == 'teaser').
$page: Flag for the full page state.
$promote: Flag for front page promotion state.
$sticky: Flags for sticky post setting.
$status: Flag for published status.
$comment: State of comment settings for the node.
$readmore: Flags true if the teaser content of the node cannot hold the main body content.
$is_front: Flags true when presented in the front page.
$logged_in: Flags true when the current user is a logged-in member.
$is_admin: Flags true when the current user is an administrator.
*/
?>

<?php
$path_to_zeroclipboard = libraries_get_path("zeroclipboard");
drupal_add_js("$path_to_zeroclipboard/ZeroClipboard.js");
?>
<script type="text/javascript">
	$(document).ready(function() 
	{
		ZeroClipboard.config({ moviePath: "<?php echo $path_to_zeroclipboard; ?>/ZeroClipboard.swf" });
		var client = new ZeroClipboard($("div.asset-embed p"));
		client.on( "load", function(client) 
		{ 
			client.on( "complete", function(client, args) 
			{
				/* console.log("Copied text to clipboard: " + args.text ); */
				alert("Successfully copied embed code text to your clipboard");
			});
			client.on( 'dataRequested', function (client, args) 
			{
				/* client.setText("SEE"); */
				client.setText($("div.asset-embed p").text().trim());
			});
		});
	});
</script>
<?php

hide($content["title_field"]); 
hide($content["body"]);
hide($content["field_file_asset"]);
hide($content["field_display_image"]);
hide($content["field_asset_thumbnail"]);
hide($content["social_share"]);

$asset_display_image_array = field_get_items('node', $node, 'field_asset_thumbnail');
$asset_title_array = field_get_items('node', $node, 'title_field');
$asset_file_array = field_get_items('node', $node, 'field_file_asset');
$file = file_load($asset_file_array[0]['fid']);
$uri = $file->uri;
$external_url = file_create_url($uri);
$img_file = file_load($asset_display_image_array[0]['fid']);
$img_uri = $img_file->uri;
$img_external_url = file_create_url($img_uri);
$external_url_link = '<a href="' . $external_url . '" target="_blank">Download</a>';
$img_with_link = '<a href="' . $external_url . '" target="_blank"><img src="' . $img_external_url . '" alt="' . $asset_title_array[0]["value"] . '" border="0"/></a>';
$embed_text = htmlspecialchars('<a href="' . $external_url . '" target="_blank"><img src="' . $img_external_url . '" alt="' . $asset_title_array[0]["value"] . '" border="0"/><p>' . $asset_title_array[0]["value"] . '</p></a>');
$email_subject_array = field_get_items('node', $node, 'field_share_by_mail_subject');
$email_body_array = field_get_items('node', $node, 'field_share_by_mail_body');
$email_subject = rawurlencode($email_subject_array[0]["value"]);
$email_body = rawurlencode($email_body_array[0]["value"]);
?>

<div class="blue_bar promotion_product_header">&nbsp;</div>

<div class="content_wrapper">
	<div id="media_box">
		<?php print $img_with_link; ?>
	</div>
	<div id="details_box">
		<div class="asset-title">
			<?php print render($content["title_field"]); ?>
			<div class="download_link">
				<?php print $external_url_link; ?>
			</div>
		</div>
		<div class="social-share-container">
			<span>Share This Media</span>
			<div class="recommend">
				<a class="social-share-mail" href="<?php print "mailto:?subject=$email_subject&body=$email_body" ?>">Mail</a>
			</div> 
			<?php print render($content["social_share"]); ?> 
		</div>
		<?php print render($content["body"]); ?>
		<div class="asset-embed">
			<span><b>Embed</b></span> <i>(click below to copy to your clipboard)</i>
			<p data-clipboard-text="COPIAAA!">
				<?php echo $embed_text; ?>
			</p>
		</div>
	</div>
</div>