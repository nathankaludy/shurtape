<?php

/**
 * @file
 * This template is used to print a single field in a view.
 *
 * It is not actually used in default Views, as this is registered as a theme
 * function which has better performance. For single overrides, the template is
 * perfectly okay.
 *
 * Variables available:
 * - $view: The view object
 * - $field: The field handler object that can process the input
 * - $row: The raw SQL result that can be used
 * - $output: The processed output that will normally be used.
 *
 * When fetching output from the $row, this construct should be used:
 * $data = $row->{$field->field_alias}
 *
 * The above will guarantee that you'll always get the correct data,
 * regardless of any changes in the aliasing that might happen if
 * the view is modified.
 */
$data = $row->_field_data;
//echo $data["nid"]["entity"]->type;
//if the title belongs to a PDF asset
if ($data["nid"]["entity"]->type == "media_library_asset") {
    $asset_file_array = field_get_items('node', $data["nid"]["entity"], 'field_file_asset');
    $file = file_load($asset_file_array[0]['fid']);
    $external_url_link = "";
    $result = file_validate_extensions($file, "pdf");
    if (count($result) == 0) {//always is an array, count == 0 means no errors, so its the extension we want
        $uri = $file->uri;
        $external_url = file_create_url($uri);
        $external_url_link = '<span class="link"><a class="" href="' . $external_url . '" target="_blank">Download PDF</a></span>';
    }
} elseif ($data["nid"]["entity"]->type == "media_asset_infographic") {
    $asset_file_array = field_get_items('node', $data["nid"]["entity"], 'field_infographic_full_image');
    $file = file_load($asset_file_array[0]['fid']);
    $external_url_link = "";
    $uri = $file->uri;
    $external_url = file_create_url($uri);
    $external_url_link = '<span class="link"><a class="" href="' . $external_url . '" target="_blank">Download</a></span>';
} elseif ($data["nid"]["entity"]->type == "media_library_logos") {
    $asset_file_array_eps = field_get_items('node', $data["nid"]["entity"], 'field_eps_file');
    $asset_file_array_jpg = field_get_items('node', $data["nid"]["entity"], 'field_jpg_image_file');
    $asset_file_array_png = field_get_items('node', $data["nid"]["entity"], 'field_png_image_file');

    if (!empty($asset_file_array_eps[0]['fid'])) {
        $file_eps = file_load($asset_file_array_eps[0]['fid']);
        $external_url_eps = file_create_url($file_eps->uri);
    }
    if (!empty($asset_file_array_jpg[0]['fid'])) {
        $file_jpg = file_load($asset_file_array_jpg[0]['fid']);
        $external_url_jpg = file_create_url($file_jpg->uri);
    }
    if (!empty($asset_file_array_png[0]['fid'])) {
        $file_png = file_load($asset_file_array_png[0]['fid']);
        $external_url_png = file_create_url($file_png->uri);
    }

    $external_url_link = '<span class="link">';
    $count = 0;
    if (!empty($asset_file_array_eps[0]['fid'])) {
        if ($count > 0) {
            $external_url_link .= '<span class="sub_left">&nbsp;</span>';
        } else {
            $external_url_link .='<span class="sub_left">Download:</span>';
        }
        $external_url_link .= '<a class="sub_right" href="' . $external_url_eps . '" target="_blank">EPS file</a>';
        $count++;
    }
    if (!empty($asset_file_array_jpg[0]['fid'])) {
        if ($count > 0) {
            $external_url_link .= '<span class="sub_left">&nbsp;</span>';
        } else {
            $external_url_link .='<span class="sub_left">Download:</span>';
        }
        $external_url_link .= '<a class="sub_right" href="' . $external_url_jpg . '" target="_blank">JPG file</a>';
        $count++;
    }
    if (!empty($asset_file_array_png[0]['fid'])) {
        if ($count > 0) {
            $external_url_link .= '<span class="sub_left">&nbsp;</span>';
        } else {
            $external_url_link .='<span class="sub_left">Download:</span>';
        }
        $external_url_link .= '<a class="sub_right" href="' . $external_url_png . '" target="_blank">PNG file</a>';
        $count++;
    }

    $external_url_link .= '</span>';
}
?>
<?php print $output . $external_url_link; ?>
