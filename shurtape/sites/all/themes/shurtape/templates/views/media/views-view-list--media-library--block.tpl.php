<?php

/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * - $title : The title of this group of rows.  May be empty.
 * - $options['type'] will either be ul or ol.
 * @ingroup views_templates
 */
?>
<div class="group">
	<h3><a class="control" href="#"><?php echo !empty( $title ) ? $title : ''; ?></a></h3>
	<div class="box">
		<ul>
			<?php foreach ( $rows as $id => $row ): ?>
				<li>
					<?php print $row; ?>
				</li>
			<?php endforeach; ?>
		</ul>
	</div>
</div>