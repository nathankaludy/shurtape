<?php
/**
 * @file
 * Main view template.
 *
 * Variables available:
 * - $classes_array: An array of classes determined in
 *   template_preprocess_views_view(). Default classes are:
 *     .view
 *     .view-[css_name]
 *     .view-id-[view_name]
 *     .view-display-id-[display_name]
 *     .view-dom-id-[dom_id]
 * - $classes: A string version of $classes_array for use in the class attribute
 * - $css_name: A css-safe version of the view name.
 * - $css_class: The user-specified classes names, if any
 * - $header: The view header
 * - $footer: The view footer
 * - $rows: The results of the view query, if any
 * - $empty: The empty text to display if the view is empty
 * - $pager: The pager next/prev links to display, if any
 * - $exposed: Exposed widget form/info to display
 * - $feed_icon: Feed icon to display, if any
 * - $more: A link to view more, if any
 *
 * @ingroup views_templates
 */
?>

<?php
if ($view->current_display == "block") {//media library normal display with accordion
    ?>

    <div class="<?php print $classes; ?>">

        <?php if ($rows): ?>
            <div id="accordian" class="view-content">
                <?php print $rows; ?>
            </div>
        <?php elseif ($empty): ?>
            <div class="view-empty">
                <?php print $empty; ?>
            </div>
        <?php endif; ?>

        <?php if ($pager): ?>
            <?php print $pager; ?>
        <?php endif; ?>

        <?php if ($feed_icon): ?>
            <div class="feed-icon">
                <?php print $feed_icon; ?>
            </div>
        <?php endif; ?>

    </div>

    <script type="text/javascript">
        /* define $ as jQuery just in case */
        ( function( $ ){

            /* doc ready */
            $( function( ){
    		
                /* init the accordian */
                $( '#accordian' ).accordian( );
    			
            });
        })( jQuery );
    </script>

    <?php
}elseif ($view->current_display == "block_1") {//brand assets display
    if (variable_get("product_catalog_pdf")) {
        $pdf = file_load(variable_get("product_catalog_pdf"));
        $pdf_url = file_create_url($pdf->uri);
    }
    if (variable_get("product_catalog_thumbnail")) {
        $catalog_thumb_image = file_load(variable_get("product_catalog_thumbnail"));
        $catalog_thumb_image_url = file_create_url($catalog_thumb_image->uri);
    }
    ?>
    <h1 class="headline">Media Library</h1>
    <div class="blue_bar">WELCOME</div>
    <div class="media_page">

        <div id="marquee" class="clearfix">
            <div class="marquee_content pull-left">
                <span class="highlight">Welcome to the Shurtape Media Library</span>, where you can view and download <a href="#product-literature" style="color:white;">product 
                    literature</a>, logos, product catalog, and more.

                <ul class="link_list">
                    <li class="additional">Additional Support</li>
                    <li><a href="<?php echo base_path(); ?>resources/news_events">View Articles and Press Releases</a></li>
                    <li><a href="<?php echo base_path(); ?>products/product-index">View Technical Data Sheets</a></li>
                    <li><a href="<?php echo base_path(); ?>resources/news_events">View Events</a></li>
                </ul>
            </div>
            <div class="marquee_image pull-right">
                <img src="<?php print base_path() . drupal_get_path('theme', 'shurtape'); ?>/images/fpo_media_marquee.png" alt="" />
            </div>
        </div>

        <div class="blue_bar for_access">
            For access to artwork and photography, email us at <a href="mailto:mediasupport@shurtape.com">mediasupport@shurtape.com</a>.
        </div>

        <div class="gray_bar clearfix">
            <div class="indicates pull-left">
                (*) Indicates printed version also available
            </div>
        </div>

        <div class="<?php print $classes; ?>">
    <?php if ($rows): ?>
                <div id="brand_assets" class="view-content">
                    <h3>Brand Assets</h3>    
        <?php print $rows; ?>
                </div>
                <?php elseif ($empty): ?>
                <div class="view-empty">
                <?php print $empty; ?>
                </div>
                <?php endif; ?>
        </div>


    <?php //print render( $content );  ?>
    </div>

    <div class="media_page">
        <div class="blue_bar subhead"><a name="product-literature" style="line-height:0px;height:0px;"></a><h3>PRODUCT LITERATURE</h3></div>
    </div>
<?php } ?>
