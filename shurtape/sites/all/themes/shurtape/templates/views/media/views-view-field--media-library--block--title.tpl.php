<?php

/**
 * @file
 * This template is used to print a single field in a view.
 *
 * It is not actually used in default Views, as this is registered as a theme
 * function which has better performance. For single overrides, the template is
 * perfectly okay.
 *
 * Variables available:
 * - $view: The view object
 * - $field: The field handler object that can process the input
 * - $row: The raw SQL result that can be used
 * - $output: The processed output that will normally be used.
 *
 * When fetching output from the $row, this construct should be used:
 * $data = $row->{$field->field_alias}
 *
 * The above will guarantee that you'll always get the correct data,
 * regardless of any changes in the aliasing that might happen if
 * the view is modified.
 */

/* set vars */
$data = $row->_field_data;

/* if the title belongs to a PDF asset */
if ($data["nid"]["entity"]->type == "media_library_asset") 
{
	$asset_file_array = field_get_items('node', $data["nid"]["entity"], 'field_file_asset');
	$file = file_load($asset_file_array[0]['fid']);
	$external_url_link="";
	$result = file_validate_extensions($file, "pdf");
	/* always is an array, count == 0 means no errors, so its the extension we want */
	if (count($result) == 0) 
	{
		$uri = $file->uri;
		$external_url = file_create_url($uri);
		$external_url_link = '<span class="library-link-separator">|</span><a class="library-pdf-link" href="' . $external_url . '" target="_blank">PDF</a>';
	}
}
?>
<?php if(isset($external_url_link)): ?>
	<?php print $output . $external_url_link; ?>
<?php else: ?>
	<?php print $output; ?>
<?php endif; ?>
</div><!-- close the title container -->
<?php /* unfortunately had to do it this way for mobile markup and Drupal = garbage clusterf#@& CMS */ ?>

