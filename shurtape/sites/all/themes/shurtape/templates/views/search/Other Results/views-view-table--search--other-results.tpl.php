<?php
/**
 * @file
 * Template to display a view as a table.
 *
 * - $title : The title of this group of rows.  May be empty.
 * - $header: An array of header labels keyed by field id.
 * - $caption: The caption for this table. May be empty.
 * - $header_classes: An array of header classes keyed by field id.
 * - $fields: An array of CSS IDs to use for each field id.
 * - $classes: A class or classes to apply to the table, based on settings.
 * - $row_classes: An array of classes to apply to each row, indexed by row
 *   number. This matches the index in $rows.
 * - $rows: An array of row items. Each row is an array of content.
 *   $rows are keyed by row number, fields within rows are keyed by field ID.
 * - $field_classes: An array of classes to apply to each field, indexed by
 *   field id, then row number. This matches the index in $rows.
 * @ingroup views_templates
 */
?>
<?php if ($title != "Product") : ?>

    <table <?php
    if ($classes) {
        print 'class="' . $classes . ' other-results" ';
    }
    ?><?php print $attributes; ?>>
            <?php if (!empty($title) || !empty($caption)) : ?>
            <caption><?php print $caption . $title; ?></caption>
        <?php endif; ?>
        <?php if (!empty($header)) : ?>
            <thead>
                <tr>
                    <th class="type_heading">Type</th>
                    <th class="name_heading">Name</th>
                </tr>
            </thead>
        <?php endif; ?>
        <tbody>
            <?php
            $row_output = "";
            $cs = $rows;

            foreach ($rows as $key => $value):

                if (isset($current_row)) {
                    unset($current_row);
                }
                print sprintf("<tr><td class='type'><span class='type %s'>%s</span></td>", $value['type'], $value['type']);
                print sprintf("<td class='name'>%s</a></td></tr>", $value['title']);

            endforeach;

            print $row_output;
            ?>
        </tbody>
    </table>
<?php else: ?>

    <table id="product_table"<?php
    if ($classes) {
        print 'class="' . $classes . ' other-results" ';
    }
    ?><?php print $attributes; ?>>
               <?php if (!empty($title) || !empty($caption)) : ?>
            <caption><?php print $caption . $title; ?></caption>
        <?php endif; ?>
        <?php if (!empty($header)) : ?>
            <thead>
                <tr>        
                    <th class="name_heading">Name</th>
                    <th class="assets_heading">Assets</th>       
                </tr>
            </thead>
        <?php endif; ?>
        <tbody>
            <?php
            $row_output = "";
            $cs = $rows;

            foreach ($rows as $key => $value):
                $has_assets = false;
                if (isset($current_row)) {
                    unset($current_row);
                }
                print "<tr>";
                print sprintf("<td class='name'>%s</td>", $value['title']);

                print "<td>";
                if (isset($value['field_product_videos']) && !empty($value['field_product_videos'])) {
                    $has_assets = true;
                    $current_row = explode(',', $value['field_product_videos']);
                    for ($b = 0; $b < count($current_row); $b++):
                        $vid = str_replace(" ", "", $current_row[$b]);
                        $url = 'http://gdata.youtube.com/feeds/api/videos/' . $vid;
                        $doc = new DOMDocument;
                        $doc->load($url);
                        $vtitle = $doc->getElementsByTagName('title')->item(0)->nodeValue;
                        $img = '<img src="/' . drupal_get_path('theme', 'shurtape') . '/images/icon_video.png' . '">';
                        $link = sprintf('<a title="%s" alt="%s" href="#" class="video_thumb open_video_modal" rel="%s">%s</a>', $vtitle, $vtitle, $vid, $img);
                        print sprintf('<span class="other_video_asset">%s</span>', $link);
                    endfor;
                }
                if (isset($value['field_product_assets']) && !empty($value['field_product_assets'])) {
                    $has_assets = true;
                    if (isset($current_row)) {
                        unset($current_row);
                    }
                    $current_row = explode(',', $value['field_product_assets']);
                    for ($b = 0; $b < count($current_row); $b++):
                        $loaded_row = node_load($current_row[$b]);
                        $url = file_create_url($loaded_row->field_file_asset['und'][0]['uri']);
                        $asset = explode('.', $loaded_row->title);
                        $img = '<img src="/' . drupal_get_path('theme', 'shurtape') . '/images/icon_pdf.png' . '">';
                        $link = sprintf('<a alt="%s" title="%s" href="%s" target="_blank">%s</a>', $loaded_row->title, $loaded_row->title, $url, $img);
                        print sprintf("<span class='other_pdf_asset'>%s</span>", $link);
                    endfor;
                }
                if ($has_assets == false) {
                    print "No assets for this product";
                }
                print "</td>";
                print "</tr>";
            endforeach;

            print $row_output;
            ?>
        </tbody>
    </table>
<?php endif; ?>