<?php

/**
 * @file
 * Defines a naked avis layout.
 */

$plugin = array(
  'title' => 'Media Asset Infographic',
  'category' => 'Avis',
  'css' => 'page_media_asset_infographic.css',
  'icon' => 'page_media_asset_infographic.png',
  'theme' => 'page-media-asset-infographic',
  'regions' => array(
	  'title' => 'Title',
	  'img_with_link' => 'Image With Link',
	  'action_links' => 'Action Links',
      'breadcrumb_section' => 'Breadcrumb Section',
      'content' => 'Content',
      'bottom' => 'Bottom',
  ),
);