<?php

/**
 * @file
 * Defines a naked one-column layout.
 */

// Plugin definition.
$plugin = array(
  'title' => t('Naked'),
  'category' => t('Avis'),
  'icon' => 'naked.png',
  'theme' => 'naked',
  'regions' => array(
    'content' => t('Content'),
  ),
);
