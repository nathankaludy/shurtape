<?php
global $theme;
$theme_main = 'shurtape';

global $language;

$applications_has_translation = false;
$nid = str_replace("node/","",$_GET['q']);
if($node = node_load($nid)) {
  if(isset($node->field_product_applications[$language->language])) {
    $applications_has_translation = true;
  }
}

$lang = ($language->language != "en") ? $language->language.'/' : '';

?>

  <div id="detail_content" class="row">
    <div class="detail_media col-md-6">
      <div id="thumb_slider" class="thumb_slider swipe_scroll">
        <a href="#" class="arrow_control mobile prev"></a>
        <?php if (!empty($content['product_images_gallery'])): ?>
          <?php print render($content['product_images_gallery']); ?>
        <?php else: ?>
          <div class="slides">
            <div class="slide"><img src="//twshurtape.s3.amazonaws.com/default_images/no-image-product-detal.png" alt=""></div>
          </div>
        <?php endif; ?>
        <a href="#" class="arrow_control mobile next"></a>
      </div>


    </div>

    <div class="detail_content col-md-6">
      <?php if (!empty($content['title'])): ?>
        <div class="detail_title">
          <?php print $content['title']; ?>
        </div>
      <?php endif; ?>

      <?php if (!empty($content['subtitle'])): ?>
        <div class="detail_subtitle">
          <?php print $content['subtitle']; ?>
        </div>
      <?php endif; ?>

      <div class="detail_desc">
        <?php if (!empty($content['body'])): ?>
          <?php print $content['body']; ?>
        <?php endif; ?>
      </div>

      <?php if (!empty($content['green_point'])): ?>
        <?php print $content['green_point']; ?>
      <?php endif; ?>

      <div class="cta_box_wrap no-pc">
        <div class="cta_box">
            <?php if($_GET['q'] == "node/47092" || $_GET['q'] == "node/47133" || $_GET['q'] == "node/47132" || $_GET['q'] == "node/47152" || $_GET['q'] == "node/47153"):  ?>
              <?php if($_GET['q'] != "node/47152" && $_GET['q'] != "node/47153"): ?>
                <ul class="menu">
                    <li class="first leaf"><a href="/" class="btn btn-orange find_data_sheet">Data Sheets</a></li>
                    <li class="leaf"><a href="/" name="request_sample_popout" class="btn open_request_popout">Request Sample</a></li>
                </ul>
              <?php else: ?>
                <ul class="menu">
                  <li class="leaf"><a href="/" name="request_sample_popout" class="btn open_request_popout">Request Sample</a></li>
                  <li class="last leaf"><a href="/" name="question_popout" class="btn open_request_popout">Question</a></li>
                </ul>
              <?php endif; ?>
            <?php else: ?>
                <?php print render($content['product_detail_links']); ?>
            <?php endif; ?>
        </div>

          <div id="request_sample_popout" class="popout" >
              <a href="#" class="close_popout">CLOSE</a>
              <h3>Request a Sample</h3>
              <div class="popout_body">
                  To request a sample please call us at<br>
                  <?php if($_GET['q'] == "node/47092" || $_GET['q'] == "node/47133" || $_GET['q'] == "node/47132"):  ?>
                      <div class="desktop">
                          <span class="phone_number">1-800-321-0253</span>
                      </div>
                      <div class="mobile">
                          <a href="tel:888-442-8273" class="phone_number">1-800-321-0253</a>
                      </div>
                  <?php else: ?>
                      <div class="desktop">
                          <span class="phone_number">1-888-442-8273</span>
                      </div>
                      <div class="mobile">
                          <a href="tel:888-442-8273" class="phone_number">1-888-442-8273</a>
                      </div>
                  <?php endif; ?>
              </div>
          </div>

        <?php print render($content['product_popout_region']); ?>
      </div>
    </div>
  </div>
<?php //if (!empty($content['product_downloads_tab_content'])): ?>
  <div class="desktop" id="downloads">
    <div id="tab_section" class="tab_group clearfix">
      <?php if ($theme == $theme_main): ?>
        <?php print theme('product__tabs_top', $content); ?>
      <?php endif; ?>
      <div class="tab_wrapper">
        <?php if (!empty($content['related_products'])): ?>
          <div class="related_products">
            <?php if ($theme == $theme_main): ?>
              <h3><?php print t('Related products'); ?></h3>
            <?php endif; ?>
            <?php print render($content['related_products']); ?>
          </div>
        <?php endif; ?>

        <div class="tab_targets">
          <?php if (!empty($content['product_properties_tab_content'])): ?>
            <div id="physical_tab" class="tab_target product_data_table">
              <?php if ($theme == $theme_main): ?>
                <h3><?php print t('Physical Properties'); ?></h3>
                <!--<div class="blue_bar">
								<div class="col title_col">&nbsp;</div>
								<div class="col standard_col"><?php //print t('Standard'); ?></div>
								<div class="col metric_col"><?php //print t('Metric'); ?></div>
								<div class="col astm_col"><?php //print t('ASTM Test Method'); ?></div>
							</div>-->
              <?php endif; ?>
              <?php print render($content['product_properties_tab_content']); ?>
            </div>
          <?php endif; ?>
          <?php if (!empty($content['product_markets']) || !empty($content['product_applications'])): ?>
            <div id="market_tab" class="tab_target inline_grid">
              <?php if (!empty($content['product_markets'])): ?>
                <div class="grid_group clearfix">
                  <?php if ($theme == $theme_main): ?>
                    <div class="grid_label text-right">
                      <h4><?php print t('MARKETS'); ?></h4>
                    </div>
                  <?php endif; ?>
                  <div class="grid_content">
                    <?php print render($content['product_markets']); ?>
                  </div>
                </div>
              <?php endif; ?>
              <?php if (!empty($content['product_applications']) && $applications_has_translation): ?>
                <div class="grid_item clearfix">
                  <?php if ($theme == $theme_main): ?>
                    <div class="grid_label text-right">
                      <h4><?php print t('TYPICAL APPLICATIONS'); ?></h4>
                    </div>
                  <?php endif; ?>
                  <div class="grid_content ">
                    <?php print render($content['product_applications']); ?>
                  </div>
                </div>
              <?php endif; ?>
            </div>
          <?php endif; ?>
          <?php if (!empty($content['product_sizes_colors_tab_content'])): ?>
            <div id="size_color_tab" class="tab_target product_data_table">
              <?php if ($theme == $theme_main): ?>
                <h3><?php print t('Sizes and Colors Available'); ?></h3>
                <div class="blue_bar">
                  <div class="col dimensions_col"><?php print t('Dimensions'); ?></div>
                  <div class="col rolls_col"><?php print t('Rolls per case'); ?></div>
                  <div class="col weight_col"><?php print t('Weight lbs/case'); ?></div>
                  <div class="col cases_col"><?php print t('Cases per pallet'); ?></div>
                  <div class="col colors_col"><?php print t('Available colors'); ?></div>
                </div>
              <?php endif; ?>

              <?php print render($content['product_sizes_colors_tab_content']); ?>
            </div>
          <?php endif; ?>
          <?php if (!empty($content['product_downloads_tab_content']) && $lang != 'es/'): ?>
            <div id="downloads_tab" class="tab_target inline_grid">
              <div class="grid_group clearfix">
                <?php if ($theme == $theme_main): ?>
                  <div class="grid_label text-right">
                    <h4><?php print t('Downloads'); ?></h4>
                  </div>
                <?php endif; ?>
                <?php print render($content['product_downloads_tab_content']); ?>
              </div>
            </div>
          <?php endif; ?>
          <?php if (isset($content['product_videos'])): ?>
            <div id="videos_tab" class="tab_target">
              <?php print render($content['product_videos']); ?>
            </div>
          <?php endif; ?>
          <?php if (isset($content['product_testimonial'])): ?>
            <div id="testimonials_tab" class="tab_target">
              <?php print render($content['product_testimonial']); ?>
            </div>
          <?php endif; ?>
        </div>
      </div>
    </div>
  </div>
<?php //endif; ?>

<?php if ($theme == $theme_main): ?>
  <div id="mobile_toggle" class="mobile">
    <?php if (!empty($content['product_properties_tab_content'])): ?>
      <div class="mobile_toggle_box" id="physical_box">
        <h4>
          <a href="#" class="mobile_toggle_control">
					<span class="icon">
						<img src="<?php print base_path() . drupal_get_path('theme', 'shurtape'); ?>/images/btn_mobile_toggle.png" alt="" />
					</span>
            <span class="title"><?php print t('Physical Properties'); ?></span>
          </a>
        </h4>
        <div class="mobile_toggle_target">
          <?php print render($content['product_properties_tab_content']); ?>
          <?php if (!empty($content['related_products'])): ?>
            <div class="related_products">
              <b><?php print t('Related products'); ?></b>
              <?php print render($content['related_products']); ?>
            </div>
          <?php endif; ?>
        </div>
      </div>
    <?php endif; ?>
    <?php if (!empty($content['product_markets']) || !empty($content['product_applications'])): ?>
      <div class="mobile_toggle_box" id="market_box">
        <h4>
          <a href="#" class="mobile_toggle_control">
					<span class="icon">
						<img src="<?php print base_path() . drupal_get_path('theme', 'shurtape'); ?>/images/btn_mobile_toggle.png" alt="" />
					</span>
            <span class="title"><?php print t('Markets/Applications'); ?></span>
          </a>
        </h4>
        <div class="mobile_toggle_target">
          <?php if (!empty($content['product_markets'])): ?>
            <div class="grid_group clearfix">
              <div class="grid_label text-right">
                <h4><?php print t('MARKETS'); ?></h4>
              </div>
              <div class="grid_content">
                <?php print render($content['product_markets']); ?>
              </div>
            </div>
          <?php endif; ?>
          <?php if (!empty($content['product_applications']) && $applications_has_translation): ?>
            <div class="grid_item clearfix">
              <div class="grid_label text-right">
                <h4><?php print t('TYPICAL APPLICATIONS'); ?></h4>
              </div>
              <div class="grid_content ">
                <?php print render($content['product_applications']); ?>
              </div>
            </div>
          <?php endif; ?>
          <?php if (!empty($content['related_products'])): ?>
            <div class="related_products">
              <b><?php print t('Related products'); ?></b>
              <?php print render($content['related_products']); ?>
            </div>
          <?php endif; ?>
        </div>
      </div>
    <?php endif; ?>
    <?php if (!empty($content['product_sizes_colors_tab_content'])): ?>
      <div class="mobile_toggle_box" id="size_box">
        <h4>
          <a href="#" class="mobile_toggle_control">
					<span class="icon">
						<img src="<?php print base_path() . drupal_get_path('theme', 'shurtape'); ?>/images/btn_mobile_toggle.png" alt="" />
					</span>
            <span class="title"><?php print t('Sizes & Colors'); ?></span>
          </a>
        </h4>
        <div class="mobile_toggle_target">
          <div class="box_heading">
            <?php print t('Sizes and Colors Available'); ?>
          </div>
          <div class="box_content">
            <?php print render($content['product_sizes_colors_tab_content']); ?>
          </div>
          <div class="box_footer">
            <?php if (!empty($content['related_products'])): ?>
              <div class="related_products">
                <b><?php print t('Related products'); ?></b>
                <?php print render($content['related_products']); ?>
              </div>
            <?php endif; ?>
          </div>
        </div>
      </div>
    <?php endif; ?>
    <?php if (!empty($content['product_downloads_tab_content']) && $lang != 'es/'): ?>
      <div class="mobile_toggle_box" id="downloads_box">
        <h4>
          <a href="#" class="mobile_toggle_control">
					<span class="icon">
						<img src="<?php print base_path() . drupal_get_path('theme', 'shurtape'); ?>/images/btn_mobile_toggle.png" alt="" />
					</span>
            <span class="title"><?php print t('Downloads'); ?></span>
          </a>
        </h4>
        <div class="mobile_toggle_target">
          <div class="grid_group clearfix">
            <?php print render($content['product_downloads_tab_content']); ?>
          </div>
        </div>
      </div>
    <?php endif; ?>
    <?php if (!empty($content['product_videos'])): ?>
      <div class="mobile_toggle_box" id="videos_box">
        <h4>
          <a href="#" class="mobile_toggle_control">
					<span class="icon">
						<img src="<?php print base_path() . drupal_get_path('theme', 'shurtape'); ?>/images/btn_mobile_toggle.png" alt="" />
					</span>
            <span class="title"><?php print t('Videos'); ?></span>
          </a>
        </h4>
        <div class="mobile_toggle_target">
          <?php print render($content['product_videos']); ?>
          <?php if (!empty($content['related_products'])): ?>
            <div class="related_products">
              <b><?php print t('Related products'); ?></b>
              <?php print render($content['related_products']); ?>
            </div>
          <?php endif; ?>
        </div>
      </div>
    <?php endif; ?>
    <?php if (!empty($content['product_testimonial'])): ?>
      <div class="mobile_toggle_box" id="testimonials_box">
        <h4>
          <a href="#" class="mobile_toggle_control">
					<span class="icon">
						<img src="<?php print base_path() . drupal_get_path('theme', 'shurtape'); ?>/images/btn_mobile_toggle.png" alt="" />
					</span>
            <span class="title"><?php print t('Testimonials'); ?></span>
          </a>
        </h4>
        <div class="mobile_toggle_target">
          <?php print render($content['product_testimonial']); ?>
        </div>
      </div>
    <?php endif; ?>
  </div>
  <div class="modal fade" id="video_modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        </div>
        <div class="modal-body">
          <!-- loaded dynamically via JS below -->
        </div>
      </div>
    </div>
  </div>

  <?php drupal_add_js(drupal_get_path('theme', 'shurtape') . '/panels/layouts/page_product/page_product.js') ?>

<?php endif; ?>
