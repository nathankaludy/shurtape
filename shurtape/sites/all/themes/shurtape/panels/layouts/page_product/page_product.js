/* define $ as jQuery just in case */
( function( $ ){
    Drupal.behaviors.kent_state_custom = {
        attach: function (context, settings) {
    /* doc ready */
    $( function( ){
        /* init the thumb slider */
        $( '#thumb_slider' ).thumb_slider( );

        /* init the tabs */
        $( '#tab_section' ).tabs( );

        /* show the video modal */
        $( '.open_video_modal' ).click( function( e )
        {
            /* set vars */
            var vid = $( this ).attr( 'rel' );
            $( '#video_modal .modal-body' ).hide( ).html( '<iframe width="620" height="349" src="//www.youtube.com/embed/' + vid + '" frameborder="0" allowfullscreen></iframe>' ).show( ).fitVids( );
            $( '#video_modal' ).modal( );
            e.preventDefault( );
        });

        /* shut down video on close trigger */
        $( '#video_modal' ).on( 'hidden.bs.modal', function ( )
        {
            $( '#video_modal .modal-body' ).html( '' );
        });

        /* open product sample popout */
        $( '.open_request_popout' ).click( function( e )
        {
            /* set var s */
            if ($(this).prop('name').length > 0) {
var prop_name = '.cta_box_wrap.no-pc #' + $(this).attr('name');
                $(prop_name).attr("style", "display:block!important");
                $('.cta_box_wrap.no-pc').attr("style", "display:block!important");
                //popout.attr("style", "display:block!important");
            }
            e.preventDefault( );
        });

        $( '#request_sample_popout.popup-style').remove();

        /* close popout */
        $( '.close_popout' ).bind("click", function( )
        {
            /* set vars */
           $( this ).parents( '.popout' ).attr("style", "display:none!important");

            return false;// e.preventDefault( );
        });

        /* find data sheet link */
        $( '.cta_box' ).on( 'click', '.find_data_sheet', function( e )
        {
            if ($( document ).width() > 640) {
                $( 'a#downloads-tab-link' ).click( );
                $('html, body').animate({
                    scrollTop:$( '#downloads-tab-link' ).offset().top
                }, 300);
                return false;
            } else {
                /* show toggle target */
                var parent_el 	= $( '.mobile_toggle_box#downloads_box' );
                var mobile_toggle_target = parent_el.find( '.mobile_toggle_target' );
                parent_el.addClass( 'open' );
                mobile_toggle_target.show( );

                /* switch the toggle_image */
                var toggle_icon = parent_el.find( '.icon img' ).attr( 'src' );
                var new_icon	= toggle_icon.replace( 'btn_mobile_toggle.png', 'btn_mobile_toggle_open.png' );
                parent_el.find( '.icon img' ).attr( 'src', new_icon );
                $('html, body').animate({
                    scrollTop:$( '#downloads_box' ).offset().top
                }, 300);
                e.preventDefault( );
            }
        });

        /* init the mobile video slider */
        $( '.mobile_toggle_box#videos_box .mobile_toggle_target .video_thumbs' ).video_slider( );

        /**
         * resize size boxes to even height
         */
        resize_size_boxes( );
        function resize_size_boxes( )
        {
            var size_boxes = $( '#mobile_toggle .mobile_toggle_box#size_box.open .mobile_toggle_target .box_content' ).find( '.views-row' );
            if ( size_boxes.length > 0 )
            {
                var max_box_height = 0;
                $.each( size_boxes, function( i )
                {
                    var box_height = $( size_boxes[i] ).outerHeight();
                    max_box_height = parseInt( box_height ) > parseInt( max_box_height ) ? box_height : max_box_height;
                });
                size_boxes.css({ 'min-height' : max_box_height + 'px' });
            }
        }

        var text_content = $('.detail_content');

        if (text_content.outerHeight() > 558) {
            var summ = text_content.find('.detail_desc').outerHeight() - (text_content.outerHeight() - 558);
            text_content.find('.detail_desc').css('height', summ).addClass('summary_cut').append('<div id="read-more-desc"><span>'+Drupal.t('More')+'</span></div>');
            console.log(summ);
        }

        $('#read-more-desc span').click(function(){
            text_content.find('.detail_desc').css('height', 'auto').removeClass('summary_cut');
            text_content.find('#read-more-desc').hide();
        });

        /* fire size box equalizer on window resize */
        $( window ).resize(function()
        {
            resize_size_boxes( );
        });

        /* fire size box equalizer on click */
        $( '.mobile_toggle_box#size_box .mobile_toggle_control' ).click(function(e)
        {
            resize_size_boxes( );
        });

    });
        }
    };
})( jQuery );
