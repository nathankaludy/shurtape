<div id="footer" class="desktop">
  <?= $content['footer_desktop__top'] ?>
  <div class="fixed_wrap clearfix">
    <div id="footer_first"
         class="col pull-left"><?= $content['footer_desktop__first'] ?></div>
    <div id="footer_second"
         class="col pull-left"><?= $content['footer_desktop__second'] ?></div>
    <div id="footer_third"
         class="col pull-left"><?= $content['footer_desktop__third'] ?></div>
    <div id="footer_fourth"
         class="col pull-left"><?= $content['footer_desktop__fourth'] ?></div>
    <div id="footer_fifth"
         class="col pull-left"><?= $content['footer_desktop__fifth'] ?></div>
  </div>
  <?= $content['footer_desktop__bottom'] ?>
</div>
<div id="mobile_footer" class="mobile"><?= $content['footer_mobile'] ?></div>