<?php

// Plugin definition.
$plugin = array(
  'title' => t('Default layout footer'),
  'category' => t('Shurtape content layouts'),
  'theme' => 'shurtape-layout__footer',
  'icon' => 'shurtape-layout__footer.png',
  'css' => 'shurtape-layout__footer.css',
  'regions' => array(
      'footer_desktop__top' => 'Footer desktop top',
      'footer_desktop__bottom' => 'Footer desktop bottom',
      'footer_desktop__first' => 'Footer desktop first',
      'footer_desktop__second' => 'Footer desktop second',
      'footer_desktop__third' => 'Footer desktop third',
      'footer_desktop__fourth' => 'Footer desktop fourth',
      'footer_desktop__fifth' => 'Footer desktop fifth',
      'footer_mobile' => 'Footer mobile',
  ),
);
