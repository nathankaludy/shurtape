<?php

/**
 * @file
 * Defines a naked avis layout.
 */

$plugin = array(
  'title' => 'Page Basic Layout',
  'category' => 'Avis',
  'css' => 'page_basic.css',
  'icon' => 'page_basic.png',
  'theme' => 'page-basic',
  'regions' => array(
	  'title' => 'Title',
	  'content_top' => 'Content top',
	  'help' => 'Help',
	  'action_links' => 'Action Links',
      'breadcrumb_section' => 'Breadcrumb Section',
      'content' => 'Content',
      'bottom' => 'Bottom',
  ),
);