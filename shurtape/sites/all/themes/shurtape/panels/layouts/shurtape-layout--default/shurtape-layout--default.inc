<?php

// Plugin definition.
$plugin = array(
  'title' => t('Default layout'),
  'category' => t('Shurtape content layouts'),
  'theme' => 'shurtape-layout--default',
  'icon' => 'shurtape-layout--default.png',
  'css' => 'shurtape-layout--default.css',
  'regions' => array(
    'uber_header' => 'Uber header',
    'common_header' => 'Common header',
    'content' => t('Content'),
    'common_footer' => 'Common footer',
  ),
);
