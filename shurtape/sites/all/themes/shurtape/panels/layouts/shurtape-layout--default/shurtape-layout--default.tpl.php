<?= $content['uber_header'] ?>

<div class="shift_box">
  <?= $content['common_header'] ?>
  <div id="content">
    <?= $content['content'] ?>
  </div>
  <?= $content['common_footer'] ?>
</div>