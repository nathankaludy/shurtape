<?php

// Plugin definition.
$plugin = array(
  'title' => t('Default layout uber-header'),
  'category' => t('Shurtape content layouts'),
  'theme' => 'shurtape-layout__uber-header',
  'icon' => 'shurtape-layout__uber-header.png',
  'css' => 'shurtape-layout__uber-header.css',
  'regions' => array(
    'top' => t('Top'),
    'header_mobile' => 'Header mobile',
    'mobile_nav' => 'Mobile nav',
  ),
);
