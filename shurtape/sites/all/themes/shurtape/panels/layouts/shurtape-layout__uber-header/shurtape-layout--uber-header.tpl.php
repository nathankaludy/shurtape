<?= $content['top'] ?>

<div id="mobile_header" class="clearfix mobile" role="banner">
  <?= $content['header_mobile'] ?>
</div>

<div id="mobile_nav" class="clearfix mobile">
  <?= $content['mobile_nav'] ?>
</div>
