<?php

/**
 * Implements hook_html_head_alter().
 * This will overwrite the default meta character type tag with HTML5 version.
 */
function shurtape_html_head_alter(&$head_elements) {
    $head_elements['system_meta_content_type']['#attributes'] = array(
        'charset' => 'utf-8'
    );
}

function shurtape_libraries_info() {
    $theme_path = drupal_get_path('theme', 'shurtape');
    return [
        'shurtape.main' => [
            'name' => 'Main Shurtape library',
            'version' => 1,
            'library path' => $theme_path,
            'path' => 'libraries',
            'files' => [
                'js' => [
                    'main.js' => [
                        'scope' => 'footer'
                    ]
                ]
            ],
            'dependencies' => [
                'jquery.hammer',
                'jquery.placeholder',
                'jquery.fitvids'
            ],
        ],
        'shurtape.carousel' => [
            'name' => 'Carousel Shurtape library',
            'version' => 1,
            'library path' => $theme_path,
            'path' => 'libraries',
            'files' => [
                'js' => [
                    'carousel.js' => [
                        'scope' => 'footer'
                    ]
                ]
            ],
        ],
        'shurtape.products' => [
            'name' => 'Products Shurtape library',
            'version' => 1,
            'library path' => $theme_path,
            'path' => 'libraries',
            'files' => [
                'js' => [
                    'products.js' => [
                        'scope' => 'footer'
                    ]
                ]
            ],
        ],
        'shurtape.search' => [
            'name' => 'Search Shurtape library',
            'version' => 1,
            'library path' => $theme_path,
            'path' => 'libraries',
            'files' => [
                'js' => [
                    'search.js' => [
                        'scope' => 'footer'
                    ]
                ]
            ],
        ],
        'shurtape.filter_bar' => [
            'name' => 'Filter bar Shurtape library',
            'version' => 1,
            'library path' => $theme_path,
            'path' => 'libraries',
            'files' => [
                'js' => [
                    'filter_bar.js' => [
                        'scope' => 'footer'
                    ]
                ]
            ],
        ],
        'typekit_load' => [
            'name' => 'Typekit loader',
            'description' => 'franklin-gothic and myriad-pro only for main domain',
            'version' => 1,
            'library path' => $theme_path,
            'path' => 'libraries',
            'files' => [
                'js' => [
                    'typekit_load.js' => [
                        'scope' => 'footer'
                    ]
                ]
            ],
        ],
        'visualwebsiteoptimizer' => [
            'name' => 'visualwebsiteoptimizer loader',
            'description' => 'hide and show body?',
            'version' => 1,
            'library path' => $theme_path,
            'path' => 'libraries',
            'files' => [
                'js' => ['visualwebsiteoptimizer_loader.js']
            ],
        ],
        'old_ie_support' => [
            'name' => 'support scripts for old ie',
            'description' => 'hide and show body?',
            'version' => 1,
            'library path' => $theme_path,
            'path' => 'libraries',
            'files' => [
                'js' => [
                    'loader' => [
                        'group' => 500,
                        'weight' => 500,
                        'type' => 'inline',
                        'data' => <<<SCRIPT
  document.write("<"+"!--[if lt IE 9]>\\n");
  document.write("<sc"+"ript src='/{$theme_path}/libraries/old_ie_support/html5.js'></sc"+"ript>\\n");
  document.write("<sc"+"ript src='/{$theme_path}/libraries/old_ie_support/respond.min.js'></sc"+"ript>\\n");
  document.write("<![endif]-->\\n");
SCRIPT
                    ]
                ],
                'css' => [
                    'ie8.css' => [
                        'data' => $theme_path . '/css/ie8.css',
                        'group' => 500,
                        'browsers' => [
                            'IE' => 'IE 8',
                            '!IE' => FALSE
                        ]
                    ]

                ]
            ],
        ],
    ];
}

/**
 * Insert themed breadcrumb page navigation at top of the node content
 */
function shurtape_breadcrumb($variables) {
    if (isset($variables['breadcrumb'])) {
        $breadcrumb = $variables['breadcrumb'];
        if (!empty($breadcrumb)) {
            /* comment below line to hide current page to breadcrumb */
            $breadcrumb[] = drupal_get_title();
            $newbread[] = l('Home', '');
            foreach ($breadcrumb as $link) {
                //Add translations for tags
                $link_title = strip_tags($link);
                $link = str_replace($link_title, t($link_title), $link);

                array_push(
                    $newbread,
                    str_replace("taxonomy/term/", "products?t=", $link)
                );
            }
            $output = '<nav class="breadcrumbs">'
                . implode(' &rsaquo; ', $newbread) . '</nav>';

            return $output;
        }
    }
}

/**
 * Override or insert variables into the html template.
 */
function shurtape_preprocess_html(&$variables) {
    /* if ($variables['page']['#theme'] === 'panels_everywhere_page') {
       $variables['theme_hook_original'] = 'html__vanilla';
       $variables['theme_hook_suggestions'] = ['html__vanilla'];
     }
   */
    // @todo is it realy need?
//  libraries_load('old_ie_support');

    // @todo is it realy need?
    libraries_load('bootstrap');

    // @todo is it realy need?
//  libraries_load('visualwebsiteoptimizer');

    libraries_load('shurtape.main');

    //@todo call this only on products pages - category / browse, detail, etc
    libraries_load('shurtape.products');

    //@todo call this only on search / filter page
    libraries_load('shurtape.search');

    //@todo call this only on products pages - category / browse, detail, etc.
    libraries_load('shurtape.filter_bar');

    //@todo franklin-gothic and myriad-pro only for main domain
    libraries_load('typekit_load');


    // Set up layout variable.
    $variables['layout'] = 'none';
    if (!empty($variables['page']['sidebar_first'])) {
        $variables['layout'] = 'first';
    }
    if (!empty($variables['page']['sidebar_second'])) {
        $variables['layout'] = ($variables['layout'] == 'first') ? 'both'
            : 'second';
    }

    $variables['base_path'] = base_path();
    $variables['front_page'] = url();
    $variables['feed_icons'] = drupal_get_feeds();
    $variables['language'] = $GLOBALS['language'];
    $variables['language']->dir = $GLOBALS['language']->direction ? 'rtl' : 'ltr';
    $variables['logo'] = theme_get_setting('logo');
    $variables['main_menu'] = theme_get_setting('toggle_main_menu') ? menu_main_menu() : array();
    $variables['secondary_menu'] = theme_get_setting('toggle_secondary_menu') ? menu_secondary_menu() : array();
    $variables['action_links'] = menu_local_actions();
    $variables['site_name'] = (theme_get_setting('toggle_name') ? filter_xss_admin(variable_get('site_name', 'Drupal')) : '');
    $variables['site_slogan'] = (theme_get_setting('toggle_slogan') ? filter_xss_admin(variable_get('site_slogan', '')) : '');
    $variables['tabs'] = menu_local_tabs();
}

function _shurtape_filter_boolean_display($field, $val, $i) {
    $field = field_info_instance('node', $field, 'product');
    $remove = shurtape_remove_filter($i);
    if (isset($field['label'])) :
        $title = $val == '1' ? $field['label'] : str_replace("Is ", "Is Not ", $field['label']);
        if ($field["widget"]["type"] != "options_onoff") {//is not boolean
            //see if is a term reference to obtain the string for value or just a number
            if ($field['display']["default"]["type"] == "number_integer") {
                $title = $field['label'] . " - " . $val;//append the selected value
            }
            else {
                $term = taxonomy_term_load($val);
                $title = $field['label'] . " - " . $term->name;//append the selected term name (the filter name)
            }
        }

        return sprintf('<li><a href="%s" class="control_remove" data-value="%s">%s</a></li>', $remove, $i, $title);
    endif;
}

function shurtape_preprocess_views_view(&$variables) {
    global $language;

    $lang = ($language->language != 'en') ? $language->language.'/' : "";
    if ($variables['name'] == "search") {
        $variables['filter_tag_output'] = "";
        $variables['result_title'] = "";
        $variables['link_results'] = base_path() .$lang. "search/products";
        $variables['link_other_results'] = base_path() .$lang. "search/products/other";

        if (isset($_GET['f'])) {
            $original_url = $_GET['f'];

            for ($i = 0; $i < count($original_url); $i++) :
                if (isset($extracted_values)) {
                    unset($extracted_values);
                }

                $extracted_values = explode(":", $original_url[$i]);
                $fieldName = $extracted_values[0];
                if ($fieldName == 'field_product_market_list' || $fieldName == 'field_product_type') {
                    //    if ($fieldName == 'field_product_market_new' || $fieldName == 'field_product_type') {
                    $load_term = taxonomy_term_load($extracted_values[1]);
                    if (isset($load_term->name)) :
                        $variables['result_title'] = sprintf(t('Product Results For').' "%s"', t($load_term->name));
                    endif;
                }
                else {
                    if ($fieldName == 'field_product_backing' ||
                        $fieldName == 'field_product_adhesive' ||
                        $fieldName == 'field_is_waterproof' ||
                        $fieldName == 'field_product_liner' ||
                        $fieldName == 'field_box_weight' ||
                        $fieldName == 'field_product_clean_removal_days' ||
                        $fieldName == 'field_handling_conditions' ||
                        $fieldName == 'field_holding_power' ||
                        $fieldName == 'field_is_cold_temp_tape' ||
                        $fieldName == 'field_is_uv_resistant' ||
                        $fieldName == 'field_is_carpet_tape' ||
                        $fieldName == 'field_is_fda_compliant' ||
                        $fieldName == 'field_is_ul_listed' ||
                        $fieldName == 'field_is_ul_723_tested' ||
                        $fieldName == 'field_is_green_point_certified'
                    ) {
                        $variables['filter_tag_output'] .= _shurtape_filter_boolean_display($fieldName, $extracted_values[1], $i);
                    }
                }
            endfor;

            $f_array = isset($_GET['f']) ? $_GET['f'] : NULL;

            if (!is_null($f_array)) :
                for ($i = 0; $i < count($f_array); $i++) :
                    if (!isset($link['output'])) {
                        $link['output'] = sprintf(
                            "f[%s]=%s", $i,
                            addslashes($f_array[$i])
                        );
                    }
                    else {
                        $link['output'] .= sprintf(
                            "&f[%s]=%s", $i,
                            addslashes($f_array[$i])
                        );
                    }
                endfor;
            endif;

            if (isset($_GET['search_api_views_fulltext'])) {
                if (!isset($link['output'])) {
                    $link['output'] = sprintf(
                        "search_api_views_fulltext=%s",
                        htmlspecialchars($_GET['search_api_views_fulltext'])
                    );
                }
                else {
                    $link['output'] .= sprintf(
                        "&search_api_views_fulltext=%s",
                        htmlspecialchars($_GET['search_api_views_fulltext'])
                    );
                }
            }

            if (isset($_GET['items_per_page'])) {
                if (!isset($link['output'])) {
                    $link['output'] = sprintf(
                        "items_per_page=%s",
                        htmlspecialchars($_GET['items_per_page'])
                    );
                }
                else {
                    $link['output'] .= sprintf(
                        "&items_per_page=%s",
                        htmlspecialchars($_GET['items_per_page'])
                    );
                }
            }

            if (isset($_GET['page'])) {
                if (!isset($link['output'])) {
                    $link['output'] = sprintf(
                        "page=%s",
                        htmlspecialchars($_GET['page'])
                    );
                }
                else {
                    $link['output'] .= sprintf(
                        "&page=%s",
                        htmlspecialchars($_GET['page'])
                    );
                }
            }

            $variables['link_results'] .= "?" . $link['output'];
            $variables['link_other_results'] = "?" . $link['output'];
        }

        if (isset($_GET['search_api_views_fulltext'])) {
            if ($_GET['search_api_views_fulltext'] != "") {
                $variables['result_title'] = sprintf(
                    t('Product Results For').' "%s"',
                    t($_GET['search_api_views_fulltext'])
                );
            }
        }
    }

    return $variables;

}

function shurtape_remove_filter($f_index) {
    if (isset($_GET['f'])) {
        $original_url = $_GET['f'];
        unset($original_url[$f_index]);
        $new_array = array_values($original_url);

        for ($i = 0; $i < count($new_array); $i++) :
            if (isset($url_path)) {
                $url_path .= sprintf("&f[%s]=%s", $i, $new_array[$i]);
            }
            else {
                $url_path = sprintf("f[%s]=%s", $i, $new_array[$i]);
            }
        endfor;

        if (isset($_GET['search_api_views_fulltext'])) {
            if (!isset($url_path)) {
                $url_path = sprintf(
                    "search_api_views_fulltext=%s",
                    htmlspecialchars($_GET['search_api_views_fulltext'])
                );
            }
            else {
                $url_path .= sprintf(
                    "&search_api_views_fulltext=%s",
                    htmlspecialchars($_GET['search_api_views_fulltext'])
                );
            }
        }

        if (isset($_GET['items_per_page'])) {
            if (!isset($url_path)) {
                $url_path = sprintf(
                    "items_per_page=%s",
                    htmlspecialchars($_GET['items_per_page'])
                );
            }
            else {
                $url_path .= sprintf(
                    "&items_per_page=%s",
                    htmlspecialchars($_GET['items_per_page'])
                );
            }
        }

        if (isset($_GET['page'])) {
            if (!isset($url_path)) {
                $url_path = sprintf("page=%s", htmlspecialchars($_GET['page']));
            }
            else {
                $url_path .= sprintf(
                    "&page=%s",
                    htmlspecialchars($_GET['page'])
                );
            }
        }

        if (isset($url_path)) {
            $url_path = "?" . $url_path;
        }
        else {
            $url_path = "";
        }

        if (arg(2) == "others" || arg(2) == "technical") {
            $url_path = sprintf(
                "/%s/%s/%s/%s", arg(0), arg(1), arg(2),
                $url_path
            );
        }
        else {
            $url_path = sprintf("/%s/%s/%s", arg(0), arg(1), $url_path);
        }

        return $url_path;
    }

}

/**
 * Duplicate of theme_menu_local_tasks( ) but adds clearfix to tabs.
 */
function shurtape_menu_local_tasks(&$variables) {
    $output = '';
    if (!empty($variables['primary'])) {
        $variables['primary']['#prefix'] = '<h2 class="element-invisible">'
            . t('Primary tabs') . '</h2>';
        $variables['primary']['#prefix'] .= '<ul class="tabs primary clearfix">';
        $variables['primary']['#suffix'] = '</ul>';
        $output .= drupal_render($variables['primary']);
    }
    if (!empty($variables['secondary'])) {
        $variables['secondary']['#prefix'] = '<h2 class="element-invisible">'
            . t('Secondary tabs') . '</h2>';
        $variables['secondary']['#prefix'] .= '<ul class="tabs secondary clearfix">';
        $variables['secondary']['#suffix'] = '</ul>';
        $output .= drupal_render($variables['secondary']);
    }

    return $output;
}

/**
 * Adds placeholder to form elements
 */

function shurtape_form_alter(&$form, &$form_state, $form_id) {
    if (strpos($form_id, "webform_client_form_") !== FALSE) {//target any webform
        foreach ($form["submitted"] as $key => $value) {
            if (in_array(
                $value["#type"],
                array("textfield", "webform_email", "textarea")
            )) {
                $form["submitted"][$key]['#attributes']["placeholder"] = t(
                    $value["#title"]
                );
            }
        }
    }

    if ($form['#id'] == 'user-login') {
        $form['#attributes']['autocomplete'] = 'off';
    }
}

/**
 * Override or insert variables into the node template.
 */
function shurtape_preprocess_node(&$vars) {
    $node = $vars['node'];
    if ($vars['view_mode'] == 'full' && node_is_page($vars['node'])) {
        $vars['classes_array'][] = 'node-full';
    }

    $vars['theme_hook_suggestions'][] = "node__{$node->type}__{$vars['view_mode']}";
    // only do this for node-type nodes and only if Path module exists
    if ($vars['view_mode'] === 'full') {
        // look up the alias from the url_alias table
        $source = entity_uri('node', $node)['path'];
        $alias = trim(url($source), '/');
        if ($alias !== $source) {
            // build a suggestion for every possibility
            $parts = explode('/', $alias);
            $suggestion = 'node';
            foreach ($parts as $part) {
                $suggestion .= "__$part";
                // add the suggestion to the array
                $vars['theme_hook_suggestions'][] = $suggestion;
            }
        }
    }
}

function shurtape_preprocess_block(&$variables) {
 $variables['theme_hook_suggestions'][] = 'block__' . $variables['block']->region;
 $variables['theme_hook_suggestions'][] = 'block__' . $variables['block']->module;
 $variables['theme_hook_suggestions'][] = 'block__' . $variables['block']->delta;

}

function shurtape_convert_standard_display($type, $value) {
    if ($value == "") {
        $value = 0;
    }
    switch (trim($type)) {
        case "Adhesion to Backing":
            return str_replace("%val%", t($value), "%val% oz/in width");
            break;
        case "Adhesion to Backing - Hand Rolls Only":
            return str_replace("%val%", t($value), "%val% oz/in width");
            break;
        case "Adhesion to Backing - Machine Rolls Only":
            return str_replace("%val%", t($value), "%val% oz/in width");
            break;
        case "Adhesion to Housewrap":
            return str_replace("%val%", t($value), "%val% oz/in width");
            break;
        case "Adhesion to Kraft":
            return str_replace("%val%", t($value), "%val% oz/in width");
            break;
        case "Adhesion to Kraft - Liner Side":
            return str_replace("%val%", t($value), "%val% oz/in width");
            break;
        case "Adhesion to LDPE - Exposed Side":
            return str_replace("%val%", t($value), "%val% oz/in width");
            break;
        case "Adhesion to LDPE - Liner Side":
            return str_replace("%val%", t($value), "%val% oz/in width");
            break;
        case "Adhesion to Stainless Steel":
            return str_replace("%val%", t($value), "%val% oz/in width");
            break;
        case "Adhesion to Stainless Steel - Exposed Side":
            return str_replace("%val%", t($value), "%val% oz/in width");
            break;
        case "Adhesion to Stainless Steel - Liner Side":
            return str_replace("%val%", t($value), "%val% oz/in width");
            break;
        case "Application Temperature Max":
            return str_replace("%val%", t($value), "%val% F");
            break;
        case "Application Temperature Min":
            return str_replace("%val%", t($value), "%val% F");
            break;
        case "Bottom Sheet (weight)":
            return str_replace("%val%", t($value), "%val% lbs");
            break;
        case "Bottom Sheet - Weight":
            return str_replace("%val%", t($value), "%val% lbs");
            break;
        case "CD Spacing":
            return str_replace("%val%", t($value), "%val% in");
            break;
        case "CD Tensile Strength":
            return str_replace("%val%", t($value), "%val% lbs/in");
            break;
        case "Continuous Operating Temperature Max":
            return str_replace("%val%", t($value), "%val% F");
            break;
        case "Dielectric Strength":
            return str_replace("%val%", t($value), "%val% volts/mil");
            break;
        case "Dielectric Breakdown":
            return str_replace("%val%", t($value), "%val% volts");
            break;
        case "Elongation":
            return str_replace("%val%", t($value), "%val%%");
            break;
        case "Elongation - Cross Direction":
            return str_replace("%val%", t($value), "%val%%");
            break;
        case "Elongation - Machine Direction":
            return str_replace("%val%", t($value), "%val%%");
            break;
        case "Emergency Overload Temperature":
            return str_replace("%val%", t($value), "%val% F");
            break;
        case "Holding Power to Fiberboard":
            return str_replace("%val%", t($value), "%val% minutes");
            break;
        case "Laminate":
            return str_replace("%val%", t($value), "%val% Hot Melt");
            break;
        case "MD Pattern":
            return str_replace("%val%", t($value), "%val%");
            break;
        case "MD Spacing":
            return str_replace("%val%", t($value), "%val% in");
            break;
        case "MD Tensile Strength":
            return str_replace("%val%", t($value), "%val% lbs/in");
            break;
        case "Nominal Tear Strength":
            return str_replace("%val%", t($value), "%val% lbs/in width");
            break;
        case "Tear Strength":
            return str_replace("%val%", t($value), "%val% lbs/in width");
            break;
        case "Paper Sheet (Weight)":
            return str_replace("%val%", t($value), "%val% lbs");
            break;
        case "Paper Sheet - Weight":
            return str_replace("%val%", t($value), "%val% lbs");
            break;
        case "Service Temperature Max":
            return str_replace("%val%", t($value), "%val% F");
            break;
        case "Service Temperature Min":
            return str_replace("%val%", t($value), "%val% F");
            break;
        case "Tensile":
            return str_replace("%val%", t($value), "%val% lbs/in width");
            break;
        case "Tensile Strength (longitudinal direction)":
            return str_replace("%val%", t($value), "%val% lbs/in width");
            break;
        case "Tensile - Cross Direction":
            return str_replace("%val%", t($value), "%val% lbs/in width");
            break;
        case "Tensile - Cross Direction (Weft)":
            return str_replace("%val%", t($value), "%val% lbs/in width");
            break;
        case "Tensile - Machine Direction":
            return str_replace("%val%", t($value), "%val% lbs/in width");
            break;
        case "Tensile - Machine Direction (Warp)":
            return str_replace("%val%", t($value), "%val% lbs/in width");
            break;
        case "Tensile Strength":
            return str_replace("%val%", t($value), "%val% psi");
            break;
        case "Tensile Strength (transverse direction)":
            return str_replace("%val%", t($value), "%val% lbs/in width");
            break;
        case "Tensile Strength - Longitudinal Direction":
            return str_replace("%val%", t($value), "%val% lbs/in width");
            break;
        case "Tensile Strength - Transverse Direction":
            return str_replace("%val%", t($value), "%val% lbs/in width");
            break;
        case "Tensile Strength - Cross Direction":
            return str_replace("%val%", t($value), "%val% lbs/in width");
            break;
        case "Tensile Strength - Machine Direction":
            return str_replace("%val%", t($value), "%val% lbs/in width");
            break;
        case "Thickness":
            return str_replace("%val%", t($value), "%val% mils");
            break;
        case "Thickness - with liner":
            return str_replace("%val%", t($value), "%val% mil");
            break;
        case "Thickness - without liner":
            return str_replace("%val%", t($value), "%val% mil");
            break;
        case "Thickness - With Liner":
            return str_replace("%val%", t($value), "%val% mils");
            break;
        case "Thickness - Without Liner":
            return str_replace("%val%", t($value), "%val% mils");
            break;
        case "Top Sheet (weight)":
            return str_replace("%val%", t($value), "%val% lbs");
            break;
        case "Top Sheet - Weight":
            return str_replace("%val%", t($value), "%val% lbs");
            break;
        case "Water Vapor Transmission Rate":
            return str_replace("%val%", t($value), "%val% gm/100 sp in/24 hr");
            break;
        default:
            return $value != "" ? $value : "-";
    }
}

function shurtape_convert_metric_display($type, $value) {
    if ($value == "") {
        $value = 0;
    }
    switch (trim($type)) {
        case "Adhesion to Backing":
            return str_replace("%val%", t($value), "%val% N/10 mm");
            break;
        case "Adhesion to Backing - Hand Rolls Only":
            return str_replace("%val%", t($value), "%val% N/10 mm");
            break;
        case "Adhesion to Backing - Machine Rolls Only":
            return str_replace("%val%", t($value), "%val% N/10 mm");
            break;
        case "Adhesion to Housewrap":
            return str_replace("%val%", t($value), "%val% N/10 mm");
            break;
        case "Adhesion to Kraft":
            return str_replace("%val%", t($value), "%val% N/10 mm");
            break;
        case "Adhesion to Kraft - Liner Side":
            return str_replace("%val%", t($value), "%val% N/10 mm");
            break;
        case "Adhesion to LDPE - Exposed Side":
            return str_replace("%val%", t($value), "%val% N/10 mm");
            break;
        case "Adhesion to LDPE - Liner Side":
            return str_replace("%val%", t($value), "%val% N/10 mm");
            break;
        case "Adhesion to Stainless Steel":
            return str_replace("%val%", t($value), "%val% N/10 mm");
            break;
        case "Adhesion to Stainless Steel - Exposed Side":
            return str_replace("%val%", t($value), "%val% N/10 mm");
            break;
        case "Adhesion to Stainless Steel - Liner Side":
            return str_replace("%val%", t($value), "%val% N/10 mm");
            break;
        case "Application Temperature Max":
            return str_replace("%val%", t($value), "%val% C");
            break;
        case "Application Temperature Min":
            return str_replace("%val%", t($value), "%val% C");
            break;
        case "Bottom Sheet (weight)":
            return str_replace("%val%", t($value), "%val% kg");
            break;
        case "Bottom Sheet - Weight":
            return str_replace("%val%", t($value), "%val% kg");
            break;
        case "CD Spacing":
            return str_replace("%val%", t($value), "%val% cm");
            break;
        case "CD Tensile Strength":
            return str_replace("%val%", t($value), "%val% N/10 mm");
            break;
        case "Continuous Operating Temperature Max":
            return str_replace("%val%", t($value), "%val% C");
            break;
        case "Dielectric Strength":
            return str_replace("%val%", t($value), "%val% volts/mil");
            break;
        case "Dielectric Breakdown":
            return str_replace("%val%", t($value), "%val% volts");
            break;
        case "Elongation":
            return str_replace("%val%", t($value), "%val%%");
            break;
        case "Elongation - Cross Direction":
            return str_replace("%val%", t($value), "%val%%");
            break;
        case "Elongation - Machine Direction":
            return str_replace("%val%", t($value), "%val%%");
            break;
        case "Emergency Overload Temperature":
            return str_replace("%val%", t($value), "%val% C");
            break;
        case "Holding Power to Fiberboard":
            return str_replace("%val%", t($value), "%val% minutes");
            break;
        case "Laminate":
            return str_replace("%val%", t($value), "%val% Hot Melt");
            break;
        case "MD Pattern":
            return str_replace("%val%", t($value), "%val%");
            break;
        case "MD Spacing":
            return str_replace("%val%", t($value), "%val% cm");
            break;
        case "MD Tensile Strength":
            return str_replace("%val%", t($value), "%val% N/10 mm");
            break;
        case "Nominal Tear Strength":
            return str_replace("%val%", t($value), "%val% N/10 mm");
            break;
        case "Tear Strength":
            return str_replace("%val%", t($value), "%val% N/10 mm");
            break;
        case "Paper Sheet (Weight)":
            return str_replace("%val%", t($value), "%val% kg");
            break;
        case "Paper Sheet - Weight":
            return str_replace("%val%", t($value), "%val% kg");
            break;
        case "Service Temperature Max":
            return str_replace("%val%", t($value), "%val% C");
            break;
        case "Service Temperature Min":
            return str_replace("%val%", t($value), "%val% C");
            break;
        case "Tensile":
            return str_replace("%val%", t($value), "%val% N/10 mm");
            break;
        case "Tensile - Cross Direction":
            return str_replace("%val%", t($value), "%val% N/10 mm");
            break;
        case "Tensile - Cross Direction (Weft)":
            return str_replace("%val%", t($value), "%val% N/10 mm");
            break;
        case "Tensile - Machine Direction":
            return str_replace("%val%", t($value), "%val% N/10 mm");
            break;
        case "Tensile - Machine Direction (Warp)":
            return str_replace("%val%", t($value), "%val% N/10 mm");
            break;
        case "Tensile Strength":
            return str_replace("%val%", t($value), "%val% psi");
            break;
        case "Tensile Strength (longitudinal direction)":
            return str_replace("%val%", t($value), "%val% N/10 mm");
            break;
        case "Tensile Strength (transverse direction)":
            return str_replace("%val%", t($value), "%val% N/10 mm");
            break;
        case "Tensile Strength - Longitudinal Direction":
            return str_replace("%val%", t($value), "%val% N/10 mm");
            break;
        case "Tensile Strength - Transverse Direction":
            return str_replace("%val%", t($value), "%val% N/10 mm");
            break;
        case "Tensile Strength - Cross Direction":
            return str_replace("%val%", t($value), "%val% N/10 mm");
            break;
        case "Tensile Strength - Machine Direction":
            return str_replace("%val%", t($value), "%val% N/10 mm");
            break;
        case "Thickness":
            return str_replace("%val%", t($value), "%val% mm");
            break;
        case "Thickness - with liner":
            return str_replace("%val%", t($value), "%val% mm");
            break;
        case "Thickness - With Liner":
            return str_replace("%val%", t($value), "%val% mm");
            break;
        case "Thickness - without liner":
            return str_replace("%val%", t($value), "%val% mm");
            break;
        case "Thickness - Without Liner":
            return str_replace("%val%", t($value), "%val% mm");
            break;
        case "Top Sheet (weight)":
            return str_replace("%val%", t($value), "%val% kg");
            break;
        case "Top Sheet Weight":
            return str_replace("%val%", t($value), "%val% kg");
            break;
        case "Water Vapor Transmission Rate":
            return str_replace("%val%", t($value), "%val% gm/msq/24 hr");
            break;
        default:
            return $value != "" ? $value : "-";
    }
}

