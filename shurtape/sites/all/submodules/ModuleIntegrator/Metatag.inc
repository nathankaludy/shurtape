<?php

namespace d\ModuleIntegrator;

class Metatag {

  /**
   * @implement page_alter
   */
  static function pageAlter(&$page) {
    if (isset($page['#handler'])) {
      // render metatags if panel_everywhere active
      render ($page['content']['metatags']);
    }
  }
}