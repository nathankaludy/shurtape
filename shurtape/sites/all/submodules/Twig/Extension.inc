<?php
namespace d\Twig;

use Twig_Extension;
use Twig_SimpleFilter;
use Twig_SimpleFunction;

class Extension extends Twig_Extension {
  public $attached = [];

  public function getFilters() {
    return [
      new Twig_SimpleFilter('file_url', 'file_create_url', ['is_safe' => ['html']]),
      new Twig_SimpleFilter('drupal_date', 'format_date', ['is_safe' => ['html']]),
      new Twig_SimpleFilter('check_markup', 'check_markup', ['is_safe' => ['html']]),
      new Twig_SimpleFilter('truncate_utf8', 'truncate_utf8'),
      new Twig_SimpleFilter('text_summary', 'text_summary', ['is_safe' => ['html']]),
      new Twig_SimpleFilter('t', 't', ['is_safe' => ['html']]),
    ];
  }

  public function getFunctions() {
    return [
      new Twig_SimpleFunction('render', [$this, 'render'], ['is_safe' => ['html']]),
      new Twig_SimpleFunction('drupal_render', [$this, 'drupal_render'], ['is_safe' => ['html']]),
      new Twig_SimpleFunction('theme', 'theme', ['is_safe' => ['html']]),
      new Twig_SimpleFunction('url', 'url', ['is_safe' => ['html']]),
      new Twig_SimpleFunction('t', 't', ['is_safe' => ['html']]),
      new Twig_SimpleFunction('drupal_date', 'format_date', ['is_safe' => ['html']]),
      new Twig_SimpleFunction('check_markup', 'check_markup', ['is_safe' => ['html']]),
      new Twig_SimpleFunction('timer_start', 'timer_start', ['is_safe' => ['html']]),
      new Twig_SimpleFunction('timer_read', 'timer_read', ['is_safe' => ['html']]),
      new Twig_SimpleFunction('base_path', 'base_path', ['is_safe' => ['html']]),
      new Twig_SimpleFunction('drupal_get_path', 'drupal_get_path', ['is_safe' => ['html']]),
      new Twig_SimpleFunction('get_last_nodes', [$this, 'getLastNodes']),
    ];
  }

  public function render($class, $context = []) {
    if (class_exists($class) && method_exists($class, 'render')) {
      return $this->drupal_render((new $class($context))->render());
    }
    return '';
  }

  public function drupal_render($data) {
    $this->extractAttached($data);
    return render($data);
  }

  private function extractAttached(&$data) {
    if (!is_array($data)) {
      return;
    }
    if (isset($data['#attached'])) {
      $this->attached = drupal_array_merge_deep($this->attached, $data['#attached']);
      unset($data['#attached']);
    }
    foreach ($data as $key => &$value) {
      if (!is_string($key) || '#' !== $key[0]) {
        $this->extractAttached($value);
      }
    }
  }

  public function getLastNodes($type = '', $count = 1) {
    $nids = db_select('node')
      ->fields('node', ['nid'])
      ->orderBy('created', 'DESC')
      ->condition('type', $type)
      ->condition('status', 1)
      ->range(0, $count)
      ->execute()
      ->fetchCol();
    return node_load_multiple($nids);
  }

  public function getName() {
    return __CLASS__;
  }
}
