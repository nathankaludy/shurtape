<h3>The Early Years</h3> 
<p>
	Shurtape Technologies grew out of Shuford Mills, founded in 1880, a longtime maker of textile yarns and a world-leading manufacturer of cordage and twine. In 1955, Shuford Mills’ management started a tape division to capture the growing demand for basic crepe and flat back paper tapes – more popularly known as masking tape. That year, we opened a small pilot plant tucked into a corner of the larger Shuford Mills factory in Hickory, North Carolina.
</p>
<p>
	We managed to do a lot of things right; our early success with that pilot plant led Shuford Mills to invest in an entire factory dedicated to manufacturing tape. In 1972, encouraged for two decades by steady growth and profitability, Shuford Mills expanded tape production and its offering to adhesive-coated cloth tapes. The manufacturing operation in Hickory fueled growth for the next 10 years; its success set the stage for Shuford Mills to convert its Stony Point yarn mill to a duct tape factory in 1981. (By the way, we’re the company that makes the original duct tape.)
</p>

<h3>The Birth of Shurtape Technologies</h3>
<p>
	By 1996, the small tape division of Shuford Mills outpaced the sales and profitability of legacy textile products. Shuford Mills’ management elected to spin off the tape division into a separately managed and operated company – what’s known today as Shurtape Technologies.
</p>
<p>
	Emboldened as a "young” company, Shurtape aggressively launched new products during the first year – notably high-quality hot melt carton sealing tapes made in a new 100,000-square-foot factory in Hudson, North Carolina. We also expanded our geographic reach by acquiring Kip, a distributor based in Germany with operations throughout Europe, and by starting converting operations in Mexico, Peru and The United Arab Emirates. These capabilities provided Shurtape the infrastructure to export our goods and better serve our growing portfolio of multi-national customers.
</p>
<p>
	In 2000, we opened a state-of-the-art plant in Hickory to produce masking, foil, printed and laminated tapes and a converting facility in Singapore – keeping our home base vital and growing, while also building our presence internationally. And in late in 2004, we opened a new distribution center in Catawba, North Carolina, creating additional capacity to turn domestic and international orders even faster than before.
</p>
<p>
	Today, we have 12 world-wide manufacturing and distribution centers delivering relentlessly reliable tape on a global basis.
</p>

<h3>A Focus on Investment and Innovation</h3>
<p>
	To further fuel our growth and market penetration, Shurtape made a series of key acquisitions that helped solidify our leadership in serving both the business and consumer industries. In 2004, we purchased Tape Specialties Ltd. and the Arts and Entertainment division of Permacel Inc. And in 2008 we obtained exclusive licensing rights for FrogTape®, before acquiring the brand in 2010. But our most significant acquisition occurred in 2009 with the purchase of the do-it-yourself (DIY) line of tapes, office and housewares products from The Henkel Group, creating the newly named subsidiary ShurTech Brands. This transaction combined the consumer Duck®, Painter’s Mate Green® and Easy Liner® brands with the Shurtape professional brand. Together, we focus 100-percent of our resources and attention on developing, manufacturing and delivering world-class, innovative adhesive tape products for users with uncompromising standards. This is made possible by the company’s substantial investments in research and development, quality management systems and continuous improvement programs.
</p>