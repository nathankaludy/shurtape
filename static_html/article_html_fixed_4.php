<p>HICKORY, N.C. (March 2013) – Work doesn’t stop just because the weather changes. Whether it’s a blizzard or a heat wave, choosing the right HVAC tape for your installation or repair is critical. Shurtape® <a href="http://shurtape.com/Default.aspx?Tabid=75&amp;Level1=15&amp;Level2=74">AF Series cold temperature foil tapes</a> are engineered to form airtight bonds on joints and seams – even when temperature conditions fluctuate. </p>

<p>Durable and resilient to withstand changes in the weather, AF Series cold temperature foil tapes are linered and feature an acrylic adhesive. They can be applied in temperatures ranging from -20 F to 260 F, and are useful for a variety of jobs and applications: </p>

<p><img src="http://twshurtape.s3.amazonaws.com/imce/Shurtape-AF100.jpg" width="500" height="333" style="width: 300px; height: 200px; float: left; margin-top: 0px; margin-right: 25px; border-width: 0px; border-style: solid; margin-bottom: 5px;" /></p>

<ul><li>AF 100: UL 181A-P/B-FX printed aluminum foil tape for joining and sealing fiberglass ductboard and Class 1 Flex Duct. </li>
<li>AF 914 CT: General-purpose aluminum foil tape for joining and sealing aluminum-backed or fibrous insulation, in addition to metal ductwork and sheet metal. </li>
<li>AF 975 CT: Contractor-grade, 2.0-mil aluminum foil tape for joining and sealing aluminum-backed or fibrous insulation, in addition to metal ductwork and sheet metal. </li>
<li>AF 984 CT: Maximum-strength, foil/scrim/kraft (FSK) tape for sealing seams in scrim-reinforced duct insulation. </li>
<li>AF 990 CT: All Service Jacket (ASJ) tape used in cold and dual temperature systems to seal, seam and join scrim-reinforced pipe and duct insulation. </li>
</ul>
<p>“You can’t control the temperature, but you can control the quality and reliability of your work,” said Roy Cox, market manager – contractor markets, Shurtape. “These tapes are designed to create an airtight bond with no leaks, even when the temperature shifts, so you can be confident every job is done right.”</p>

<p>AF Series cold temperature foil tapes are tested in accordance with UL 723 and are also Shurtape Green Point Contributor Products – meaning they contribute to LEED®* certification points when combined with other eligible building materials.</p>